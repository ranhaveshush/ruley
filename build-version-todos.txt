Build Version TODOs
-------------------

Before RELEASE build mode:
--------------------------
1. Change the build mode to RELEASE at MyApplication.onCreate(...) -> AppState.setBuildMode(AppState.BUILD_RELEASE);
2. Change the assets > logback.xml file contents with logback.release.xml;
3. Change the Google Analytics log level to error at analytics_global_config.xml.
4. Validate At strings_api_keys.xml that all prod api keys.
5. Remove the android:debuggable="true" tools:ignore="HardcodedDebugMode" attributes from the application element in the AndroidManifest.xml.
6. Validate the Facebook app id suffix for com.facebook.NativeAppCallContentProvider in the AndroidManifest.xml.
7. Export Crashlytics-enabled Android Application (signed application package).
8. Refactor Proguard mapping.txt name with the build version code (e.g. mapping_0860.txt)


Before BETA build mode:
-----------------------
1. Change the build mode to RELEASE at MyApplication.onCreate(...) -> AppState.setBuildMode(AppState.BUILD_BETA);
2. Change the assets > logback.xml file contents with logback.release.xml;
3. Change the Google Analytics log level to error at analytics_global_config.xml.
4. Validate At strings_api_keys.xml that all beta api keys.
5. Remove the android:debuggable="true" tools:ignore="HardcodedDebugMode" attributes from the application element in the AndroidManifest.xml.
6. Validate the Facebook app id suffix for com.facebook.NativeAppCallContentProvider in the AndroidManifest.xml.
7. Export Crashlytics-enabled Android Application (signed application package).
8. Refactor Proguard mapping.txt name with the build version code (e.g. mapping_0860.txt)


Before DEBUG build mode:
------------------------
1. Change the build mode to DEBUG at MyApplication.onCreate(...) -> AppState.setBuildMode(AppState.BUILD_DEBUG);
2. Change the assets > logback.xml file contents with logback.debug.xml;
3. Change the Google Analytics log level to verbose at analytics_global_config.xml.
4. Validate At strings_api_keys.xml that all dev api keys.
5. Add the android:debuggable="true" tools:ignore="HardcodedDebugMode" attributes from the application element in the AndroidManifest.xml.
6. Validate the Facebook app id suffix for com.facebook.NativeAppCallContentProvider in the AndroidManifest.xml.
7. May increment the android:versionCode="X+1" android:versionName="x.x.x" attributes of the manifest element in the AndroidManifest.xml.

/**
 * 
 */
package com.ruley.contentobservers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.CallLog.Calls;

import com.ruley.app.MyApplication;
import com.ruley.model.beans.triggers.MissedCallContentObserverTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class MissedCallContentObserver extends ContentObserver {

	private static final String CLASS_NAME = MissedCallContentObserver.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MissedCallContentObserver.class.getName());
	
	private static int prevMissedCallsCount = 0;

	public MissedCallContentObserver() {
		super(null);
	}

	public MissedCallContentObserver(Handler handler) {
		super(handler);
	}

	@Override
	public void onChange(boolean selfChange) {
		LOGGER.debug(CLASS_NAME + "#onChange(...)");
		super.onChange(selfChange);
		
		LOGGER.info(CLASS_NAME + "#onChange(...) selfChange = " + selfChange);

		Context appCtx = MyApplication.getContext();

		String selection = Calls.TYPE + " = ? AND " + Calls.NEW + " = ?";
		String[] selectionArgs = new String[] { Integer.toString(Calls.MISSED_TYPE), "1" };
		String sortOrder = Calls.DATE + " DESC";
		Cursor cursor = appCtx.getContentResolver().query(Calls.CONTENT_URI,
				null, selection, selectionArgs, sortOrder);

		int currMissedCallsCount = cursor.getCount();

		LOGGER.info(CLASS_NAME + "#onChange(...) missed calls count = " + currMissedCallsCount);
		
		if (currMissedCallsCount > 0 && currMissedCallsCount != prevMissedCallsCount) {
			prevMissedCallsCount = currMissedCallsCount;
			Intent missedCallsIntent = new Intent(MissedCallContentObserverTrigger.ACTION_MISSED_CALL);
			appCtx.sendBroadcast(missedCallsIntent);
		}

		cursor.close();
	}

}

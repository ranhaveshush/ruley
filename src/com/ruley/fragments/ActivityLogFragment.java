/**
 * 
 */
package com.ruley.fragments;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.RuleEditorActivity;
import com.ruley.activities.RulesStoreActivity;
import com.ruley.adapters.ActivityLogAdapter;
import com.ruley.app.AppState;
import com.ruley.config.Config;
import com.ruley.fragments.base.BaseFragment;
import com.ruley.model.beans.rules.ActivityLog;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ActivityLogFragment extends BaseFragment {
	
	private static final String CLASS_NAME = ActivityLogFragment.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityLogFragment.class.getName());
	
	private static final IActivityLogDAO ACTIVITY_LOG_DAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
	
	private static final IRuleDAO RULE_DAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
	
	private ActivityLogAdapter mActivityLogAdapter = null;

	private StickyListHeadersListView mSlhActivityLog = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreateView(...)");
		
		// Sets the action bar title
		mActivity.setTitle(R.string.title_fragment_activity_log);
		
		View view = inflater.inflate(R.layout.fragment_activity_log, container, false);
		mActivityLogAdapter = new ActivityLogAdapter(mActivity, ACTIVITY_LOG_DAO.list());
		mSlhActivityLog = (StickyListHeadersListView) view.findViewById(android.R.id.list);
		mSlhActivityLog.setAdapter(mActivityLogAdapter);
		mSlhActivityLog.setOnItemClickListener(mOnItemClickListener);
		
		// Sets empty view for empty list activity log
		View emptyView = getEmptyView(view);
		mSlhActivityLog.setEmptyView(emptyView);
		
		return view;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");

		inflater.inflate(R.menu.fragment_activity_log_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");

		switch (item.getItemId()) {
		case R.id.action_store:
			Intent rulesStoreIntent = new Intent(mActivity, RulesStoreActivity.class);
			startActivity(rulesStoreIntent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private OnItemClickListener mOnItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			ActivityLog activityLog = mActivityLogAdapter.getItem(position);
			UUID ruleId = activityLog.getRuleId();
			Rule rule = RULE_DAO.get(ruleId);
			if (rule != null) {
				Intent intent = new Intent(mActivity, RuleEditorActivity.class);
				intent.setAction(Config.Action.ACTION_EDIT_RULE);
				intent.putExtra(Config.Extra.EXTRA_RULE_ID, ruleId);
				startActivity(intent);
			} else {
				ToastUtils.show(AppState.BUILD_RELEASE, mActivity,
						R.string.toast_rule_not_found, Toast.LENGTH_SHORT);
			}
		}
	};
	
	private View getEmptyView(View view) {
		View emptyView = view.findViewById(android.R.id.empty);
		ImageView ivMessageBoxIcon = (ImageView) emptyView.findViewById(R.id.ivMessageBoxIcon);
		TextView tvMessageBoxTitle = (TextView) emptyView.findViewById(R.id.tvMessageBoxTitle);
		TextView tvMessageBoxText = (TextView) emptyView.findViewById(R.id.tvMessageBoxText);
		ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_activity_log);
		tvMessageBoxTitle.setText(R.string.list_view_empty_activity_log_message_title);
		tvMessageBoxText.setText(R.string.list_view_empty_activity_log_message_text);
		return emptyView;
	}
	
}

/**
 * 
 */
package com.ruley.fragments.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.Fragment;

import com.ruley.activities.base.BaseActivity;
import com.ruley.activities.base.DynamicActionBar;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BaseFragment extends Fragment {
	
	private static final String CLASS_NAME = BaseFragment.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseFragment.class.getName());

	protected BaseActivity mActivity = null;
	
	protected DynamicActionBar mDynamicActionBar = new DynamicActionBar();
	
	@Override
	public void onAttach(Activity activity) {
		LOGGER.debug(CLASS_NAME + "#onAttach(...)");
		super.onAttach(activity);
		
		try {
			mActivity = (BaseActivity) getActivity();
		} catch (Exception e) {
			throw new ClassCastException(CLASS_NAME
				+ " fragment must be attach to ActionBarActivity or its descendants");
		}
	}

}

/**
 * 
 */
package com.ruley.fragments.dialogs;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DialogLauncher {
	
	private static final String CLASS_NAME = DialogLauncher.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DialogLauncher.class.getName());
	
	private DialogLauncher() {
	}
	
	public static void launchExitAppDialog(Activity activity, String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#launchExitAppDialog(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		TwoButtonDialogFragment dialog = TwoButtonDialogFragment.newInstance(
				R.string.dialog_title_exit_app,
				R.string.dialog_message_exit_app,
				R.string.dialog_button_exit,
				R.string.dialog_button_stay);
		dialog.show(activity.getFragmentManager(), fragmentTag);
	}
	
	public static void launchActionSecuritySettings(Activity activity, String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#launchActionSecuritySettings(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		TwoButtonDialogFragment dialog = TwoButtonDialogFragment.newInstance(
				R.string.dialog_title_location_services_disabled,
				R.string.dialog_message_location_services_disabled,
				R.string.dialog_button_yes,
				R.string.dialog_button_no);
		dialog.show(activity.getFragmentManager(), fragmentTag);
	}
	
	public static void launchAirplaneModeSettings(Activity activity, String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#launchAirplaneModeSettings(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		TwoButtonDialogFragment dialog = TwoButtonDialogFragment.newInstance(
				R.string.dialog_title_airplane_mode_on,
				R.string.dialog_message_airplane_mode_on,
				R.string.dialog_button_yes, R.string.dialog_button_no);
		dialog.show(supportFragmentManager, fragmentTag);
	}
	
	public static void launchNetworkConnectionSettings(Activity activity, String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#launchNetworkConnectionSettings(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		TwoButtonDialogFragment dialog = TwoButtonDialogFragment.newInstance(
				R.string.dialog_title_network_connection_offline,
				R.string.dialog_message_network_connection_offline,
				R.string.dialog_button_check, R.string.dialog_button_dismiss);
		dialog.show(supportFragmentManager, fragmentTag);
	}
	
	public static void launchInvalidRuleDialog(Activity activity, String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#launchInvalidRuleDialog(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		TwoButtonDialogFragment dialog = TwoButtonDialogFragment.newInstance(
				R.string.dialog_title_rule_editor_exit,
				R.string.dialog_message_rule_editor_invalid_rule,
				R.string.dialog_button_exit,
				R.string.dialog_button_stay);
		dialog.show(activity.getFragmentManager(), fragmentTag);
	}
	
	public static void launchRuleNameMissingDialog(Activity activity, String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#launch(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		TwoButtonDialogFragment dialog = TwoButtonDialogFragment.newInstance(
				R.string.dialog_title_rule_editor_exit,
				R.string.dialog_message_rule_editor_rule_name_missing,
				R.string.dialog_button_exit,
				R.string.dialog_button_stay);
		dialog.show(activity.getFragmentManager(), fragmentTag);
	}
	
	public static void launchRuleActivationErrorDialog(Activity activity, String fragmentTag, int errorMessageResourceId, UUID ruleId) {
		LOGGER.debug(CLASS_NAME + "#launchRuleActivationErrorDialog(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		RuleActivationErrorDialogFragment dialog = RuleActivationErrorDialogFragment.newInstance(
				R.string.dialog_title_rule_activation_failure,
				errorMessageResourceId,
				R.string.dialog_button_yes,
				R.string.dialog_button_no,
				ruleId);
		dialog.show(activity.getFragmentManager(), fragmentTag);
	}
	
	public static void launchGeneralErrorDialog(Activity activity, String fragmentTag, int errorMessageResourceId) {
		LOGGER.debug(CLASS_NAME + "#launchGeneralErrorDialog(...)");
		
		// Validates the dialog isn't already launched
		FragmentManager supportFragmentManager = activity.getFragmentManager();
		Fragment fragment = supportFragmentManager.findFragmentByTag(fragmentTag);
		if (fragment != null) {
			return;
		}
		
		OneButtonDialogFragment dialog = OneButtonDialogFragment.newInstance(
				R.string.dialog_title_oops,
				errorMessageResourceId,
				R.string.dialog_button_ok);
		dialog.show(activity.getFragmentManager(), fragmentTag);
	}

}

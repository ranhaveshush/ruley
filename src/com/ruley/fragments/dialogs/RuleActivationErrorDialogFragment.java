/**
 * 
 */
package com.ruley.fragments.dialogs;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RuleActivationErrorDialogFragment extends DialogFragment {

	private static final String CLASS_NAME = RuleActivationErrorDialogFragment.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RuleActivationErrorDialogFragment.class.getName());
	
	/**
	 * Callback interface.
	 * Container activity must implement this interface.
	 * 
	 * @author ran
	 *
	 */
	public interface Listener {
		
		/**
		 * Confirmation event callback function to notify the parent activity.
		 * 
		 * @param fragmentTag
		 * @param ruleId
		 *            UUID represents the rule which activation error occurred
		 */
		public void onConfirm(String fragmentTag, UUID ruleId);

		/**
		 * Cancel event callback function to notify the parent activity.
		 * 
		 * @param fragmentTag
		 */
		public void onCancel(String fragmentTag);
		
	}
	
	private RuleActivationErrorDialogFragment.Listener mListener = null;
	
	 /**
     * Creates a new instance of Fragment with given parameters.
     */
	public static RuleActivationErrorDialogFragment newInstance(
			int titleTextResourceId,
			int messageTextResourceId,
			int positiveTextResourceId,
			int negativeTextResourceId,
			UUID ruleId) {
    	LOGGER.debug(CLASS_NAME + "#newInstance(...)");
    	
    	RuleActivationErrorDialogFragment f = new RuleActivationErrorDialogFragment();

        Bundle args = new Bundle();
        args.putInt("titleTextResourceId", titleTextResourceId);
        args.putInt("messageTextResourceId", messageTextResourceId);
        args.putInt("positiveTextResourceId", positiveTextResourceId);
        args.putInt("negativeTextResourceId", negativeTextResourceId);
        args.putSerializable("ruleId", ruleId);
        f.setArguments(args);

        return f;
    }
	
	@Override
	public void onAttach(Activity activity) {
		LOGGER.debug(CLASS_NAME + "#onAttach(...)");
		
		super.onAttach(activity);
		
		try {
			mListener = (RuleActivationErrorDialogFragment.Listener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.getClass().getSimpleName() + " must implement " + CLASS_NAME + ".Listener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		// Inflates the dialog view
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View customDialogview = inflater.inflate(R.layout.dialog_custom, null);
		TextView tvDialogTitle = (TextView) customDialogview.findViewById(R.id.tvDialogTitle);
		TextView tvDialogMessage = (TextView) customDialogview.findViewById(R.id.tvDialogMessage);
		
		Bundle args = getArguments();
		final UUID ruleId = (UUID) args.getSerializable("ruleId");
		tvDialogTitle.setText( args.getInt("titleTextResourceId") );
		tvDialogMessage.setText( args.getInt("messageTextResourceId") );
		
		Builder builder = new AlertDialog.Builder(getActivity());
		AlertDialog alertDialog = builder
			.setView(customDialogview)
			.setPositiveButton( args.getInt("positiveTextResourceId") , new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onConfirm(getTag(), ruleId);
				}
			})
			.setNegativeButton( args.getInt("negativeTextResourceId") , new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onCancel(getTag());
				}
			})
			.create();
		
		return alertDialog;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		LOGGER.debug(CLASS_NAME + "#onCancel(...)");
		
		super.onCancel(dialog);
		mListener.onCancel(getTag());
	}
	
}

/**
 * 
 */
package com.ruley.fragments.dialogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class OneButtonDialogFragment extends DialogFragment {

	private static final String CLASS_NAME = OneButtonDialogFragment.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(OneButtonDialogFragment.class.getName());
	
	/**
	 * Callback interface.
	 * Container activity must implement this interface.
	 * 
	 * @author ran
	 *
	 */
	public interface Listener {
		
		/**
		 * Confirmation event callback function
		 * to notify the parent activity.
		 * 
		 * @param fragmentTag
		 */
		public void onConfirm(String fragmentTag);
		
	}
	
	private OneButtonDialogFragment.Listener mListener = null;
	
	 /**
     * Creates a new instance of Fragment with given parameters.
     */
	public static OneButtonDialogFragment newInstance(
			int titleTextResourceId,
			int messageTextResourceId,
			int confirmTextResourceId) {
    	LOGGER.debug(CLASS_NAME + "#newInstance(...)");
    	
    	OneButtonDialogFragment f = new OneButtonDialogFragment();

        Bundle args = new Bundle();
        args.putInt("titleTextResourceId", titleTextResourceId);
        args.putInt("messageTextResourceId", messageTextResourceId);
        args.putInt("confirmTextResourceId", confirmTextResourceId);
        f.setArguments(args);

        return f;
    }
	
	@Override
	public void onAttach(Activity activity) {
		LOGGER.debug(CLASS_NAME + "#onAttach(...)");
		
		super.onAttach(activity);
		
		try {
			mListener = (OneButtonDialogFragment.Listener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.getClass().getSimpleName() + " must implement " + CLASS_NAME + ".Listener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Inflates the dialog view
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View customDialogview = inflater.inflate(R.layout.dialog_custom, null);
		TextView tvDialogTitle = (TextView) customDialogview.findViewById(R.id.tvDialogTitle);
		TextView tvDialogMessage = (TextView) customDialogview.findViewById(R.id.tvDialogMessage);
		
		Bundle args = getArguments();
		tvDialogTitle.setText( args.getInt("titleTextResourceId") );
		tvDialogMessage.setText( args.getInt("messageTextResourceId") );
		
		Builder builder = new AlertDialog.Builder(getActivity());		
		AlertDialog alertDialog = builder
			.setView(customDialogview)
			.setPositiveButton(args.getInt("confirmTextResourceId") , new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mListener.onConfirm(getTag());
				}
			})
			.create();
		
		return alertDialog;
	}
	
}

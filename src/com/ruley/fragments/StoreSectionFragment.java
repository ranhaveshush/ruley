/**
 * 
 */
package com.ruley.fragments;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.parse.ParseQueryAdapter;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;
import com.parse.ParseQueryAdapter.QueryFactory;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.ruley.R;
import com.ruley.adapters.TemplateRulesParseQueryAdapter;
import com.ruley.app.AppState;
import com.ruley.fragments.base.BaseFragment;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.parse.TemplateRuleParse;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.IntentUtils;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.RulesUtils;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class StoreSectionFragment extends BaseFragment implements TemplateRulesParseQueryAdapter.Listener {
	
	private static final String CLASS_NAME = StoreSectionFragment.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(StoreSectionFragment.class.getName());
	
	private static final String ARG_SECTION_NUMBER = "section_number";
	
	public static final int SECTION_FEATURED = 0;
	public static final int SECTION_TOP = 1;
	public static final int SECTION_TRENDING = 2;
	public static final int SECTION_LIKED = 3;
	public static final int SECTION_UPLOADED = 4;
	
	private static final CachePolicy PARSE_QUERY_CACHE_POLICY = ParseQuery.CachePolicy.NETWORK_ONLY;
	private static final int PARSE_QUERY_RESULTS_LIMIT = 50;

	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		public void onTemplateRuleToggleLike(TemplateRuleParse templateRuleParse);
		
		public void onTemplateRuleDownloaded(TemplateRuleParse templateRuleParse);

		public void onTemplateRuleDeleted(TemplateRuleParse templateRuleParse);
		
	}
	
	private StoreSectionFragment.Listener mListener = null;
	
	private int mSectionIndex;

	private TemplateRulesParseQueryAdapter mStoreRulesAdapter = null;
	
	private ListView mLvStoreRules = null;
	
	private View mVgLoading = null;

	public static StoreSectionFragment newInstance(int sectionNumber) {
		LOGGER.debug(CLASS_NAME + "#newInstance(...)");
		
		StoreSectionFragment fragment = new StoreSectionFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle args = getArguments();
		mSectionIndex = args.getInt(ARG_SECTION_NUMBER);
	}

	@Override
	public void onAttach(Activity activity) {
		LOGGER.debug(CLASS_NAME + "#onAttach(...)");
		
		super.onAttach(activity);
		
		try {
			mListener = (Listener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.getClass().getSimpleName() + " must implement " + CLASS_NAME + ".Listener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreateView(...)");
		
		View rootView = inflater.inflate(R.layout.fragment_rules_store_section, container, false);
		mVgLoading = rootView.findViewById(R.id.vgLoading);
		mStoreRulesAdapter = getStoreRulesAdapter(mSectionIndex);
		mLvStoreRules = (ListView) rootView.findViewById(R.id.lvStoreRules);
		mLvStoreRules.setAdapter(mStoreRulesAdapter);
		mLvStoreRules.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TemplateRuleParse templateRuleParse = mStoreRulesAdapter.getItem(position);
				mListener.onTemplateRuleDownloaded(templateRuleParse);
			}
		});
		
		// Sets the empty view
		View emptyView = getEmptyView(rootView, mSectionIndex);
		mLvStoreRules.setEmptyView(emptyView);
		
		return rootView;
	}

	private TemplateRulesParseQueryAdapter getStoreRulesAdapter(int sectionIndex) {
		TemplateRulesParseQueryAdapter queryAdapter = new TemplateRulesParseQueryAdapter(
				getActivity(), getRelationName(sectionIndex),
				getQueryFactory(sectionIndex), StoreSectionFragment.this);
		queryAdapter.addOnQueryLoadListener(new OnQueryLoadListener<TemplateRuleParse>() {
			@Override
			public void onLoaded(List<TemplateRuleParse> objects, Exception e) {
				hideLoadingView();
			}
			@Override
			public void onLoading() {
				showLoadingView();
			}
		});
		
		return queryAdapter;
	}
	
	private String getRelationName(int sectionIndex) {
		String parseRelationName = null;
		switch (sectionIndex) {
		case SECTION_LIKED:
			parseRelationName = UserParse.RELATION_LIKED_TEMPLATE_RULES;
			break;
		case SECTION_UPLOADED:
			parseRelationName = UserParse.RELATION_UPLOADED_TEMPLATE_RULES;
			break;
		}
		return parseRelationName;
	}
	
	private QueryFactory<TemplateRuleParse> getQueryFactory(int sectionIndex) {
		QueryFactory<TemplateRuleParse> queryFactory = null;
		switch (sectionIndex) {
		case SECTION_FEATURED:
			queryFactory = new ParseQueryAdapter.QueryFactory<TemplateRuleParse>() {
				@Override
				public ParseQuery<TemplateRuleParse> create() {
					ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
					query.setCachePolicy(PARSE_QUERY_CACHE_POLICY);
					query.whereEqualTo(TemplateRuleParse.FIELD_IS_FEATURED, true);
					query.orderByDescending(TemplateRuleParse.FIELD_CREATED_AT);
					query.include(TemplateRuleParse.FIELD_CREATED_BY);
					query.setLimit(PARSE_QUERY_RESULTS_LIMIT);
					return query;
				}
			};
			break;
		case SECTION_TOP:
			queryFactory = new ParseQueryAdapter.QueryFactory<TemplateRuleParse>() {
				@Override
				public ParseQuery<TemplateRuleParse> create() {
					ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
					query.setCachePolicy(PARSE_QUERY_CACHE_POLICY);
					query.include(TemplateRuleParse.FIELD_CREATED_BY);
					query.orderByDescending(TemplateRuleParse.FIELD_NUM_OF_DOWNLOADS);
					query.setLimit(PARSE_QUERY_RESULTS_LIMIT);
					return query;
				}
			};
			break;
		case SECTION_TRENDING:
			queryFactory = new ParseQueryAdapter.QueryFactory<TemplateRuleParse>() {
				@Override
				public ParseQuery<TemplateRuleParse> create() {
					ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
					query.setCachePolicy(PARSE_QUERY_CACHE_POLICY);
					query.include(TemplateRuleParse.FIELD_CREATED_BY);
					query.orderByDescending(TemplateRuleParse.FIELD_CREATED_AT);
					query.setLimit(PARSE_QUERY_RESULTS_LIMIT);
					return query;
				}
			};
			break;
		case SECTION_LIKED:
			queryFactory = new ParseQueryAdapter.QueryFactory<TemplateRuleParse>() {
				@Override
				public ParseQuery<TemplateRuleParse> create() {
					ParseUser currentUser = ParseUser.getCurrentUser();
					ParseRelation<TemplateRuleParse> likedRulesRelation = currentUser.getRelation(UserParse.RELATION_LIKED_TEMPLATE_RULES);
					ParseQuery<TemplateRuleParse> query = likedRulesRelation.getQuery();
					query.setCachePolicy(PARSE_QUERY_CACHE_POLICY);
					query.include(TemplateRuleParse.FIELD_CREATED_BY);
					query.orderByDescending(TemplateRuleParse.FIELD_CREATED_AT);
					query.setLimit(PARSE_QUERY_RESULTS_LIMIT);
					return query;
				}
			};
			break;
		case SECTION_UPLOADED:
			queryFactory = new ParseQueryAdapter.QueryFactory<TemplateRuleParse>() {
				@Override
				public ParseQuery<TemplateRuleParse> create() {
					ParseUser currentUser = ParseUser.getCurrentUser();
					ParseRelation<TemplateRuleParse> uploadedRulesRelation = currentUser.getRelation(UserParse.RELATION_UPLOADED_TEMPLATE_RULES);
					ParseQuery<TemplateRuleParse> query = uploadedRulesRelation.getQuery();
					query.setCachePolicy(PARSE_QUERY_CACHE_POLICY);
					query.include(TemplateRuleParse.FIELD_CREATED_BY);
					query.orderByDescending(TemplateRuleParse.FIELD_CREATED_AT);
					query.setLimit(PARSE_QUERY_RESULTS_LIMIT);
					return query;
				}
			};
			break;
		}
		return queryFactory;
	}
	
	private View getEmptyView(View view, int sectionIndex) {
		View emptyView = view.findViewById(android.R.id.empty);
		ImageView ivMessageBoxIcon = (ImageView) emptyView.findViewById(R.id.ivMessageBoxIcon);
		TextView tvMessageBoxTitle = (TextView) emptyView.findViewById(R.id.tvMessageBoxTitle);
		TextView tvMessageBoxText = (TextView) emptyView.findViewById(R.id.tvMessageBoxText);
		switch (sectionIndex) {
		case SECTION_FEATURED:
			ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_featured);
			tvMessageBoxTitle.setText(R.string.list_view_empty_featured_rules_message_title);
			tvMessageBoxText.setText(R.string.list_view_empty_featured_rules_message_text);
			break;
		case SECTION_TOP:
			ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_top);
			tvMessageBoxTitle.setText(R.string.list_view_empty_top_rules_message_title);
			tvMessageBoxText.setText(R.string.list_view_empty_top_rules_message_text);
			break;
		case SECTION_TRENDING:
			ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_trending);
			tvMessageBoxTitle.setText(R.string.list_view_empty_trending_rules_message_title);
			tvMessageBoxText.setText(R.string.list_view_empty_trending_rules_message_text);
			break;
		case SECTION_LIKED:
			ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_like);
			tvMessageBoxTitle.setText(R.string.list_view_empty_liked_rules_message_title);
			tvMessageBoxText.setText(R.string.list_view_empty_liked_rules_message_text);
			break;
		case SECTION_UPLOADED:
			ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_uploaded);
			tvMessageBoxTitle.setText(R.string.list_view_empty_uploaded_rules_message_title);
			tvMessageBoxText.setText(R.string.list_view_empty_uploaded_rules_message_text);
		}
		return emptyView;
	}
	
	private void showLoadingView() {
		mVgLoading.setAlpha(1f);
		mVgLoading.setVisibility(View.VISIBLE);
	}
	
	private void hideLoadingView() {
		mVgLoading.animate().alpha(0f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mVgLoading.setVisibility(View.GONE);
			}
		}).start();
	}
	
	public boolean isSection(int sectionIndex) {
		return mSectionIndex == sectionIndex;
	}
	
	public boolean contains(TemplateRuleParse templateRuleParse) {
		if (mStoreRulesAdapter != null) {
			return mStoreRulesAdapter.getItemPosition(templateRuleParse) != -1;
		}
		return false;
	}
	
	public void refreshAll() {
		// Validates adapter
		if (mStoreRulesAdapter == null) {
			return;
		}
		
		LOGGER.info("Refresh all sec " + mSectionIndex);
		mStoreRulesAdapter = getStoreRulesAdapter(mSectionIndex);
		mLvStoreRules.setAdapter(mStoreRulesAdapter);
	}
	
	public void refresh(final TemplateRuleParse templateRuleParse) {
		// Validates adapter
		if (mStoreRulesAdapter == null) {
			return;
		}
		// Validates if liked store section refresh all template rules
		if (mSectionIndex == SECTION_LIKED && !getUserVisibleHint()) {
			refreshAll();
			return;
		}
		// Validates template rule parse exists in this store section fragment
		final int position = mStoreRulesAdapter.getItemPosition(templateRuleParse);
		if (position == -1) {
			return;
		}
		templateRuleParse.fetchInBackground(new GetCallback<TemplateRuleParse>() {
			@Override
			public void done(TemplateRuleParse object, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							getActivity(),
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				}
				if (object != null) {
					LOGGER.info("Refresh sec " + mSectionIndex + " pos " + position + " likes " + object.getNumOfLikes());
					mStoreRulesAdapter.remove(templateRuleParse);
					try {
						mStoreRulesAdapter.add(position, object);
					} catch (IndexOutOfBoundsException ioobe) {
						mStoreRulesAdapter.add(object);
					}
				}
			}
		});
	}
	
	@Override
	public void onTemplateRuleToggleLike(int position) {
		final TemplateRuleParse templateRuleParse = (TemplateRuleParse) mStoreRulesAdapter.getItem(position);
		final TemplateRule templateRule = templateRuleParse.getTemplateRule();
		// Updates the client
		templateRule.toggleLiked();
		if (mSectionIndex == SECTION_LIKED) {
			if (templateRule.isLiked()) {
				mStoreRulesAdapter.add(templateRuleParse);
			} else {
				mStoreRulesAdapter.remove(templateRuleParse);
			}
		}
		// Updates the server
		ParseUtils.toggleTemplateRuleLiked(getActivity(), templateRule, templateRule.isLiked(), new DoneCallback() {
			@Override
			public void done() {
				mListener.onTemplateRuleToggleLike(templateRuleParse);
			}
		});
	}
	
	@Override
	public void onTemplateRuleDownload(int position) {
		TemplateRuleParse templateRuleParse = mStoreRulesAdapter.getItem(position);
		mListener.onTemplateRuleDownloaded(templateRuleParse);
	}
	
	@Override
	public void onTemplateRuleDelete(int position) {
		final TemplateRuleParse templateRuleParse = (TemplateRuleParse) mStoreRulesAdapter.getItem(position);
		mStoreRulesAdapter.removeInBackground(templateRuleParse, new DoneCallback() {
			@Override
			public void done() {
				// Toasts delete rules to notify the user
				String deleteRuleToastText = getString(R.string.toast_delete_rule, templateRuleParse.getTemplateRule().getName());
				ToastUtils.show(AppState.BUILD_RELEASE,
						getActivity(),
						deleteRuleToastText,
						Toast.LENGTH_SHORT);
				mListener.onTemplateRuleDeleted(templateRuleParse);
			}
		});
	}

	@Override
	public void onTemplateRuleShare(int position) {
		TemplateRuleParse templateRuleParse = (TemplateRuleParse) mStoreRulesAdapter.getItem(position);
		TemplateRule templateRule = templateRuleParse.getTemplateRule();
		Intent shareIntent = RulesUtils.getShareIntent(templateRule);
		if (IntentUtils.isIntentSafe(shareIntent)) {
			String chooserTitle = getString(R.string.title_chooser_share_rules);
			startActivity(Intent.createChooser(shareIntent, chooserTitle));
		}
	}
}
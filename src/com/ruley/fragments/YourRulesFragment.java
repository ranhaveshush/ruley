package com.ruley.fragments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.MainActivity;
import com.ruley.activities.RuleEditorActivity;
import com.ruley.activities.RulesStoreActivity;
import com.ruley.activities.TemplateRuleEditorActivity;
import com.ruley.adapters.AlertsAdapter;
import com.ruley.adapters.MultiSelectionBaseAdapter;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.fragments.base.BaseFragment;
import com.ruley.fragments.dialogs.DialogLauncher;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.alerts.AlertsManager;
import com.ruley.model.beans.device.DeviceState;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.Rule.Listener;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.IUserDAO;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.RulesDependencyGraphUtils;
import com.ruley.utilities.RulesUtils;
import com.ruley.utilities.RulesUtils.BackupCallback;
import com.ruley.utilities.ToastUtils;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;

import de.timroes.android.listview.EnhancedListView;
import de.timroes.android.listview.EnhancedListView.Undoable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class YourRulesFragment extends BaseFragment {
	
	private static final String CLASS_NAME = YourRulesFragment.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(YourRulesFragment.class.getName());
	
	private static final IActivityLogDAO ACTIVITY_LOG_DAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
	
	private static final IUserDAO USER_DAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
	
	private static final IRuleDAO RULE_DAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();

	private static final int CREATE_RULE_REQUEST_CODE = 1001;
	
	private static final int EDIT_RULE_REQUEST_CODE = 1002;
	
	private static final int DOWNLOAD_RULE_REQUEST_CODE = 1003;
	
	private SlidingUpPanelLayout mSlidingLayout = null;
	
	private ImageView mSlidingToggleIcon = null;
	
	private AlertsManager mAlertsManager = null;
	
	private AlertsAdapter mAlertsAdapter = null;
	
	private ListView mLvAlerts = null;
	
	private RulesAdapter mRulesAdapter = null;

	private EnhancedListView mElvRules = null;

	private ActionMode mActionMode = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		mAlertsManager = AlertsManager.getInstance();
		
		// If this is the app's first run
		// and the user doesn't have any rules,
		// launch the first time use tutorial
//		boolean isFirstRun = AppState.isFirstRun();
//		if (isFirstRun) {
//			
//		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreateView(...)");
		
		// Sets the action bar title
		mActivity.setTitle(R.string.title_fragment_your_rules);
		
		View view = inflater.inflate(R.layout.fragment_your_rules, container, false);
		
		// Constructs the alerts sliding panel
		mSlidingToggleIcon = (ImageView) view.findViewById(R.id.sliding_toggle_icon);
		mSlidingLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
		mSlidingLayout.setPanelSlideListener(new PanelSlideListener() {
			@Override
			public void onPanelSlide(View panel, float slideOffset) {
			}
			@Override
			public void onPanelHidden(View panel) {
			}
			@Override
			public void onPanelExpanded(View panel) {
				mSlidingToggleIcon.setImageResource(R.drawable.ic_alerts_hide);
			}
			@Override
			public void onPanelCollapsed(View panel) {
				mSlidingToggleIcon.setImageResource(R.drawable.ic_alerts_show);
			}
			@Override
			public void onPanelAnchored(View panel) {
			}
		});
		// Constructs the alerts list view
		mAlertsAdapter = new AlertsAdapter(mActivity, R.layout.list_item_alert_card, mAlertsManager.getAlerts());
		mLvAlerts = (ListView) view.findViewById(R.id.lvAlerts);
		mLvAlerts.setAdapter(mAlertsAdapter);
		// Constructs the complex rules list view
		List<Rule> rules = getRules();
		mRulesAdapter = new RulesAdapter(mActivity, rules);
		mElvRules = (EnhancedListView) view.findViewById(R.id.elvRules);
		mElvRules.setAdapter(mRulesAdapter);
		mElvRules.setOnItemClickListener(mRuleItemEditorReferral);
		mElvRules.setOnItemLongClickListener(mRuleItemStartActionMode);
		mElvRules.setDismissCallback(new EnhancedListView.OnDismissCallback() {
			@Override
			public Undoable onDismiss(EnhancedListView listView, final int position) {
				List<Rule> rulesToDelete = new ArrayList<Rule>();
				final Rule ruleToDelete = mRulesAdapter.getItem(position);
				rulesToDelete.add(ruleToDelete);
				mRulesAdapter.removeAll(rulesToDelete);
				deleteLocalRules(rulesToDelete);
				ParseUtils.deleteTemplateRules(mActivity, rulesToDelete);
				return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                    	restoreLocalRule(position, ruleToDelete);
                    	ParseUtils.restoreTemplateRule(mActivity, ruleToDelete);
                    }
                };
			}
		});
		mElvRules.enableSwipeToDismiss()
			.setSwipeDirection(EnhancedListView.SwipeDirection.BOTH)
			.setUndoStyle(EnhancedListView.UndoStyle.MULTILEVEL_POPUP)
			.setRequireTouchBeforeDismiss(false)
			.setUndoHideDelay(3000);
		
		// Sets empty view for empty list your rules
		View emptyView = getEmptyView(view);
		mElvRules.setEmptyView(emptyView);
		
		return view;
	}

	@Override
	public void onResume() {
		LOGGER.debug(CLASS_NAME + "#onResume(...)");
		super.onResume();
		
		refreshRules();
		handleAlerts();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");
		
		inflater.inflate(R.menu.fragment_your_rules_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");

		switch (item.getItemId()) {
		case R.id.action_create_rule:
			Intent addRuleIntent = new Intent(mActivity, RuleEditorActivity.class);
			addRuleIntent.setAction(Config.Action.ACTION_CREATE_RULE);
			startActivityForResult(addRuleIntent, CREATE_RULE_REQUEST_CODE);
			return true;
		case R.id.action_store:
			Intent rulesStoreIntent = new Intent(mActivity, RulesStoreActivity.class);
			startActivityForResult(rulesStoreIntent, DOWNLOAD_RULE_REQUEST_CODE);
			return true;
		case R.id.action_rules_backup:
			// TODO: Popup a dialog, notify the override of the previous backup.
			RulesUtils.backupAllRules(mActivity, new BackupCallback() {
				@Override
				public void done() {
					String backupRulesToastText = getString(R.string.toast_backup_all_rules);
					ToastUtils.show(AppState.BUILD_RELEASE,
							mActivity,
							backupRulesToastText, Toast.LENGTH_SHORT);
				}
			});
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case CREATE_RULE_REQUEST_CODE: {
				Rule rule = (Rule) data.getSerializableExtra(Config.Extra.EXTRA_RULE);
				LOGGER.info("Created Rule: " + rule);
				mRulesAdapter.add(0, rule);
				mRulesAdapter.notifyDataSetChanged();
				mElvRules.setSelectionAfterHeaderView();
				break;
			}
			case EDIT_RULE_REQUEST_CODE: {
				Rule rule = (Rule) data.getSerializableExtra(Config.Extra.EXTRA_RULE);
				List<Rule> rules = mRulesAdapter.getItems();
				int position = rules.indexOf(rule);
				mRulesAdapter.remove(rule);
				mRulesAdapter.add(position, rule);
				mRulesAdapter.notifyDataSetChanged();
				break;
			}
			case DOWNLOAD_RULE_REQUEST_CODE: {
				LOGGER.info("Downloaded Rules");
				List<Rule> rules = getRules();
				mRulesAdapter.clear();
				mRulesAdapter.addAll(rules);
				mElvRules.setSelectionAfterHeaderView();
				break;
			}
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
	}
	
	private List<Rule> getRules() {
		User defaultUser = USER_DAO.get(DefaultUser.class);
		List<Rule> rules = (defaultUser != null) ? defaultUser.getRules() : new ArrayList<Rule>();
		return rules;
	}

	private OnItemClickListener mRuleItemEditorReferral = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Rule rule = mRulesAdapter.getItem(position);
			Intent intent = new Intent(mActivity, RuleEditorActivity.class);
			intent.setAction(Config.Action.ACTION_EDIT_RULE);
			intent.putExtra(Config.Extra.EXTRA_RULE_ID, rule.getId());
			startActivityForResult(intent, EDIT_RULE_REQUEST_CODE);
		}
	};
	
	private OnItemLongClickListener mRuleItemStartActionMode = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			mRulesAdapter.toggleSelectedItem(position);
			List<Rule> selectedItems = mRulesAdapter.getSelectedItems();
			boolean hasSelectedItems = !selectedItems.isEmpty();
			if (mActionMode == null && hasSelectedItems) {
				mActionMode = mActivity.startActionMode(mActionModeCallback);
				String acitonModeTitle = getString(R.string.title_action_mode);
				mActionMode.setTitle(String.format(acitonModeTitle, selectedItems.size()));
			}
			return true;
		}
	};
	
	private OnItemClickListener mRuleItemToggleSelection = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
			mRulesAdapter.toggleSelectedItem(position);
			List<Rule> selectedItems = mRulesAdapter.getSelectedItems();
			String acitonModeTitle = getString(R.string.title_action_mode);
			mActionMode.setTitle(String.format(acitonModeTitle, selectedItems.size()));
			boolean hasSelectedItems = !selectedItems.isEmpty();
			if (mActionMode != null && !hasSelectedItems) {
				mActionMode.finish();
				mActionMode = null;
			}
		}
	};
	
	private void storeRules(List<Rule> rules) {
		LOGGER.debug(CLASS_NAME + "#storeRules(...)");
		
		Collections.reverse(rules);
		
		for (Rule rule : rules) {
			RULE_DAO.store(DefaultUser.class, rule);
		}
	}
	
	private void deleteLocalRules(List<Rule> selectedRules) {
		LOGGER.debug(CLASS_NAME + "#deleteLocalRules(...)");
		
		// Inactivates all the selected rules to delete
		for (final Rule selectedRule : selectedRules) {
			if (selectedRule.isActive()) {
				selectedRule.setActive(mActivity, false, new Listener() {
					@Override
					public void onInactivated() {
						RULE_DAO.delete(selectedRule);
						String action = getString(R.string.rule_deleted);
						ACTIVITY_LOG_DAO.log(selectedRule.getId(), selectedRule.getName(), action);
						if (AppState.isAppEnabled()) {
							MyApplication.updateBackgroundServices();
						}
					}
					@Override
					public void onActivated() {
					}
					@Override
					public void onError(int errorMessageResourceId) {
					}
				});
			} else {
				RULE_DAO.delete(selectedRule);
				String action = getString(R.string.rule_deleted);
				ACTIVITY_LOG_DAO.log(selectedRule.getId(), selectedRule.getName(), action);
			}
		}
	}
	
	private void restoreLocalRule(int position, Rule ruleToRestore) {
		LOGGER.debug(CLASS_NAME + "#restoreLocalRule(...)");
		
		RULE_DAO.store(DefaultUser.class, ruleToRestore);
		mRulesAdapter.add(position, ruleToRestore);
		String action = getString(R.string.rule_restored);
		ACTIVITY_LOG_DAO.log(ruleToRestore.getId(), ruleToRestore.getName(), action);
	}
	
	private void refreshRules() {
		List<Rule> rules = getRules();
		mRulesAdapter.clear();
		mRulesAdapter.addAll(rules);
	}
	
	private void handleAlerts() {
		mAlertsManager.handleAlerts(mActivity);
		mAlertsAdapter.notifyDataSetChanged();
		
		if (mAlertsAdapter.isEmpty()) {
			mSlidingLayout.hidePanel();
		} else {
			mSlidingLayout.showPanel();
		}
	}
	
	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.list_your_rules_action_mode, menu);
			
			mElvRules.setOnItemClickListener(mRuleItemToggleSelection);
			mElvRules.setOnItemLongClickListener(null);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			List<Rule> selectedRules = mRulesAdapter.getSelectedItems();
			
			switch (item.getItemId()) {
			case R.id.action_copy_selected:
				copyRules(selectedRules);
				mode.finish();
				return true;
			case R.id.action_delete_selected:
				deleteRules(selectedRules);
				mode.finish();
				return true;
			case R.id.action_backup_selected:
				backupRules(selectedRules);
				mode.finish();
				return true;
			case R.id.action_enable_selected:
				changeActiveStateRules(selectedRules, true);
				mode.finish();
				return true;
			case R.id.action_disable_selected:
				changeActiveStateRules(selectedRules, false);
				mode.finish();
				return true;
			default:
				return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mElvRules.setOnItemClickListener(mRuleItemEditorReferral);
			mElvRules.setOnItemLongClickListener(mRuleItemStartActionMode);
			mActionMode = null;
			mRulesAdapter.clearSelectedItems();
		}
	};
	
	public void copyRules(List<Rule> selectedRules) {
		List<Rule> cloneRules = new ArrayList<Rule>();
		for (Rule selectedRule : selectedRules) {
			cloneRules.add(selectedRule.getClone());
		}
		mRulesAdapter.addAll(cloneRules);
		storeRules(cloneRules);
		
		String copyRulesToastText = null;
		if (selectedRules.size() != 1) {
			copyRulesToastText = getString(R.string.toast_copy_rules, selectedRules.size());
		} else {
			copyRulesToastText = getString(R.string.toast_copy_rule, selectedRules.get(0).getName());	
		}
		ToastUtils.show(AppState.BUILD_RELEASE,
				mActivity,
				copyRulesToastText, Toast.LENGTH_SHORT);
	}
	
	public void backupRules(final List<Rule> selectedRules) {
		RulesUtils.backupRules(mActivity, selectedRules, new BackupCallback() {
			@Override
			public void done() {
				String backupRulesToastText = null;
				if (selectedRules.size() != 1) {
					backupRulesToastText = getString(R.string.toast_backup_rules, selectedRules.size());
				} else {
					backupRulesToastText = getString(R.string.toast_backup_rule, selectedRules.get(0).getName());
				}
				ToastUtils.show(AppState.BUILD_RELEASE,
						mActivity,
						backupRulesToastText, Toast.LENGTH_SHORT);
			}
		});
	}
	
	public void deleteRules(List<Rule> selectedRules) {
		List<Rule> rulesToDelete = new ArrayList<Rule>();
		rulesToDelete.addAll(selectedRules);
		mRulesAdapter.removeAll(rulesToDelete);
		deleteLocalRules(rulesToDelete);
		ParseUtils.deleteTemplateRules(mActivity, rulesToDelete);
		
		String deleteRulesToastText = null;
		if (rulesToDelete.size() != 1) {
			deleteRulesToastText = getString(R.string.toast_delete_rules, rulesToDelete.size());
		} else {
			deleteRulesToastText = getString(R.string.toast_delete_rule, rulesToDelete.get(0).getName());	
		}
		ToastUtils.show(AppState.BUILD_RELEASE,
				mActivity,
				deleteRulesToastText, Toast.LENGTH_SHORT);
	}
	
	public void changeActiveStateRules(List<Rule> selectedRules, boolean active) {
		LOGGER.debug(CLASS_NAME + "#changeActiveStateRules(...)");

		for (Rule rule : selectedRules) {
			if (rule.isActive() != active) {
				final Rule ruleToModify = rule;
				ruleToModify.setActive(mActivity, active, new Rule.Listener() {
					@Override
					public void onInactivated() {
						RULE_DAO.store(DefaultUser.class, ruleToModify);
						if (AppState.isAppEnabled()) {
							MyApplication.updateBackgroundServices();
						}
						mRulesAdapter.notifyDataSetChanged();
					}
					@Override
					public void onActivated() {
						RULE_DAO.store(DefaultUser.class, ruleToModify);
						if (AppState.isAppEnabled()) {
							MyApplication.updateBackgroundServices();
							ruleToModify.perform(MyApplication.getContext());
						}
						mRulesAdapter.notifyDataSetChanged();
					}
					@Override
					public void onError(int errorMessageResourceId) {
						switch (errorMessageResourceId) {
						case R.string.rule_activation_error_circular_dependency:							
							RulesDependencyGraphUtils.resolveStronglyConnectedSets(mActivity, ruleToModify);
							break;
						case R.string.location_status_codes_geofence_not_available:
						case R.string.trigger_activation_error_location_service_disabled:
							DialogLauncher.launchActionSecuritySettings(
									mActivity,
									MainActivity.FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR);
							break;
						case R.string.trigger_activation_error_time_elapsed:
							DialogLauncher.launchRuleActivationErrorDialog(
									mActivity,
									MainActivity.FRAGMENT_TAG_TRIGGER_ACTIVATION_ERROR,
									errorMessageResourceId,
									ruleToModify.getId());
							break;
						default:
							DialogLauncher.launchGeneralErrorDialog(
									mActivity,
									MainActivity.FRAGMENT_TAG_GENERAL_ERROR,
									errorMessageResourceId);
							break;
						}
					}
				});
			}
		}
	}
	
	private View getEmptyView(View view) {
		View emptyView = view.findViewById(android.R.id.empty);
		TextView tvHintPrefix = (TextView) emptyView.findViewById(R.id.tvHintPrefix);
		tvHintPrefix.setText(getString(R.string.list_view_empty_your_rules_hint_prefix));
		ImageView ivHintIcon = (ImageView) emptyView.findViewById(R.id.ivHintIcon);
		ivHintIcon.setImageResource(R.drawable.ic_action_create_disabled);
		TextView tvHintSuffix = (TextView) emptyView.findViewById(R.id.tvHintSuffix);
		tvHintSuffix.setText(getString(R.string.list_view_empty_your_rules_hint_suffix));
		TextView tvMessageBoxTitle = (TextView) emptyView.findViewById(R.id.tvMessageBoxTitle);
		tvMessageBoxTitle.setText(getString(R.string.list_view_empty_your_rules_message_title));
		TextView tvMessageBoxText = (TextView) emptyView.findViewById(R.id.tvMessageBoxText);
		tvMessageBoxText.setText(getString(R.string.list_view_empty_your_rules_message_text));
		return emptyView;
	}
	
	/*
	 * 
	 * 
	 * Rules Adapter.
	 * 
	 * 
	 */
	
	/**
	 * @author Ran
	 *
	 */
	private class RulesAdapter extends MultiSelectionBaseAdapter<Rule> {
		
		private Context mContext = null;
		
		private LayoutInflater mInflater = null;
		
		private int mCardPaddingHorizontal;
		
		private int mCardPaddingVertical;
		
		private String mCreatedByPrefix = null;

		public RulesAdapter(Context context, List<Rule> rules) {
			super(rules);
			
			mContext = context;
			mInflater  = LayoutInflater.from(context);
			mCardPaddingHorizontal = (int) context.getResources().getDimension(R.dimen.card_padding_horizontal);
			mCardPaddingVertical = (int) context.getResources().getDimension(R.dimen.card_padding_vertical);
			mCreatedByPrefix = context.getString(R.string.created_by_prefix);
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.list_item_rule_card, parent, false);
				holder = new ViewHolder();
				holder.vgCard = (ViewGroup) convertView.findViewById(R.id.vgCard);
				holder.tvRuleTitle = (TextView) convertView.findViewById(R.id.tvRuleTitle);
				holder.tvCreatedBy = (TextView) convertView.findViewById(R.id.tvCreatedBy);
				holder.cbSwitch = (CompoundButton) convertView.findViewById(R.id.cbSwitch);
				holder.llTriggers = (LinearLayout) convertView.findViewById(R.id.llTriggers);
				holder.llActions = (LinearLayout) convertView.findViewById(R.id.llActions);
				holder.vgCardFooter = (ViewGroup) convertView.findViewById(R.id.vgCardFooter);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			Rule rule = getItems().get(position);
			boolean isRuleActive = rule.isActive();
			boolean isRuleSelected = (getSelectedItems() != null) ? getSelectedItems().contains(rule) : false;
			
			reflectRuleActiveState(holder, isRuleActive);
			reflectRuleSelectionState(holder, isRuleSelected);
			holder.position = position;
			holder.tvRuleTitle.setText(rule.getName());
			holder.tvCreatedBy.setText(mCreatedByPrefix + rule.getCreatedByName());
			holder.cbSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					Rule rule = getItem(position);
					if (rule.isActive() != isChecked) {
						LOGGER.info(CLASS_NAME + "#onCheckedChanged(...) rule " + rule.getName());
						changeActiveStateRules(Arrays.asList(rule), isChecked);
					}
				}
			});
			holder.cbSwitch.setChecked(isRuleActive);
			inflateRuleTriggersViews(holder, rule);
			inflateRuleActionsViews(holder, rule);
			inflateCardFooter(convertView, holder, rule);

			return convertView;
		}

		/**
		 * Reflects the rule's selection state on the card view,
		 * by changing the card background color.
		 * 
		 * @param holder
		 * @param isRuleSelected
		 */
		private void reflectRuleSelectionState(ViewHolder holder,
				boolean isRuleSelected) {
			if (isRuleSelected) {
				holder.vgCard.setBackgroundResource(R.drawable.square_card_selected);
			} else {
				holder.vgCard.setBackgroundResource(R.drawable.square_card_selector);
			}
			holder.vgCard.setPadding(mCardPaddingHorizontal,
					mCardPaddingVertical, mCardPaddingHorizontal,
					mCardPaddingVertical);
		}

		/**
		 * Reflects the rule's active state on the card view,
		 * by the card alpha.
		 * 
		 * @param holder
		 * @param isRuleActive
		 */
		private void reflectRuleActiveState(ViewHolder holder, boolean isRuleActive) {
			float alpha;
			if (!isRuleActive) {
				alpha = 0.3F;
			} else {
				alpha = 1F;
			}
			holder.vgCard.setAlpha(alpha);
		}

		private void inflateRuleActionsViews(ViewHolder holder, Rule rule) {
			holder.llActions.removeAllViews();
			for (Action action : rule.getActions()) {
				View ruleComponentView = mInflater.inflate(R.layout.list_item_rule_component_in_rule_card, holder.llActions, false);
				ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
				TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
				ivIcon.setImageBitmap(action.getIcon());
				tvDescription.setText(action.getDescription(mContext));
				holder.llActions.addView(ruleComponentView);
			}
		}

		private void inflateRuleTriggersViews(ViewHolder holder, Rule rule) {
			holder.llTriggers.removeAllViews();
			for (Trigger trigger : rule.getTriggers()) {
				View ruleComponentView = mInflater.inflate(R.layout.list_item_rule_component_in_rule_card, holder.llTriggers, false);
				ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
				TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
				ivIcon.setImageBitmap(trigger.getIcon());
				tvDescription.setText(trigger.getDescription(mContext));
				holder.llTriggers.addView(ruleComponentView);
			}
		}
		
		private void inflateCardFooter(View convertView, ViewHolder holder, Rule rule) {
			// Shows the card footer
			holder.vgCardFooter = (ViewGroup) convertView.findViewById(R.id.vgCardFooter);
			holder.vgCardFooter.setTag(R.id.tag_list_item_position, holder.position);
			holder.vgCardFooter.setVisibility(View.VISIBLE);
			// Initialize like button
			holder.bContextualAction1 = (Button) convertView.findViewById(R.id.bContextualAction1);
			if (rule.isLiked()) {
				holder.bContextualAction1.setText(R.string.contextual_action_button_liked);
				holder.bContextualAction1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_liked, 0, 0, 0);
			} else {
				holder.bContextualAction1.setText(R.string.contextual_action_button_like);
				holder.bContextualAction1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_like, 0, 0, 0);
			}
			holder.bContextualAction1.setOnClickListener(mOnToggleLikeClickListener);
			holder.bContextualAction1.setVisibility(View.VISIBLE);
			// Initialize upload button
			holder.bContextualAction2 = (Button) convertView.findViewById(R.id.bContextualAction2);
			if (rule.isMine()) {
				holder.bContextualAction2.setText(R.string.contextual_action_button_upload);
				holder.bContextualAction2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_upload, 0, 0, 0);
				holder.bContextualAction2.setOnClickListener(mOnUploadClickListener);
				holder.bContextualAction2.setVisibility(View.VISIBLE);
			} else {
				holder.bContextualAction2.setVisibility(View.GONE);
			}
			// Initialize more button
			holder.bContextualAction4 = (Button) convertView.findViewById(R.id.bContextualAction4);
			holder.bContextualAction4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_more_actions, 0, 0, 0);
			holder.bContextualAction4.setOnClickListener(mOnMoreActionsClickListener);
			holder.bContextualAction4.setVisibility(View.VISIBLE);
		}

		class ViewHolder {
			int position;
			ViewGroup vgCard;
			TextView tvRuleTitle;
			TextView tvCreatedBy;
			CompoundButton cbSwitch;
			LinearLayout llTriggers;
			LinearLayout llActions;
			ViewGroup vgCardFooter;
			Button bContextualAction1;
			Button bContextualAction2;
			Button bContextualAction3;
			Button bContextualAction4;
		}
		
		private OnClickListener mOnToggleLikeClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Validates Internet connection
				boolean isConnected = DeviceState.isNetworkConnectionOnline();
				if (!isConnected) {
					String toastText = getString(R.string.toast_contextual_action_failed);
					String contextualAction = getString(R.string.contextual_action_button_like); 
					toastText = String.format(toastText, contextualAction);
					ToastUtils.show(AppState.BUILD_RELEASE, getActivity(),
							toastText, Toast.LENGTH_SHORT);
					return;
				}
				Button button = (Button) v;
				// Gets the rule's position
				View parent = (View) v.getParent();
				int position = (Integer) parent.getTag(R.id.tag_list_item_position);
				Rule rule = getItem(position);
				// Updates the client
				rule.toggleLiked();
				if (rule.isLiked()) {
					button.setText(R.string.contextual_action_button_liked);
					button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_liked, 0, 0, 0);
				} else {
					button.setText(R.string.contextual_action_button_like);
					button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_like, 0, 0, 0);
				}
				// Updates the server
				TemplateRule templateRule = rule.toTemplateRule();
				ParseUtils.toggleTemplateRuleLiked(getActivity(), templateRule, templateRule.isLiked(), null);
			}
		};
		
		private OnClickListener mOnUploadClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Validates Internet connection
				boolean isConnected = DeviceState.isNetworkConnectionOnline();
				if (!isConnected) {
					String toastText = getString(R.string.toast_contextual_action_failed);
					String contextualAction = getString(R.string.contextual_action_button_upload); 
					toastText = String.format(toastText, contextualAction);
					ToastUtils.show(AppState.BUILD_RELEASE, getActivity(),
							toastText, Toast.LENGTH_SHORT);
					return;
				}
				// Gets the rule's position
				View parent = (View) v.getParent();
				int position = (Integer) parent.getTag(R.id.tag_list_item_position);
				Rule rule = getItem(position);
				// Launches upload upload rule editor
				Intent intent = new Intent(getActivity(), TemplateRuleEditorActivity.class);
				intent.setAction(Config.Action.ACTION_UPLOAD_RULE);
				intent.putExtra(Config.Extra.EXTRA_TEMPLATE_RULE, rule.toTemplateRule());
				startActivity(intent);
			}
		};
		
		private OnClickListener mOnMoreActionsClickListener = new OnClickListener() {
			@Override
			public void onClick(View view) {
				// Gets the rule's position
				View parent = (View) view.getParent();
				int position = (Integer) parent.getTag(R.id.tag_list_item_position);
				final Rule rule = getItem(position);
				// Creates the popup menu
				PopupMenu popupMenu = new PopupMenu(mContext, view);
				popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
						case R.id.action_backup_rule:
							backupRules(Arrays.asList(rule));
							return true;
						case R.id.action_copy_rule:
							copyRules(Arrays.asList(rule));
							return true;
						case R.id.action_delete_rule:
							deleteRules(Arrays.asList(rule));
							return true;
						default:
							return false;
						}
					}
				});
				Menu menu = popupMenu.getMenu();
				MenuInflater menuInflater = popupMenu.getMenuInflater();
				menuInflater.inflate(R.menu.list_item_rule_actions, menu);
				popupMenu.show();
			}
		};
	}

}

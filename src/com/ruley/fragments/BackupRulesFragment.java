/**
 * 
 */
package com.ruley.fragments;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;
import com.parse.ParseQueryAdapter.QueryFactory;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.ruley.R;
import com.ruley.activities.RuleEditorActivity;
import com.ruley.adapters.RulesParseQueryAdapter;
import com.ruley.app.AppState;
import com.ruley.config.Config;
import com.ruley.fragments.base.BaseFragment;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.parse.RuleParse;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.IntentUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.RulesUtils;
import com.ruley.utilities.RulesUtils.RestoreCallback;
import com.ruley.utilities.RulesUtils.RestoreType;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BackupRulesFragment extends BaseFragment implements RulesParseQueryAdapter.Listener {
	
	private static final String CLASS_NAME = BackupRulesFragment.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BackupRulesFragment.class.getName());
	
	private static final int EDIT_RULE_REQUEST_CODE = 1001;
	
	private RulesParseQueryAdapter mBackupRulesAdapter = null;

	private ListView mLvBackupRules = null;
	
	private View mVgLoading = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreateView(...)");
		
		View rootView = inflater.inflate(R.layout.fragment_backup_rules, container, false);
		mVgLoading = rootView.findViewById(R.id.vgLoading);
		mBackupRulesAdapter = new RulesParseQueryAdapter(getActivity(),
				UserParse.RELATION_SAVED_RULES, mQuerySaved,
				BackupRulesFragment.this);
		mBackupRulesAdapter.addOnQueryLoadListener(new OnQueryLoadListener<RuleParse>() {
			@Override
			public void onLoaded(List<RuleParse> objects, Exception e) {
				hideLoadingView();
			}
			@Override
			public void onLoading() {
				showLoadingView();
			}
		});
		mLvBackupRules = (ListView) rootView.findViewById(R.id.lvStoreRules);
		mLvBackupRules.setAdapter(mBackupRulesAdapter);
		mLvBackupRules.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				RuleParse ruleParse = (RuleParse) mBackupRulesAdapter.getItem(position);
				Rule backupRule = ruleParse.getRule();
				Intent intent = new Intent(mActivity, RuleEditorActivity.class);
				intent.setAction(Config.Action.ACTION_EDIT_BACKUP_RULE);
				intent.putExtra(Config.Extra.EXTRA_RULE, backupRule);
				startActivityForResult(intent, EDIT_RULE_REQUEST_CODE);
			}
		});
		
		// Sets the empty view
		View emptyView = getEmptyView(rootView);
		mLvBackupRules.setEmptyView(emptyView);
		
		return rootView;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");
		
		inflater.inflate(R.menu.fragment_backup_rules_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");
		
		switch (item.getItemId()) {
		case R.id.action_rules_restore:
			// TODO: Popup a dialog, notify the deletion of all current rules.
			RulesUtils.restoreAllRules(mActivity, RestoreType.MERGE, new RestoreCallback<Rule>() {
				@Override
				public void done(List<Rule> items) {
					String restoreRulesToastText = getString(R.string.toast_restore_all_rules);
					ToastUtils.show(AppState.BUILD_RELEASE, getActivity(), restoreRulesToastText, Toast.LENGTH_SHORT);
				}
			});
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		if (resultCode == Activity.RESULT_OK) {
			mBackupRulesAdapter.loadObjects();
		}
	}

	private QueryFactory<RuleParse> mQuerySaved = new ParseQueryAdapter.QueryFactory<RuleParse>() {
		@Override
		public ParseQuery<RuleParse> create() {
			ParseUser currentUser = ParseUser.getCurrentUser();
			ParseRelation<RuleParse> backupRulesRelation = currentUser.getRelation(UserParse.RELATION_SAVED_RULES);
			ParseQuery<RuleParse> query = backupRulesRelation.getQuery();
			query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
			query.orderByDescending(RuleParse.FIELD_CREATED_AT);
			query.setLimit(50);
			return query;
		}
	};
	
	private View getEmptyView(View view) {
		View emptyView = view.findViewById(android.R.id.empty);
		ImageView ivMessageBoxIcon = (ImageView) emptyView.findViewById(R.id.ivMessageBoxIcon);
		TextView tvMessageBoxTitle = (TextView) emptyView.findViewById(R.id.tvMessageBoxTitle);
		TextView tvMessageBoxText = (TextView) emptyView.findViewById(R.id.tvMessageBoxText);
		ivMessageBoxIcon.setImageResource(R.drawable.ic_message_box_backup);
		tvMessageBoxTitle.setText(R.string.list_view_empty_backup_rules_message_title);
		tvMessageBoxText.setText(R.string.list_view_empty_backup_rules_message_text);
		
		return emptyView;
	}
	
	private void showLoadingView() {
		mVgLoading.setAlpha(1f);
		mVgLoading.setVisibility(View.VISIBLE);
	}
	
	private void hideLoadingView() {
		mVgLoading.animate().alpha(0f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mVgLoading.setVisibility(View.GONE);
			}
		}).start();
	}

	@Override
	public void onRuleRestore(RuleParse ruleParse) {
		Rule rule = ruleParse.getRule();
		RulesUtils.restoreRules(getActivity(), RestoreType.MERGE, Arrays.asList(rule));
		
		String restoreRulesToastText = getString(R.string.toast_restore_rule, rule.getName()); 
		ToastUtils.show(AppState.BUILD_RELEASE, getActivity(), restoreRulesToastText,
				Toast.LENGTH_SHORT);
	}

	@Override
	public void onRuleDelete(final RuleParse ruleParse) {
		mBackupRulesAdapter.removeInBackground(ruleParse, new DoneCallback() {
			@Override
			public void done() {
				String deleteRuleToastText = getString(R.string.toast_delete_rule, ruleParse.getRule().getName());
				ToastUtils.show(AppState.BUILD_RELEASE, getActivity(), deleteRuleToastText, Toast.LENGTH_SHORT);
			}
		});
	}

	@Override
	public void onRuleShare(RuleParse ruleParse) {
		Rule rule = ruleParse.getRule();
		TemplateRule templateRule = rule.toTemplateRule();
		Intent shareIntent = RulesUtils.getShareIntent(templateRule);
		if (IntentUtils.isIntentSafe(shareIntent)) {
			String chooserTitle = getString(R.string.title_chooser_share_rules);
			startActivity(Intent.createChooser(shareIntent, chooserTitle));
		}
	}
	
}
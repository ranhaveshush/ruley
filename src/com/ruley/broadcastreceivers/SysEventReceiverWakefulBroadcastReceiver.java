/**
 * 
 */
package com.ruley.broadcastreceivers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

import com.ruley.app.AppState;
import com.ruley.services.SysEventHandlerIntentService;
import com.ruley.utilities.ToastUtils;

/**
 * This class receives system events and delegates the work
 * to the SysEventHandlerIntentService a worker service in a saparate thread
 * then the main application thread.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SysEventReceiverWakefulBroadcastReceiver extends WakefulBroadcastReceiver {
	
	private static final String CLASS_NAME = SysEventReceiverWakefulBroadcastReceiver.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SysEventReceiverWakefulBroadcastReceiver.class.getName());

	@Override
	public void onReceive(Context context, Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onReceive(...)");

		String action = intent.getAction();
		ToastUtils.show(AppState.BUILD_DEBUG, context, action, Toast.LENGTH_SHORT);
		Bundle extras = intent.getExtras();

		Intent serviceIntent = new Intent(context, SysEventHandlerIntentService.class);
		serviceIntent.setAction(action);
		if (extras != null) {
			serviceIntent.putExtras(extras);
		}

		startWakefulService(context, serviceIntent);
	}
}

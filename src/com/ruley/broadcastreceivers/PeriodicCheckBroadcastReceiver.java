/**
 * 
 */
package com.ruley.broadcastreceivers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PeriodicCheckBroadcastReceiver extends BroadcastReceiver {
	
	public static final long PERIODIC_CHECK_TIME_INTERVAL_MILLIS = AlarmManager.INTERVAL_HALF_HOUR;
	
	private static final String CLASS_NAME = PeriodicCheckBroadcastReceiver.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PeriodicCheckBroadcastReceiver.class);

	@Override
	public void onReceive(Context context, Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onReceive(...)");
		
		ToastUtils.show(AppState.BUILD_DEBUG, context, "Periodic check!", Toast.LENGTH_LONG);
		
		if (MyApplication.isAppRestartNeeded()) {
			ToastUtils.show(AppState.BUILD_DEBUG, context, "Restarting the app!", Toast.LENGTH_LONG);
			LOGGER.info("Restarting the app");
			MyApplication.startApp();
		}
	}
	
	public static void start(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		Intent periodicCheckIntent = new Intent(context, PeriodicCheckBroadcastReceiver.class);
		PendingIntent periodicCheckPendingIntent = PendingIntent.getBroadcast(context, 0,
				periodicCheckIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		// Validates pending intent
		if (periodicCheckPendingIntent == null) {
			return;
		}
		
		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis()
						+ PERIODIC_CHECK_TIME_INTERVAL_MILLIS,
				PERIODIC_CHECK_TIME_INTERVAL_MILLIS, periodicCheckPendingIntent);
	}
	
	public static void cancel(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		Intent periodicCheckIntent = new Intent(context, PeriodicCheckBroadcastReceiver.class);
		PendingIntent periodicCheckPendingIntent = PendingIntent.getBroadcast(context, 0,
				periodicCheckIntent, PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT);
		
		// Validates pending intent
		if (periodicCheckPendingIntent == null) {
			return;
		}
		
		alarmManager.cancel(periodicCheckPendingIntent);
	}

}

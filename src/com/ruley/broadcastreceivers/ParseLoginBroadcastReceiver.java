/**
 * 
 */
package com.ruley.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ruley.app.MyApplication;
import com.ruley.config.Config;

/**
 * This broadcast receiver listens to Parse login and sign out
 * and creates or destroys the app background services respectively.
 * 
 * @author Ran
 *
 */
public class ParseLoginBroadcastReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent != null) {
			String action = intent.getAction();
			if (Config.Action.ACTION_SIGNUP.equals(action)) {
				MyApplication.onSignup();
			} else if (Config.Action.ACTION_LOGIN.equals(action)) {
				MyApplication.onLogin();
				MyApplication.startApp();
			} else if (Config.Action.ACTION_LOGOUT.equals(action)) {
				MyApplication.onLogout();
				MyApplication.stopApp();
			}
		}
	}
	
}

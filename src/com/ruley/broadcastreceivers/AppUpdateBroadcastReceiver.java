/**
 * 
 */
package com.ruley.broadcastreceivers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.utilities.ToastUtils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AppUpdateBroadcastReceiver extends BroadcastReceiver {
	
	private static final String CLASS_NAME = AppUpdateBroadcastReceiver.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AppUpdateBroadcastReceiver.class);

	@Override
	public void onReceive(Context context, Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onReceive(...)");
		
		ToastUtils.show(AppState.BUILD_DEBUG, context, "Ruley app has been updated!", Toast.LENGTH_LONG);
		
		if (MyApplication.isAppRestartNeeded()) {
			ToastUtils.show(AppState.BUILD_DEBUG, context, "Restarting the app!", Toast.LENGTH_LONG);
			LOGGER.info("Restarting the app");
			MyApplication.startApp();
		}
	}

}

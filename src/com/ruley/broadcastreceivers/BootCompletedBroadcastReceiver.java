/**
 * 
 */
package com.ruley.broadcastreceivers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ruley.model.beans.triggers.BootCompletedTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.RulesUtils;

/**
 * This broadcast receiver, restarts the background service
 * SysEventReceiverService after the device reboots completed,
 * if and only if the application mode state is enabled
 * which means the application is enabled by the user
 * and the background service should be started
 * and all the rules defined by the user should be reactivated.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BootCompletedBroadcastReceiver extends BroadcastReceiver {
	
	private static final String CLASS_NAME = BootCompletedBroadcastReceiver.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BootCompletedBroadcastReceiver.class.getName());
	
	@Override
	public void onReceive(final Context context, Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onReceive(...)");
		
		setBootCompletedTriggersSatisfaction(true);
		RulesUtils.activateActiveRules();
		RulesUtils.performActiveRules();
		setBootCompletedTriggersSatisfaction(false);
	}
	
	private void setBootCompletedTriggersSatisfaction(boolean satisfied) {
		LOGGER.debug(CLASS_NAME + "#setBootCompletedTriggersSatisfaction(...)");
		
		ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		List<Trigger> activeBootCompletedTriggers = triggerDAO.list(BootCompletedTrigger.class, true);
		
		for (Trigger trigger : activeBootCompletedTriggers) {
			trigger.setSatisfied(satisfied);
			triggerDAO.store(trigger);
		}
	}

}
/**
 * 
 */
package com.ruley.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import android.content.IntentFilter;
import android.os.Environment;
import android.util.Log;

import com.parse.ParseUser;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.Category;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.triggers.base.Trigger.ITriggerHandler;
import com.ruley.utilities.FilesUtils;
import com.ruley.utilities.PrefsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public final class Config {

	private Config() {
	}
	
	public static final String APP_NAME = "Ruley";

	public static final String APP_PACKAGE = "com.ruley";
	
	public static final String CONTENT_OBSERVER_PACKAGE = Config.APP_PACKAGE + ".contentobservers";
	
	public static final String MY_EMAIL = "ruley.team@gmail.com";
	
	/**
	 * File system paths.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static final class Paths {
		
		private Paths() {
		}
		
		/**
		 * This is the app root storage directory.
		 */
		private static String appStorageDirPath = null;
		
		/**
		 * Gets the app root storage directory.
		 * 
		 * @return
		 */
		public static String getAppStorageDirPath() {
			// If anonymous user, throw exception
			if (ParseUser.getCurrentUser() == null) {
				Log.w("WARN", "Anonymous user requesting app storage directory path, which will return NULL, logging out.");
				MyApplication.logoutApp();
			// If app started by logged in user,
			// get the app root storage directory path from prefs
			} else if (appStorageDirPath == null) {
				appStorageDirPath = PrefsUtils.getAppStorageDirPath();
			}
			return appStorageDirPath;
		}
		
		/**
		 * Sets the app root storage directory.
		 * 
		 * @return
		 */
		public static void setDirAppStorage(String dirPath) {
			FilesUtils.isFilePathValid(dirPath);
			PrefsUtils.setAppStorageDirPath(dirPath);
			PrefsUtils.setKeyMaster(null);
			appStorageDirPath = dirPath;
		}
		
		public static final String DIR_STORAGE_EXTERNAL = Environment
				.getExternalStorageDirectory().getPath()
				+ File.separator + Config.APP_NAME
				+ File.separator + "Users" + File.separator;
		
		public static final String DIR_STORAGE_INTERNAL = MyApplication
				.getContext().getDir("data", 0).toString()
				+ File.separator + "Users" + File.separator;
		
		public static final String DIR_MEDIA = "Media" + File.separator;
		
		public static final String DIR_IMAGES = DIR_MEDIA + "Images" + File.separator;
		
		public static final String DIR_VIDEOS = DIR_MEDIA + "Videos" + File.separator;
		
		public static final String DIR_AUDIO = DIR_MEDIA + "Audio" + File.separator;
		
		public static final String DIR_DATA = "Data" + File.separator;
		
		public static final String FILE_MASTER = DIR_DATA + "master";
		
		public static final String FILE_JSON_DEVICE_STATE = DIR_DATA + "device_state.json";
		
		public static final String FILE_JSON_DATA = DIR_DATA + "data.json";
		
		public static final String FILE_JSON_ACTIVITY_LOG = DIR_DATA + "activity_log.json";
		
		public static final String DIR_BACKUPS = "Backups" + File.separator;
		
		public static final String FILE_JSON_RULES = DIR_BACKUPS + "rules.json";
		
		public static final String FILE_JSON_SHARED_RULES = DIR_BACKUPS + "shared_rules.ruley";
		
		public static final String DIR_ASSETS_FONTS = "fonts" + File.separator;
		
		public static final String DIR_ASSETS_FILTERS = "filters" + File.separator;
		
		public static final String DIR_API = DIR_ASSETS_FILTERS + "api" + File.separator;
		
		public static final String FILE_ACTIONS_API_FILTER = DIR_API + "actions/actions.xml";
		
		public static final String FILE_TRIGGERS_API_FILTER = DIR_API + "triggers/triggers.xml";
		
		public static final String FILE_BROADCAST_ACTIONS_FILTER = DIR_API + "broadcast_actions/broadcast_actions_api_%1$d.txt";
		
	}

	/**
	 * Intent actions contact.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 * 
	 */
	public static final class Action {

		private Action() {
		}
		
		public static final String ACTION_SIGNUP = APP_PACKAGE + ".ACTION_SIGNUP";
		
		public static final String ACTION_LOGIN = APP_PACKAGE + ".ACTION_LOGIN";
		
		public static final String ACTION_LOGOUT = APP_PACKAGE + ".ACTION_LOGOUT";
		
		public static final String ACTION_CREATE_RULE = APP_PACKAGE + ".ACTION_CREATE_RULE";

		public static final String ACTION_EDIT_RULE = APP_PACKAGE + ".ACTION_EDIT_RULE";
		
		public static final String ACTION_DOWNLOAD_RULE = APP_PACKAGE + ".ACTION_DOWNLOAD_RULE";
		
		public static final String ACTION_UPLOAD_RULE = APP_PACKAGE + ".ACTION_UPLOAD_RULE";
		
		public static final String ACTION_EDIT_BACKUP_RULE = APP_PACKAGE + ".ACTION_EDIT_BACKUP_RULE";

		public static final String ACTION_UPDATE_BROADCAST_RECEIVER =  APP_PACKAGE + ".ACTION_UPDATE_BROADCAST_RECEIVER";

		public static final String ACTION_ADD_TRIGGER = APP_PACKAGE + ".ACTION_EDIT_TRIGGER";
		
		public static final String ACTION_ADD_ACTION = APP_PACKAGE + ".ACTION_EDIT_ACTION";
		
		public static final String ACTION_LOCATION_SERVICE_DISABLED = APP_PACKAGE + ".ACTION_LOCATION_SERVICE_DISABLED";
		
		public static final String ACTION_REGISTER_CONTENT_OBSERVER = APP_PACKAGE + ".ACTION_REGISTER_CONTENT_OBSERVER";
		
		public static final String ACTION_UNREGISTER_CONTENT_OBSERVER = APP_PACKAGE + ".ACTION_UNREGISTER_CONTENT_OBSERVER";
		
		public static final String ACTION_UPDATE_CONTENT_OBSERVERS = APP_PACKAGE + ".ACTION_UPDATE_CONTENT_OBSERVERS";
		
		public static final String ACTION_EXCLUDE_APPS =  APP_PACKAGE + ".ACTION_EXCLUDE_APPS";
	}

	/**
	 * Intent Extras contract.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 * 
	 */
	public static final class Extra {

		private Extra() {
		}
		
		public static final String EXTRA_INTENT = APP_PACKAGE + ".EXTRA_INTENT";
		
		public static final String EXTRA_RULE = APP_PACKAGE + ".EXTRA_RULE";
		
		public static final String EXTRA_TEMPLATE_RULE = APP_PACKAGE + ".EXTRA_TEMPLATE_RULE";
		
		public static final String EXTRA_ACTION = APP_PACKAGE + ".EXTRA_ACTION";
				
		public static final String EXTRA_TRIGGER = APP_PACKAGE + ".EXTRA_TRIGGER";
		
		public static final String EXTRA_VARIABLE = APP_PACKAGE + ".EXTRA_VARIABLE";

		public static final String EXTRA_RULE_ID = APP_PACKAGE + ".EXTRA_RULE_ID";
		
		public static final String EXTRA_RULE_POSITION = APP_PACKAGE + ".EXTRA_RULE_POSITION";

		public static final String EXTRA_TRIGGER_ID = APP_PACKAGE + ".EXTRA_TRIGGER_ID";

		public static final String EXTRA_STATE = APP_PACKAGE + ".EXTRA_STATE";

		public static final String EXTRA_APP_INFO = APP_PACKAGE + ".EXTRA_APP_INFO";
		
		public static final String EXTRA_CONTENT_OBSERVER_NAME = APP_PACKAGE + ".EXTRA_CONTENT_OBSERVER_NAME";

		public static final String EXTRA_NOTIFY_FOR_DESCENDENTS = APP_PACKAGE + ".EXTRA_NOTIFY_FOR_DESCENDENTS";
		
		public static final String EXTRA_EXCLUDED_APPS_PACKAGE_NAMES = APP_PACKAGE + ".EXTRA_EXCLUDED_APPS_PACKAGE_NAMES";
	}
	
	/**
	 * Pending Intents request codes.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static final class PendingIntent {
		
		private PendingIntent() {
		}
		
		public static final int LOCATION_PROXIMITY_REQUEST_CODE = 5001;
		
		public static final int LOCATION_STATE_REQUEST_CODE = 5002;
		
		public static final int ACTIVITY_RECOGNITION_REQUEST_CODE = 5003;
		
		public static final int LOCATION_SERVICE_DISABLED_REQUEST_CODE = 5004;
	}
	
	/**
	 * Fonts.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static final class Fonts {
		
		private Fonts() {
		}
		
		public static final String FONT_TITLE = "Pacifico.ttf";
	}
	
	/**
	 * Users contract.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static final class Users {
		
		private Users() {
		}
		
		public static final String PACKAGE = APP_PACKAGE + ".model.beans.users";
		
	}

	/**
	 * Rules contract.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 * 
	 */
	public static final class Rules {

		private Rules() {
		}

		public static final String PACKAGE = APP_PACKAGE + ".model.beans.rules";
		
	}

	/**
	 * Triggers contract.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 * 
	 */
	public static final class Triggers {

		private Triggers() {
		}

		public static final String PACKAGE = APP_PACKAGE + ".model.beans.triggers";

		/**
		 * Maps intent filters to triggers handlers.
		 */
		public static final Map<IntentFilter, ITriggerHandler> INTENT_FILTERS_TO_TRIGGERS_HANDLERS = new HashMap<IntentFilter, ITriggerHandler>();

		/**
		 * Maps trigger class simple names to intent filters.
		 */
		public static final Map<String, Set<IntentFilter>> TRIGGERS_NAMES_TO_INTENT_FILTERS = new HashMap<String, Set<IntentFilter>>();
		
		/**
		 * Maps trigger class simple names to the trigger's category.
		 */
		public static final Map<String, Category> TRIGGERS_NAMES_TO_CATEGORIES = new TreeMap<String, Category>();

		/**
		 * Lists all triggers.
		 * 
		 * Filled by the static method:
		 * @see MyApplication.filterTriggersListByAPILevel.
		 */
		public static final List<Trigger> LIST = new ArrayList<Trigger>();

	}

	/**
	 * Actions contract.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 * 
	 */
	public static final class Actions {

		private Actions() {
		}

		public static final String PACKAGE = APP_PACKAGE + ".model.beans.actions";
		
		/**
		 * Maps action class simple name to the action's category.
		 */
		public static final Map<String, Category> ACTIONS_NAMES_TO_CATEGORIES = new TreeMap<String, Category>();

		/**
		 * Lists all actions.
		 * 
		 * Filled by the static method:
		 * @see MyApplication.filterActionsListByAPILevel.
		 */
		public static final List<com.ruley.model.beans.actions.base.Action> LIST = new ArrayList<com.ruley.model.beans.actions.base.Action>();
	}

}

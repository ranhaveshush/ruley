/**
 * 
 */
package com.ruley.model.beans.rules;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import com.ruley.model.beans.IValidatable;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.triggers.base.Trigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public interface IRule extends Serializable, IValidatable {
	
	public UUID getId();

	public String getName();

	public void setName(String name);

	public String getDescription();

	public void setDescription(String description);

	public List<Trigger> getTriggers();

	public void setTriggers(List<Trigger> triggers);

	public List<Action> getActions();

	public void setActions(List<Action> actions);

	public String getCreatedBy();

	public void setCreatedBy(String createdBy);
	
	public String getCreatedByName();

	public void setCreatedByName(String createdByName);

}

/**
 * 
 */
package com.ruley.model.beans.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.text.TextUtils;

import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.model.parse.UserParse;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TemplateRule implements IRule {

	private static final long serialVersionUID = -1967020853495177265L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TemplateRule.class.getName());
	
	@JsonProperty("id")
	private UUID id = null;
	
	@JsonProperty("name")
	private String name = null;

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("triggers")
	private List<Trigger> triggers = null;

	@JsonProperty("actions")
	private List<Action> actions = null;
	
	@JsonProperty("createdBy")
	private String createdBy = null;
	
	@JsonProperty("createdByName")
	private String createdByName = null;
	
	public TemplateRule() {
		super();
	}
	
	public TemplateRule(Rule rule) {
		super();
		
		this.id = rule.getId();
		this.name = rule.getName();
		this.description = rule.getDescription();
		this.createdBy = rule.getCreatedBy();
		this.createdByName = rule.getCreatedByName();
		
		List<Trigger> triggers = new ArrayList<Trigger>();
		for (Trigger trigger : rule.getTriggers()) {
			triggers.add(trigger.toTemplate());
		}
		setTriggers(triggers);
		
		List<Action> actions = new ArrayList<Action>();
		for (Action action : rule.getActions()) {
			actions.add(action.toTemplate());
		}
		setActions(actions);
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Trigger> getTriggers() {
		return triggers;
	}

	public void setTriggers(List<Trigger> triggers) {
		this.triggers = triggers;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public boolean isLiked() {
		return UserParse.isLiked(getId());
	}

	public void setLiked(boolean isLiked) {
		if (isLiked) {
			UserParse.addLikedRule(getId());
		} else {
			UserParse.removeLikedRule(getId());
		}
	}
	
	public void toggleLiked() {
		if (isLiked()) {
			setLiked(false);
		} else {
			setLiked(true);
		}
	}
	
	public Rule toRule() {
		return new Rule(TemplateRule.this);
	}
	
	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(name)
				&& !TextUtils.isEmpty(createdBy)
				&& !TextUtils.isEmpty(createdByName)
				&& triggers != null && !triggers.isEmpty()
				&& actions != null && !actions.isEmpty()
				&& validateTriggers(context)
				&& validateActions(context);
	}
	
	@Override
	public boolean equals(Object another) {
		if (another == null) return false;
	    if (another == this) return true;
	    if (!(another instanceof TemplateRule)) return false;
	    TemplateRule anotherTemplateRule = ((TemplateRule)another);
		return this.id.equals(anotherTemplateRule.id)
				&& this.createdBy.equals(anotherTemplateRule.createdBy);
	}

	@Override
	public String toString() {
		String jsonData = null;

		try {
			ObjectMapper objectMapper = JsonHelper.getInstance().getMapper();
			jsonData = objectMapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}

		return jsonData;
	}
	
	/**
	 * Validates that every trigger is valid and
	 * that there are no more then one stateless triggers (snap trigger).
	 * If the rule has more then one stateless trigger (snap trigger),
	 * The rule probably won't be satisfied ever.
	 * 
	 * @return	Boolean represents if the list of triggers are valid
	 */
	public boolean validateTriggers(Context context) {
		int statelessTriggerCount = 0;
		for (Trigger trigger : triggers) {
			if (!trigger.isValid(context) || 
				(!(trigger instanceof StateTrigger) && (++statelessTriggerCount >= 2)) ) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validates that every action is valid.
	 * 
	 * @param context
	 * @return	Boolean represents if the list of actions are valid
	 */
	private boolean validateActions(Context context) {
		for (Action action : actions) {
			if (!action.isValid(context)) {
				return false;
			}
		}
		return true;
	}
	
}

/**
 * 
 */
package com.ruley.model.beans.rules;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;

import com.ruley.R;
import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ActivityLog implements Serializable, Comparable<ActivityLog> {

	private static final long serialVersionUID = -1716868776097770563L;
	
	public static final Locale DEFAULT_LOCALE = Locale.US;
	
	public static final SimpleDateFormat SDF_TIMESTAMP = new SimpleDateFormat("MM-dd HH:mm", DEFAULT_LOCALE);
	
	public static final SimpleDateFormat SDF_DATE = new SimpleDateFormat("MMMM, dd", DEFAULT_LOCALE);
	
	public static final SimpleDateFormat SDF_DAY = new SimpleDateFormat("EEEE", DEFAULT_LOCALE);
	
	public static final SimpleDateFormat SDF_TIME = new SimpleDateFormat("HH:mm", DEFAULT_LOCALE);
	
	@JsonProperty("userName")
	private String userName = null;

	@JsonProperty("ruleId")
	private UUID ruleId = null;
	
	@JsonProperty("ruleName")
	private String ruleName= null;
	
	@JsonProperty("action")
	private String action = null;

	@JsonProperty("timestamp")
	private Calendar timestamp;

	public ActivityLog() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UUID getRuleId() {
		return ruleId;
	}

	public void setRuleId(UUID ruleId) {
		this.ruleId = ruleId;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Calendar getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getTimestamp(SimpleDateFormat sdf) {
		return sdf.format(timestamp.getTime());
	}
	
	public String toMessage() {
		Context appCtx = MyApplication.getContext();
		
		String message = appCtx.getString(R.string.log_message);
		return String.format(message, this.action);
	}
	
	public String getElapsedTime() {
		Calendar now = Calendar.getInstance(DEFAULT_LOCALE);
		long timeDelta = now.getTimeInMillis() - timestamp.getTimeInMillis();
		
		long duration = TimeUnit.MILLISECONDS.toDays(timeDelta);
		if (duration > 0) {
			return duration + " days ago";
		}
		duration = TimeUnit.MILLISECONDS.toHours(timeDelta);
		if (duration > 0) {
			return duration + " hours ago";
		}
		duration = TimeUnit.MILLISECONDS.toMinutes(timeDelta);
		if (duration > 0) {
			return duration + " minutes ago";
		}
		duration = TimeUnit.MILLISECONDS.toSeconds(timeDelta);
		if (duration > 0) {
			return duration + " seconds ago";
		}
		return " now";
	}

	@Override
	public int compareTo(ActivityLog another) {
		if (this == another) return 0;
		int compareTo = this.timestamp.compareTo(another.timestamp);
		return -1 * compareTo;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder()
		.append("[").append(getTimestamp(SDF_TIMESTAMP)).append("]")
		.append("[").append(getUserName()).append("]")
		.append("[").append(getRuleName()).append("]")
		.append("[").append(getAction()).append("]");
		
		return sb.toString();
	}

}

/**
 * 
 */
package com.ruley.model.beans.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.parse.ParseUser;
import com.ruley.R;
import com.ruley.activities.dialogs.CheckRuleDialogActivity;
import com.ruley.activities.dialogs.ReportRuleDialogActivity;
import com.ruley.activities.dialogs.ShareRuleDialogActivity;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.ICloneable;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.parse.UserParse;
import com.ruley.services.TextToSpeechService;
import com.ruley.utilities.AnalyticsUtils;
import com.ruley.utilities.PrefsUtils;
import com.ruley.utilities.RulesDependencyGraphUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class Rule implements IRule, Comparable<Rule>, ICloneable<Rule>, Trigger.Listener {
	
	private static final long serialVersionUID = 5124701728697959963L;
	
	private static final String CLASS_NAME = Rule.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(Rule.class.getName());
	
	private static final IActivityLogDAO ACTIVITY_LOG_DAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
	
	public static final int PERFORMANCE_STATE_COUNT_START = 3;
	public static final int PERFORMANCE_STATE_COUNT_END = 0;
	public static final int PERFORMANCE_STATE_CHECK = -1;
	public static final int PERFORMANCE_STATE_SHARE = -2;
	public static final int PERFORMANCE_STATE_REPORT = -3;
	public static final int PERFORMANCE_STATE_SHARED = -4;
	public static final int PERFORMANCE_STATE_WONT_SHARE = -5;
	public static final int PERFORMANCE_STATE_REPORTED = -6;
	public static final int PERFORMANCE_STATE_WONT_REPORT = -7;
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		/**
		 * Invoked when finish rule activation.
		 */
		public void onActivated();
		
		/**
		 * Invoked when finish rule inactivatation.
		 */
		public void onInactivated();
		
		/**
		 * Invoked when activation error occured.
		 */
		public void onError(int errorMessageResourceId);
		
	}
	
	private transient Rule.Listener listener = null;

	@JsonProperty("id")
	private UUID id = null;

	@JsonProperty("name")
	private String name = null;

	@JsonProperty("description")
	private String description = null;
	
	@JsonProperty("active")
	private boolean active = false;

	@JsonProperty("triggers")
	private List<Trigger> triggers = null;

	@JsonProperty("actions")
	private List<Action> actions = null;
	
	@JsonProperty("createdBy")
	private String createdBy = null;
	
	@JsonProperty("createdByName")
	private String createdByName = null;
	
	@JsonProperty("isMine")
	private boolean isMine = true;
	
	@JsonProperty("performanceState")
	private int performanceState = PERFORMANCE_STATE_COUNT_START;
	
	public Rule() {
		super();
		
		this.id = UUID.randomUUID();
		this.triggers = new ArrayList<Trigger>();
		this.actions = new ArrayList<Action>();
		this.createdBy = ParseUser.getCurrentUser().getUsername();
		this.createdByName = ParseUser.getCurrentUser().getString(UserParse.FIELD_NAME);
	}
	
	public Rule(TemplateRule templateRule) {
		super();
		
		this.id = templateRule.getId();
		this.name = templateRule.getName();
		this.description = templateRule.getDescription();
		this.createdBy = templateRule.getCreatedBy();
		this.createdByName = templateRule.getCreatedByName();
		
		List<Trigger> triggers = new ArrayList<Trigger>();
		for (Trigger trigger : templateRule.getTriggers()) {
			triggers.add(trigger.getClone());
		}
		setTriggers(triggers);
		
		List<Action> actions = new ArrayList<Action>();
		for (Action action : templateRule.getActions()) {
			actions.add(action.getClone());
		}
		setActions(actions);
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(Context context, boolean active, Rule.Listener listener) {
		this.listener = listener;
		if (active) {
			boolean hasStronglyConnectedSets = RulesDependencyGraphUtils.hasStronglyConnectedSets(context, Rule.this);
			if (hasStronglyConnectedSets) {
				listener.onError(R.string.rule_activation_error_circular_dependency);
				return;
			}
			for (Trigger trigger : triggers) {
				trigger.setActive(context, true, this);
			}
		} else {
			for (Trigger trigger : triggers) {
				trigger.setActive(context, false, this);
			}
		}
	}
	
	public void reactivate(final Context context, final Rule.Listener listener) {
		setActive(context, false, new Listener() {
			@Override
			public void onInactivated() {
				setActive(context, true, listener);
			}
			@Override
			public void onActivated() {
			}
			@Override
			public void onError(int errorMessageResourceId) {
			}
		});
	}

	public List<Trigger> getTriggers() {
		return triggers;
	}

	public void setTriggers(List<Trigger> triggers) {
		this.triggers = triggers;
	}

	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public boolean isMine() {
		return isMine;
	}

	public void setMine(boolean isMine) {
		this.isMine = isMine;
	}
	
	public boolean isLiked() {
		return UserParse.isLiked(getId());
	}

	public void setLiked(boolean isLiked) {
		if (isLiked) {
			UserParse.addLikedRule(getId());
		} else {
			UserParse.removeLikedRule(getId());
		}
	}
	
	public void toggleLiked() {
		if (isLiked()) {
			setLiked(false);
		} else {
			setLiked(true);
		}
	}
	
	public int getPerformanceState() {
		return performanceState;
	}

	public void setPerformanceState(int performanceState) {
		this.performanceState = performanceState;
	}

	public boolean isSatisfied() {
		for (Trigger trigger : triggers) {
			if (!trigger.isSatisfied()) {
				LOGGER.info("\"" + this.name + "\" is NOT Satisfied");
				return false;
			}
		}

		LOGGER.info("\"" + this.name + "\" is Satisfied");
		return true;
	}

	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		if (active && isSatisfied()) {
			for (Action action : actions) {
				action.perform(context);
			}
			
			if (PrefsUtils.isVocallyAnnounce()) {
				TextToSpeechService.speek(this.name + " preformed");
			}
			
			LOGGER.info("\"" + this.name + "\" performed");
			
			String action = context.getString(R.string.rule_performed);
			ACTIVITY_LOG_DAO.log(this.id, this.name, action);
			
			AnalyticsUtils.sendRulePerformed();
			
			LOGGER.info(CLASS_NAME + "#perform(...) performanceState = " + this.performanceState);
			
			if (this.performanceState > PERFORMANCE_STATE_COUNT_END) {
				--this.performanceState;
			} else {
				switch (this.performanceState) {
				case PERFORMANCE_STATE_COUNT_END:
				case PERFORMANCE_STATE_CHECK: {
					// Launches check rule activity dialog
					Intent intent = new Intent(context, CheckRuleDialogActivity.class);
					intent.putExtra(Config.Extra.EXTRA_RULE, Rule.this);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					this.performanceState = PERFORMANCE_STATE_CHECK;
					break;
				}
				case PERFORMANCE_STATE_SHARE: {
					// Launches share rule dialog activity
					Intent intent = new Intent(context, ShareRuleDialogActivity.class);
					intent.putExtra(Config.Extra.EXTRA_RULE, Rule.this);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					break;
				}
				case PERFORMANCE_STATE_REPORT:
					// Launches report rule dialog activity
					Intent intent = new Intent(context, ReportRuleDialogActivity.class);
					intent.putExtra(Config.Extra.EXTRA_RULE, Rule.this);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					break;
				}
			}
		} else {
			LOGGER.info("\"" + this.name + "\" isn't active or isn't satisfied");
		}
	}
	
	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(name)
				&& !TextUtils.isEmpty(createdBy)
				&& !TextUtils.isEmpty(createdByName)
				&& triggers != null && !triggers.isEmpty()
				&& actions != null && !actions.isEmpty()
				&& validateTriggers(context)
				&& validateActions(context);
	}

	@Override
	public Rule getClone() {
		Rule rule = new Rule();
		rule.setName("Copy of " + getName());
		rule.setDescription(getDescription());
		
		List<Trigger> triggers = new ArrayList<Trigger>();
		for (Trigger trigger : getTriggers()) {
			triggers.add(trigger.getClone());
		}
		rule.setTriggers(triggers);
		
		List<Action> actions = new ArrayList<Action>();
		for (Action action : getActions()) {
			actions.add(action.getClone());
		}
		rule.setActions(actions);
		
		return rule;
	}
	
	public TemplateRule toTemplateRule() {
		return new TemplateRule(Rule.this);
	}
	
	@Override
	public boolean equals(Object another) {
		if (another == null) return false;
	    if (another == this) return true;
	    if (!(another instanceof Rule)) return false;
	    Rule anotherRule = ((Rule)another);
		return this.id.equals(anotherRule.id)
				&& this.createdBy.equals(anotherRule.createdBy);
	}

	@Override
	public int compareTo(Rule another) {
		if (another == null) {
			return 1;
		} else {
			return this.id.compareTo(another.id);
		}
	}

	@Override
	public String toString() {
		String jsonData = null;

		try {
			ObjectMapper objectMapper = JsonHelper.getInstance().getMapper();
			jsonData = objectMapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}

		return jsonData;
	}
	
	public String generateDescription(Context context) {
		StringBuilder sb = new StringBuilder();
		// Describe all the triggers
		String triggersTitle = context.getString(R.string.triggers_title);
		sb.append(triggersTitle).append(" this:\n");
		for (Trigger trigger : getTriggers()) {
			sb.append(" - ").append(trigger.getDescription(context)).append("\n");
		}
		// Describe all the actions
		String actionsTitle = context.getString(R.string.actions_title);
		sb.append(actionsTitle).append(" that:\n");
		for (Action action : getActions()) {
			sb.append(" - ").append(action.getDescription(context)).append("\n");
		}
		return sb.toString();
	}

	@Override
	public void onRegistered() {
		// Validates that all the rule's triggers are activated
		for (Trigger trigger : triggers) {
			if (!trigger.isActive()) {
				return;
			}
		}
		this.active = true;
		
		// Logs rule was enabled
		Context appCtx = MyApplication.getContext();
		String action = appCtx.getString(R.string.rule_enabled);
		ACTIVITY_LOG_DAO.log(this.id, this.name, action);
		
		LOGGER.info("\"" + this.name + "\" activated");
		if (listener != null) {
			listener.onActivated();
		}
	}

	@Override
	public void onUnregistered() {
		// Validates that all the rule's triggers aren't activated
		for (Trigger trigger : triggers) {
			if (trigger.isActive()) {
				return;
			}
		}
		this.active = false;
		
		// Logs rule was disabled
		Context appCtx = MyApplication.getContext();
		String action = appCtx.getString(R.string.rule_disabled);
		ACTIVITY_LOG_DAO.log(this.id, this.name, action);
		
		LOGGER.info("\"" + this.name + "\" inactivated");
		if (listener != null) {
			listener.onInactivated();
		}
	}
	
	@Override
	public void onError(int errorMessageResourceId) {
		Context appCtx = MyApplication.getContext();
		
		LOGGER.warn("\"" + this.name + "\" error");
		LOGGER.warn("Reason: " + appCtx.getString(errorMessageResourceId));
		if (listener != null) {
			listener.onError(errorMessageResourceId);
		}
	}
	
	/**
	 * Validates that every trigger is valid and
	 * that there are no more then one stateless triggers (snap trigger).
	 * If the rule has more then one stateless trigger (snap trigger),
	 * The rule probably won't be satisfied ever.
	 * 
	 * @return	Boolean represents if the list of triggers are valid
	 */
	public boolean validateTriggers(Context context) {
		int statelessTriggerCount = 0;
		for (Trigger trigger : triggers) {
			if (!trigger.isValid(context) || 
				(!(trigger instanceof StateTrigger) && (++statelessTriggerCount >= 2)) ) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validates that every action is valid.
	 * 
	 * @param context
	 * @return	Boolean represents if the list of actions are valid
	 */
	private boolean validateActions(Context context) {
		for (Action action : actions) {
			if (!action.isValid(context)) {
				return false;
			}
		}
		return true;
	}

}

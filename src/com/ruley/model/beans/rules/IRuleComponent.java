/**
 * 
 */
package com.ruley.model.beans.rules;

import java.io.Serializable;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.model.beans.IValidatable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public interface IRuleComponent extends Serializable, IValidatable {

	/**
	 * Gets the rule component's unique id.
	 * 
	 * @return UUID represents the component id
	 */
	public UUID getId();

	/**
	 * Gets the rule component's name resource id.
	 * 
	 * @return Int represents the name resource id
	 */
	public int getNameId();

	/**
	 * Gets the rule component's static description.
	 * 
	 * @return Int represents the static description resource id
	 */
	public int getDescriptionId();

	/**
	 * Gets the rule component's dynamic description.
	 * 
	 * @param context
	 * @return String represents the dynamic description
	 */
	public String getDescription(Context context);

	/**
	 * Gets the rule component's icon drawable.
	 * 
	 * @return Bitmap represents the icon
	 */
	public Bitmap getIcon();

	/**
	 * Gets the rule component's editor activity or derived class of activity.
	 * 
	 * @return Activity or derived class of Activity
	 */
	public Class<? extends Activity> getEditor();

	/**
	 * If the rule component's instance variables valid returns true, Otherwise
	 * returns false.
	 * 
	 * @param context
	 * @return boolean represents if the components is valid
	 */
	@Override
	public boolean isValid(Context context);

	/**
	 * Returns the JSON string represents the object.
	 * 
	 * @return JSON string represents the object
	 */
	@Override
	public String toString();

}

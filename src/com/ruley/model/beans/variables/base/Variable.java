/**
 * 
 */
package com.ruley.model.beans.variables.base;

import java.util.UUID;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.text.TextUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public abstract class Variable implements IVariable {

	private static final long serialVersionUID = 6646900861838594046L;
	
	@JsonProperty("id")
	private UUID id;
	
	@JsonProperty("name")
	private String name = null;

	public Variable() {
		super();
		this.id = UUID.randomUUID();
	}
	
	public Variable(String name) {
		this();
		this.name = name;
	}
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(name);
	}

	@Override
	public String toString() {
		return name;
	}

}

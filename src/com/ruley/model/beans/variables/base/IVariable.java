/**
 * 
 */
package com.ruley.model.beans.variables.base;

import java.io.Serializable;
import java.util.UUID;

import android.content.Context;

import com.ruley.model.beans.IValidatable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public interface IVariable extends Serializable, IValidatable {
	
	/**
	 * Gets the variable's unique id.
	 * 
	 * @return UUID represents the variable id
	 */
	public UUID getId();
	
	/**
	 * Gets the variable's name.
	 * 
	 * @return String represents the variable's name
	 */
	public String getName();
	
	/**
	 * Sets the variable's name.
	 * 
	 * @param name String represents the variable's name
	 */
	public void setName(String name);
	
	/**
	 * If the variable's member variables are valid returns true, Otherwise
	 * returns false.
	 * 
	 * @param context
	 * @return boolean represents if the variable is valid
	 */
	@Override
	public boolean isValid(Context context);
	
	/**
	 * Returns the variable's name.
	 * 
	 * @return String represents the variable name
	 */
	@Override
	public String toString();

}

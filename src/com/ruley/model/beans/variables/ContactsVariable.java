/**
 * 
 */
package com.ruley.model.beans.variables;

import java.util.SortedSet;
import java.util.TreeSet;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;
import com.ruley.utilities.ContactsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ContactsVariable extends Variable {

	private static final long serialVersionUID = 2743005631617198395L;
	
	@JsonProperty("contactsIds")
	private SortedSet<String> contactsIds = new TreeSet<String>(); 

	public ContactsVariable() {
		super(MyApplication.getContext().getString(R.string.variable_contacts_name));
	}

	public SortedSet<String> getContactsIds() {
		return contactsIds;
	}

	public void setContactsIds(SortedSet<String> contactsIds) {
		this.contactsIds = contactsIds;
	}

	@Override
	public boolean isValid(Context context) {
		return super.isValid(context)
				&& contactsIds != null
				&& !contactsIds.isEmpty();
	}
	
	@Override
	public String toString() {
		String variableName = super.toString();
		
		Context appCtx = MyApplication.getContext();
		if (isValid(appCtx)) {
			int contactsCount = contactsIds.size();
			String firstContactId = contactsIds.first();
			String firstContactName = ContactsUtils.getContactName(appCtx, firstContactId);
			variableName = null;
			if (contactsCount == 1) {
				variableName = firstContactName;
			} else if (contactsCount > 1) {
				variableName = firstContactName + " & others";
			}
		}
		
		return variableName;
	}

}

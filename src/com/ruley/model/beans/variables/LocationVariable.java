/**
 * 
 */
package com.ruley.model.beans.variables;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class LocationVariable extends Variable {

	private static final long serialVersionUID = -1648426233335080822L;

	@JsonProperty("address")
	private String address = null;

	@JsonProperty("latitude")
	private double latitude = 0D;

	@JsonProperty("longitude")
	private double longitude = 0D;

	public LocationVariable() {
		super(MyApplication.getContext().getString(R.string.variable_location_name));
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public boolean isValid(Context context) {
		return super.isValid(context) && !TextUtils.isEmpty(address);
	}
	
	@Override
	public String toString() {
		String variableName = super.toString();
		
		if (isValid(MyApplication.getContext())) {
			variableName = address;
		}
		
		return variableName;
	}

}

/**
 * 
 */
package com.ruley.model.beans.variables;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;
import com.ruley.utilities.ContactsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ContactVariable extends Variable {

	private static final long serialVersionUID = 861501445477756286L;

	@JsonProperty("contactId")
	private String contactId = null;
	
	public ContactVariable() {
		super(MyApplication.getContext().getString(R.string.variable_contact_name));
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	@Override
	public boolean isValid(Context context) {
		return super.isValid(context)
				&& !TextUtils.isEmpty(contactId);
	}

	@Override
	public String toString() {
		String variableName = super.toString();
		
		Context appCtx = MyApplication.getContext();
		if (isValid(appCtx)) {
			variableName = ContactsUtils.getContactName(appCtx, contactId);
		}
		
		return variableName;
	}

}

/**
 * 
 */
package com.ruley.model.beans.variables;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;
import com.ruley.utilities.ImageUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class FileVariable extends Variable {

	private static final long serialVersionUID = 3831582471123153036L;

	@JsonProperty("fileUri")
	private String fileUri = null;

	public FileVariable() {
		super(MyApplication.getContext().getString(R.string.variable_file_name));
	}

	public String getFileUri() {
		return fileUri;
	}

	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}

	@Override
	public boolean isValid(Context context) {
		return super.isValid(context)
				&& !TextUtils.isEmpty(fileUri);
	}
	
	@Override
	public String toString() {
		String variableName = super.toString();
		
		if (isValid(MyApplication.getContext())) {
			Uri bitmapUri = Uri.parse(fileUri);
			variableName = ImageUtils.getBitmapFileName(bitmapUri);
		}
		
		return variableName;
	}

}

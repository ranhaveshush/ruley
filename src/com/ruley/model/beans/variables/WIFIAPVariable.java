/**
 * 
 */
package com.ruley.model.beans.variables;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class WIFIAPVariable extends Variable {

	private static final long serialVersionUID = -6553678002722656362L;
	
	@JsonProperty("networkSSID")
	private String networkSSID = null;
	
	@JsonProperty("networkPassword")
	private String networkPassword = null;
	
	@JsonProperty("isNetworkSSIDVisible")
	private boolean isNetworkSSIDVisible = true;

	public WIFIAPVariable() {
		super(MyApplication.getContext().getString(R.string.variable_wifi_ap_name));
	}

	public String getNetworkSSID() {
		return networkSSID;
	}

	public void setNetworkSSID(String networkSSID) {
		this.networkSSID = networkSSID;
	}

	public String getNetworkPassword() {
		return networkPassword;
	}

	public void setNetworkPassword(String networkPassword) {
		this.networkPassword = networkPassword;
	}

	public boolean isNetworkSSIDVisible() {
		return isNetworkSSIDVisible;
	}

	public void setNetworkSSIDVisible(boolean isNetworkSSIDVisible) {
		this.isNetworkSSIDVisible = isNetworkSSIDVisible;
	}

	@Override
	public boolean isValid(Context context) {
		return super.isValid(context)
				&& !TextUtils.isEmpty(networkSSID)
				&& !TextUtils.isEmpty(networkPassword);
	}

	@Override
	public String toString() {
		String variableName = super.toString();
		
		if (isValid(MyApplication.getContext())) {
			variableName = networkSSID;
		}
		
		return variableName;
	}

}

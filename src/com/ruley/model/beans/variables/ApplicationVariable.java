/**
 * 
 */
package com.ruley.model.beans.variables;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ApplicationVariable extends Variable {

	private static final long serialVersionUID = 8324011262507059974L;
	
	@JsonProperty("appName")
	private String appName = null;

	@JsonProperty("appPackage")
	private String appPackage = null;

	public ApplicationVariable() {
		super(MyApplication.getContext().getString(R.string.variable_application_name));
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppPackage() {
		return appPackage;
	}

	public void setAppPackage(String appPackage) {
		this.appPackage = appPackage;
	}
	
	@Override
	public boolean isValid(Context context) {
		if (super.isValid(context)
			&& !TextUtils.isEmpty(appPackage)
			&& !TextUtils.isEmpty(appName)) {			
			PackageManager packageManager = context.getPackageManager();
			Intent intent = packageManager.getLaunchIntentForPackage(appPackage);
			return intent != null;
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		String variableName = super.toString();
		
		if (isValid(MyApplication.getContext())) {
			variableName = appName;
		}
		
		return variableName;
	}

}

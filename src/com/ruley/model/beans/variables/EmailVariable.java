package com.ruley.model.beans.variables;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.variables.base.Variable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class EmailVariable extends Variable {

	private static final long serialVersionUID = -7951224799190308743L;
	
	@JsonProperty("fromEmailAddress")
	private String fromEmailAddress = null;
	
	@JsonProperty("toEmailAddresses")
	private List<String> toEmailAddresses = null;
	
	@JsonProperty("subject")
	private String subject = null;
	
	@JsonProperty("text")
	private String text = null;
	
	@JsonProperty("authToken")
	private String authToken = null;

	public EmailVariable() {
		super(MyApplication.getContext().getString(R.string.variable_email_name));
	}

	public String getFromEmailAddress() {
		return fromEmailAddress;
	}

	public void setFromEmailAddress(String fromEmailAddress) {
		this.fromEmailAddress = fromEmailAddress;
	}

	public List<String> getToEmailAddresses() {
		return toEmailAddresses;
	}

	public void setToEmailAddresses(List<String> toEmailAddresses) {
		this.toEmailAddresses = toEmailAddresses;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	@Override
	public boolean isValid(Context context) {
		if (TextUtils.isEmpty(fromEmailAddress)
				|| toEmailAddresses == null
				|| toEmailAddresses.isEmpty()
				|| TextUtils.isEmpty(subject)) {
			return false;
		}
		
		return super.isValid(context)
				&& isEmailAddressValid(fromEmailAddress)
				&& isEmailAddressesValid(toEmailAddresses);
	}
	
	public boolean isEmailAddressValid(String email) {
		if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
			return false;
		}
		
		return true;
	}

	public boolean isEmailAddressesValid(List<String> emails) {
		// Validates to email addresses
		for (String email : emails) {
			if (!isEmailAddressValid(email)) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public String toString() {
		String variableName = super.toString();
		
		if (isValid(MyApplication.getContext())) {
			variableName = subject;
		}
		
		return variableName;
	}
	
}

/**
 * 
 */
package com.ruley.model.beans.users;

import com.ruley.model.beans.users.base.User;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DefaultUser extends User {
	
	private static final long serialVersionUID = 5555330456215664960L;
	
	public DefaultUser() {
		super();
	}

}

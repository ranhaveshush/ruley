/**
 * 
 */
package com.ruley.model.beans.users.base;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.connection.file.json.JsonHelper;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public abstract class User implements Comparable<User>, Serializable {
	
	private static final long serialVersionUID = 3856384926179520812L;

	private static final Logger LOGGER = LoggerFactory.getLogger(User.class.getName());

	@JsonProperty("id")
	private UUID id;
	
	@JsonProperty("rules")
	private List<Rule> rules = null;
	
	public User() {
		super();
		this.id = UUID.randomUUID();
		this.rules = new ArrayList<Rule>();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public List<Rule> getRules() {
		return rules;
	}

	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}
	
	public List<Rule> getActiveRules() {
		List<Rule> activeRules = new ArrayList<Rule>();
		
		for (Rule rule : rules) {
			if (rule.isActive()) {
				activeRules.add(rule);
			}
		}
		
		return activeRules;
	}
	
	@Override
	public boolean equals(Object another) {
		if (another == null) return false;
	    if (another == this) return true;
	    if (!(another instanceof User)) return false;
	    User anotherBaseUser = ((User)another);
		return this.getId().equals(anotherBaseUser.getId());
	}

	@Override
	public int compareTo(User another) {
		if (another == null) {
			return 1;
		} else {
			return this.getId().compareTo(another.getId());
		}
	}

	@Override
	public String toString() {
		String jsonData = null;
		
		try {
			ObjectMapper objectMapper = JsonHelper.getInstance().getMapper();
			jsonData = objectMapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			 LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}
		
		return jsonData;
	}

}

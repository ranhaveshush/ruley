/**
 * 
 */
package com.ruley.model.beans.users;

import com.ruley.model.beans.users.base.User;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AdminUser extends User {

	private static final long serialVersionUID = 6299352013027954944L;

	public AdminUser() {
		super();
	}
}

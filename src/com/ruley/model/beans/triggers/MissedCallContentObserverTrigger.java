/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.provider.CallLog.Calls;

import com.ruley.R;
import com.ruley.activities.editors.triggers.MissedCallTriggerEditorActivity;
import com.ruley.config.Config;
import com.ruley.contentobservers.MissedCallContentObserver;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.ContentObserverTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.services.ContentObserversService;
import com.ruley.utilities.ImageUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class MissedCallContentObserverTrigger extends ContentObserverTrigger {
	
	private static final long serialVersionUID = -6291560553179675123L;

	private static final String CLASS_NAME = MissedCallContentObserverTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(MissedCallContentObserverTrigger.class.getName());
	
	public static final String ACTION_MISSED_CALL = Config.APP_PACKAGE + ".ACTION_MISSED_CALL";
	
	@Override
	public Class<? extends ContentObserver> getContentObserverClass() {
		return MissedCallContentObserver.class;
	}

	@Override
	public void registerContentObservers(Context context) {
		ContentObserversService.registerContentObserver(Calls.CONTENT_URI,
				false, getContentObserverClass().getSimpleName());
	}

	@Override
	public void unregisterContentObservers(Context context) {
		ContentObserversService.unregisterContentObserver(
				getContentObserverClass().getSimpleName());
	}

	@Override
	public int getNameId() {
		return R.string.trigger_missed_call_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.trigger_missed_call_static_description;
	}

	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.trigger_missed_call_dynamic_description);
	}
	
	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.phone_ringing);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return MissedCallTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Trigger getClone() {
		MissedCallContentObserverTrigger trigger = new MissedCallContentObserverTrigger();
		return trigger;
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(MissedCallContentObserverTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(true);
				triggerDAO.store(trigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(MissedCallContentObserverTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(false);
				triggerDAO.store(trigger);
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

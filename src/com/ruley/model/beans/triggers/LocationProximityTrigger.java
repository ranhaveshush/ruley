/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.ruley.R;
import com.ruley.activities.editors.triggers.LocationProximityTriggerEditorActivity;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.variables.LocationVariable;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.LocationClientUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class LocationProximityTrigger extends Trigger implements LocationClientUtils.Listener {

	private static final long serialVersionUID = -1727097297078221962L;
	
	private static final String CLASS_NAME = LocationProximityTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(LocationProximityTrigger.class.getName());
	
	public static final String ACTION_LOCATION_PROXIMITY = Config.APP_PACKAGE + ".ACTION_LOCATION_PROXIMITY";
	
	public static final String RADIUS_UNITS = "m";
	
	public static final float RADIUS_MIN = 150F;
	
	public static final float RADIUS_MAX = 1000F;

	public static final int TRANSITION_ENTER = Geofence.GEOFENCE_TRANSITION_ENTER;
	
	public static final int TRANSITION_EXIT = Geofence.GEOFENCE_TRANSITION_EXIT;
	
	@JsonProperty("location")
	private LocationVariable location = new LocationVariable();

	@JsonProperty("radius")
	private float radius = RADIUS_MIN;

	@JsonProperty("expirationDuration")
	private long expirationDuration = Geofence.NEVER_EXPIRE;
	
	@JsonProperty("transitionType")
	private int transitionType = TRANSITION_ENTER;

	public LocationProximityTrigger() {
		super();
	}

	public LocationVariable getLocation() {
		return location;
	}

	public void setLocation(LocationVariable location) {
		this.location = location;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		if (RADIUS_MIN <= radius && radius <= RADIUS_MAX) {
			this.radius = radius;
		}
	}

	public long getExpirationDuration() {
		return expirationDuration;
	}

	public void setExpirationDuration(long expirationDuration) {
		this.expirationDuration = expirationDuration;
	}

	public int getTransitionType() {
		return transitionType;
	}

	public void setTransitionType(int transitionType) {
		this.transitionType = transitionType;
	}
	
	public Geofence toGeofence() {
		LOGGER.debug(CLASS_NAME + "#toGeofence(...)");
		
		LocationVariable location = getLocation();
		
		return new Geofence.Builder()
				.setRequestId(getId().toString()) // The geofence request id is the trigger's UUID
				.setCircularRegion(location.getLatitude(), location.getLongitude(), getRadius())
				.setExpirationDuration(getExpirationDuration())
				.setTransitionTypes(getTransitionType())
				.build();
	}
	
	@Override
	protected void register(Context context) {
		LOGGER.debug(CLASS_NAME + "#register(...)");

		Intent intent = new Intent(ACTION_LOCATION_PROXIMITY);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.LOCATION_PROXIMITY_REQUEST_CODE,
				intent, PendingIntent.FLAG_NO_CREATE);
		// If such pending intent doesn't exists, create one
		if (pendingIntent == null) {
			LOGGER.info("Creating new pending intent for " + CLASS_NAME + "s");
			pendingIntent = PendingIntent.getBroadcast(context,
					Config.PendingIntent.LOCATION_PROXIMITY_REQUEST_CODE,
					intent, PendingIntent.FLAG_UPDATE_CURRENT);
		// If such pending intent does exists, use it
		} else {
			LOGGER.info("Using exsiting pending intent for " + CLASS_NAME + "s");
		}
		
		new LocationClientUtils().addGeofences(context, new ArrayList<Geofence>() {
			{
				add(toGeofence());
			}
		}, pendingIntent, this);
	}
	
	@Override
	protected void unregister(Context context) {
		LOGGER.debug(CLASS_NAME + "#unregister(...)");
		
		Context appCtx = MyApplication.getContext();
		
		// If the "Location Services" disabled,
		// the disable event dumps all pending intents so,
		// just unregister the trigger
		boolean locationServiceEnabled = LocationClientUtils.isLocationServiceEnabled(context);
		if (!locationServiceEnabled) {
			LOGGER.warn("None " + CLASS_NAME + "s pending intent exists, can't cancel it");
			super.unregister(appCtx);
			return;
		}
		
		Intent intent = new Intent(ACTION_LOCATION_PROXIMITY);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.LOCATION_PROXIMITY_REQUEST_CODE,
				intent, PendingIntent.FLAG_NO_CREATE);
		// If such pending intent doesn't exists, can't cancel it
		if (pendingIntent == null) {
			LOGGER.warn("None " + CLASS_NAME + "s pending intent exists, can't cancel it");
			super.unregister(appCtx);
			return;
		}
		
		ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		List<Trigger> triggers = triggerDAO.list(LocationProximityTrigger.class, true);
		
		// If there are no triggers, but there is pending intent, warn (wrong buggy state)
		if (triggers == null || triggers.isEmpty()) {
			LOGGER.warn("There are not " + CLASS_NAME + "s but there is pending intent (wrong buggy state)");
			super.unregister(appCtx);
		// If the last trigger needs to be unregistered, cancel the pending intent
		} else if (triggers.size() == 1) {
			LOGGER.info("Canceling " + CLASS_NAME + "s pending intent");
			pendingIntent = PendingIntent.getBroadcast(context,
					Config.PendingIntent.LOCATION_PROXIMITY_REQUEST_CODE, intent,
					PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT);
			// TODO: check if i can use a canceled pending intent
			new LocationClientUtils().removeGeofences(context, pendingIntent, this);
		// If there are other triggers, keep the pending intent
		} else {
			LOGGER.info("Other " + CLASS_NAME + "s registered, so keeping the pending intent");
			super.unregister(appCtx);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_location_proximity_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_location_proximity_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (location == null) {
			return context.getString(R.string.trigger_location_proximity_location_invalid);
		} else {
			String description = context.getString(R.string.trigger_location_proximity_dynamic_description);
			String transitionTypeStr = null;
			switch (transitionType) {
			case TRANSITION_ENTER:
				transitionTypeStr = context.getString(R.string.trigger_location_proximity_entering);
				break;
			case TRANSITION_EXIT:
				transitionTypeStr = context.getString(R.string.trigger_location_proximity_exiting);
				break;
			default:
				break;
			}
			return String.format(description, transitionTypeStr, location);
		}
	}

	@Override
	public Bitmap getIcon() {
		switch (transitionType) {
		case TRANSITION_ENTER:
			return ImageUtils.getBitmap(R.drawable.location_enter);
		case TRANSITION_EXIT:
			return ImageUtils.getBitmap(R.drawable.location_exit);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return LocationProximityTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return location != null
				&& location.isValid(context)
				&& (radius > 0)
				&& (expirationDuration == Geofence.NEVER_EXPIRE)
				&& (transitionType == TRANSITION_ENTER 
						|| transitionType == TRANSITION_EXIT);				
	}
	
	public Trigger getClone() {
		LocationProximityTrigger trigger = new LocationProximityTrigger();
		trigger.setLocation(location);
		trigger.setTransitionType(getTransitionType());
		trigger.setRadius(getRadius());
		trigger.setExpirationDuration(getExpirationDuration());
		return trigger;
	}

	@Override
	public Trigger toTemplate() {
		LocationProximityTrigger trigger = new LocationProximityTrigger();
		trigger.setLocation(new LocationVariable());
		trigger.setTransitionType(getTransitionType());
		trigger.setRadius(getRadius());
		trigger.setExpirationDuration(getExpirationDuration());
		return trigger;
	}

	@Override
	public void onKnownLastLocation(Location location) {
		// Do nothing, not implemented
	}

	@Override
	public void onAddedGeofences() {
		super.register(MyApplication.getContext());
	}

	@Override
	public void onRemovedGeofencesByPendingIntent() {
		super.unregister(MyApplication.getContext());
	}
	
	@Override
	public void onError(int errorMessageResourceId) {
		listener.onError(errorMessageResourceId);
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			int transitionType = LocationClient.getGeofenceTransition(intent);
			List<Geofence> triggeringGeofences = LocationClient.getTriggeringGeofences(intent);
			
			// Extracts the triggering triggers ids from the geofences requests ids
			final List<String> triggeringTriggersIds = new ArrayList<String>();
			for (Geofence geofence : triggeringGeofences) {
				triggeringTriggersIds.add(geofence.getRequestId());
			}
			
			switch (transitionType) {
			case TRANSITION_ENTER:
				LOGGER.info("TRANSITION_ENTER");
				break;
			case TRANSITION_EXIT:
				LOGGER.info("TRANSITION_EXIT");
				break;
			case TRANSITION_ENTER | TRANSITION_EXIT:
				LOGGER.warn("Geofence transition enter and exit (both) not supported");
				return;
			default:
				LOGGER.error("Geofence transition type error: " + transitionType);
				return;
			}
			
			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(LocationProximityTrigger.class, true);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				// Filters the triggering triggers
				if (!triggeringTriggersIds.contains(trigger.getId().toString())) {
					continue;
				}
				
				trigger.setSatisfied(true);
				triggerDAO.store(trigger);

				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				List<Rule> rules = ruleDAO.list(LocationProximityTrigger.class, true, true);
				
				if (rules != null && !rules.isEmpty()) {
					for (Rule rule : rules) {
						rule.perform(appCtx);
					}
				}
				
				trigger.setSatisfied(false);
				triggerDAO.store(trigger);
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

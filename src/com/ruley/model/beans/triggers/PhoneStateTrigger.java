/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.telephony.TelephonyManager;

import com.ruley.R;
import com.ruley.activities.editors.triggers.PhoneStateTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast actions:
 * "android.intent.action.PHONE_STATE"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class PhoneStateTrigger extends StateTrigger {

	private static final long serialVersionUID = 1634508893484626957L;
	
	private static final String CLASS_NAME = PhoneStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(PhoneStateTrigger.class.getName());
	
	public static final String ACTION_PHONE_STATE = "android.intent.action.PHONE_STATE";
	
	public static final int PHONE_STATE_UNKNOWN = -1;

	private static int SAVED_PHONE_STATE = PHONE_STATE_UNKNOWN; 

	private static final Map<String, Integer> PHONE_STATE = Collections.unmodifiableMap(new HashMap<String, Integer>() {
		{
			put("IDLE", TelephonyManager.CALL_STATE_IDLE);
			put("OFFHOOK", TelephonyManager.CALL_STATE_OFFHOOK);
			put("RINGING", TelephonyManager.CALL_STATE_RINGING);
		}
	});

	/**
	 * Holds the phone wanted state:
	 * 
	 * <p>
	 * TelephonyManager.CALL_STATE_IDLE<br/>
	 * TelephonyManager.CALL_STATE_OFFHOOK<br/>
	 * TelephonyManager.CALL_STATE_RINGING
	 * </p>
	 * 
	 * @see android.telephony.TelephonyManager
	 */
	@JsonProperty("wantedPhoneState")
	private int wantedPhoneState = TelephonyManager.CALL_STATE_IDLE;

	public int getWantedPhoneState() {
		return wantedPhoneState;
	}

	public void setWantedPhoneState(int wantedPhoneState) {
		this.wantedPhoneState = wantedPhoneState;
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_phone_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_phone_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String phoneState = null;
		switch (wantedPhoneState) {
		case TelephonyManager.CALL_STATE_IDLE:
			phoneState = context.getString(R.string.trigger_phone_state_idle);
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			phoneState = context.getString(R.string.trigger_phone_state_off_hook);
			break;
		case TelephonyManager.CALL_STATE_RINGING:
			phoneState = context.getString(R.string.trigger_phone_state_ringing);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.trigger_phone_state_dynamic_description);
		return String.format(description, phoneState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedPhoneState) {
		case TelephonyManager.CALL_STATE_IDLE:
			return ImageUtils.getBitmap(R.drawable.phone_idle);
		case TelephonyManager.CALL_STATE_OFFHOOK:
			return ImageUtils.getBitmap(R.drawable.phone_off_hook);
		case TelephonyManager.CALL_STATE_RINGING:
			return ImageUtils.getBitmap(R.drawable.phone_ringing);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return PhoneStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedPhoneState) {
		case TelephonyManager.CALL_STATE_IDLE:
		case TelephonyManager.CALL_STATE_OFFHOOK:
		case TelephonyManager.CALL_STATE_RINGING:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Trigger getClone() {
		PhoneStateTrigger trigger = new PhoneStateTrigger();
		trigger.setWantedPhoneState(getWantedPhoneState());
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		
		if (telephonyManager == null) {
			return;
		}
		
		int callState = telephonyManager.getCallState();
		if (wantedPhoneState == callState) {
			setSatisfied(true);
		} else {
			setSatisfied(false);
		}
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			String phoneStateKey = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

			int currPhoneState = PHONE_STATE.get(phoneStateKey);

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(PhoneStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			// If the phone state unknown, do nothing
			if (PHONE_STATE_UNKNOWN == currPhoneState) {
				LOGGER.info(CLASS_NAME + " The phone state unknown");
				return;
			// If the phone state didn't change, do nothing
			} else if (SAVED_PHONE_STATE == currPhoneState) {
				LOGGER.info(CLASS_NAME + " The phone state didn't changed");
				return;
			// If the phone state did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The phone state did changed");
				SAVED_PHONE_STATE = currPhoneState;
			}

			for (Trigger trigger : triggers) {
				PhoneStateTrigger phoneStateTrigger = (PhoneStateTrigger) trigger;
				if (phoneStateTrigger.getWantedPhoneState() == currPhoneState) {
					phoneStateTrigger.setSatisfied(true);
				} else {
					phoneStateTrigger.setSatisfied(false);
				}
				triggerDAO.store(phoneStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(PhoneStateTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

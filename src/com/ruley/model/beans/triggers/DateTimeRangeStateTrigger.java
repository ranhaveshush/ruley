/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.triggers.DateTimeRangeStateTriggerEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DateTimeRangeStateTrigger extends StateTrigger {

	private static final long serialVersionUID = 5640804990813210612L;
	
	private static final String CLASS_NAME = DateTimeRangeStateTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateTimeRangeStateTrigger.class.getName());
	
	public static final String ACTION_DATE_TIME_RANGE_START = Config.APP_PACKAGE + ".ACTION_DATE_TIME_RANGE_START";
	public static final String ACTION_DATE_TIME_RANGE_END = Config.APP_PACKAGE + ".ACTION_DATE_TIME_RANGE_END";
	
	public static final Locale LOCALE = Locale.getDefault();
	
	public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yy", LOCALE);
	public static final SimpleDateFormat SIMPLE_TIME_FORMAT = new SimpleDateFormat("HH:mm", LOCALE);
	
	public static final int TRESHHOLD_IN_MILLIS = 5 * 1000;
	
	public static final long INTERVAL_WEEK = AlarmManager.INTERVAL_DAY * 7;
	
	@JsonProperty("dateTimeStart")
	private GregorianCalendar dateTimeStart = new GregorianCalendar(LOCALE);
	
	@JsonProperty("dateTimeEnd")
	private GregorianCalendar dateTimeEnd = new GregorianCalendar(LOCALE);
	
	@JsonProperty("isRepeat")
	private boolean isRepeat = false;
	
	@JsonProperty("daysOfTheWeek")
	private boolean[] daysOfTheWeek = new boolean[7];

	public GregorianCalendar getDateTimeStart() {
		return dateTimeStart;
	}

	public void setDateTimeStart(GregorianCalendar dateTimeStart) {
		this.dateTimeStart = dateTimeStart;
	}

	public GregorianCalendar getDateTimeEnd() {
		return dateTimeEnd;
	}

	public void setDateTimeEnd(GregorianCalendar dateTimeEnd) {
		this.dateTimeEnd = dateTimeEnd;
	}
	
	public boolean isRepeat() {
		return isRepeat;
	}

	public void setRepeat(boolean isRepeat) {
		this.isRepeat = isRepeat;
	}
	
	public boolean[] getDaysOfTheWeek() {
		return daysOfTheWeek;
	}
	
	public boolean isRepeatDayOfTheWeek(int index) {
		if (0 <= index && index <= 7) {
			return daysOfTheWeek[index];
		} else {
			return false;
		}
	}
	
	public void setRepeatDayOfTheWeek(int index, boolean isRepeat) {
		if (0 <= index && index <= 7) {
			daysOfTheWeek[index] = isRepeat;
		}
	}

	@Override
	protected void register(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager == null) {
			return;
		}
		
		if (isRepeat) {
			registerRepeatAlarm(context, alarmManager,
					ACTION_DATE_TIME_RANGE_START, dateTimeStart,
					ACTION_DATE_TIME_RANGE_END, dateTimeEnd);
		} else {
			registerAlarm(context, alarmManager,
					ACTION_DATE_TIME_RANGE_START, dateTimeStart,
					ACTION_DATE_TIME_RANGE_END, dateTimeEnd);
		}
	}

	@Override
	protected void unregister(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager == null) {
			return;
		}
		
		if (isRepeat) {
			unregisterRepeatAlarm(context, alarmManager, ACTION_DATE_TIME_RANGE_START);
			unregisterRepeatAlarm(context, alarmManager, ACTION_DATE_TIME_RANGE_END);
		} else {
			unregisterAlarm(context, alarmManager, ACTION_DATE_TIME_RANGE_START);
			unregisterAlarm(context, alarmManager, ACTION_DATE_TIME_RANGE_END);			
		}
		
		super.unregister(context);
	}

	@Override
	public int getNameId() {
		return R.string.trigger_date_time_range_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.trigger_date_time_range_static_description;
	}

	@Override
	public String getDescription(Context context) {
		if (isRepeat) {
			String selectedDaysOfTheWeekStr = getSelectedDaysOfTheWeekString();
			if (selectedDaysOfTheWeekStr != null) {
				String timeStartStr = SIMPLE_TIME_FORMAT.format(dateTimeStart.getTime());
				String timeEndStr = SIMPLE_TIME_FORMAT.format(dateTimeEnd.getTime());
				String description = context.getString(R.string.trigger_date_time_range_repeat_dynamic_description);
				return String.format(description, selectedDaysOfTheWeekStr, timeStartStr, timeEndStr);
			} else {
				return context.getString(R.string.trigger_date_time_repeat_days_missing);
			}
		} else {
			String description = context.getString(R.string.trigger_date_time_range_once_dynamic_description);
			String timeStartStr = SIMPLE_TIME_FORMAT.format(dateTimeStart.getTime());
			String dateStartStr = SIMPLE_DATE_FORMAT.format(dateTimeStart.getTime());
			String timeEndStr = SIMPLE_TIME_FORMAT.format(dateTimeEnd.getTime());
			String dateEndStr = SIMPLE_DATE_FORMAT.format(dateTimeEnd.getTime());
			String dateTimeStartStr = timeStartStr + ", " + dateStartStr;
			String dateTimeEndStr = timeEndStr + ", " + dateEndStr;
			return String.format(description, dateTimeStartStr, dateTimeEndStr);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.date_time_range);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return DateTimeRangeStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		boolean valid = false;
		
		if (isRepeat) {
			for (boolean dayOfTheWeek : daysOfTheWeek) {
				if (dayOfTheWeek) {
					valid = true;
				}
			}
		} else {
			Calendar now = GregorianCalendar.getInstance(LOCALE);
			valid = this.dateTimeStart.before(this.dateTimeEnd) && now.before(dateTimeEnd);
		}
		
		return valid;
	}
	
	@Override
	public Trigger getClone() {
		DateTimeRangeStateTrigger trigger = new DateTimeRangeStateTrigger();
		trigger.setDateTimeStart(getDateTimeStart());
		trigger.setDateTimeEnd(getDateTimeEnd());
		trigger.setRepeat(isRepeat());
		boolean[] repeatDaysOfTheWeek = getDaysOfTheWeek();
		for (int i=0; i<repeatDaysOfTheWeek.length; ++i) {
			trigger.setRepeatDayOfTheWeek(i, repeatDaysOfTheWeek[i]);
		}
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		// Do nothing
	}
	
	@SuppressLint("NewApi")
	private void registerAlarm(
			Context context, AlarmManager alarmManager,
			String startAction, Calendar dateTimeStart,
			String endAction, Calendar dateTimeEnd) {
		
		Calendar nowCal = Calendar.getInstance(LOCALE);
		nowCal.add(Calendar.MILLISECOND, TRESHHOLD_IN_MILLIS);
		
		PendingIntent startAlarmPendingIntent = getAlarmPendingIntent(context, startAction,
				PendingIntent.FLAG_UPDATE_CURRENT, 0);
		PendingIntent endAlarmPendingIntent = getAlarmPendingIntent(context, endAction,
				PendingIntent.FLAG_UPDATE_CURRENT, 0);
		
		// If now is before start or between start and end,
		// Sets the alarm to start from this week
		if (nowCal.before(dateTimeStart)
			|| (dateTimeStart.before(nowCal) && nowCal.before(dateTimeEnd))) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, dateTimeStart.getTimeInMillis(), startAlarmPendingIntent);
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, dateTimeEnd.getTimeInMillis(), endAlarmPendingIntent);
			} else {
				alarmManager.set(AlarmManager.RTC_WAKEUP, dateTimeStart.getTimeInMillis(), startAlarmPendingIntent);
				alarmManager.set(AlarmManager.RTC_WAKEUP, dateTimeEnd.getTimeInMillis(), endAlarmPendingIntent);
			}
			
			super.register(context);
		} else {
			listener.onError(R.string.trigger_activation_error_time_elapsed);
		}
	}
	
	private void registerRepeatAlarm(
			Context context, AlarmManager alarmManager,
			String startAction, Calendar dateTimeStart,
			String endAction, Calendar dateTimeEnd) {
		
		Calendar nowCal = Calendar.getInstance(LOCALE);
		nowCal.add(Calendar.MILLISECOND, TRESHHOLD_IN_MILLIS);
		Calendar startCal = null;
		Calendar endCal = null;
		boolean isOverNight = false;
		
		// Normalize the start date time to today
		startCal = Calendar.getInstance(LOCALE);
		startCal.set(Calendar.HOUR_OF_DAY, dateTimeStart.get(Calendar.HOUR_OF_DAY));
		startCal.set(Calendar.MINUTE, dateTimeStart.get(Calendar.MINUTE));
		startCal.set(Calendar.SECOND, 0);
		startCal.set(Calendar.MILLISECOND, 0);
		// Normalize the end date time to today
		endCal = Calendar.getInstance(LOCALE);
		endCal.set(Calendar.HOUR_OF_DAY, dateTimeEnd.get(Calendar.HOUR_OF_DAY));
		endCal.set(Calendar.MINUTE, dateTimeEnd.get(Calendar.MINUTE));
		endCal.set(Calendar.SECOND, 0);
		endCal.set(Calendar.MILLISECOND, 0);
		// If end is before start, increment it's day by one
		if (endCal.before(startCal)) {
			endCal.add(Calendar.DATE, 1);
			isOverNight = true;
		}
		
		for (int i=0; i < daysOfTheWeek.length; ++i) {
			if (daysOfTheWeek[i]) {
				int dayOfTheWeek = i + 1;
				
				PendingIntent startAlarmPendingIntent = getAlarmPendingIntent(context, startAction,
						PendingIntent.FLAG_UPDATE_CURRENT, dayOfTheWeek);
				PendingIntent endAlarmPendingIntent = getAlarmPendingIntent(context, endAction,
						PendingIntent.FLAG_UPDATE_CURRENT, dayOfTheWeek);
				
				// Sets the calendar's day field to the selected day of the week
				startCal.set(Calendar.DAY_OF_WEEK, dayOfTheWeek);
				endCal.set(Calendar.DAY_OF_WEEK, dayOfTheWeek);
				if (isOverNight) {
					endCal.add(Calendar.DATE, 1);
				}
				
				// If now is before start or between start and end,
				// Sets the alarm to start from this week
				if (nowCal.before(startCal)
					|| (startCal.before(nowCal) && nowCal.before(endCal))) {
					alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startCal.getTimeInMillis(), INTERVAL_WEEK, startAlarmPendingIntent);
					alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, endCal.getTimeInMillis(), INTERVAL_WEEK, endAlarmPendingIntent);
				// If now after end, sets the alarm to start from next week
				} else {
					alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startCal.getTimeInMillis() + INTERVAL_WEEK, INTERVAL_WEEK, startAlarmPendingIntent);
					alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, endCal.getTimeInMillis() + INTERVAL_WEEK, INTERVAL_WEEK, endAlarmPendingIntent);
				}
			}
		}
		
		super.register(context);
	}
	
	private void unregisterAlarm(Context context, AlarmManager alarmManager, String action) {
		PendingIntent alarmPendingIntent = getAlarmPendingIntent(context, action,
				PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT, 0);
		if (alarmPendingIntent != null) {
			alarmManager.cancel(alarmPendingIntent);
		}
	}
	
	private void unregisterRepeatAlarm(Context context, AlarmManager alarmManager, String action) {
		for (int i=0; i < daysOfTheWeek.length; ++i) {
			if (daysOfTheWeek[i]) {
				PendingIntent alarmPendingIntent = getAlarmPendingIntent(context, action,
						PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT, i+1);
				if (alarmPendingIntent != null) {
					alarmManager.cancel(alarmPendingIntent);
				}
			}
		}
	}
	
	private String getSelectedDaysOfTheWeekString() {
		String[] shortWeekdays = DateFormatSymbols.getInstance(LOCALE).getShortWeekdays();
		String selectedDaysOfTheWeekStr = "";
		for (int i=0; i < daysOfTheWeek.length; ++i) {
			if (daysOfTheWeek[i]) {
				selectedDaysOfTheWeekStr += shortWeekdays[i+1] + ", ";
			}
		}
		if (!TextUtils.isEmpty(selectedDaysOfTheWeekStr)) {
			return selectedDaysOfTheWeekStr.substring(0, selectedDaysOfTheWeekStr.length() - 2);
		} else {
			return null;
		}
	}
	
	private PendingIntent getAlarmPendingIntent(Context context, String action, int flags, int dayOfTheWeek) {
		Intent alarmIntent = new Intent(action);
		alarmIntent.putExtra(Config.Extra.EXTRA_TRIGGER_ID, getId());
		PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context,
				(int) (this.getId().getLeastSignificantBits()) + dayOfTheWeek, alarmIntent, flags);
		return alarmPendingIntent;
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context ctxApp, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			UUID triggerId = (UUID) intent.getSerializableExtra(Config.Extra.EXTRA_TRIGGER_ID);
			
			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			Trigger trigger = triggerDAO.get(triggerId);
			
			if (trigger == null || !trigger.isActive()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			String action = (String) intent.getAction();
			if (ACTION_DATE_TIME_RANGE_START.equals(action)) {
				trigger.setSatisfied(true);
				triggerDAO.store(trigger);
				
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				Rule rule = ruleDAO.get(triggerId, true, true);
				
				if (rule != null) {
					rule.perform(ctxApp);
				}
			} else if (ACTION_DATE_TIME_RANGE_END.equals(action)) {
				trigger.setSatisfied(false);
				triggerDAO.store(trigger);
			} else {
				LOGGER.warn(CLASS_NAME + " invalid action: " + action);
				return;
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

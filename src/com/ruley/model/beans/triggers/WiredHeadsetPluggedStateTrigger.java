/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.triggers.WiredHeadsetPluggedTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IDeviceStateDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast actions:
 * "android.intent.action.HEADSET_PLUG"
 * 
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public class WiredHeadsetPluggedStateTrigger extends StateTrigger {

	private static final long serialVersionUID = -7233007190107298819L;
	
	private static final String CLASS_NAME = WiredHeadsetPluggedStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(WiredHeadsetPluggedStateTrigger.class.getName());
	
	public static final String ACTION_HEADSET_PLUG = "android.intent.action.HEADSET_PLUG";
	
	
	
	public static final int HEADSET_UNPLUGGED = 0;

	public static final int HEADSET_PLUGGED = 1;

	/**
	 * <P>
	 * Holds the trigger wanted plugged state.
	 * </P>
	 * 
	 * @see android.media.AudioManager
	 */
	@JsonProperty("wantedPluggedState")
	private int wantedPluggedState = HEADSET_PLUGGED;

	public int getWantedPluggedState() {
		return wantedPluggedState;
	}

	public void setWantedPluggedState(int wantedPluggedState) {
		this.wantedPluggedState = wantedPluggedState;
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_wired_headset_plugged_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_wired_headset_plugged_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String pluggedState = null;
		switch (wantedPluggedState) {
		case HEADSET_PLUGGED:
			pluggedState = context.getString(R.string.trigger_wired_headset_plugged);
			break;
		case HEADSET_UNPLUGGED:
			pluggedState = context.getString(R.string.trigger_wired_headset_unplugged);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.trigger_wired_headset_plugged_dynamic_description);
		return String.format(description, pluggedState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedPluggedState) {
		case HEADSET_PLUGGED:
			return ImageUtils.getBitmap(R.drawable.plugged);
		case HEADSET_UNPLUGGED:
			return ImageUtils.getBitmap(R.drawable.unplugged);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return WiredHeadsetPluggedTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedPluggedState) {
		case HEADSET_PLUGGED:
		case HEADSET_UNPLUGGED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Trigger getClone() {
		WiredHeadsetPluggedStateTrigger trigger = new WiredHeadsetPluggedStateTrigger();
		trigger.setWantedPluggedState(getWantedPluggedState());
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		if (audioManager == null) {
			return;
		}
		
		boolean wiredHeadsetOn = audioManager.isWiredHeadsetOn();
		if (wantedPluggedState == HEADSET_PLUGGED && wiredHeadsetOn
			|| wantedPluggedState == HEADSET_UNPLUGGED && !wiredHeadsetOn) {
			setSatisfied(true);
		} else {
			setSatisfied(false);
		}
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			// Gets the headset plugged state from json file
			IDeviceStateDAO deviceStateDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getDeviceStateDAO();
			Integer savedPluggedState = deviceStateDAO.get(CLASS_NAME);
			
			int currPluggedState = intent.getIntExtra("state", -1);
//			String headsetName = stateExtras.getString("name");
//			int hasMicrophone = stateExtras.getInt("microphone");
			
			// If the wired headset plugged state didn't change, do nothing
			if (savedPluggedState == currPluggedState) {
				LOGGER.info(CLASS_NAME + " The wired headset plugged state didn't change, do nothing");
				return;
			// If the wired headset plugged state changed, save it
			} else {
				deviceStateDAO.set(CLASS_NAME, currPluggedState);
			}

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(WiredHeadsetPluggedStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				WiredHeadsetPluggedStateTrigger wiredHeadsetPluggedTrigger = (WiredHeadsetPluggedStateTrigger) trigger;
				if (wiredHeadsetPluggedTrigger.getWantedPluggedState() == currPluggedState) {
					wiredHeadsetPluggedTrigger.setSatisfied(true);
				} else {
					wiredHeadsetPluggedTrigger.setSatisfied(false);
				}
				triggerDAO.store(wiredHeadsetPluggedTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(WiredHeadsetPluggedStateTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

/**
 * 
 */
package com.ruley.model.beans.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ruley.R;
import com.ruley.activities.editors.triggers.TestTriggerEditorActivity;
import com.ruley.model.beans.triggers.base.Trigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TestTrigger extends Trigger {

	private static final long serialVersionUID = 5582787197982378755L;
	
	private static final String CLASS_NAME = TestTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestTrigger.class.getName());

	@Override
	protected void register(Context context) {
		LOGGER.debug(CLASS_NAME + "#register(...)");

		super.register(context);
	}

	@Override
	protected void unregister(Context context) {
		LOGGER.debug(CLASS_NAME + "#unregister(...)");
		
		super.unregister(context);
	}

	@Override
	public int getNameId() {
		return R.string.trigger_test_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.trigger_test_static_description;
	}

	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.trigger_test_dynamic_description);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return TestTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Trigger getClone() {
		TestTrigger trigger = new TestTrigger();
		return trigger;
	}

	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			
		}
		
	}
	
	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

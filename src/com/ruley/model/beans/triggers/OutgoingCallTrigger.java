/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.triggers.OutgoingCallTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.variables.ContactsVariable;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast actions:
 * "android.intent.action.NEW_OUTGOING_CALL"
 * "android.phone.extra.ALREADY_CALLED"
 * "android.phone.extra.ORIGINAL_URI"
 * 
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public class OutgoingCallTrigger extends Trigger {

	private static final long serialVersionUID = -8535112477462662621L;
	
	private static final String CLASS_NAME = OutgoingCallTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OutgoingCallTrigger.class.getName());
	
	public static final String ACTION_NEW_OUTGOING_CALL = "android.intent.action.NEW_OUTGOING_CALL";

//	private static final String EXTRA_ALREADY_CALLED = "android.phone.extra.ALREADY_CALLED";

//	private static final String EXTRA_ORIGINAL_URI = "android.phone.extra.ORIGINAL_URI";

	@JsonProperty("contacts")
	private ContactsVariable contacts = new ContactsVariable();

	public OutgoingCallTrigger() {
		super();
	}

	public ContactsVariable getContacts() {
		return contacts;
	}

	public void setContacts(ContactsVariable contacts) {
		this.contacts = contacts;
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_outgoing_call_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_outgoing_call_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (contacts == null) {
			return context.getString(R.string.trigger_outgoing_call_contact_missing);
		} else {
			String description = context.getString(R.string.trigger_outgoing_call_dynamic_description);
			return String.format(description, contacts);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.outgoing_call);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return OutgoingCallTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		if (contacts != null) {
			Set<String> contactsIds = contacts.getContactsIds();
			return contactsIds != null
					&& !contactsIds.isEmpty();
		}
		
		return false;
	}
	
	@Override
	public Trigger getClone() {
		OutgoingCallTrigger trigger = new OutgoingCallTrigger();
		trigger.setContacts(contacts);
		return trigger;
	}
	
	@Override
	public Trigger toTemplate() {
		OutgoingCallTrigger trigger = new OutgoingCallTrigger();
		trigger.setContacts(new ContactsVariable());
		return trigger;
	}

	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

//			boolean isAlreadyCalled = intent.getBooleanExtra(EXTRA_ALREADY_CALLED);
			String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//			String originalURI = intent.getStringExtra(EXTRA_ORIGINAL_URI);

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(OutgoingCallTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			String contactId = ContactsUtils.getContactId(appCtx, phoneNumber);
			for (Trigger trigger : triggers) {
				OutgoingCallTrigger outgoingCallTrigger = (OutgoingCallTrigger) trigger;
				if (outgoingCallTrigger.getContacts().getContactsIds().contains(contactId)) {
					outgoingCallTrigger.setSatisfied(true);
				} else {
					outgoingCallTrigger.setSatisfied(false);
				}
				triggerDAO.store(outgoingCallTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(OutgoingCallTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(false);
				triggerDAO.store(trigger);
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

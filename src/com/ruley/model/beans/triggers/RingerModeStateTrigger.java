/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.triggers.RingerModeStateTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast actions:
 * "android.media.RINGER_MODE_CHANGED"
 * 
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public class RingerModeStateTrigger extends StateTrigger {

	private static final long serialVersionUID = -7369661532663301316L;
	
	private static final String CLASS_NAME = RingerModeStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(RingerModeStateTrigger.class.getName());
	
	public static final String ACTION_RINGER_MODE_CHANGED = "android.media.RINGER_MODE_CHANGED";
	
	public static final int RINGER_MODE_UNKNOWN = -1;
	
	private static int SAVED_RINGER_MODE = RINGER_MODE_UNKNOWN;

	/**
	 * Holds the rinder wanted mode.
	 * 
	 * <p>
	 * AudioManager.RINGER_MODE_NORMAL</br>
	 * AudioManager.RINGER_MODE_SILENT</br>
	 * AudioManager.RINGER_MODE_VIBRATE
	 * </p>
	 * 
	 * @see android.media.AudioManager
	 */
	@JsonProperty("wantedRingerMode")
	private int wantedRingerMode = AudioManager.RINGER_MODE_SILENT;

	public int getWantedRingerMode() {
		return wantedRingerMode;
	}

	public void setWantedRingerMode(int wantedRingerMode) {
		this.wantedRingerMode = wantedRingerMode;
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_ringer_mode_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_ringer_mode_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String ringerMode = null;
		switch (wantedRingerMode) {
		case AudioManager.RINGER_MODE_NORMAL:
			ringerMode = context.getString(R.string.trigger_ringer_mode_state_normal);
			break;
		case AudioManager.RINGER_MODE_SILENT:
			ringerMode = context.getString(R.string.trigger_ringer_mode_state_silent);
			break;
		case AudioManager.RINGER_MODE_VIBRATE:
			ringerMode = context.getString(R.string.trigger_ringer_mode_state_vibrate);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.trigger_ringer_mode_state_dynamic_description);
		return String.format(description, ringerMode);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedRingerMode) {
		case AudioManager.RINGER_MODE_NORMAL:
			return ImageUtils.getBitmap(R.drawable.ringer_normal);
		case AudioManager.RINGER_MODE_SILENT:
			return ImageUtils.getBitmap(R.drawable.ringer_silent);
		case AudioManager.RINGER_MODE_VIBRATE:
			return ImageUtils.getBitmap(R.drawable.ringer_vibrate);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return RingerModeStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedRingerMode) {
		case AudioManager.RINGER_MODE_NORMAL:
		case AudioManager.RINGER_MODE_SILENT:
		case AudioManager.RINGER_MODE_VIBRATE:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Trigger getClone() {
		RingerModeStateTrigger trigger = new RingerModeStateTrigger();
		trigger.setWantedRingerMode(getWantedRingerMode());
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		
		if (audioManager == null) {
			return;
		}
		
		int ringerMode = audioManager.getRingerMode();
		if (wantedRingerMode == ringerMode) {
			setSatisfied(true);
		} else {
			setSatisfied(false);
		}
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			int currRingerMode = intent.getIntExtra(AudioManager.EXTRA_RINGER_MODE, -1);

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(RingerModeStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			// If the ringer mode unknown, do nothing
			if (RINGER_MODE_UNKNOWN == currRingerMode) {
				LOGGER.info(CLASS_NAME + " The ringer mode unknown");
				return;
			// If the ringer mode didn't change, do nothing
			} else if (SAVED_RINGER_MODE == currRingerMode) {
				LOGGER.info(CLASS_NAME + " The ringer mode didn't changed");
				return;
			// If the ringer mode did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The ringer mode did changed");
				SAVED_RINGER_MODE = currRingerMode;
			}

			for (Trigger trigger : triggers) {
				RingerModeStateTrigger ringerModeStateTrigger = (RingerModeStateTrigger) trigger;
				if (ringerModeStateTrigger.getWantedRingerMode() == currRingerMode) {
					ringerModeStateTrigger.setSatisfied(true);
				} else {
					ringerModeStateTrigger.setSatisfied(false);
				}
				triggerDAO.store(ringerModeStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(RingerModeStateTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.triggers.ScreenStateTriggerEditorActivity;
import com.ruley.model.beans.device.DeviceState;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles two broadcasts actions:
 * "android.intent.action.SCREEN_ON"
 * "android.intent.action.SCREEN_OFF"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ScreenStateTrigger extends StateTrigger {

	private static final long serialVersionUID = -5171610796691961801L;
	
	private static final String CLASS_NAME = ScreenStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenStateTrigger.class.getName());
	
	public static final String ACTION_SCREEN_ON = "android.intent.action.SCREEN_ON";
	
	public static final String ACTION_SCREEN_OFF = "android.intent.action.SCREEN_OFF";
	
	public static final int SCREEN_STATE_UNKNOWN = -1;
	
	public static final int SCREEN_OFF = 0;
	
	public static final int SCREEN_ON = 1;
	
	private static int SAVED_SCREEN_STATE = SCREEN_STATE_UNKNOWN;
	
	@JsonProperty("wantedScreenState")
	private int wantedScreenState = SCREEN_ON;

	public int getWantedScreenState() {
		return wantedScreenState;
	}

	public void setWantedScreenState(int wantedScreenState) {
		this.wantedScreenState = wantedScreenState;
	}

	@Override
	public int getNameId() {
		return R.string.trigger_screen_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_screen_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String screenState = null;
		switch (wantedScreenState) {
		case SCREEN_ON:
			screenState = context.getString(R.string.trigger_screen_state_on);
			break;
		case SCREEN_OFF:
			screenState = context.getString(R.string.trigger_screen_state_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.trigger_screen_state_dynamic_description);
		return String.format(description, screenState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedScreenState) {
		case SCREEN_ON:
			return ImageUtils.getBitmap(R.drawable.screen_on);
		case SCREEN_OFF:
			return ImageUtils.getBitmap(R.drawable.screen_off);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ScreenStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedScreenState) {
		case SCREEN_ON:
		case SCREEN_OFF:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Trigger getClone() {
		ScreenStateTrigger trigger = new ScreenStateTrigger();
		trigger.setWantedScreenState(getWantedScreenState());
		return trigger;
	}
	
	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		boolean isScreenOn = DeviceState.isScreenOn();
		
		if ( (isScreenOn && wantedScreenState == SCREEN_ON)
				|| (!isScreenOn && wantedScreenState == SCREEN_OFF) ) {
			setSatisfied(true);
		} else {
			setSatisfied(false);
		}
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(ScreenStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			int currScreenState = SCREEN_STATE_UNKNOWN;
			String action = intent.getAction();
			if (ACTION_SCREEN_ON.equals(action)) {
				currScreenState = SCREEN_ON;
			} else if (ACTION_SCREEN_OFF.equals(action)) {
				currScreenState = SCREEN_OFF;
			}
			
			// If the screen state unknown, do nothing
			if (SCREEN_STATE_UNKNOWN == currScreenState) {
				LOGGER.info(CLASS_NAME + " The screen state unknown");
				return;
			// If the screen state didn't change, do nothing
			} else if (SAVED_SCREEN_STATE == currScreenState) {
				LOGGER.info(CLASS_NAME + " The screen state didn't changed");
				return;
			// If the screen state did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The screen state did changed");
				SAVED_SCREEN_STATE = currScreenState;
			}

			for (Trigger trigger : triggers) {
				ScreenStateTrigger screenStateTrigger = (ScreenStateTrigger) trigger;
				if (screenStateTrigger.getWantedScreenState() == currScreenState) {
					screenStateTrigger.setSatisfied(true);
				} else {
					screenStateTrigger.setSatisfied(false);
				}
				triggerDAO.store(screenStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(ScreenStateTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.triggers.BootCompletedTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast acitons:
 * "android.intent.action.BOOT_COMPLETED"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class BootCompletedTrigger extends Trigger {

	private static final long serialVersionUID = 4332196698985476132L;
	
	private static final String CLASS_NAME = BootCompletedTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(BootCompletedTrigger.class.getName());
	
	public static final String ACTION_BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";

	@Override
	public int getNameId() {
		return R.string.trigger_boot_completed_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_boot_completed_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.trigger_boot_completed_static_description);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.phone_turned_on);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return BootCompletedTriggerEditorActivity.class;
	}
	

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Trigger getClone() {
		BootCompletedTrigger trigger = new BootCompletedTrigger(); 
		return trigger;
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(BootCompletedTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(true);
				triggerDAO.store(trigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(BootCompletedTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(false);
				triggerDAO.store(trigger);
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

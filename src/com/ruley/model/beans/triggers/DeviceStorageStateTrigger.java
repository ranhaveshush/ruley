/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.triggers.DeviceStorageStateTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast acitons:
 * "android.intent.action.DEVICE_STORAGE_LOW"
 * "android.intent.action.DEVICE_STORAGE_OK"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class DeviceStorageStateTrigger extends StateTrigger {
	
	private static final long serialVersionUID = -5807187774264880399L;

	private static final String CLASS_NAME = DeviceStorageStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(DeviceStorageStateTrigger.class.getName());
	
	public static final String ACTION_DEVICE_STORAGE_LOW = "android.intent.action.DEVICE_STORAGE_LOW";
	
	public static final String ACTION_DEVICE_STORAGE_OK = "android.intent.action.DEVICE_STORAGE_OK";

	public static final int DEVICE_STORAGE_UNKNOWN = -1;
	
	public static final int DEVICE_STORAGE_LOW = 0;
	
	public static final int DEVICE_STORAGE_OK = 1;
	
	private static int SAVED_DEVICE_STORAGE = DEVICE_STORAGE_UNKNOWN;
	
	@JsonProperty("wantedDeviceStorageState")
	private int wantedDeviceStorageState = DEVICE_STORAGE_OK;

	public int getWantedDeviceStorageState() {
		return wantedDeviceStorageState;
	}

	public void setWantedDeviceStorageState(int wantedDeviceStorageState) {
		
		assert(wantedDeviceStorageState == DEVICE_STORAGE_LOW
				|| wantedDeviceStorageState == DEVICE_STORAGE_OK);
		
		this.wantedDeviceStorageState = wantedDeviceStorageState;
	}

	@Override
	public int getNameId() {
		return R.string.trigger_device_storage_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_device_storage_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String deviceStorageState = null;
		switch (wantedDeviceStorageState) {
		case DEVICE_STORAGE_OK:
			deviceStorageState = context.getString(R.string.trigger_device_storage_ok);
			break;
		case DEVICE_STORAGE_LOW:
			deviceStorageState = context.getString(R.string.trigger_device_storage_low);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.trigger_device_storage_state_dynamic_description);
		return String.format(description, deviceStorageState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedDeviceStorageState) {
		case DEVICE_STORAGE_OK:
			return ImageUtils.getBitmap(R.drawable.storage_ok);
		case DEVICE_STORAGE_LOW:
			return ImageUtils.getBitmap(R.drawable.storage_low);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return DeviceStorageStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedDeviceStorageState) {
		case DEVICE_STORAGE_OK:
		case DEVICE_STORAGE_LOW:
			return true;
		default:
			return false;
		}
	}
	
	public Trigger getClone() {
		DeviceStorageStateTrigger trigger = new DeviceStorageStateTrigger();
		trigger.setWantedDeviceStorageState(getWantedDeviceStorageState());
		return trigger;
	}
	
	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		switch (wantedDeviceStorageState) {
		case DEVICE_STORAGE_OK:			
			setSatisfied(true);
			break;
		case DEVICE_STORAGE_LOW:			
			setSatisfied(false);
			break;
		}
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(DeviceStorageStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			int currDeviceStorageState = DEVICE_STORAGE_UNKNOWN;
			String action = intent.getAction();
			if (ACTION_DEVICE_STORAGE_OK.equals(action)) {
				currDeviceStorageState = DEVICE_STORAGE_OK;
			} else if (ACTION_DEVICE_STORAGE_LOW.equals(action)) {
				currDeviceStorageState = DEVICE_STORAGE_LOW;
			}
			
			// If the device storage unknown, do nothing
			if (DEVICE_STORAGE_UNKNOWN == currDeviceStorageState) {
				LOGGER.info(CLASS_NAME + " The device storage unknown");
				return;
			// If the device storage didn't change, do nothing
			} else if (SAVED_DEVICE_STORAGE == currDeviceStorageState) {
				LOGGER.info(CLASS_NAME + " The device storage didn't changed");
				return;
			// If the device storage did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The device storage did changed");
				SAVED_DEVICE_STORAGE = currDeviceStorageState;
			}

			for (Trigger trigger : triggers) {
				DeviceStorageStateTrigger powerStateTrigger = (DeviceStorageStateTrigger) trigger;
				if (powerStateTrigger.getWantedDeviceStorageState() == currDeviceStorageState) {
					powerStateTrigger.setSatisfied(true);
				} else {
					powerStateTrigger.setSatisfied(false);
				}
				triggerDAO.store(powerStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(DeviceStorageStateTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

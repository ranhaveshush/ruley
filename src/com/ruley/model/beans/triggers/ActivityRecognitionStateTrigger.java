/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.io.File;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.ruley.R;
import com.ruley.activities.editors.triggers.ActivityRecognitionStateTriggerEditorActivity;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IDeviceStateDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ActivityRecognitionClientUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.LocationClientUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 * 
 */
public class ActivityRecognitionStateTrigger extends StateTrigger implements
		ActivityRecognitionClientUtils.Listener, LocationClientUtils.Listener {

	private static final long serialVersionUID = -1611143359498365579L;

	private static final String CLASS_NAME = ActivityRecognitionStateTrigger.class
			.getSimpleName();
	
	private static final String ACTIVITY_STILL_KEY = CLASS_NAME + File.separator + DetectedActivity.STILL;
	private static final String ACTIVITY_IN_VEHICLE_KEY = CLASS_NAME + File.separator + DetectedActivity.IN_VEHICLE;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ActivityRecognitionStateTrigger.class.getName());

	public static final String ACTION_ACTIVITY_RECOGNITION = Config.APP_PACKAGE
			+ ".ACTION_ACTIVITY_RECOGNITION";

	private static final int MIN_CONFIDENCE_LEVEL = 40;
	
	private static final int STILL_CONFIDENCE_COUNT = 12;
	private static final int IN_VEHICLE_CONFIDENCE_COUNT = 2;

	/**
	 * Possible values for wanted user activity:
	 * 
	 * <P>
	 * DetectedActivity.UNKNOWN - Unable to detect the current activity.<BR/>
	 * DetectedActivity.STILL - The device is still (not moving).<BR/>
	 * DetectedActivity.TILTING - The device angle relative to gravity changed
	 * significantly.<BR/>
	 * DetectedActivity.RUNNING - The device is on a user who is running.
	 * DetectedActivity.WALKING - The device is on a user who is walking.
	 * DetectedActivity.ON_FOOT - The device is on a user who is walking or
	 * running.<BR/>
	 * DetectedActivity.ON_BICYCLE - The device is on a bicycle.<BR/>
	 * DetectedActivity.IN_VEHICLE - The device is in a vehicle, such as a car.
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see com.google.android.gms.location.DetectedActivity
	 * 
	 */
	@JsonProperty("wantedUserActivityState")
	private int wantedUserActivityState = DetectedActivity.STILL;

	public int getWantedUserActivityState() {
		return wantedUserActivityState;
	}

	public void setWantedUserActivityState(int wantedUserActivityState) {
		this.wantedUserActivityState = wantedUserActivityState;
	}

	@Override
	protected void register(Context context) {
		LOGGER.debug(CLASS_NAME + "#register(...)");

		// If Location Services disabled, do not register activity recognition,
		// cos activity recognition gets it's updates from Location Services
		boolean isLocationServiceEnabled = LocationClientUtils
				.isLocationServiceEnabled(context);
		if (!isLocationServiceEnabled) {
			listener.onError(R.string.trigger_activation_error_location_service_disabled);
			return;
		}

		Intent intent = new Intent(ACTION_ACTIVITY_RECOGNITION);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.ACTIVITY_RECOGNITION_REQUEST_CODE, intent,
				PendingIntent.FLAG_NO_CREATE);
		// If such pending intent doesn't exists, create one
		if (pendingIntent == null) {
			LOGGER.info("Registering " + CLASS_NAME
					+ " for user activity updates");
			pendingIntent = PendingIntent.getBroadcast(context,
					Config.PendingIntent.ACTIVITY_RECOGNITION_REQUEST_CODE,
					intent, PendingIntent.FLAG_UPDATE_CURRENT);
			// Starts monitoring activity recognition state
			IDeviceStateDAO deviceStateDAO = DAOFactory.getFactory(
					DAOFactory.getFactoryType()).getDeviceStateDAO();
			deviceStateDAO.set(CLASS_NAME, DetectedActivity.UNKNOWN);
			// Starts listening to location services disabled
			new LocationClientUtils().listenLocationServicesDisabled(
					MyApplication.getContext(), this);
			// If such pending intent does exists, use it
		} else {
			LOGGER.info("Using exsiting pending intent for " + CLASS_NAME + "s");
		}

		new ActivityRecognitionClientUtils().startActivityRecognition(context,
				pendingIntent, this);
	}

	@Override
	protected void unregister(Context context) {
		LOGGER.debug(CLASS_NAME + "#unregister(...)");

		Context appCtx = MyApplication.getContext();

		Intent intent = new Intent(ACTION_ACTIVITY_RECOGNITION);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.ACTIVITY_RECOGNITION_REQUEST_CODE, intent,
				PendingIntent.FLAG_NO_CREATE);
		// If such pending intent doesn't exists, can't cancel it
		if (pendingIntent == null) {
			LOGGER.warn("None " + CLASS_NAME
					+ "s pending intent exists, can't cancel it");
			super.unregister(appCtx);
			return;
		}

		ITriggerDAO triggerDAO = DAOFactory.getFactory(
				DAOFactory.getFactoryType()).getTriggerDAO();
		List<Trigger> triggers = triggerDAO.list(
				ActivityRecognitionStateTrigger.class, true);

		// If there are no triggers, but there is pending intent, warn (wrong
		// buggy state)
		if (triggers == null || triggers.isEmpty()) {
			LOGGER.warn("There are not " + CLASS_NAME
					+ "s but there is pending intent (wrong buggy state)");
			super.unregister(appCtx);
		} else if (triggers.size() == 1) {
			LOGGER.info("Unregistering " + CLASS_NAME
					+ " for user activity updates");
			pendingIntent = PendingIntent.getBroadcast(context,
					Config.PendingIntent.ACTIVITY_RECOGNITION_REQUEST_CODE,
					intent, PendingIntent.FLAG_NO_CREATE
							| PendingIntent.FLAG_CANCEL_CURRENT);
			// Stops monitoring activity recognition state
			IDeviceStateDAO deviceStateDAO = DAOFactory.getFactory(
					DAOFactory.getFactoryType()).getDeviceStateDAO();
			deviceStateDAO.set(CLASS_NAME, DetectedActivity.UNKNOWN);
			// TODO: check if i can use a canceled pending intent
			new ActivityRecognitionClientUtils().stopActivityRecognition(
					context, pendingIntent, this);
			// Stops listening to location services disabled
			boolean locationServiceEnabled = LocationClientUtils
					.isLocationServiceEnabled(appCtx);
			if (locationServiceEnabled) {
				new LocationClientUtils().unlistenLocationServicesDisabled(
						appCtx, this);
			}
			// If there are other triggers, keep the pending intent
		} else {
			LOGGER.info("Other " + CLASS_NAME
					+ "s registered, so keeping user activity updates");
			super.unregister(appCtx);
		}
	}

	@Override
	public int getNameId() {
		return R.string.trigger_activity_recognition_state_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.trigger_activity_recognition_state_static_description;
	}

	@Override
	public String getDescription(Context context) {
		String userActivity = null;
		switch (wantedUserActivityState) {
		case DetectedActivity.STILL:
			userActivity = context
					.getString(R.string.trigger_activity_recognition_state_still);
			break;
		case DetectedActivity.WALKING:
			userActivity = context
					.getString(R.string.trigger_activity_recognition_state_walking);
			break;
		case DetectedActivity.RUNNING:
			userActivity = context
					.getString(R.string.trigger_activity_recognition_state_running);
			break;
		case DetectedActivity.ON_BICYCLE:
			userActivity = context
					.getString(R.string.trigger_activity_recognition_state_on_bycicle);
			break;
		case DetectedActivity.IN_VEHICLE:
			userActivity = context
					.getString(R.string.trigger_activity_recognition_state_in_vehicle);
			break;
		case DetectedActivity.TILTING:
		case DetectedActivity.UNKNOWN:
		default:
			break;
		}
		String description = context
				.getString(R.string.trigger_activity_recognition_state_dynamic_description);
		return String.format(description, userActivity);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedUserActivityState) {
		case DetectedActivity.STILL:
			return ImageUtils.getBitmap(R.drawable.still);
		case DetectedActivity.WALKING:
			return ImageUtils.getBitmap(R.drawable.walking);
		case DetectedActivity.RUNNING:
			return ImageUtils.getBitmap(R.drawable.running);
		case DetectedActivity.ON_BICYCLE:
			return ImageUtils.getBitmap(R.drawable.cycling);
		case DetectedActivity.IN_VEHICLE:
			return ImageUtils.getBitmap(R.drawable.driving);
		case DetectedActivity.TILTING:
		case DetectedActivity.UNKNOWN:
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ActivityRecognitionStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedUserActivityState) {
		case DetectedActivity.STILL:
		case DetectedActivity.WALKING:
		case DetectedActivity.RUNNING:
		case DetectedActivity.ON_BICYCLE:
		case DetectedActivity.IN_VEHICLE:
			return true;
		case DetectedActivity.TILTING:
		case DetectedActivity.UNKNOWN:
		default:
			return false;
		}
	}

	@Override
	public Trigger getClone() {
		ActivityRecognitionStateTrigger trigger = new ActivityRecognitionStateTrigger();
		trigger.setWantedUserActivityState(getWantedUserActivityState());
		return trigger;
	}

	@Override
	public void onStartActivityRecognition() {
		super.register(MyApplication.getContext());
	}

	@Override
	public void onStopActivityRecognition() {
		super.unregister(MyApplication.getContext());
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");

		IDeviceStateDAO deviceStateDAO = DAOFactory.getFactory(
				DAOFactory.getFactoryType()).getDeviceStateDAO();
		int savedActivityType = deviceStateDAO.get(CLASS_NAME);
		deviceStateDAO.set(ACTIVITY_STILL_KEY, 0);
		deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, 0);

		if (wantedUserActivityState == savedActivityType) {
			setSatisfied(true);
		} else {
			setSatisfied(false);
		}
	}

	@Override
	public void onKnownLastLocation(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAddedGeofences() {
		LOGGER.info(CLASS_NAME
				+ "#onAddedGeofences(...) ADD dummy Geofence to listen to Location Services DISABLED");
	}

	@Override
	public void onRemovedGeofencesByPendingIntent() {
		LOGGER.info(CLASS_NAME
				+ "#onAddedGeofences(...) REMOVE dummy Geofence to listen to Location Services DISABLED");
	}

	@Override
	public void onError(int errorMessageResourceId) {
		listener.onError(R.string.trigger_activation_error_location_service_disabled);
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ActivityRecognitionResult result = (ActivityRecognitionResult) intent
					.getExtras().get(ActivityRecognitionResult.EXTRA_ACTIVITY_RESULT);

			// Validates result
			if (result == null) {
				LOGGER.warn("Extras missing an activity recognition result");
				return;
			}

			// Gets the user activity recognition state from JSON file
			IDeviceStateDAO deviceStateDAO = DAOFactory.getFactory(
					DAOFactory.getFactoryType()).getDeviceStateDAO();
			Integer savedActivityType = deviceStateDAO.get(CLASS_NAME);
			Integer stillConfidenceCount = deviceStateDAO.get(ACTIVITY_STILL_KEY);
			Integer inVehicleConfidenceCount = deviceStateDAO.get(ACTIVITY_IN_VEHICLE_KEY);

			int mostProbableActivityType = DetectedActivity.UNKNOWN;
			int mostProbableActivityConfidence = -1;

			// Logs all probable activities
			List<DetectedActivity> probableActivities = result.getProbableActivities();
			LOGGER.info("Probable activities number is " + probableActivities.size());
			StringBuilder sb = new StringBuilder();
			for (DetectedActivity probableActivity : probableActivities) {
				String activityName = ActivityRecognitionClientUtils.getNameFromType(probableActivity.getType());
				int activityConfidence = probableActivity.getConfidence();
				sb.append(activityName).append(" = ").append(activityConfidence).append("\n");
			}
			LOGGER.info(sb.toString());

			// Logs and finds the most probable activity
			DetectedActivity mostProbableActivity = result.getMostProbableActivity();
			mostProbableActivityType = mostProbableActivity.getType();
			mostProbableActivityConfidence = mostProbableActivity.getConfidence();
			// If the most probable activity type is on foot,
			// gets the second most probable activity type (walking or running)
			if (DetectedActivity.ON_FOOT == mostProbableActivityType
					&& probableActivities.size() >= 2) {
				DetectedActivity secondMostProbableActivity = probableActivities.get(1);
				mostProbableActivityType = secondMostProbableActivity.getType();
				mostProbableActivityConfidence = secondMostProbableActivity.getConfidence();
			}
			LOGGER.info("Saved activity: "
					+ ActivityRecognitionClientUtils.getNameFromType(savedActivityType));
			LOGGER.info("Detected activity: "
					+ ActivityRecognitionClientUtils.getNameFromType(mostProbableActivityType));
			LOGGER.info("Confidence: " + mostProbableActivityConfidence);

			// If the activity confidence level to low, do nothing
			if (mostProbableActivityConfidence < MIN_CONFIDENCE_LEVEL) {
				LOGGER.info("The user activity confidence " + mostProbableActivityConfidence + " < " + MIN_CONFIDENCE_LEVEL + ", do nothing");
				deviceStateDAO.set(ACTIVITY_STILL_KEY, 0);
				deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, 0);
				return;
			// If the activity type unknown, do nothing
			} else if (DetectedActivity.UNKNOWN == mostProbableActivityType) {
				LOGGER.info("Unknown user activity, do nothing");
				deviceStateDAO.set(ACTIVITY_STILL_KEY, 0);
				deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, 0);
				return;
			// If the activity type tilting, do nothing
			} else if (DetectedActivity.TILTING == mostProbableActivityType) {
				LOGGER.info("Tilting user activity, do nothing");
				deviceStateDAO.set(ACTIVITY_STILL_KEY, 0);
				deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, 0);
				return;
			// If the activity type on foot, do nothing
			} else if (DetectedActivity.ON_FOOT == mostProbableActivityType) {
				LOGGER.info("On Foot user activity, do nothing");
				return;
			// If the activity type didn't change, do nothing
			} else if (savedActivityType == mostProbableActivityType) {
				LOGGER.info("The user activity didn't change, do nothing");
				LOGGER.info("Init Still and Driving confidence counters to zero!");
				deviceStateDAO.set(ACTIVITY_STILL_KEY, 0);
				deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, 0);
				return;
			// If the previous activity is driving and the current is still,
			// count still because after driving should come walking.
			} else if (DetectedActivity.IN_VEHICLE == savedActivityType
					&& DetectedActivity.STILL == mostProbableActivityType
					&& stillConfidenceCount < STILL_CONFIDENCE_COUNT) {
				deviceStateDAO.set(ACTIVITY_STILL_KEY, ++stillConfidenceCount);
				LOGGER.info("Saved user activity is Driving and detected Still, update Still confidence counting to "
						+ stillConfidenceCount
						+ "/"
						+ STILL_CONFIDENCE_COUNT);
				return;
			// If the previous activity is still and the current is driving,
			// count driving because after still should come walking.
			} else if (DetectedActivity.STILL == savedActivityType
					&& DetectedActivity.IN_VEHICLE == mostProbableActivityType
					&& inVehicleConfidenceCount < IN_VEHICLE_CONFIDENCE_COUNT) {
				deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, ++inVehicleConfidenceCount);
				LOGGER.info("Saved user activity is Still and detected Driving, update Driving confidence counting to "
						+ inVehicleConfidenceCount
						+ "/"
						+ IN_VEHICLE_CONFIDENCE_COUNT);
				if (inVehicleConfidenceCount < IN_VEHICLE_CONFIDENCE_COUNT) {
					return;
				}
			// If the previous activity is walking and the current is driving,
			// count driving.
			} else if (DetectedActivity.WALKING == savedActivityType
					&& DetectedActivity.IN_VEHICLE == mostProbableActivityType
					&& inVehicleConfidenceCount < IN_VEHICLE_CONFIDENCE_COUNT) {
				deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, ++inVehicleConfidenceCount);
				LOGGER.info("Saved user activity is Walking and detected Driving, update Driving confidence counting to "
						+ inVehicleConfidenceCount
						+ "/"
						+ IN_VEHICLE_CONFIDENCE_COUNT);
				if (inVehicleConfidenceCount < IN_VEHICLE_CONFIDENCE_COUNT) {
					return;
				}
			}

			// If the activity type did change with the right confidence level,
			// save the activity type and continue to handle the trigger
			LOGGER.info("The user activity changed "
					+ ActivityRecognitionClientUtils
							.getNameFromType(mostProbableActivityType));
			deviceStateDAO.set(CLASS_NAME, mostProbableActivityType);
			LOGGER.info("Init Still and Driving confidence counters to zero!");
			deviceStateDAO.set(ACTIVITY_STILL_KEY, 0);
			deviceStateDAO.set(ACTIVITY_IN_VEHICLE_KEY, 0);

			ITriggerDAO triggerDAO = DAOFactory.getFactory(
					DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO
					.list(ActivityRecognitionStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				ActivityRecognitionStateTrigger activityRecognitionTrigger = (ActivityRecognitionStateTrigger) trigger;
				if (activityRecognitionTrigger.getWantedUserActivityState() == mostProbableActivityType) {
					activityRecognitionTrigger.setSatisfied(true);
				} else {
					activityRecognitionTrigger.setSatisfied(false);
				}
				triggerDAO.store(activityRecognitionTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(ActivityRecognitionStateTrigger.class, true, true);
			
			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

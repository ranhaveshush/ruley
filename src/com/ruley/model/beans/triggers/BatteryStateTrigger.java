/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.BatteryManager;

import com.ruley.R;
import com.ruley.activities.editors.triggers.BatteryStateTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast acitons:
 * "android.intent.action.BATTERY_LOW"
 * "android.intent.action.BATTERY_OKAY"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class BatteryStateTrigger extends StateTrigger {

	private static final long serialVersionUID = -2834930542747690329L;
	
	private static final String CLASS_NAME = BatteryStateTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BatteryStateTrigger.class.getName());
	
	private static final int BATTERY_LOW_LEVEL = 14;
	
	public static final String ACTION_BATTERY_LOW = "android.intent.action.BATTERY_LOW";
	
	public static final String ACTION_BATTERY_OKAY = "android.intent.action.BATTERY_OKAY";
	
	public static final int BATTERY_STATE_UNKNOWN = -1;
	
	public static final int BATTERY_LOW = 0;
	
	public static final int BATTERY_OK = 1;
	
	private static int SAVED_BATTERY_STATE = BATTERY_STATE_UNKNOWN;
	
	@JsonProperty("wantedBatteryState")
	private int wantedBatteryState = BATTERY_LOW;

	public int getWantedBatteryState() {
		return wantedBatteryState;
	}

	public void setWantedBatteryState(int wantedBatteryState) {
		this.wantedBatteryState = wantedBatteryState;
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_battery_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_battery_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String batteryState = null;
		switch (wantedBatteryState) {
		case BATTERY_LOW:
			batteryState = context.getString(R.string.trigger_battery_state_low);
			break;
		case BATTERY_OK:
			batteryState = context.getString(R.string.trigger_battery_state_ok);
			break;
		}
		String description = context.getString(R.string.trigger_battery_state_dynamic_description);
		return String.format(description, batteryState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedBatteryState) {
		case BATTERY_LOW:
			return ImageUtils.getBitmap(R.drawable.battery_low);
		case BATTERY_OK:
			return ImageUtils.getBitmap(R.drawable.battery_full);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return BatteryStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedBatteryState) {
		case BATTERY_LOW:
		case BATTERY_OK:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Trigger getClone() {
		BatteryStateTrigger trigger = new BatteryStateTrigger();
		trigger.setWantedBatteryState(getWantedBatteryState());
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		getBatteryPercentage(context);
	}
	
	private void getBatteryPercentage(Context context) {
		BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				context.unregisterReceiver(this);
				int currentLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, BATTERY_STATE_UNKNOWN);
				int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, BATTERY_STATE_UNKNOWN);
				int level = BATTERY_STATE_UNKNOWN;
				if (currentLevel >= 0 && scale > 0) {
					level = (currentLevel * 100) / scale;
				}
				// Changes the trigger's satisfaction according to the level.
				// If above 14% battery OK, otherwise battery low.
				if ( (wantedBatteryState == BATTERY_OK && level > BATTERY_LOW_LEVEL)
					|| (wantedBatteryState == BATTERY_LOW && level <= BATTERY_LOW_LEVEL) ) {
					setSatisfied(true);
				} else {
					setSatisfied(false);
				}
				// NOTICE: this is the super.super.register(...) logic.
				// I put it here cos the super.super isn't accessible.
				BatteryStateTrigger.this.active = true;
				LOGGER.info("\"" + this.getClass().getSimpleName() + "\" activated");
				if (BatteryStateTrigger.this.listener != null) {
					BatteryStateTrigger.this.listener.onRegistered();
				}
			}
		};
		IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		context.registerReceiver(batteryLevelReceiver, batteryLevelFilter);
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(BatteryStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			int currBatteryState = BATTERY_STATE_UNKNOWN;
			String action = intent.getAction();
			if (Intent.ACTION_BATTERY_LOW.equals(action)) {
				currBatteryState = BATTERY_LOW;
			} else if (Intent.ACTION_BATTERY_OKAY.equals(action)) {
				currBatteryState = BATTERY_OK;
			}
			
			// If the battery state unknown, do nothing
			if (BATTERY_STATE_UNKNOWN == currBatteryState) {
				LOGGER.info(CLASS_NAME + " The battery state unknown");
				return;
			// If the battery state didn't change, do nothing
			} else if (SAVED_BATTERY_STATE == currBatteryState) {
				LOGGER.info(CLASS_NAME + " The battery state didn't changed");
				return;
			// If the battery state did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The battery state did changed");
				SAVED_BATTERY_STATE = currBatteryState;
			}

			for (Trigger trigger : triggers) {
				BatteryStateTrigger batteryStateTrigger = (BatteryStateTrigger) trigger;
				if (batteryStateTrigger.getWantedBatteryState() == currBatteryState) {
					batteryStateTrigger.setSatisfied(true);
				} else {
					batteryStateTrigger.setSatisfied(false);
				}
				triggerDAO.store(batteryStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(BatteryStateTrigger.class, true, true);
			
			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

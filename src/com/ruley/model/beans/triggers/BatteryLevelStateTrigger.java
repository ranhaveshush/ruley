/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.BatteryManager;
import android.os.Bundle;

import com.ruley.R;
import com.ruley.activities.editors.triggers.BatteryLevelStateTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcast actions:
 * "android.intent.action.BATTERY_CHANGED"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class BatteryLevelStateTrigger extends StateTrigger {

	private static final long serialVersionUID = 3407763636763968814L;

	private static final String CLASS_NAME = BatteryLevelStateTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BatteryLevelStateTrigger.class.getName());
	
	public static final String ACTION_BATTERY_CHANGED = "android.intent.action.BATTERY_CHANGED";
	
	public static final int BATTERY_LEVEL_UNKNOWN = -1;
	
	private static int SAVED_BATTERY_LEVEL = BATTERY_LEVEL_UNKNOWN;
	
	@JsonProperty("isNeverPerformed")
	private boolean isNeverPerformed = true;
	
	@JsonProperty("wantedBatteryLevelStart")
	private int wantedBatteryLevelStart = 0;
	
	@JsonProperty("wantedBatteryLevelEnd")
	private int wantedBatteryLevelEnd = 100;

	public boolean isNeverPerformed() {
		return isNeverPerformed;
	}

	public void setNeverPerformed(boolean isNeverPerformed) {
		this.isNeverPerformed = isNeverPerformed;
	}

	public int getWantedBatteryLevelStart() {
		return wantedBatteryLevelStart;
	}

	public void setWantedBatteryLevelStart(int wantedBatteryLevelStart) {
		this.wantedBatteryLevelStart = wantedBatteryLevelStart;
	}

	public int getWantedBatteryLevelEnd() {
		return wantedBatteryLevelEnd;
	}

	public void setWantedBatteryLevelEnd(int wantedBatteryLevelEnd) {
		this.wantedBatteryLevelEnd = wantedBatteryLevelEnd;
	}

	@Override
	public int getNameId() {
		return R.string.trigger_battery_level_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_battery_level_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String description = context.getString(R.string.trigger_battery_level_state_dynamic_description);
		return String.format(description, wantedBatteryLevelStart, wantedBatteryLevelEnd);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.battery_range);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return BatteryLevelStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return wantedBatteryLevelStart <= wantedBatteryLevelEnd;
	}
	
	@Override
	public Trigger getClone() {
		BatteryLevelStateTrigger trigger = new BatteryLevelStateTrigger();
		trigger.setWantedBatteryLevelStart(getWantedBatteryLevelStart());
		trigger.setWantedBatteryLevelEnd(getWantedBatteryLevelEnd());
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		getBatteryPercentage(context);
	}
	
	private void getBatteryPercentage(Context context) {
		BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				context.unregisterReceiver(this);
				int currBatteryLevel = getCurrentBatteryLevel(intent);
				// Changes the trigger's satisfaction according to the current battery level
				setSatisfied(isBatteryLevelInRange(currBatteryLevel));
				// NOTICE: this is the super.super.register(...) logic.
				// I put it here because the super.super isn't accessible.
				BatteryLevelStateTrigger.this.active = true;
				LOGGER.info("\"" + this.getClass().getSimpleName() + "\" activated");
				if (BatteryLevelStateTrigger.this.listener != null) {
					BatteryLevelStateTrigger.this.listener.onRegistered();
				}
			}
		};
		IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		context.registerReceiver(batteryLevelReceiver, batteryLevelFilter);
	}
	
	private boolean isBatteryLevelInRange(int currBatteryLevel) {
		if (wantedBatteryLevelStart <= currBatteryLevel
			&& currBatteryLevel <= wantedBatteryLevelEnd) {
			return true;
		} else {
			return false;
		}
	}
	
	private static int getCurrentBatteryLevel(Intent intent) {
		Bundle extras = intent.getExtras();
		int currentLevel = extras.getInt(BatteryManager.EXTRA_LEVEL, BATTERY_LEVEL_UNKNOWN);
		int scale = extras.getInt(BatteryManager.EXTRA_SCALE, BATTERY_LEVEL_UNKNOWN);
		int currBatteryLevel = BATTERY_LEVEL_UNKNOWN;
		if (currentLevel >=0 && scale > 0) {
			currBatteryLevel = (currentLevel * 100) / scale;
		}
		return currBatteryLevel;
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {

		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(BatteryLevelStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			int currBatteryLevel = getCurrentBatteryLevel(intent);
			
			// If the battery level unknown, do nothing
			if (BATTERY_LEVEL_UNKNOWN == currBatteryLevel) {
				LOGGER.info(CLASS_NAME + " The battery level unknown");
				return;
			// If the battery level didn't change, do nothing
			} else if (SAVED_BATTERY_LEVEL == currBatteryLevel) {
				LOGGER.info(CLASS_NAME + " The battery level didn't changed");
				return;
			// If the battery level did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The battery level did changed");
				SAVED_BATTERY_LEVEL = currBatteryLevel;
			}

			for (Trigger trigger : triggers) {
				BatteryLevelStateTrigger batteryStateTrigger = (BatteryLevelStateTrigger) trigger;
				if (batteryStateTrigger.isBatteryLevelInRange(currBatteryLevel)) {
					batteryStateTrigger.setSatisfied(true);
				} else {
					batteryStateTrigger.setSatisfied(false);
					batteryStateTrigger.setNeverPerformed(true);
				}
				triggerDAO.store(batteryStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(BatteryLevelStateTrigger.class, true, true);
			
			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					List<Trigger> ruleTriggers = rule.getTriggers();
					for (Trigger trigger : ruleTriggers) {
						if (trigger instanceof BatteryLevelStateTrigger) {
							BatteryLevelStateTrigger batteryLevelStateTrigger = (BatteryLevelStateTrigger) trigger;
							boolean isNeverPerformed = batteryLevelStateTrigger.isNeverPerformed();
							if (isNeverPerformed) {
								rule.perform(appCtx);
								batteryLevelStateTrigger.setNeverPerformed(false);
								triggerDAO.store(batteryLevelStateTrigger);
							}
						}
					}
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

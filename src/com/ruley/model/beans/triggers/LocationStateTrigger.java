/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.text.TextUtils;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.ruley.R;
import com.ruley.activities.editors.triggers.LocationStateTriggerEditorActivity;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.variables.LocationVariable;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.LocationClientUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class LocationStateTrigger extends StateTrigger implements LocationClientUtils.Listener {
	
	private static final long serialVersionUID = -5311034373679560255L;

	private static final String CLASS_NAME = LocationStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(LocationStateTrigger.class.getName());
	
	private static final int TRANSITION_DWELL = Geofence.GEOFENCE_TRANSITION_DWELL;
	private static final int TRANSITION_EXIT = Geofence.GEOFENCE_TRANSITION_EXIT;
	private static final int TRANSITION_TYPE = TRANSITION_DWELL | TRANSITION_EXIT;
	private static final int LOITERING_DELAY_MILLIS = 3 * 60 * 1000;
	
	public static final String ACTION_LOCATION_STATE = Config.APP_PACKAGE + ".ACTION_LOCATION_STATE";
	
	public static final String RADIUS_UNITS = "m";
	public static final float RADIUS_MIN = 150F;
	public static final float RADIUS_MAX = 1000F;
	
	public static final int RELATION_IN = 1;
	public static final int RELATION_OUT = 2;
	
	@JsonProperty("location")
	private LocationVariable location = new LocationVariable();

	@JsonProperty("radius")
	private float radius = RADIUS_MIN;

	@JsonProperty("expirationDuration")
	private long expirationDuration = Geofence.NEVER_EXPIRE;
	
	@JsonProperty("relationType")
	private int relationType = RELATION_IN;

	public LocationStateTrigger() {
		super();
	}

	public LocationVariable getLocation() {
		return location;
	}

	public void setLocation(LocationVariable location) {
		this.location = location;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		if (RADIUS_MIN <= radius && radius <= RADIUS_MAX) {
			this.radius = radius;
		}
	}

	public long getExpirationDuration() {
		return expirationDuration;
	}

	public void setExpirationDuration(long expirationDuration) {
		this.expirationDuration = expirationDuration;
	}
	
	public int getRelationType() {
		return relationType;
	}

	public void setRelationType(int relationType) {
		this.relationType = relationType;
	}

	public Geofence toGeofence(int transitionType, int loiteringDelayMs) {
		LOGGER.debug(CLASS_NAME + "#toGeofence(...)");
		
		LocationVariable location = getLocation();
		
		return new Geofence.Builder()
				.setRequestId(getId().toString()) // The geofence request id is the trigger's UUID
				.setCircularRegion(location.getLatitude(), location.getLongitude(), getRadius())
				.setExpirationDuration(getExpirationDuration())
				.setTransitionTypes(transitionType)
				.setLoiteringDelay(loiteringDelayMs)
				.build();
	}
	
	@Override
	protected void register(Context context) {
		LOGGER.debug(CLASS_NAME + "#register(...)");

		Intent intent = new Intent(ACTION_LOCATION_STATE);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.LOCATION_STATE_REQUEST_CODE,
				intent, PendingIntent.FLAG_NO_CREATE);
		// If such pending intent doesn't exists, create one
		if (pendingIntent == null) {
			LOGGER.info("Creating new pending intent for " + CLASS_NAME + "s");
			pendingIntent = PendingIntent.getBroadcast(context,
					Config.PendingIntent.LOCATION_STATE_REQUEST_CODE,
					intent, PendingIntent.FLAG_UPDATE_CURRENT);
		// If such pending intent does exists, use it
		} else {
			LOGGER.info("Using exsiting pending intent for " + CLASS_NAME + "s");
		}
		
		new LocationClientUtils().addGeofences(context, new ArrayList<Geofence>() {
			{
				add(toGeofence(TRANSITION_TYPE, LOITERING_DELAY_MILLIS));
			}
		}, pendingIntent, this);
	}
	
	@Override
	protected void unregister(Context context) {
		LOGGER.debug(CLASS_NAME + "#unregister(...)");
		
		Context appCtx = MyApplication.getContext();
		
		// If the "Location Services" disabled,
		// the disable event dumps all pending intents so,
		// just unregister the trigger
		boolean locationServiceEnabled = LocationClientUtils.isLocationServiceEnabled(context);
		if (!locationServiceEnabled) {
			LOGGER.info("\"Location Services\" DISABLED, the disable event dumps all pending intents");
			LOGGER.warn("None " + CLASS_NAME + "s pending intent exists, can't cancel it");
			super.unregister(appCtx);
			return;
		}
		
		Intent intent = new Intent(ACTION_LOCATION_STATE);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.LOCATION_STATE_REQUEST_CODE,
				intent, PendingIntent.FLAG_NO_CREATE);
		// If such pending intent doesn't exists, can't cancel it
		if (pendingIntent == null) {
			LOGGER.warn("None " + CLASS_NAME + "s pending intent exists, can't cancel it");
			super.unregister(appCtx);
			return;
		}
		
		ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		List<Trigger> triggers = triggerDAO.list(LocationStateTrigger.class, true);
		
		// If there are no triggers, but there is pending intent, warn (wrong buggy state)
		if (triggers == null || triggers.isEmpty()) {
			LOGGER.warn("There are not " + CLASS_NAME + "s but there is pending intent (wrong buggy state)");
			super.unregister(appCtx);
		// If the last trigger needs to be unregistered, cancel the pending intent
		} else if (triggers.size() == 1) {
			LOGGER.info("Canceling " + CLASS_NAME + "s pending intent");
			pendingIntent = PendingIntent.getBroadcast(context,
					Config.PendingIntent.LOCATION_STATE_REQUEST_CODE, intent,
					PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT);
			// TODO: check if i can use a canceled pending intent
			new LocationClientUtils().removeGeofences(context, pendingIntent, this);
		// If there are other triggers, keep the pending intent
		} else {
			LOGGER.info("Other " + CLASS_NAME + "s registered, so keeping the pending intent");
			super.unregister(appCtx);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_location_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_location_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (location == null) {
			return context.getString(R.string.trigger_location_state_location_invalid);
		} else {
			String description = context.getString(R.string.trigger_location_state_dynamic_description);
			String relationTypeStr = null;
			switch (relationType) {
			case RELATION_IN:
				relationTypeStr = context.getString(R.string.trigger_location_state_in);
				break;
			case RELATION_OUT:
				relationTypeStr = context.getString(R.string.trigger_location_state_out);
				break;
			default:
				break;
			}
			return String.format(description, relationTypeStr, location);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.location);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return LocationStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return location != null
				&& location.isValid(context)
				&& (radius > 0)
				&& (expirationDuration == Geofence.NEVER_EXPIRE);				
	}
	
	@Override
	public Trigger getClone() {
		LocationStateTrigger trigger = new LocationStateTrigger();
		trigger.setLocation(location);
		trigger.setRelationType(getRelationType());
		trigger.setRadius(getRadius());
		trigger.setExpirationDuration(getExpirationDuration());
		return trigger;
	}
	
	@Override
	public Trigger toTemplate() {
		LocationStateTrigger trigger = new LocationStateTrigger();
		trigger.setLocation(new LocationVariable());
		trigger.setRelationType(getRelationType());
		trigger.setRadius(getRadius());
		trigger.setExpirationDuration(getExpirationDuration());
		return trigger;
	}

	@Override
	protected void setInitialSatisfaction(Context context) {
		setSatisfied(relationType == RELATION_OUT);
	}

	@Override
	public void onKnownLastLocation(Location location) {
		// Do nothing, not implemented
	}

	@Override
	public void onAddedGeofences() {
		super.register(MyApplication.getContext());
	}

	@Override
	public void onRemovedGeofencesByPendingIntent() {
		super.unregister(MyApplication.getContext());
	}
	
	public void onError(int errorMessageResourceId) {
		listener.onError(errorMessageResourceId);
	}
	
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			int transitionType = LocationClient.getGeofenceTransition(intent);
			List<Geofence> triggeringGeofences = LocationClient.getTriggeringGeofences(intent);
			
			switch (transitionType) {
			case TRANSITION_DWELL:
				LOGGER.info("TRANSITION_DWELL");
				break;
			case TRANSITION_EXIT:
				LOGGER.info("TRANSITION_EXIT");
				break;
			case TRANSITION_DWELL | TRANSITION_EXIT:
				LOGGER.warn(CLASS_NAME + " geofence transition dwell and exit (both)");
				return;
			default:
				LOGGER.error(CLASS_NAME + " geofence transition type error: " + transitionType);
				return;
			}
			
			// Validates triggering geofences
			if (triggeringGeofences == null || triggeringGeofences.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " there aren't any triggering geofences");
				return;
			}
			
			LOGGER.info(CLASS_NAME + " triggeringGeofences.size() = " + triggeringGeofences.size());
			
			// Extracts the triggering triggers ids from the geofences requests ids
			final List<String> triggeringTriggersIds = new ArrayList<String>();
			for (Geofence geofence : triggeringGeofences) {
				String requestId = geofence.getRequestId();
				LOGGER.info(CLASS_NAME + " geofence.getRequestId() = " + requestId);
				if (!TextUtils.isEmpty(requestId)) {
					triggeringTriggersIds.add(requestId);
				}
			}
			
			// Validates triggering triggers ids
			if (triggeringTriggersIds == null || triggeringTriggersIds.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " there aren't any triggering triggers ids");
				return;
			}
			
			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(LocationStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				// Filters the triggering triggers
				if (!triggeringTriggersIds.contains(trigger.getId().toString())) {
					continue;
				}
				
				LocationStateTrigger locationStateTrigger = (LocationStateTrigger) trigger;
				int relationType = locationStateTrigger.getRelationType();
				
				if ((relationType == RELATION_IN && transitionType == TRANSITION_DWELL)
					|| (relationType == RELATION_OUT && transitionType == TRANSITION_EXIT)) {
					trigger.setSatisfied(true);
					triggerDAO.store(trigger);
					
					IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
					Rule rule = ruleDAO.get(trigger.getId(), true, true);
					
					if (rule != null) {
						rule.perform(appCtx);
					}
				} else if ((relationType == RELATION_IN && transitionType == TRANSITION_EXIT)
						|| (relationType == RELATION_OUT && transitionType == TRANSITION_DWELL)) {
					trigger.setSatisfied(false);
					triggerDAO.store(trigger);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

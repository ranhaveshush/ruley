/**
 * 
 */
package com.ruley.model.beans.triggers.base;

import android.content.Context;
import android.database.ContentObserver;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public abstract class ContentObserverTrigger extends Trigger {

	private static final long serialVersionUID = -5135361524745851124L;

	@Override
	protected void register(Context context) {
		registerContentObservers(context);

		// TODO: register with a listener
		super.register(context);
	}

	@Override
	protected void unregister(Context context) {
		unregisterContentObservers(context);

		// TODO: unregister with a listener
		super.unregister(context);
	}

	/**
	 * Invoked by the trigger's register method.<BR/>
	 * Verify right content observers registration.
	 * 
	 * @param context
	 */
	public abstract void registerContentObservers(Context context);

	/**
	 * Invoked by the trigger's unregister method.<BR/>
	 * Verify right content observers unregistration.
	 * 
	 * @param context
	 */
	public abstract void unregisterContentObservers(Context context);
	
	/**
	 * Gets the content observer trigger's content observer class.
	 * 
	 * @return Class represents the content observer class
	 */
	public abstract Class<? extends ContentObserver> getContentObserverClass();

}

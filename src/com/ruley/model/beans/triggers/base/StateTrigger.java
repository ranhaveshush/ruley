/**
 * 
 */
package com.ruley.model.beans.triggers.base;

import android.content.Context;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class StateTrigger extends Trigger {
	
	private static final long serialVersionUID = 5323499449886372406L;

	@Override
	protected void register(Context context) {
		setInitialSatisfaction(context);
		super.register(context);
	}
	
	/**
	 * Invoked by the trigger's register method.
	 * Sets the initial satisfaction state of the trigger,
	 * so that the trigger will be in a valid state (satisfied / unsatisfed).
	 * This helps the rule to be perform right after activation,
	 * without waiting for the unstated trigger to be fired to get it's initial state.
	 * 
	 * @param context
	 */
	protected abstract void setInitialSatisfaction(Context context);

}

/**
 * 
 */
package com.ruley.model.beans.triggers.base;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.model.beans.ICloneable;
import com.ruley.model.beans.ITemplatable;
import com.ruley.model.beans.rules.IRuleComponent;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public abstract class Trigger implements IRuleComponent, ICloneable<Trigger>, ITemplatable<Trigger> {
	
	private static final long serialVersionUID = 4336672933605925687L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Trigger.class.getName());
	
	/**
	 * This interface represents the static logic for handling the concrete trigger class.<BR/>
	 * The static logic inside the handle method will be invoked by,<BR/>
	 * The SysEventHandlerIntentService whenever it will receive an intent which the trigger handler can handle.
	 *  
	 * @see com.ruley.services.SysEventHandlerIntentService
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface ITriggerHandler {
		
		public void handle(Context appCtx, Intent intent);
		
	}
	
	/**
	 * Gets the concrete trigger class specific static trigger handler implementation.<BR/>
	 * Every concrete trigger class must implement Trigger.ITriggerHandler interface,<BR/>
	 * and return it's public static class TriggerHandler specific implementation of ITriggerHandler.
	 * 
	 * @see com.ruley.model.beans.triggers.base.Trigger.ITriggerHandler
	 * 
	 */
	public abstract ITriggerHandler getHandler();
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		/**
		 * Invoked when finish trigger registration.
		 */
		public void onRegistered();
		
		/**
		 * Invoked when finish trigger unregistration.
		 */
		public void onUnregistered();
		
		/**
		 * Invoked when trigger registration error occured.
		 */
		public void onError(int errorMessageResourceId);
		
	}
	
	protected transient Trigger.Listener listener = null;

	@JsonProperty("id")
	private UUID id;
	
	@JsonProperty("active")
	protected boolean active = false;

	@JsonProperty("satisfied")
	private boolean satisfied = false;

	public Trigger() {
		super();
		this.id = UUID.randomUUID();
	}
	
	public Trigger(Trigger otherTrigger) {
		super();
		this.id = UUID.randomUUID();
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

//	public void setActive(Context context, boolean active, Trigger.Listener listener) {
//		this.listener = listener;
//		// If the trigger is inactive and wanted state is active,
//		// then activate the trigger
//		if (!this.active && active) {
//			register(context);
//		// If the trigger is active and wanted state is inactive,
//		// then inactivate the trigger
//		} else if (this.active && !active) {
//			unregister(context);
//		}
//		// If the trigger already active and wanted state is active,
//		// then notify listener, but don't register it
//		} else if (this.active && active) {
//			LOGGER.info("\"" + this.getClass().getSimpleName() + "\" already activated");
//			if (this.listener != null) {
//				this.listener.onRegistered();
//			}
//		// If the trigger already inactive and wanted state is inactive,
//		// then notify listener, but don't unregister it
//		} else if (!this.active && !active) {
//			LOGGER.info("\"" + this.getClass().getSimpleName() + "\" already inactivated");
//			if (this.listener != null) {
//				this.listener.onUnregistered();
//			}
//		}
//	}
	
	public void setActive(Context context, boolean active, Trigger.Listener listener) {
		this.listener = listener;
		// If the trigger is inactive and wanted state is active,
		// then activate the trigger
		if (active) {
			register(context);
		// If the trigger is active and wanted state is inactive,
		// then inactivate the trigger
		} else if (!active) {
			unregister(context);
		}
	}

	public boolean isSatisfied() {
		return satisfied;
	}

	public void setSatisfied(boolean satisfied) {
		this.satisfied = satisfied;
	}

	/**
	 * Registers the trigger to the system. Derived class may implemenet this
	 * method. This is where the trigger registration logic implementation.
	 * 
	 * @param context
	 */
	protected void register(Context context) {
		this.active = true;
		LOGGER.info("\"" + this.getClass().getSimpleName() + "\" activated");
		if (this.listener != null) {
			this.listener.onRegistered();
		}
	}

	/**
	 * Unregisters the trigger to the system. Derived class may implement this
	 * method. This is where the trigger registration logic implementation.
	 * 
	 * @param context
	 */
	protected void unregister(Context context) {
		this.active = false;
		setSatisfied(false);
		LOGGER.info("\"" + this.getClass().getSimpleName() + "\" inactivated");
		if (this.listener != null) {
			this.listener.onUnregistered();
		}
	}

	@Override
	public Trigger toTemplate() {
		return getClone();
	}

	@Override
	public boolean equals(Object another) {
		if (another == null) return false;
	    if (another == this) return true;
	    if (!(another instanceof Trigger)) return false;
	    Trigger anotherTrigger = ((Trigger)another);
		return this.getId().equals(anotherTrigger.getId());
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.trigger);
	}

	@Override
	public String toString() {
		String jsonData = null;
		
		try {
			ObjectMapper objectMapper = JsonHelper.getInstance().getMapper();
			jsonData = objectMapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			 LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}
		
		return jsonData;
	}
	
}

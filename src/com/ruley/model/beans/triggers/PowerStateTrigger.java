/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.BatteryManager;

import com.ruley.R;
import com.ruley.activities.editors.triggers.PowerStateTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.StateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * This trigger handles broadcasts actions:
 * "android.intent.action.ACTION_POWER_CONNECTED"
 * "android.intent.action.ACTION_POWER_DISCONNECTED"
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class PowerStateTrigger extends StateTrigger {

	private static final long serialVersionUID = -5171610796691961801L;
	
	private static final String CLASS_NAME = PowerStateTrigger.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(PowerStateTrigger.class.getName());
	
	public static final String ACTION_POWER_CONNECTED = "android.intent.action.ACTION_POWER_CONNECTED";
	
	public static final String ACTION_POWER_DISCONNECTED = "android.intent.action.ACTION_POWER_DISCONNECTED";
	
	public static final int POWER_STATE_UNKNOWN = -1;
	
	public static final int POWER_DISCONNECTED = 0;
	
	public static final int POWER_CONNECTED = 1;
	
	private static int SAVED_POWER_STATE = POWER_STATE_UNKNOWN;
	
	@JsonProperty("wantedPowerState")
	private int wantedPowerState = POWER_CONNECTED;

	public int getWantedPowerState() {
		return wantedPowerState;
	}

	public void setWantedPowerState(int wantedPowerState) {
		this.wantedPowerState = wantedPowerState;
	}

	@Override
	public int getNameId() {
		return R.string.trigger_power_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.trigger_power_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String powerState = null;
		switch (wantedPowerState) {
		case POWER_CONNECTED:
			powerState = context.getString(R.string.trigger_power_state_connected);
			break;
		case POWER_DISCONNECTED:
			powerState = context.getString(R.string.trigger_power_state_disconnected);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.trigger_power_state_dynamic_description);
		return String.format(description, powerState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedPowerState) {
		case POWER_CONNECTED:
			return ImageUtils.getBitmap(R.drawable.power_connected);
		case POWER_DISCONNECTED:
			return ImageUtils.getBitmap(R.drawable.power_disconnected);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return PowerStateTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedPowerState) {
		case POWER_CONNECTED:
		case POWER_DISCONNECTED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Trigger getClone() {
		PowerStateTrigger trigger = new PowerStateTrigger();
		trigger.setWantedPowerState(getWantedPowerState());
		return trigger;
	}
	
	@Override
	protected void setInitialSatisfaction(Context context) {
		LOGGER.debug(CLASS_NAME + "#setInitialSatisfaction(...)");
		
		boolean isPowerConnected;
		Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		if (plugged == BatteryManager.BATTERY_PLUGGED_AC
			|| plugged == BatteryManager.BATTERY_PLUGGED_USB
			|| plugged == BatteryManager.BATTERY_PLUGGED_WIRELESS) {
			isPowerConnected = true;
		} else {
			isPowerConnected = false;
		}
		
		if ( (!isPowerConnected && wantedPowerState == POWER_DISCONNECTED)
				|| (isPowerConnected && wantedPowerState == POWER_CONNECTED) ) {
			setSatisfied(true);
		} else {
			setSatisfied(false);
		}
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");

			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(PowerStateTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			int currPowerState = -1;
			String action = intent.getAction();
			if (ACTION_POWER_CONNECTED.equals(action)) {
				currPowerState = POWER_CONNECTED;
			} else if (ACTION_POWER_DISCONNECTED.equals(action)) {
				currPowerState = POWER_DISCONNECTED;
			}
			
			// If the power state unknown, do nothing
			if (POWER_STATE_UNKNOWN == currPowerState) {
				LOGGER.info(CLASS_NAME + " The power state unknown");
				return;
			// If the power state didn't change, do nothing
			} else if (SAVED_POWER_STATE == currPowerState) {
				LOGGER.info(CLASS_NAME + " The power state didn't changed");
				return;
			// If the power state did change, continue to handle the trigger
			} else {
				LOGGER.info(CLASS_NAME + " The power state did changed");
				SAVED_POWER_STATE = currPowerState;
			}

			for (Trigger trigger : triggers) {
				PowerStateTrigger powerStateTrigger = (PowerStateTrigger) trigger;
				if (powerStateTrigger.getWantedPowerState() == currPowerState) {
					powerStateTrigger.setSatisfied(true);
				} else {
					powerStateTrigger.setSatisfied(false);
				}
				triggerDAO.store(powerStateTrigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(PowerStateTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

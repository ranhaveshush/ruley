/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.telephony.SmsMessage;

import com.ruley.R;
import com.ruley.activities.editors.triggers.ReceivedSMSTriggerEditorActivity;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ReceivedSMSTrigger extends Trigger {
	
	private static final long serialVersionUID = 4462093350354911653L;

	private static final String CLASS_NAME = ReceivedSMSTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ReceivedSMSTrigger.class.getName());
	
	public static final String ACTION_RECEIVED_SMS = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) ? "android.provider.Telephony.SMS_RECEIVED_ACTION"
			: "android.provider.Telephony.SMS_RECEIVED";
	
	private static List<SmsMessage> sReceivedMessages = null;
	
	public static boolean isReceivedMessagesEmpty() {
		return sReceivedMessages == null || sReceivedMessages.isEmpty();
	}
	
	public static List<SmsMessage> getReceivedMessages() {
		return sReceivedMessages;
	}

	public static void setReceivedSmses(List<SmsMessage> receivedSmses) {
		sReceivedMessages = receivedSmses;
	}
	
	@Override
	public int getNameId() {
		return R.string.trigger_received_sms_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.trigger_received_sms_static_description;
	}

	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.trigger_received_sms_dynamic_description);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.sms4);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ReceivedSMSTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Trigger getClone() {
		ReceivedSMSTrigger trigger = new ReceivedSMSTrigger();
		return trigger;
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {

		@Override
		public void handle(Context appCtx, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			// Gets the SMSs from the intent
			List<SmsMessage> smses = new ArrayList<SmsMessage>();
			Object[] pdus = (Object[]) intent.getExtras().get("pdus");
			for (Object pdu : pdus) {
				SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
				smses.add(smsMessage);
			}
			
			// Validates there are any SMSs
			if (smses == null || smses.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " there aren't any SMSs");
				return;
			}
			
			setReceivedSmses(smses);
			
			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			List<Trigger> triggers = triggerDAO.list(ReceivedSMSTrigger.class);

			if (triggers == null || triggers.isEmpty()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(true);
				triggerDAO.store(trigger);
			}

			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			List<Rule> rules = ruleDAO.list(ReceivedSMSTrigger.class, true, true);

			if (rules != null && !rules.isEmpty()) {
				for (Rule rule : rules) {
					rule.perform(appCtx);
				}
			}

			for (Trigger trigger : triggers) {
				trigger.setSatisfied(false);
				triggerDAO.store(trigger);
			}
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}	

}

/**
 * 
 */
package com.ruley.model.beans.triggers;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.UUID;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.triggers.DateTimeTriggerEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DateTimeTrigger extends Trigger {

	private static final long serialVersionUID = 5640804990813210612L;
	
	private static final String CLASS_NAME = DateTimeTrigger.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateTimeTrigger.class.getName());
	
	public static final String ACTION_DATE_TIME = Config.APP_PACKAGE + ".ACTION_DATE_TIME";
	
	public static final Locale LOCALE = Locale.getDefault();
	
	public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yy", LOCALE);
	public static final SimpleDateFormat SIMPLE_TIME_FORMAT = new SimpleDateFormat("HH:mm", LOCALE);
	
	public static final long INTERVAL_WEEK = AlarmManager.INTERVAL_DAY * 7;
	
	@JsonProperty("dateTime")
	private GregorianCalendar dateTime = new GregorianCalendar(LOCALE);
	
	@JsonProperty("isRepeat")
	private boolean isRepeat = false;
	
	@JsonProperty("daysOfTheWeek")
	private boolean[] daysOfTheWeek = new boolean[7];

	public GregorianCalendar getDateTime() {
		return dateTime;
	}

	public void setDateTime(GregorianCalendar dateTime) {
		this.dateTime = dateTime;
	}

	public boolean isRepeat() {
		return isRepeat;
	}

	public void setRepeat(boolean isRepeat) {
		this.isRepeat = isRepeat;
	}
	
	public boolean[] getDaysOfTheWeek() {
		return daysOfTheWeek;
	}
	
	public boolean isRepeatDayOfTheWeek(int index) {
		if (0 <= index && index <= 7) {
			return daysOfTheWeek[index];
		} else {
			return false;
		}
	}
	
	public void setRepeatDayOfTheWeek(int index, boolean isRepeat) {
		if (0 <= index && index <= 7) {
			daysOfTheWeek[index] = isRepeat;
		}
	}

	@Override
	protected void register(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager == null) {
			return;
		}
		
		registerAlarm(context, alarmManager);
		
		super.register(context);
	}

	@Override
	protected void unregister(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager == null) {
			return;
		}
		
		unregisterAlarm(context, alarmManager);
		
		super.unregister(context);
	}

	@Override
	public int getNameId() {
		return R.string.trigger_date_time_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.trigger_date_time_static_description;
	}

	@Override
	public String getDescription(Context context) {
		if (isRepeat) {
			String selectedDaysOfTheWeekStr = getSelectedDaysOfTheWeekString();
			if (selectedDaysOfTheWeekStr != null) {
				String timeStr = SIMPLE_TIME_FORMAT.format(dateTime.getTime());
				String description = context.getString(R.string.trigger_date_time_repeat_dynamic_description);
				return String.format(description, selectedDaysOfTheWeekStr, timeStr);
			} else {
				return context.getString(R.string.trigger_date_time_repeat_days_missing);
			}
		} else {
			String description = context.getString(R.string.trigger_date_time_once_dynamic_description);
			String timeStr = SIMPLE_TIME_FORMAT.format(dateTime.getTime());
			String dateStr = SIMPLE_DATE_FORMAT.format(dateTime.getTime());
			String dateTimeStr = timeStr + ", " + dateStr;
			return String.format(description, dateTimeStr);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.date_time);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return DateTimeTriggerEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		boolean valid = false;
		
		if (isRepeat) {
			for (boolean dayOfTheWeek : daysOfTheWeek) {
				if (dayOfTheWeek) {
					valid = true;
				}
			}
		} else {
			valid = true;
		}
		
		return valid;
	}
	
	@Override
	public Trigger getClone() {
		DateTimeTrigger trigger = new DateTimeTrigger();
		trigger.setDateTime(getDateTime());
		trigger.setRepeat(isRepeat());
		boolean[] repeatDaysOfTheWeek = getDaysOfTheWeek();
		for (int i=0; i<repeatDaysOfTheWeek.length; ++i) {
			trigger.setRepeatDayOfTheWeek(i, repeatDaysOfTheWeek[i]);
		}
		return trigger;
	}
	
	@SuppressLint("NewApi")
	private void registerAlarm(Context context, AlarmManager alarmManager) {
		if (isRepeat) {
			for (int i=0; i < daysOfTheWeek.length; ++i) {
				if (daysOfTheWeek[i]) {
					PendingIntent alarmPendingIntent = getAlarmPendingIntent(context,
							PendingIntent.FLAG_UPDATE_CURRENT, i+1);
					setWeekRepeatAlarm(alarmManager, alarmPendingIntent, i+1);
				}
			}
		} else {
			PendingIntent alarmPendingIntent = getAlarmPendingIntent(context,
					PendingIntent.FLAG_UPDATE_CURRENT, 0);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, dateTime.getTimeInMillis(), alarmPendingIntent);
			} else {
				alarmManager.set(AlarmManager.RTC_WAKEUP, dateTime.getTimeInMillis(), alarmPendingIntent);
			}
		}
	}
	
	private void unregisterAlarm(Context context, AlarmManager alarmManager) {
		if (isRepeat) {
			for (int i=0; i < daysOfTheWeek.length; ++i) {
				if (daysOfTheWeek[i]) {
					PendingIntent alarmPendingIntent = getAlarmPendingIntent(context,
							PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT, i+1);
					if (alarmPendingIntent != null) {
						alarmManager.cancel(alarmPendingIntent);
					}
				}
			}
		} else {
			PendingIntent alarmPendingIntent = getAlarmPendingIntent(context,
					PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT, 0);
			if (alarmPendingIntent != null) {
				alarmManager.cancel(alarmPendingIntent);
			}
		}
	}
	
	/**
	 * @param alarmManager
	 * @param alarmPendingIntent
	 * @param dayOfTheWeek
	 *            Integer represents the day of the week starting from 1
	 *            (Convenience use Calendar.SUNDAY ...)
	 * 
	 * @see java.util.Calendar
	 */
	private void setWeekRepeatAlarm(AlarmManager alarmManager, PendingIntent alarmPendingIntent, int dayOfTheWeek) {
		Calendar calendar = Calendar.getInstance(LOCALE);
		int today = calendar.get(Calendar.DAY_OF_WEEK);
		int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		calendar.set(Calendar.DAY_OF_WEEK, dayOfTheWeek);
		calendar.set(Calendar.HOUR_OF_DAY, dateTime.get(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, dateTime.get(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		// If day of the week before today,
		// sets the alarm to start from next week
		if (dayOfTheWeek < today) {
			calendar.setTimeInMillis(calendar.getTimeInMillis() + INTERVAL_WEEK);
		// If day is today and datetime passed end datetime,
		// sets the alarm to start from next week
		} else if (dayOfTheWeek == today) {
			int dateTimeHourOfDay = dateTime.get(Calendar.HOUR_OF_DAY);
			int dateTimeMinute = dateTime.get(Calendar.MINUTE);
			if ((dateTimeHourOfDay < hourOfDay)
					|| (dateTimeHourOfDay == hourOfDay && dateTimeMinute <= minute)) {
				calendar.setTimeInMillis(calendar.getTimeInMillis() + INTERVAL_WEEK);
			}
		}
		
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), INTERVAL_WEEK, alarmPendingIntent);
	}
	
	private String getSelectedDaysOfTheWeekString() {
		String[] shortWeekdays = DateFormatSymbols.getInstance(LOCALE).getShortWeekdays();
		String selectedDaysOfTheWeekStr = "";
		for (int i=0; i < daysOfTheWeek.length; ++i) {
			if (daysOfTheWeek[i]) {
				selectedDaysOfTheWeekStr += shortWeekdays[i+1] + ", ";
			}
		}
		if (!TextUtils.isEmpty(selectedDaysOfTheWeekStr)) {
			return selectedDaysOfTheWeekStr.substring(0, selectedDaysOfTheWeekStr.length() - 2);
		} else {
			return null;
		}
	}
	
	private PendingIntent getAlarmPendingIntent(Context context, int flags, int dayOfTheWeek) {
		Intent alarmIntent = new Intent(ACTION_DATE_TIME);
		alarmIntent.putExtra(Config.Extra.EXTRA_TRIGGER_ID, getId());
		PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context,
				(int) (this.getId().getLeastSignificantBits()) + dayOfTheWeek, alarmIntent, flags);
		return alarmPendingIntent;
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public static class TriggerHandler implements ITriggerHandler {
		
		@Override
		public void handle(Context ctxApp, Intent intent) {
			LOGGER.debug(CLASS_NAME + "#handle(...)");
			
			UUID triggerId = (UUID) intent.getSerializableExtra(Config.Extra.EXTRA_TRIGGER_ID);
			
			ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
			Trigger trigger = triggerDAO.get(triggerId);
			
			if (trigger == null || !trigger.isActive()) {
				LOGGER.warn(CLASS_NAME + " wasn't defined by the user");
				return;
			}
			
			trigger.setSatisfied(true);
			triggerDAO.store(trigger);
			
			IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
			Rule rule = ruleDAO.get(triggerId, true, true);
			
			if (rule != null) {
				rule.perform(ctxApp);
			}
			
			trigger.setSatisfied(false);
			triggerDAO.store(trigger);
		}
		
	}

	@Override
	public ITriggerHandler getHandler() {
		return new TriggerHandler();
	}

}

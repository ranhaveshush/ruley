/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.MainDispatchActivity;
import com.ruley.activities.editors.actions.NotificationActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class NotificationAction extends Action {

	private static final long serialVersionUID = 4216179750856890786L;
	
	private static final String CLASS_NAME = NotificationAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NotificationAction.class.getName());

	@JsonProperty("title")
	private String title = null;

	@JsonProperty("text")
	private String text = null;

	/**
	 * Integer represents the notification use defaults.
	 * 
	 * <p>
	 * Use all: Notification.DEFAULT_ALL<br/>
	 * Use lights: Notification.DEFAULT_LIGHTS<br/>
	 * Use sound: Notification.DEFAULT_SOUND<br/>
	 * Use vibrate: Notification.DEFAULT_VIBRATE
	 * </p>
	 * 
	 * For notification defaults info:
	 * 
	 * @see android.app.Notification
	 * 
	 */
	@JsonProperty("defaults")
	private int defaults = Notification.DEFAULT_SOUND;

	/**
	 * <p>
	 * Integer represents the notification flags.
	 * </p>
	 * 
	 * For notification flags info:
	 * 
	 * @see android.app.Notification
	 * 
	 */
	@JsonProperty("flags")
	private int flags = 0;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getDefaults() {
		return defaults;
	}

	public void setDefaults(int defaults) {
		this.defaults = defaults;
	}
	
	public void addDefaults(int defaultsToAdd) {
		this.defaults |= defaultsToAdd;
	}

	public void removeDefaults(int defaultsToRemove) {
		this.defaults &= ~defaultsToRemove;
	}
	
	public boolean hasDefaults(int defaultsToCheck) {
		return (this.defaults & defaultsToCheck) == defaultsToCheck;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}
	
	public void addFlag(int flagsToAdd) {
		this.flags |= flagsToAdd;
	}

	public void removeFlag(int flagsToRemove) {
		this.flags &= ~flagsToRemove;
	}
	
	public boolean hasFlags(int flagsToCheck) {
		return (this.flags & flagsToCheck) == flagsToCheck;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		int notificationId = (int) getId().getLeastSignificantBits();
		Intent intent = new Intent(context, MainDispatchActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, notificationId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		// Constructs the notification
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
	    .setSmallIcon(R.drawable.ic_stat_notification)
	    .setContentTitle(title)
	    .setContentText(text)
		.setDefaults(defaults)
		.setContentIntent(pendingIntent);
		Notification notification = builder.build();
		notification.ledARGB = 0xff00ff00;
		notification.ledOnMS = 300;
		notification.ledOffMS = 1000;
		notification.flags = flags;

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(notificationId, notification);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_notification_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_notification_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (TextUtils.isEmpty(title)) {
			return context.getString(R.string.action_notification_title_missing);
		} else {
			String description = context.getString(R.string.action_notification_dynamic_description);
			return String.format(description, title);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.notification);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return NotificationActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(title);
	}
	
	@Override
	public Action getClone() {
		NotificationAction action = new NotificationAction();
		action.setTitle(getTitle());
		action.setText(getText());
		action.setDefaults(getDefaults());
		action.setFlags(getFlags());
		return action;
	}

}

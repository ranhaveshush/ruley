/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;
import android.provider.Settings.System;

import com.ruley.R;
import com.ruley.activities.editors.actions.ScreenOffTimeoutActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ScreenOffTimeoutAction extends Action {

	private static final long serialVersionUID = 6425780287379449664L;
	
	private static final String CLASS_NAME = ScreenOffTimeoutAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenOffTimeoutAction.class.getName());
	
	private static final int NOT_FOUND = -1;

	private static final int SECOND = 1000;
	
	private static final int MINUTE = 60*SECOND;
	
	public static final Map<String, Integer> TIMEOUTS = Collections.unmodifiableMap(new LinkedHashMap<String, Integer>() {
		{
			put("15 seconds", 15*SECOND);
			put("30 seconds", 30*SECOND);
			put("1 minute", MINUTE);
			put("2 minutes", 2*MINUTE);
			put("5 minutes", 5*MINUTE);
			put("10 minutes", 10*MINUTE);
			put("30 minutes", 30*MINUTE);
		}
	});
	
	@JsonProperty("wantedScreenOffTimeout")
	private String wantedScreenOffTimeout = "30 seconds";

	public String getWantedScreenOffTimeout() {
		return wantedScreenOffTimeout;
	}

	public void setWantedScreenOffTimeout(String wantedScreenOffTimeout) {
		this.wantedScreenOffTimeout = wantedScreenOffTimeout;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
				
		int currScreenOffTimeout = Settings.System.getInt(context.getContentResolver(), System.SCREEN_OFF_TIMEOUT, NOT_FOUND);
		if (currScreenOffTimeout == NOT_FOUND) {
			LOGGER.warn(CLASS_NAME + "#perform(...) invalid settings " + System.SCREEN_OFF_TIMEOUT);
			return;
		}
		Settings.System.putInt(context.getContentResolver(),
					System.SCREEN_OFF_TIMEOUT, TIMEOUTS.get(wantedScreenOffTimeout));
		currScreenOffTimeout = Settings.System.getInt(context.getContentResolver(), System.SCREEN_OFF_TIMEOUT, NOT_FOUND);
		
		if (currScreenOffTimeout != NOT_FOUND) {
			LOGGER.info("Screen off wantedScreenOffTimeout changed to: " + wantedScreenOffTimeout);
		} else {
			LOGGER.info("Screen off timeout already at the wanted state");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_screen_off_timeout_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_screen_off_timeout_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String description = context.getString(R.string.action_screen_off_timeout_dynamic_description);
		return String.format(description , wantedScreenOffTimeout);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.screen_sleep_timeout);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ScreenOffTimeoutActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return TIMEOUTS.containsKey(wantedScreenOffTimeout);
	}
	
	@Override
	public Action getClone() {
		ScreenOffTimeoutAction action = new ScreenOffTimeoutAction();
		action.setWantedScreenOffTimeout(getWantedScreenOffTimeout());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;

import com.ruley.R;
import com.ruley.activities.editors.actions.ToggleStayOnWhilePluggedInActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToggleStayOnWhilePluggedInAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ToggleStayOnWhilePluggedInAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToggleStayOnWhilePluggedInAction.class.getName());
	
	private static final int NOT_FOUND = -1;
	
	public static final int STAY_ON_WHILE_PLUGGED_IN_ENABLED = 1;
	
	public static final int STAY_ON_WHILE_PLUGGED_IN_DISABLED = 0;

	/**
	 * For more info:
	 * 
	 * @see android.provider.Settings.System#STAY_ON_WHILE_PLUGGED_IN
	 */
	@JsonProperty("wantedStayOnWhilePluggedInState")
	private int wantedStayOnWhilePluggedInState = STAY_ON_WHILE_PLUGGED_IN_ENABLED;

	public int getWantedStayOnWhilePluggedInState() {
		return wantedStayOnWhilePluggedInState;
	}

	public void setWantedStayOnWhilePluggedInState(
			int wantedStayOnWhilePluggedInState) {
		this.wantedStayOnWhilePluggedInState = wantedStayOnWhilePluggedInState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String stayOnWhilePluggedInName = Settings.System.STAY_ON_WHILE_PLUGGED_IN;
		int currStayOnWhilePluggedInState = Settings.System.getInt(context.getContentResolver(), stayOnWhilePluggedInName, NOT_FOUND);
		if (currStayOnWhilePluggedInState == NOT_FOUND) {
			LOGGER.warn(CLASS_NAME + "#perform(...) invalid settings " + stayOnWhilePluggedInName);
			return;
		}
		Settings.System.putInt(context.getContentResolver(), stayOnWhilePluggedInName, wantedStayOnWhilePluggedInState);
		currStayOnWhilePluggedInState = Settings.System.getInt(context.getContentResolver(), stayOnWhilePluggedInName, NOT_FOUND);
		
		switch (currStayOnWhilePluggedInState) {
		case STAY_ON_WHILE_PLUGGED_IN_ENABLED:
			LOGGER.info("Stay on while plugged in ENABLED");
			break;
		case STAY_ON_WHILE_PLUGGED_IN_DISABLED:
			LOGGER.info("Stay on while plugged in DISABLED");
			break;
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_toggle_stay_on_while_plugged_in_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_toggle_stay_on_while_plugged_in_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String stayOnWhilePluggedInState = null;
		switch (wantedStayOnWhilePluggedInState) {
		case STAY_ON_WHILE_PLUGGED_IN_ENABLED:
			stayOnWhilePluggedInState = context.getString(R.string.action_toggle_stay_on_while_plugged_in_on);
			break;
		case STAY_ON_WHILE_PLUGGED_IN_DISABLED:
			stayOnWhilePluggedInState = context.getString(R.string.action_toggle_stay_on_while_plugged_in_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_toggle_stay_on_while_plugged_in_dynamic_description);
		return String.format(description, stayOnWhilePluggedInState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedStayOnWhilePluggedInState) {
		case STAY_ON_WHILE_PLUGGED_IN_ENABLED:
			return ImageUtils.getBitmap(R.drawable.plugged);
		case STAY_ON_WHILE_PLUGGED_IN_DISABLED:
			return ImageUtils.getBitmap(R.drawable.unplugged);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ToggleStayOnWhilePluggedInActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedStayOnWhilePluggedInState) {
		case STAY_ON_WHILE_PLUGGED_IN_ENABLED:
		case STAY_ON_WHILE_PLUGGED_IN_DISABLED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ToggleStayOnWhilePluggedInAction action = new ToggleStayOnWhilePluggedInAction();
		action.setWantedStayOnWhilePluggedInState(getWantedStayOnWhilePluggedInState());
		return action;
	}

}

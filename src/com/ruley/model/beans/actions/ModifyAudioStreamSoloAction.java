/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.Map.Entry;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ModifyAudioStreamSoloActionEditorActivity;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.AudioStreamsUtils;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ModifyAudioStreamSoloAction extends Action {

	private static final long serialVersionUID = -3013284038569483636L;
	
	private static final String CLASS_NAME = ModifyAudioStreamSoloAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyAudioStreamSoloAction.class.getName());
	
	public static boolean AUDIO_STREAM_SOLO = true;
	
	public static boolean AUDIO_STREAM_UNSOLO = false;

	/**
	 * Holds the audio stream.
	 * 
	 * <P>
	 * AudioManager.STREAM_ALARM:<BR/>
	 * The audio stream for alarms.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_DTMF:<BR/>
	 * The audio stream for DTMF Tones.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_MUSIC:<BR/>
	 * The audio stream for music playback.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_NOTIFICATION:<BR/>
	 * The audio stream for notifications.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_RING:<BR/>
	 * The audio stream for the phone ring.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_SYSTEM:<BR/>
	 * The audio stream for system sounds.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_VOICE_CALL:<BR/>
	 * The audio stream for phone calls.
	 * </P>
	 * 
	 * <P>
	 * Holds the audio stream type.<BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager
	 * @see android.media.AudioManager#setStreamSolo(int streamType, boolean state)
	 */
	@JsonProperty("audioStreamType")
	private int audioStreamType = AudioManager.STREAM_ALARM;

	/**
	 * <P>
	 * Holds the audio stream solo state.<BR/>
	 * The required solo state: true for solo ON, false for unsolo OFF.
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager
	 * @see android.media.AudioManager#setStreamSolo(int streamType, boolean state)
	 */
	@JsonProperty("audioStreamSolo")
	private boolean audioStreamSolo = AUDIO_STREAM_SOLO;

	public int getAudioStreamType() {
		return audioStreamType;
	}

	public void setAudioStreamType(int audioStreamType) {
		if (this.audioStreamType != audioStreamType) {
			unsoloAudioStream(this.audioStreamType);
			this.audioStreamType = audioStreamType;
		}
	}

	public boolean isAudioStreamSolo() {
		return audioStreamSolo;
	}

	public void setAudioStreamSolo(boolean audioStreamSolo) {
		this.audioStreamSolo = audioStreamSolo;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		Context appCtx = MyApplication.getContext();
		
		String audioStreamName = AudioStreamsUtils.audioStreamsTypeToNameMap.get(audioStreamType);
		String audioStreamSoloState = null;

		// Changes audio stream solo settings
		// If wanted state is solo, sets other audio streams volumes to 0
		if (audioStreamSolo && soloAudioStream(audioStreamType)) {
			audioStreamSoloState = appCtx.getString(R.string.action_modify_audio_stream_solo);
			LOGGER.info(audioStreamName + " " + audioStreamSoloState);
		// If wanted state is unsolo, sets previous audio streams volumes back
		} else if (!audioStreamSolo && unsoloAudioStream(audioStreamType)) {
			audioStreamSoloState = appCtx.getString(R.string.action_modify_audio_stream_unsolo);
			LOGGER.info(audioStreamName + " " + audioStreamSoloState);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_modify_audio_stream_solo_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_modify_audio_stream_solo_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		Context appCtx = MyApplication.getContext();
		
		String description = context.getString(R.string.action_modify_audio_stream_solo_dynamic_description);
		String audioStreamName = AudioStreamsUtils.audioStreamsTypeToNameMap.get(audioStreamType);
		String audioStreamSoloState = null;
		if (audioStreamSolo) {
			audioStreamSoloState = appCtx.getString(R.string.action_modify_audio_stream_solo);
		} else {
			audioStreamSoloState = appCtx.getString(R.string.action_modify_audio_stream_unsolo);
		}
		return String.format(description, audioStreamSoloState, audioStreamName);
	}

	@Override
	public Bitmap getIcon() {
		if (audioStreamSolo) {
			return ImageUtils.getBitmap(R.drawable.solo);
		} else {
			return ImageUtils.getBitmap(R.drawable.unsolo);
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ModifyAudioStreamSoloActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (audioStreamType) {
		case AudioManager.STREAM_ALARM:
		case AudioManager.STREAM_DTMF:
		case AudioManager.STREAM_MUSIC:
		case AudioManager.STREAM_NOTIFICATION:
		case AudioManager.STREAM_RING:
		case AudioManager.STREAM_SYSTEM:
		case AudioManager.STREAM_VOICE_CALL:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ModifyAudioStreamMuteAction action = new ModifyAudioStreamMuteAction();
		action.setAudioStreamType(getAudioStreamType());
		action.setAudioStreamMuted(isAudioStreamSolo());
		return action;
	}
	
	private boolean isAudioStreamSolo(int audioStream) {
		Context appCtx = MyApplication.getContext();
		
		AudioManager audioManager = (AudioManager) appCtx.getSystemService(Service.AUDIO_SERVICE);
		
		for (Entry<Integer, Integer> entry : AudioStreamsUtils.audioStreamsTypeToPosMap.entrySet()) {
			int currAudioStream = entry.getKey();
			int currVolume = audioManager.getStreamVolume(currAudioStream);
			
			// Checks if all the audio streams other then the solo are muted
			if (currAudioStream != audioStream && currVolume > 0) {
				return false;
			}
		}
		
		return true;
	}
	
	private boolean soloAudioStream(int audioStreamToSolo) {
		Context appCtx = MyApplication.getContext();
		
		// If the audio stream is already solo, do nothing
		if (isAudioStreamSolo(audioStreamToSolo)) {
			String audioStreamName = AudioStreamsUtils.audioStreamsTypeToNameMap.get(audioStreamToSolo);
			String audioStreamSoloState = appCtx.getString(R.string.action_modify_audio_stream_solo);
			LOGGER.info(audioStreamName + " already " + audioStreamSoloState);
			return false;
		}
		
		AudioManager audioManager = (AudioManager) appCtx.getSystemService(Service.AUDIO_SERVICE);
		
		for (Entry<Integer, Integer> entry : AudioStreamsUtils.audioStreamsTypeToPosMap.entrySet()) {
			int audioStream = entry.getKey();

			if (audioStreamToSolo != audioStream) {
				// Mutes the audio stream
				audioManager.setStreamVolume(audioStream, 0, 0);
			}
		}
		
		return true;
	}
	
	private boolean unsoloAudioStream(int audioStreamToUnsolo) {
		Context appCtx = MyApplication.getContext();
		
		// If the audio stream is already unsolo, do nothing
		if (!isAudioStreamSolo(audioStreamToUnsolo)) {
			String audioStreamName = AudioStreamsUtils.audioStreamsTypeToNameMap.get(audioStreamToUnsolo);
			String audioStreamSoloState = appCtx.getString(R.string.action_modify_audio_stream_unsolo);
			LOGGER.info(audioStreamName + " already " + audioStreamSoloState);
			return false;
		}
		
		AudioManager audioManager = (AudioManager) appCtx.getSystemService(Service.AUDIO_SERVICE);
		
		for (Entry<Integer, Integer> entry : AudioStreamsUtils.audioStreamsTypeToPosMap.entrySet()) {
			int audioStream = entry.getKey();

			if (audioStreamToUnsolo != audioStream) {
				int currVolume = audioManager.getStreamVolume(audioStream);
				// Sets audio stream volume, only if the audio stream is muted
				if (currVolume == 0) {
					int streamMaxVolume = audioManager.getStreamMaxVolume(audioStream);
					audioManager.setStreamVolume(audioStream, streamMaxVolume, 0);
				}
			}
		}
		
		return true;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ModifyMicrophoneMuteActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ModifyMicrophoneMuteAction extends Action {

	private static final long serialVersionUID = -4703821329828171357L;
	
	private static final String CLASS_NAME = ModifyMicrophoneMuteAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ModifyMicrophoneMuteAction.class.getName());

	/**
	 * Holds the wanted microphone mute.
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager#setMicrophoneMute(boolean on)
	 */
	@JsonProperty("wantedMicrophoneMute")
	private boolean wantedMicrophoneMute = true;

	public boolean isWantedMicrophoneMute() {
		return wantedMicrophoneMute;
	}

	public void setWantedMicrophoneMute(boolean wantedMicrophoneMute) {
		this.wantedMicrophoneMute = wantedMicrophoneMute;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		// Changes microphone mute settings
		audioManager.setMicrophoneMute(wantedMicrophoneMute);
		
		if (wantedMicrophoneMute) {
			LOGGER.info("Microphone mute on");
		} else {
			LOGGER.info("Microphone mute off");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_modify_microphone_mute_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_modify_microphone_mute_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String ringerMode = null;
		if (wantedMicrophoneMute) {
			ringerMode = context.getString(R.string.action_modify_microphone_mute_on);
		} else {
			ringerMode = context.getString(R.string.action_modify_microphone_mute_off);
		}
		String description = context.getString(R.string.action_modify_microphone_mute_dynamic_description);
		return String.format(description, ringerMode);
	}

	@Override
	public Bitmap getIcon() {
		if (wantedMicrophoneMute) {
			return ImageUtils.getBitmap(R.drawable.microphone);
		} else {
			return ImageUtils.getBitmap(R.drawable.microphone);
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ModifyMicrophoneMuteActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		ModifyMicrophoneMuteAction action = new ModifyMicrophoneMuteAction();
		action.setWantedMicrophoneMute(isWantedMicrophoneMute());
		return action;
	}

}

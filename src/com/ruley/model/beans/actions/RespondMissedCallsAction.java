/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.RespondMissedCallsActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.services.TextToSpeechService;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RespondMissedCallsAction extends Action {

	private static final long serialVersionUID = -8180208607021865486L;

	private static final String CLASS_NAME = RespondMissedCallsAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RespondMissedCallsAction.class.getName());
	
	@JsonProperty("responseMessage")
	private String responseMessage = null;

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		Uri uri = Calls.CONTENT_URI;
		String[] projection = new String[] {
				CallLog.Calls.NUMBER,
				CallLog.Calls.CACHED_NAME
			};
		String selection = Calls.TYPE + " = ? AND " + Calls.NEW + " = ?";
		String[] selectionArgs = new String[] { Integer.toString(Calls.MISSED_TYPE), "1" };
		
		Cursor cursor = context.getContentResolver().query(uri,
				projection, selection, selectionArgs, null);
		
		// If there are none missed calls, do nothing
		if (cursor.getCount() < 1) {
			cursor.close();
			LOGGER.warn("There are none missed calls");
			return;
		}
		
		SmsManager smsManager = SmsManager.getDefault();
		
		String responseMessage = this.responseMessage + context.getString(R.string.action_respond_missed_calls_signature);
		ArrayList<String> messageParts = smsManager.divideMessage(responseMessage);
		
		while (cursor.moveToNext()) {
			String phoneNumber = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
			LOGGER.debug(CLASS_NAME + "#perform(...) phoneNumber = " + phoneNumber);
			String contactName = cursor.getString(cursor.getColumnIndex(CallLog.Calls.CACHED_NAME));
			LOGGER.debug(CLASS_NAME + "#perform(...) contactName = " + contactName);
			
			// Validates the phone number, before sending SMS
			if (!TextUtils.isEmpty(phoneNumber) && PhoneNumberUtils.isWellFormedSmsAddress(phoneNumber)) {
				// Sends SMS message
				smsManager.sendMultipartTextMessage(phoneNumber, null, messageParts, null, null);
				
				String from = (!TextUtils.isEmpty(contactName)) ? contactName : phoneNumber;
				String speak = "Texting to " + from + ": " + this.responseMessage;
				LOGGER.info(CLASS_NAME + "#handle(...) speak = " + speak);
				
				TextToSpeechService.speek(speak);
			}
		}
		
		cursor.close();
	}
	
	@Override
	public int getNameId() {
		return R.string.action_respond_missed_calls_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_respond_missed_calls_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_respond_missed_calls_dynamic_description);
	}
	
	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.phone_ringing);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return RespondMissedCallsActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		RespondMissedCallsAction action = new RespondMissedCallsAction();
		action.setResponseMessage(getResponseMessage());
		return action;
	}

}

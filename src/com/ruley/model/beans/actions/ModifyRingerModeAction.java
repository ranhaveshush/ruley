/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ModifyRingerModeActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ModifyRingerModeAction extends Action {

	private static final long serialVersionUID = -4703821329828171357L;
	
	private static final String CLASS_NAME = ModifyRingerModeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ModifyRingerModeAction.class.getName());

	/**
	 * Holds the wanted ringer mode.
	 * 
	 * <P>
	 * AudioManager.RINGER_MODE_NORMAL:<BR/>
	 * Ringer mode that may be audible and may vibrate.<BR/>
	 * <BR/>
	 * AudioManager.RINGER_MODE_SILENT:<BR/>
	 * Ringer mode that will be silent and will not vibrate.<BR/>
	 * <BR/>
	 * AudioManager.RINGER_MODE_VIBRATE:<BR/>
	 * Ringer mode that will be silent and will vibrate.<BR/>
	 * <BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager#setRingerMode(int ringerMode)
	 */
	@JsonProperty("wantedRingerMode")
	private int wantedRingerMode = AudioManager.RINGER_MODE_SILENT;

	public int getWantedRingerMode() {
		return wantedRingerMode;
	}

	public void setWantedRingerMode(int wantedRingerMode) {
		this.wantedRingerMode = wantedRingerMode;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		// Changes ringer mode settings
		audioManager.setRingerMode(wantedRingerMode);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_modify_ringer_mode_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_modify_ringer_mode_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String ringerMode = null;
		switch (wantedRingerMode) {
		case AudioManager.RINGER_MODE_NORMAL:
			ringerMode = context.getString(R.string.action_modify_ringer_mode_normal);
			break;
		case AudioManager.RINGER_MODE_SILENT:
			ringerMode = context.getString(R.string.action_modify_ringer_mode_silent);
			break;
		case AudioManager.RINGER_MODE_VIBRATE:
			ringerMode = context.getString(R.string.action_modify_ringer_mode_vibrate);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_modify_ringer_mode_dynamic_description);
		return String.format(description, ringerMode);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedRingerMode) {
		case AudioManager.RINGER_MODE_NORMAL:
			return ImageUtils.getBitmap(R.drawable.ringer_normal);
		case AudioManager.RINGER_MODE_SILENT:
			return ImageUtils.getBitmap(R.drawable.ringer_silent);
		case AudioManager.RINGER_MODE_VIBRATE:
			return ImageUtils.getBitmap(R.drawable.ringer_vibrate);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ModifyRingerModeActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedRingerMode) {
		case AudioManager.RINGER_MODE_NORMAL:
		case AudioManager.RINGER_MODE_SILENT:
		case AudioManager.RINGER_MODE_VIBRATE:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ModifyRingerModeAction action = new ModifyRingerModeAction();
		action.setWantedRingerMode(getWantedRingerMode());
		return action;
	}

}

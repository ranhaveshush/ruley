/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;

import com.ruley.R;
import com.ruley.activities.editors.actions.TestActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.services.MyNotificationListenerService;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TestAction extends Action {

	private static final long serialVersionUID = 4550375853437775148L;

	private static final String CLASS_NAME = TestAction.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestAction.class.getName());
	
	public static final int NOTIFICATION_LISTENER_ON = 1;
	
	public static final int NOTIFICATION_LISTENER_OFF = 2;
	
	@JsonProperty("wantedNotifcationListenerState")
	private int wantedNotifcationListenerState = NOTIFICATION_LISTENER_ON;
	
	@JsonProperty("excludedAppsPackageNames")
	private Set<String> excludedAppsPackageNames = new HashSet<String>();
	
	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
	
		if (wantedNotifcationListenerState == NOTIFICATION_LISTENER_ON) {
			MyNotificationListenerService.startService();
			excludedAppsPackageNames.add("android");
			excludedAppsPackageNames.add("com.android.vending");
			excludedAppsPackageNames.add("com.google.android.deskclock");
			excludedAppsPackageNames.add("com.google.android.googlequicksearchbox");
			excludedAppsPackageNames.add("com.android.providers.downloads");
			excludedAppsPackageNames.add("com.whatsapp");
			excludedAppsPackageNames.add("com.google.android.dialer");
			MyNotificationListenerService.exclude(excludedAppsPackageNames);
		} else if (wantedNotifcationListenerState == NOTIFICATION_LISTENER_OFF) {
			MyNotificationListenerService.stopService();
		}
	}

	@Override
	public int getNameId() {
		return R.string.action_test_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.action_test_static_description;
	}

	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_test_dynamic_description);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return TestActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		TestAction action = new TestAction();
		return action;
	}

}

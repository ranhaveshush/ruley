/**
 * 
 */
package com.ruley.model.beans.actions;

import java.io.InputStream;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.SetWallpaperActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.variables.FileVariable;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class SetWallpaperAction extends Action {

	private static final long serialVersionUID = -2298583485118078586L;
	
	private static final String CLASS_NAME = SetWallpaperAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SetWallpaperAction.class.getName());

	@JsonProperty("file")
	private FileVariable file = new FileVariable();

	public FileVariable getFile() {
		return file;
	}

	public void setFile(FileVariable file) {
		this.file = file;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);

		// Changes wallpaper by given URI to bitmap file
		try {
			Uri imageUri = Uri.parse(file.getFileUri());
			InputStream imageStream = context.getContentResolver().openInputStream(imageUri);
			Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
			wallpaperManager.setBitmap(bitmap);
			LOGGER.info("Wallpaper image set to " + file.getFileUri());
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_wallpaper_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_wallpaper_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (file == null) {
			return context.getString(R.string.action_change_wallpaper_image_missing);
		} else {
			String description = context.getString(R.string.action_change_wallpaper_dynamic_description);
			return String.format(description, file);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.image);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return SetWallpaperActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		if (file != null) {
			String bitmapUri = file.getFileUri();
			if (!TextUtils.isEmpty(bitmapUri)) {
				Uri imageUri = Uri.parse(bitmapUri);
				if (imageUri != null) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public Action getClone() {
		SetWallpaperAction action = new SetWallpaperAction();
		action.setFile(file);
		return action;
	}

	@Override
	public Action toTemplate() {
		SetWallpaperAction action = new SetWallpaperAction();
		action.setFile(new FileVariable());
		return action;
	}

}


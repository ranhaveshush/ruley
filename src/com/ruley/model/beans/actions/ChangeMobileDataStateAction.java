/**
 * 
 */
package com.ruley.model.beans.actions;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeMobileDataStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * Notice:
 * This action is a workaround with help of Refleciton.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeMobileDataStateAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ChangeMobileDataStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeMobileDataStateAction.class.getName());
	
	public static final int MOBILE_DATA_ENABLE = 1;
	
	public static final int MOBILE_DATA_DISABLE	 = 2;

	/**
	 * Holds the wanted mobile data state.
	 */
	@JsonProperty("wantedMobileDataState")
	private int wantedMobileDataState = MOBILE_DATA_ENABLE;

	public int getWantedMobileDataState() {
		return wantedMobileDataState;
	}

	public void setWantedMobileDataState(int wantedMobileDataState) {
		this.wantedMobileDataState = wantedMobileDataState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		try {
			// Gets the hidden setMobileDataEnabledMethod from connectivityManager with reflection
			final ConnectivityManager connectivityManager = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
			final Class connectivityManangerClass = Class.forName(connectivityManager.getClass().getName());
			final Field mServiceField = connectivityManangerClass.getDeclaredField("mService");
			mServiceField.setAccessible(true);
			final Object iConnectivityManager = mServiceField.get(connectivityManager);
			final Class iConnectivityManagerClass =  Class.forName(iConnectivityManager.getClass().getName());
			final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);
			
			boolean enable = (wantedMobileDataState == MOBILE_DATA_ENABLE) ? true : false;
			
			// Changes mobile data connectivity state
			setMobileDataEnabledMethod.invoke(iConnectivityManager, enable);
			if (enable) {
				LOGGER.info("Mobile data state enabled");
			} else {
				LOGGER.info("Mobile data state disabled");
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("Error: ", e);
		} catch (ClassNotFoundException e) {
			LOGGER.error("Error: ", e);
		} catch (NoSuchFieldException e) {
			LOGGER.error("Error: ", e);
		} catch (IllegalAccessException e) {
			LOGGER.error("Error: ", e);
		} catch (NoSuchMethodException e) {
			LOGGER.error("Error: ", e);
		} catch (InvocationTargetException e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_mobile_data_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_mobile_data_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String mobileDataState = null;
		switch (wantedMobileDataState) {
		case MOBILE_DATA_ENABLE:
			mobileDataState = context.getString(R.string.action_change_mobile_data_state_on);
			break;
		case MOBILE_DATA_DISABLE:
			mobileDataState = context.getString(R.string.action_change_mobile_data_state_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_mobile_data_state_dynamic_description);
		return String.format(description, mobileDataState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedMobileDataState) {
		case MOBILE_DATA_ENABLE:
			return ImageUtils.getBitmap(R.drawable.mobile_data_on);
		case MOBILE_DATA_DISABLE:
			return ImageUtils.getBitmap(R.drawable.mobile_data_off);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeMobileDataStateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedMobileDataState) {
		case MOBILE_DATA_ENABLE:
		case MOBILE_DATA_DISABLE:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeMobileDataStateAction action = new ChangeMobileDataStateAction();
		action.setWantedMobileDataState(getWantedMobileDataState());
		return action;
	}

}

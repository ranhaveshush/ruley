/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.WebSearchActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class WebSearchWakeAction extends WakeAction {

	private static final long serialVersionUID = -2636820665753602143L;
	
	private static final String CLASS_NAME = WebSearchWakeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(WebSearchWakeAction.class.getName());
	
	@JsonProperty("searchQuery")
	private String searchQuery = null;

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		super.perform(context);

		PickersUtils.launchWebSearchPicker(context, searchQuery);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_web_search_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_web_search_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (TextUtils.isEmpty(searchQuery)) {
			return context.getString(R.string.action_web_search_query_missing);
		} else {
			String description = context.getString(R.string.action_web_search_dynamic_description);
			return String.format(description, searchQuery);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.search);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return WebSearchActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(searchQuery);
	}
	
	@Override
	public Action getClone() {
		WebSearchWakeAction action = new WebSearchWakeAction();
		action.setSearchQuery(getSearchQuery());
		return action;
	}

}

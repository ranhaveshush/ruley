/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ModifySpeakerPhoneStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ModifySpeakerPhoneStateAction extends Action {

	private static final long serialVersionUID = -4703821329828171357L;
	
	private static final String CLASS_NAME = ModifySpeakerPhoneStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ModifySpeakerPhoneStateAction.class.getName());
	
	public static final boolean SPEAKER_PHONE_ON = true;
	
	public static final boolean SPEAKER_PHONE_OFF = false;

	/**
	 * Holds the wanted speaker phone state.
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager#setSpeakerphoneOn(boolean on)
	 */
	@JsonProperty("wantedSpeakerPhoneState")
	private boolean wantedSpeakerPhoneState = SPEAKER_PHONE_ON;

	public boolean isWantedSpeakerPhoneState() {
		return wantedSpeakerPhoneState;
	}

	public void setWantedSpeakerPhoneState(boolean wantedSpeakerPhoneState) {
		this.wantedSpeakerPhoneState = wantedSpeakerPhoneState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		// Changes speaker phone state settings
		audioManager.setSpeakerphoneOn(wantedSpeakerPhoneState);
		
		if (wantedSpeakerPhoneState) {
			LOGGER.info("Speaker phone on");
		} else {
			LOGGER.info("Speaker phone off");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_modify_speaker_phone_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_modify_speaker_phone_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String speakerPhoneState = null;
		if (wantedSpeakerPhoneState) {
			speakerPhoneState = context.getString(R.string.action_modify_speaker_phone_on);
		} else {
			speakerPhoneState = context.getString(R.string.action_modify_speaker_phone_off);
		}
		String description = context.getString(R.string.action_modify_speaker_phone_dynamic_description);
		return String.format(description, speakerPhoneState);
	}

	@Override
	public Bitmap getIcon() {
		if (wantedSpeakerPhoneState) {
			return ImageUtils.getBitmap(R.drawable.speaker_phone);
		} else {
			return ImageUtils.getBitmap(R.drawable.speaker_phone);
		}
		
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ModifySpeakerPhoneStateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		ModifySpeakerPhoneStateAction action = new ModifySpeakerPhoneStateAction();
		action.setWantedSpeakerPhoneState(isWantedSpeakerPhoneState());
		return action;
	}

}

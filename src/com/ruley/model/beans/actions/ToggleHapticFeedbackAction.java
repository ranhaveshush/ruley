/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;

import com.ruley.R;
import com.ruley.activities.editors.actions.ToggleHapticFeedbackActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToggleHapticFeedbackAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ToggleHapticFeedbackAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToggleHapticFeedbackAction.class.getName());
	
	private static final int NOT_FOUND = -1;
	
	public static final int HAPTIC_FEEDBACK_ENABLED = 1;
	
	public static final int HAPTIC_FEEDBACK_DISABLED = 0;

	/**
	 * For more info:
	 * 
	 * @see android.provider.Settings.System#HAPTIC_FEEDBACK_ENABLED
	 */
	@JsonProperty("wantedHapticFeedbackState")
	private int wantedHapticFeedbackState = HAPTIC_FEEDBACK_ENABLED;

	public int getWantedHapticFeedbackState() {
		return wantedHapticFeedbackState;
	}

	public void setWantedHapticFeedbackState(int wantedHapticFeedbackState) {
		this.wantedHapticFeedbackState = wantedHapticFeedbackState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String hapticFeedbackStateName = Settings.System.HAPTIC_FEEDBACK_ENABLED;
		int currHapticFeedbackState = Settings.System.getInt(context.getContentResolver(), hapticFeedbackStateName, NOT_FOUND);
		if (currHapticFeedbackState == NOT_FOUND) {
			LOGGER.warn(CLASS_NAME + "#perform(...) invalid settings " + hapticFeedbackStateName);
			return;
		}
		Settings.System.putInt(context.getContentResolver(), hapticFeedbackStateName, wantedHapticFeedbackState);
		currHapticFeedbackState = Settings.System.getInt(context.getContentResolver(), hapticFeedbackStateName, NOT_FOUND);
		
		switch (currHapticFeedbackState) {
		case HAPTIC_FEEDBACK_ENABLED:
			LOGGER.info("Haptic feedback ENABLED");
			break;
		case HAPTIC_FEEDBACK_DISABLED:
			LOGGER.info("Haptic feedback DISABLED");
			break;
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_toggle_haptic_feedback_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_toggle_haptic_feedback_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String hapticFeedbackState = null;
		switch (wantedHapticFeedbackState) {
		case HAPTIC_FEEDBACK_ENABLED:
			hapticFeedbackState = context.getString(R.string.action_toggle_haptic_feedback_on);
			break;
		case HAPTIC_FEEDBACK_DISABLED:
			hapticFeedbackState = context.getString(R.string.action_toggle_haptic_feedback_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_toggle_haptic_feedback_dynamic_description);
		return String.format(description, hapticFeedbackState);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.ringer_vibrate);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ToggleHapticFeedbackActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedHapticFeedbackState) {
		case HAPTIC_FEEDBACK_ENABLED:
		case HAPTIC_FEEDBACK_DISABLED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ToggleHapticFeedbackAction action = new ToggleHapticFeedbackAction();
		action.setWantedHapticFeedbackState(getWantedHapticFeedbackState());
		return action;
	}

}

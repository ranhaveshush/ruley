/**
 * 
 */
package com.ruley.model.beans.actions;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.actions.SendEmailActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.variables.EmailVariable;
import com.ruley.utilities.GMailOauthSender;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.UserAccountUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SendEmailAction extends Action {
	
	private static final long serialVersionUID = -1039523154621907548L;
	
	private static final String CLASS_NAME =  SendEmailAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailAction.class.getName());
	
	public static final String AUTH_TOKEN_TYPE = "oauth2:https://mail.google.com/";
	
	public static final String EMAILS_DELIMITER = ",";
	
	@JsonProperty("email")
	public EmailVariable email = new EmailVariable();
	
	public SendEmailAction() {
		super();
	}

	public EmailVariable getEmail() {
		return email;
	}

	public void setEmail(EmailVariable email) {
		this.email = email;
	}

	@Override
	public void perform(final Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		final EmailVariable emailToSend = getEmail();
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String accountName = emailToSend.getFromEmailAddress();
					AccountManager accountManager = AccountManager.get(context);
					Account account = UserAccountUtils.getAccount(accountManager, UserAccountUtils.ACCOUNT_TYPE_GOOGLE, accountName);
					String authToken = accountManager.blockingGetAuthToken(account, AUTH_TOKEN_TYPE, true);
					LOGGER.info("authtoken: " + authToken);
					sendMail(context, authToken);
				} catch (OperationCanceledException e) {
					LOGGER.error("Error: ", e);
				} catch (AuthenticatorException e) {
					LOGGER.error("Error: ", e);
				} catch (IOException e) {
					LOGGER.error("Error: ", e);
				}
			}
		}).start();
	}
	
	@Override
	public int getNameId() {
		return R.string.action_send_email_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_send_email_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (email == null) {
			return context.getString(R.string.action_send_email_missing);
		} else {
			String description = context.getString(R.string.action_send_email_dynamic_description);
			return String.format(description, email);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.email);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return SendEmailActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return email != null && email.isValid(context);
	}
	
	@Override
	public Action getClone() {
		SendEmailAction action = new SendEmailAction();
		action.setEmail(email);
		return action;
	}
	
	@Override
	public Action toTemplate() {
		SendEmailAction action = new SendEmailAction();
		action.setEmail(new EmailVariable());
		return action;
	}

	private void sendMail(Context context, String authtoken) {
		LOGGER.debug(CLASS_NAME + "#sendMail(...)");
		
		String fromEmailAddress = email.getFromEmailAddress();
		List<String> toEmailAddresses = email.getToEmailAddresses();
		String subject = email.getSubject();
		String text = email.getText() + context.getString(R.string.action_send_email_signature);
		
		String toEmailAddressStr = toEmailAddresses .toString();
		toEmailAddressStr = toEmailAddressStr.substring(1, toEmailAddressStr.length()-1);
		
		GMailOauthSender gmailOauthSender = new GMailOauthSender();
		
		gmailOauthSender.sendMail(subject, text, fromEmailAddress, toEmailAddressStr, authtoken);
	}

}

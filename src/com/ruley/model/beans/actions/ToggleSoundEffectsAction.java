/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;

import com.ruley.R;
import com.ruley.activities.editors.actions.ToggleSoundEffectsActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToggleSoundEffectsAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ToggleSoundEffectsAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToggleSoundEffectsAction.class.getName());
	
	private static final int NOT_FOUND = -1;
	
	public static final int SOUND_EFFECTS_ENABLED = 1;
	
	public static final int SOUND_EFFECTS_DISABLED = 0;

	/**
	 * For more info:
	 * 
	 * @see android.provider.Settings.System#SOUND_EFFECTS_ENABLED
	 */
	@JsonProperty("wantedSoundEffectsState")
	private int wantedSoundEffectsState = SOUND_EFFECTS_ENABLED;

	public int getWantedSoundEffectsState() {
		return wantedSoundEffectsState;
	}

	public void setWantedSoundEffectsState(int wantedSoundEffectsState) {
		this.wantedSoundEffectsState = wantedSoundEffectsState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String soundEffectsEnabledName = Settings.System.SOUND_EFFECTS_ENABLED;
		int currSoundEffectsState = Settings.System.getInt(context.getContentResolver(), soundEffectsEnabledName, NOT_FOUND);
		if (currSoundEffectsState == NOT_FOUND) {
			LOGGER.warn(CLASS_NAME + "#perform(...) invalid settings " + soundEffectsEnabledName);
			return;
		}
		Settings.System.putInt(context.getContentResolver(), soundEffectsEnabledName, wantedSoundEffectsState);
		currSoundEffectsState = Settings.System.getInt(context.getContentResolver(), soundEffectsEnabledName, NOT_FOUND);
		
		switch (currSoundEffectsState) {
		case SOUND_EFFECTS_ENABLED:
			LOGGER.info("Sound effects ENABLED");
			break;
		case SOUND_EFFECTS_DISABLED:
			LOGGER.info("Sound effects DISABLED");
			break;
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_toggle_sound_effects_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_toggle_sound_effects_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String soundEffectsState = null;
		switch (wantedSoundEffectsState) {
		case SOUND_EFFECTS_ENABLED:
			soundEffectsState = context.getString(R.string.action_toggle_sound_effects_on);
			break;
		case SOUND_EFFECTS_DISABLED:
			soundEffectsState = context.getString(R.string.action_toggle_sound_effects_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_toggle_sound_effects_dynamic_description);
		return String.format(description, soundEffectsState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedSoundEffectsState) {
		case SOUND_EFFECTS_ENABLED:
			return ImageUtils.getBitmap(R.drawable.ringer_normal);
		case SOUND_EFFECTS_DISABLED:
			return ImageUtils.getBitmap(R.drawable.ringer_silent);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ToggleSoundEffectsActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedSoundEffectsState) {
		case SOUND_EFFECTS_ENABLED:
		case SOUND_EFFECTS_DISABLED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ToggleSoundEffectsAction action = new ToggleSoundEffectsAction();
		action.setWantedSoundEffectsState(getWantedSoundEffectsState());
		return action;
	}

}

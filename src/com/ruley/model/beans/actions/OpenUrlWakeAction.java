/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.OpenUrlActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class OpenUrlWakeAction extends WakeAction {

	private static final long serialVersionUID = -1039523154621907548L;
	
	private static final String CLASS_NAME =  OpenUrlWakeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(OpenUrlWakeAction.class.getName());
	
	public static final String HTTP_PROTOCOL = "http://";
	
	public static final String HTTPS_PROTOCOL = "https://";

	@JsonProperty("url")
	private String url = null;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		super.perform(context);

		PickersUtils.launchBrowserPicker(context, url);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_open_url_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_open_url_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		// If the URL is null or empty
		if (TextUtils.isEmpty(url) || HTTP_PROTOCOL.equals(url) || HTTPS_PROTOCOL.equals(url)) {
			return context.getString(R.string.action_open_url_missing);
		// If the URL isn't valid
		} else if (!url.startsWith(HTTP_PROTOCOL) && !url.startsWith(HTTPS_PROTOCOL)) {
			return context.getString(R.string.action_open_url_invalid);
		// IF the URL is valid
		} else {
			String description = context.getString(R.string.action_open_url_dynamic_description);
			String website = null;
			if (url.startsWith(HTTP_PROTOCOL)) {			
				website = url.replace(HTTP_PROTOCOL, "");
			} else if (url.startsWith(HTTPS_PROTOCOL)) {			
				website = url.replace(HTTPS_PROTOCOL, "");
			}
			return String.format(description, website);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.website);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return OpenUrlActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		if (!TextUtils.isEmpty(url)) {
			return url.matches("^(https?:\\/\\/)([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$");
		}
		return false;
	}
	
	@Override
	public Action getClone() {
		OpenUrlWakeAction action = new OpenUrlWakeAction();
		action.setUrl(getUrl());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ModifyAudioStreamMuteActionEditorActivity;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.AudioStreamsUtils;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ModifyAudioStreamMuteAction extends Action {

	private static final long serialVersionUID = -3013284038569483636L;
	
	private static final String CLASS_NAME = ModifyAudioStreamMuteAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyAudioStreamMuteAction.class.getName());
	
	public static boolean AUDIO_STREAM_MUTED = true;
	
	public static boolean AUDIO_STREAM_UNMUTED = false;

	/**
	 * Holds the audio stream.
	 * 
	 * <P>
	 * AudioManager.STREAM_ALARM:<BR/>
	 * The audio stream for alarms.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_DTMF:<BR/>
	 * The audio stream for DTMF Tones.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_MUSIC:<BR/>
	 * The audio stream for music playback.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_NOTIFICATION:<BR/>
	 * The audio stream for notifications.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_RING:<BR/>
	 * The audio stream for the phone ring.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_SYSTEM:<BR/>
	 * The audio stream for system sounds.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_VOICE_CALL:<BR/>
	 * The audio stream for phone calls.
	 * </P>
	 * 
	 * <P>
	 * Holds the audio stream type.<BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager
	 * @see android.media.AudioManager#setStreamMute(int streamType, boolean state)
	 */
	@JsonProperty("audioStreamType")
	private int audioStreamType = AudioManager.STREAM_ALARM;

	/**
	 * <P>
	 * Holds the audio stream mute state.<BR/>
	 * The required mute state: true for mute ON, false for mute OFF.
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager
	 * @see android.media.AudioManager#setStreamMute(int streamType, boolean state)
	 */
	@JsonProperty("audioStreamMuted")
	private boolean audioStreamMuted = AUDIO_STREAM_MUTED;

	public int getAudioStreamType() {
		return audioStreamType;
	}

	public void setAudioStreamType(int audioStreamType) {
		if (this.audioStreamType != audioStreamType) {
			this.audioStreamType = audioStreamType;
		}
	}

	public boolean isAudioStreamMuted() {
		return audioStreamMuted;
	}

	public void setAudioStreamMuted(boolean audioStreamMuted) {
		this.audioStreamMuted = audioStreamMuted;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		Context appCtx = MyApplication.getContext();
		
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		
		String audioStreamName = AudioStreamsUtils.audioStreamsTypeToNameMap.get(audioStreamType);
		String audioStreamMuteState = null;

		// Changes audio stream mute settings
		// If wanted state is muted, sets stream volume to 0
		if (audioStreamMuted) {
			audioManager.setStreamVolume(audioStreamType, 0, 0);
			audioStreamMuteState = appCtx.getString(R.string.action_modify_audio_stream_mute);
			LOGGER.info(audioStreamName + " " + audioStreamMuteState);
		// If wanted state is unmuted, sets stream volume to max
		} else {
			int streamMaxVolume = audioManager.getStreamMaxVolume(audioStreamType);
			audioManager.setStreamVolume(audioStreamType, streamMaxVolume, 0);
			audioStreamMuteState = appCtx.getString(R.string.action_modify_audio_stream_unmute);
			LOGGER.info(audioStreamName + " " + audioStreamMuteState);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_modify_audio_stream_mute_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_modify_audio_stream_mute_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		Context appCtx = MyApplication.getContext();
		
		String description = context.getString(R.string.action_modify_audio_stream_mute_dynamic_description);
		String audioStreamName = AudioStreamsUtils.audioStreamsTypeToNameMap.get(audioStreamType);
		String audioStreamMuteState = null;
		if (audioStreamMuted) {
			audioStreamMuteState = appCtx.getString(R.string.action_modify_audio_stream_mute);
		} else {
			audioStreamMuteState = appCtx.getString(R.string.action_modify_audio_stream_unmute);
		}
		return String.format(description, audioStreamMuteState, audioStreamName);
	}

	@Override
	public Bitmap getIcon() {
		if (audioStreamMuted) {
			return ImageUtils.getBitmap(R.drawable.ringer_silent);
		} else {
			return ImageUtils.getBitmap(R.drawable.ringer_normal);
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ModifyAudioStreamMuteActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (audioStreamType) {
		case AudioManager.STREAM_ALARM:
		case AudioManager.STREAM_DTMF:
		case AudioManager.STREAM_MUSIC:
		case AudioManager.STREAM_NOTIFICATION:
		case AudioManager.STREAM_RING:
		case AudioManager.STREAM_SYSTEM:
		case AudioManager.STREAM_VOICE_CALL:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ModifyAudioStreamMuteAction action = new ModifyAudioStreamMuteAction();
		action.setAudioStreamType(getAudioStreamType());
		action.setAudioStreamMuted(isAudioStreamMuted());
		return action;
	}

}

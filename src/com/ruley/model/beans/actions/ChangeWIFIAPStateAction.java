/**
 * 
 */
package com.ruley.model.beans.actions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeWIFIAPStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.variables.WIFIAPVariable;
import com.ruley.utilities.ImageUtils;


/**
 * Notice:
 * 
 * This action is a workaround with the help of Refleciton.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeWIFIAPStateAction extends Action {

	private static final long serialVersionUID = -7449220062216807323L;
	
	private static final String CLASS_NAME = ChangeWIFIAPStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ChangeWIFIAPStateAction.class.getName());

	public static final int WIFI_AP_ENABLED = 0;
	
	public static final int WIFI_AP_DISABLED = 1;

	@JsonProperty("wifiAP")
	private WIFIAPVariable wifiAP = new WIFIAPVariable();
	
	public ChangeWIFIAPStateAction() {
		super();
	}
	
	public WIFIAPVariable getWifiAP() {
		return wifiAP;
	}

	public void setWifiAP(WIFIAPVariable wifiAP) {
		this.wifiAP = wifiAP;
	}

	/**
	 * @see android.net.wifi.WifiManager
	 */
	@JsonProperty("wantedWifiAPState")
	private int wantedWifiAPState = WIFI_AP_ENABLED;

	public int getWantedWifiAPState() {
		return wantedWifiAPState;
	}

	public void setWantedWifiAPState(int wantedWifiAPState) {
		this.wantedWifiAPState = wantedWifiAPState;
	}
	
	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (wifiManager == null) {
			LOGGER.error("WifiManager is not availble!");
			return;
		}

		// If wanted wifi access point enabled disable WIFI
		if (wantedWifiAPState == WIFI_AP_ENABLED && wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(false);
		}

		WifiConfiguration wc = new WifiConfiguration();
		wc.SSID = wifiAP.getNetworkSSID(); // This is the network name
		wc.preSharedKey = wifiAP.getNetworkPassword(); // This is the WEP password
	    wc.hiddenSSID = !wifiAP.isNetworkSSIDVisible();
	   
		wc.status = WifiConfiguration.Status.ENABLED;
		wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
		wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
		wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
		wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
		wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
		wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
		wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
		wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
		wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
	    
		try {
			Method setWifiApEnabled = wifiManager.getClass().getMethod(
					"setWifiApEnabled", WifiConfiguration.class, boolean.class);
			boolean enable = (wantedWifiAPState == WIFI_AP_ENABLED) ? true : false;
			boolean isAccessPointEnabled = (Boolean) setWifiApEnabled.invoke(wifiManager, wc, enable);
			if (isAccessPointEnabled) {
				LOGGER.info("\"" + wifiAP.getNetworkSSID() + "\" WIFI Access Point enabled");
			} else {
				LOGGER.info("\"" + wifiAP.getNetworkPassword() + "\" WIFI Access Point disabled");
			}
		} catch (IllegalArgumentException e) {
			LOGGER.error("Error: ", e);
		} catch (IllegalAccessException e) {
			LOGGER.error("Error: ", e);
		} catch (InvocationTargetException e) {
			LOGGER.error("Error: ", e);
		} catch (NoSuchMethodException e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_wifi_ap_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_wifi_ap_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (wifiAP == null) {
			return context.getString(R.string.action_change_wifi_ap_state_network_ssid_invalid);
		} else {
			String wifiAPstate = null;
			switch (wantedWifiAPState) {
			case WIFI_AP_ENABLED:
				wifiAPstate = context.getString(R.string.action_change_wifi_ap_state_enabled);
				break;
			case WIFI_AP_DISABLED:
				wifiAPstate = context.getString(R.string.action_change_wifi_ap_state_disabled);
				break;
			}
			String description = context.getString(R.string.action_change_wifi_ap_state_dynamic_description);
			return String.format(description, wifiAPstate, wifiAP);
		}
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedWifiAPState) {
		case WIFI_AP_ENABLED:
			return ImageUtils.getBitmap(R.drawable.hot_spot_on);
		case WIFI_AP_DISABLED:
			return ImageUtils.getBitmap(R.drawable.hot_spot_off);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeWIFIAPStateActionEditorActivity.class;
	}
	

	@Override
	public boolean isValid(Context context) {
		return wifiAP != null
				&& wifiAP.isValid(context)
				&& (wantedWifiAPState == WIFI_AP_ENABLED || wantedWifiAPState == WIFI_AP_DISABLED);
				
	}
	
	@Override
	public Action getClone() {
		ChangeWIFIAPStateAction action = new ChangeWIFIAPStateAction();
		action.setWifiAP(wifiAP);
		action.setWantedWifiAPState(getWantedWifiAPState());
		return action;
	}

	@Override
	public Action toTemplate() {
		ChangeWIFIAPStateAction action = new ChangeWIFIAPStateAction();
		action.setWifiAP(new WIFIAPVariable());
		action.setWantedWifiAPState(getWantedWifiAPState());
		return action;
	}

}

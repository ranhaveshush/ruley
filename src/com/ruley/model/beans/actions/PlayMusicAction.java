/**
 * 
 */
package com.ruley.model.beans.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;

import com.ruley.R;
import com.ruley.activities.editors.actions.PlayMusicActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.IntentUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PlayMusicAction extends Action {
	
	private static final long serialVersionUID = 8403251921046117293L;

	private static final String CLASS_NAME = PlayMusicAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PlayMusicAction.class.getName());
	
	private static final String GOOGLE_PLAY_MUSIC_PACKAGE_NAME = "com.google.android.music";

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		Intent playIntent = new Intent(MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH);
		playIntent.setPackage(GOOGLE_PLAY_MUSIC_PACKAGE_NAME);
		playIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		playIntent.putExtra(MediaStore.EXTRA_MEDIA_FOCUS, "vnd.android.cursor.item/*");
		playIntent.putExtra(SearchManager.QUERY, "");
		
		if (IntentUtils.isIntentSafe(playIntent)) {
			context.startActivity(playIntent);
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_play_music_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_play_music_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_play_music_dynamic_description);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.music2);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return PlayMusicActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		PlayMusicAction action = new PlayMusicAction();
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.actions.PhoneCallActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.variables.ContactVariable;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class PhoneCallAction extends Action {

	private static final long serialVersionUID = -9164626618825503948L;
	
	private static final String CLASS_NAME = PhoneCallAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PhoneCallAction.class.getName());

	@JsonProperty("contact")
	private ContactVariable contact = new ContactVariable();
	
	public PhoneCallAction() {
		super();
	}

	public ContactVariable getContact() {
		return contact;
	}

	public void setContact(ContactVariable contact) {
		this.contact = contact;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String phoneNumber = ContactsUtils.getContactPhoneNumber(context, contact.getContactId());
		PickersUtils.launchDialerPicker(context, phoneNumber);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_phone_call_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_phone_call_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (contact == null) {
			return context.getString(R.string.action_phone_call_to_missing);
		} else {
			String description = context.getString(R.string.action_phone_call_dynamic_description);
			return String.format(description, contact);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.phone_call);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return PhoneCallActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return contact != null && contact.isValid(context);
	}
	
	@Override
	public Action getClone() {
		PhoneCallAction action = new PhoneCallAction();
		action.setContact(contact);
		return action;
	}

	@Override
	public Action toTemplate() {
		PhoneCallAction action = new PhoneCallAction();
		action.setContact(new ContactVariable());
		return action; 
	}

}
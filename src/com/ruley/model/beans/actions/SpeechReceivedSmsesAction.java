/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.SpeechReceivedSmsesActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.triggers.ReceivedSMSTrigger;
import com.ruley.services.TextToSpeechService;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@TargetApi(19)
public class SpeechReceivedSmsesAction extends Action {

	private static final long serialVersionUID = -2294533314173954715L;

	private static final String CLASS_NAME = SpeechReceivedSmsesAction.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SpeechReceivedSmsesAction.class.getName());
	
	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		// Validates there are any received messages
		if (ReceivedSMSTrigger.isReceivedMessagesEmpty()) {
			return;
		}

		List<SmsMessage> receivedMessages = ReceivedSMSTrigger.getReceivedMessages();
		for (SmsMessage smsMessage : receivedMessages) {
			String phoneNumber = smsMessage.getDisplayOriginatingAddress();
			String contactId = ContactsUtils.getContactId(context, phoneNumber);
			String contactName = ContactsUtils.getContactName(context, contactId);
			String from = (!TextUtils.isEmpty(contactName)) ? contactName : phoneNumber;
			String speak = "Received SMS from " + from + ": " + smsMessage.getMessageBody();
			LOGGER.info(CLASS_NAME + "#handle(...) speak = " + speak);
			
			TextToSpeechService.speek(speak);
		}
	}

	@Override
	public int getNameId() {
		return R.string.action_speech_received_smses_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.action_speech_received_smses_static_description;
	}

	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_speech_received_smses_dynamic_description);
	}
	
	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.speak);
	}
	
	@Override
	public Class<? extends Activity> getEditor() {
		return SpeechReceivedSmsesActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		SpeechReceivedSmsesAction action = new SpeechReceivedSmsesAction();
		return action;
	}

}

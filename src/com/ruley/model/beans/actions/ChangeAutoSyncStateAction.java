/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeAutoSyncStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeAutoSyncStateAction extends Action {
	
	private static final long serialVersionUID = 1100883758528268007L;

	private static final String CLASS_NAME = ChangeAutoSyncStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeAutoSyncStateAction.class.getName());
	
	public static final int AUTO_SYNC_OFF = 0;
	
	public static final int AUTO_SYNC_ON = 1;

	/**
	 * Holds the wanted auto sync state.
	 * 
	 * <P>
	 * AUTO_SYNC_OFF:<BR/>
	 * Indicates the master auto sync is disabled.<BR/>
	 * AUTO_SYNC_ON<BR/>
	 * Indicates the master auto sync is enabled.<BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.content.ContentResolver#setMasterSyncAutomatically
	 */
	@JsonProperty("wantedAutoSyncState")
	private int wantedAutoSyncState = AUTO_SYNC_ON;

	public int getWantedAutoSyncState() {
		return wantedAutoSyncState;
	}

	public void setWantedAutoSyncState(int wantedAutoSyncState) {
		this.wantedAutoSyncState = wantedAutoSyncState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		switch (wantedAutoSyncState) {
		case AUTO_SYNC_ON:
			ContentResolver.setMasterSyncAutomatically(true);
			LOGGER.info("Auto-sync data turned ON");
			break;
		case AUTO_SYNC_OFF:
			ContentResolver.setMasterSyncAutomatically(false);
			LOGGER.info("Auto-sync data turned OFF");
			break;
		default:
			LOGGER.error("Invalid Auto-sync state wantedAutoSyncState: " + wantedAutoSyncState);
			break;
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_auto_sync_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_auto_sync_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String autoSyncState = null;
		switch (wantedAutoSyncState) {
		case AUTO_SYNC_ON:
			autoSyncState = context.getString(R.string.action_change_auto_sync_state_on);
			break;
		case AUTO_SYNC_OFF:
			autoSyncState = context.getString(R.string.action_change_auto_sync_state_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_auto_sync_state_dynamic_description);
		return String.format(description, autoSyncState);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.auto_sync);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeAutoSyncStateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedAutoSyncState) {
		case AUTO_SYNC_ON:
		case AUTO_SYNC_OFF:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeAutoSyncStateAction action = new ChangeAutoSyncStateAction();
		action.setWantedAutoSyncState(getWantedAutoSyncState());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeWIFISleepPolicyActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeWIFISleepPolicyAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ChangeWIFISleepPolicyAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeWIFISleepPolicyAction.class.getName());
	
	private static final int NOT_FOUND = -1;

	/**
	 * Holds the wanted WIFI sleep policy.
	 * 
	 * <P>
	 * Settings.System.WIFI_SLEEP_POLICY_DEFAULT:<BR/>
	 * Value for WIFI_SLEEP_POLICY to use the default Wi-Fi sleep policy,<BR/>
	 * which is to sleep shortly after the turning off<BR/>
	 * according to the STAY_ON_WHILE_PLUGGED_IN setting.<BR/>
	 * <BR/>
	 * Settings.System.WIFI_SLEEP_POLICY_NEVER:<BR/>
	 * Value for WIFI_SLEEP_POLICY to never go to sleep.<BR/>
	 * <BR/>
	 * Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:<BR/>
	 * Value for WIFI_SLEEP_POLICY to use the default policy when the device is on battery,<BR/>
	 * and never go to sleep when the device is plugged in.<BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.provider.Settings.System#WIFI_SLEEP_POLICY
	 */
	@JsonProperty("wantedWIFISleepPolicy")
	private int wantedWIFISleepPolicy = Settings.System.WIFI_SLEEP_POLICY_DEFAULT;

	public int getWantedWIFISleepPolicy() {
		return wantedWIFISleepPolicy;
	}

	public void setWantedWIFISleepPolicy(int wantedWIFISleepPolicy) {
		this.wantedWIFISleepPolicy = wantedWIFISleepPolicy;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String wifiSleepPolicyName = Settings.System.WIFI_SLEEP_POLICY;
		int currWifiSleepPolicy = Settings.System.getInt(context.getContentResolver(), wifiSleepPolicyName, NOT_FOUND);
		if (currWifiSleepPolicy == NOT_FOUND) {
			LOGGER.warn(CLASS_NAME + "#perform(...) invalid settings " + wifiSleepPolicyName);
			return;
		}
		Settings.System.putInt(context.getContentResolver(), wifiSleepPolicyName, wantedWIFISleepPolicy);
		currWifiSleepPolicy = Settings.System.getInt(context.getContentResolver(), wifiSleepPolicyName, NOT_FOUND);

		switch (currWifiSleepPolicy) {
		case Settings.System.WIFI_SLEEP_POLICY_DEFAULT:
			LOGGER.info("WIFI sleep policy DEFAULT");
			break;
		case Settings.System.WIFI_SLEEP_POLICY_NEVER:
			LOGGER.info("WIFI sleep policy NEVER");
			break;
		case Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:
			LOGGER.info("WIFI sleep policy NEVER WHILE PLUGGED");
			break;
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_wifi_sleep_policy_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_wifi_sleep_policy_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String wifiSleepPolicy = null;
		switch (wantedWIFISleepPolicy) {
		case Settings.System.WIFI_SLEEP_POLICY_DEFAULT:
			wifiSleepPolicy = context.getString(R.string.action_change_wifi_sleep_policy_default);
			break;
		case Settings.System.WIFI_SLEEP_POLICY_NEVER:
			wifiSleepPolicy = context.getString(R.string.action_change_wifi_sleep_policy_never);
			break;
		case Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:
			wifiSleepPolicy = context.getString(R.string.action_change_wifi_sleep_policy_never_while_plugged);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_wifi_sleep_policy_dynamic_description);
		return String.format(description, wifiSleepPolicy);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedWIFISleepPolicy) {
		case Settings.System.WIFI_SLEEP_POLICY_DEFAULT:
		case Settings.System.WIFI_SLEEP_POLICY_NEVER:
		case Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:
			return ImageUtils.getBitmap(R.drawable.sleep);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeWIFISleepPolicyActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedWIFISleepPolicy) {
		case Settings.System.WIFI_SLEEP_POLICY_DEFAULT:
		case Settings.System.WIFI_SLEEP_POLICY_NEVER:
		case Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeWIFISleepPolicyAction action = new ChangeWIFISleepPolicyAction();
		action.setWantedWIFISleepPolicy(getWantedWIFISleepPolicy());
		return action;
	}

}

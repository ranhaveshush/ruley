/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera.CameraInfo;
import android.provider.MediaStore;

import com.ruley.R;
import com.ruley.activities.editors.actions.TakeVideoActionEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class TakeVideoWakeAction extends WakeAction {

	private static final long serialVersionUID = -1039523154621907548L;

	private static final String CLASS_NAME = TakeVideoWakeAction.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TakeVideoWakeAction.class.getName());
	
	/**
	 * Video capture preview will be displayed when true,
	 * otherwise false.
	 */
	@JsonProperty("preview")
	private boolean preview = true;
	
	/**
	 * Holds the camera side front or back:
	 * <P>
	 * CameraInfo.CAMERA_FACING_BACK<BR/>
	 * CameraInfo.CAMERA_FACING_FRONT<BR/>
	 * </P>
	 * 
	 * @see android.hardware.Camera.CameraInfo#CAMERA_FACING_BACK
	 * @see android.hardware.Camera.CameraInfo#CAMERA_FACING_FRONT
	 */
	@JsonProperty("cameraFacing")
	private int cameraFacing = CameraInfo.CAMERA_FACING_BACK;

	public TakeVideoWakeAction() {
	}

	public TakeVideoWakeAction(boolean preview, int cameraFacing) {
		super();
		this.preview = preview;
		this.cameraFacing = cameraFacing;
	}

	public boolean isPreview() {
		return preview;
	}

	public void setPreview(boolean preview) {
		this.preview = preview;
	}

	public int getCameraFacing() {
		return cameraFacing;
	}

	public void setCameraFacing(int cameraFacing) {
		this.cameraFacing = cameraFacing;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		String videosDirPath = Config.Paths.getAppStorageDirPath() + Config.Paths.DIR_VIDEOS;
		
		if (preview) {
			super.perform(context);
			
			PickersUtils.launchCameraPicker(context,
					MediaStore.ACTION_VIDEO_CAPTURE,
					videosDirPath);
		} else {
			PickersUtils.launchTakeVidPicker(context, cameraFacing,
					videosDirPath);
		}
	}

	@Override
	public int getNameId() {
		return R.string.action_take_video_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.action_take_video_static_description;
	}

	@Override
	public String getDescription(Context context) {
		String description = context.getString(R.string.action_take_video_dynamic_description);
		String withPreviewText = context.getString(R.string.action_take_video_with);
		String withoutPreviewText = context.getString(R.string.action_take_video_without);
		
		switch (cameraFacing) {
		case CameraInfo.CAMERA_FACING_BACK:
			String cameraFacingBackText = context.getString(R.string.action_take_video_camera_facing_back);
			if (preview) {
				description = String.format(description, cameraFacingBackText, withPreviewText);
			} else {
				description = String.format(description, cameraFacingBackText, withoutPreviewText);
			}
			break;
		case CameraInfo.CAMERA_FACING_FRONT:
			String cameraFacingFrontText = context.getString(R.string.action_take_video_camera_facing_front);
			if (preview) {
				description = String.format(description, cameraFacingFrontText, withPreviewText);
			} else {
				description = String.format(description, cameraFacingFrontText, withoutPreviewText);
			}
			break;
		}
		
		return description;
	}

	@Override
	public Bitmap getIcon() {
		switch (cameraFacing) {
		case CameraInfo.CAMERA_FACING_BACK:
			return ImageUtils.getBitmap(R.drawable.video_back);
		case CameraInfo.CAMERA_FACING_FRONT:
			return ImageUtils.getBitmap(R.drawable.video_front);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return TakeVideoActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return (cameraFacing == CameraInfo.CAMERA_FACING_BACK
				|| cameraFacing == CameraInfo.CAMERA_FACING_FRONT);
	}
	
	@Override
	public Action getClone() {
		TakeVideoWakeAction action = new TakeVideoWakeAction();
		action.setPreview(isPreview());
		action.setCameraFacing(getCameraFacing());
		return action;
	}

}

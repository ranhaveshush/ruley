/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.StartAppActionEditorAcivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.model.beans.variables.ApplicationVariable;
import com.ruley.utilities.AppsUtils;
import com.ruley.utilities.ImageUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class StartAppWakeAction extends WakeAction {

	private static final long serialVersionUID = -4550934575962211043L;
	
	private static final String CLASS_NAME = StartAppWakeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(StartAppWakeAction.class.getName());
	
	@JsonProperty("application")
	private ApplicationVariable application = new ApplicationVariable();

	public ApplicationVariable getApplication() {
		return application;
	}

	public void setApplication(ApplicationVariable application) {
		this.application = application;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		super.perform(context);

		AppsUtils.launchApp(context, application.getAppPackage());
	}
	
	@Override
	public int getNameId() {
		return R.string.action_start_application_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_start_application_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (application == null) {
			return context.getString(R.string.action_start_application_app_missing);
		} else {
			String description = context.getString(R.string.action_start_application_dynamic_description);
			return String.format(description, application);
		}
	}

	@Override
	public Bitmap getIcon() {
		if (application == null || TextUtils.isEmpty(application.getAppPackage())) {
			return ImageUtils.getBitmap(R.drawable.start_application);
		} else {
			return AppsUtils.getAppIcon(application.getAppPackage());
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return StartAppActionEditorAcivity.class;
	}
	
	@Override
	public boolean isValid(Context context) {
		return application != null && application.isValid(context);
	}
	
	@Override
	public Action getClone() {
		StartAppWakeAction action = new StartAppWakeAction();
		action.setApplication(application);
		return action;
	}

	@Override
	public Action toTemplate() {
		StartAppWakeAction action = new StartAppWakeAction();
		action.setApplication(new ApplicationVariable());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Vibrator;

import com.ruley.R;
import com.ruley.activities.editors.actions.VibrateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class VibrateAction extends Action {
	
	private static final long serialVersionUID = 1591808758642705838L;

	private static final String CLASS_NAME =  VibrateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(VibrateAction.class.getName());
	
	public static final int REPEAT_ON = 0;
	
	public static final int REPEAT_OFF = -1;
	
	@JsonProperty("vibratePattern")
	private long[] vibratePattern = null;
	
	@JsonProperty("repeat")
	private int repeat = REPEAT_OFF;

	public long[] getVibratePattern() {
		return vibratePattern;
	}

	public void setVibratePattern(long[] vibratePattern) {
		this.vibratePattern = vibratePattern;
	}

	public int getRepeat() {
		return repeat;
	}

	public void setRepeat(int repeat) {
		this.repeat = repeat;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(vibratePattern, repeat);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_vibrate_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_vibrate_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_vibrate_dynamic_description);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.ringer_vibrate);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return VibrateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
//		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
//		if (vibrator.hasVibrator()) {
//			return true;
//		} else {
//			return false;
//		}
		return true;
	}
	
	@Override
	public Action getClone() {
		VibrateAction action = new VibrateAction();
		action.setRepeat(getRepeat());
		action.setVibratePattern(getVibratePattern());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.actions.MapsNavigateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.model.beans.variables.LocationVariable;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class MapsNavigateWakeAction extends WakeAction {

	private static final long serialVersionUID = 4926347486338219192L;
	
	private static final String CLASS_NAME = MapsNavigateWakeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MapsNavigateWakeAction.class.getName());

	public static final String NAVIGATION_MODE_VEHICLE = "t";

	public static final String NAVIGATION_MODE_TRANSPORT = "r";

	public static final String NAVIGATION_MODE_WALKING = "w";
	
	@JsonProperty("location")
	private LocationVariable location = new LocationVariable();
	
	@JsonProperty("navigationMode")
	private String navigationMode = NAVIGATION_MODE_VEHICLE;

	public MapsNavigateWakeAction() {
		super();
	}

	public LocationVariable getLocation() {
		return location;
	}

	public void setLocation(LocationVariable location) {
		this.location = location;
	}

	public String getNavigationMode() {
		return navigationMode;
	}

	public void setNavigationMode(String navigationMode) {
		this.navigationMode = navigationMode;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		super.perform(context);
		
		PickersUtils.launchMapsNavigatePicker(context, location.getAddress(), navigationMode);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_maps_navigate_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_maps_navigate_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (location == null) {
			return context.getString(R.string.action_maps_navigate_location_invalid);
		} else {
			String description = context.getString(R.string.action_maps_navigate_dynamic_description);
			String navigationModeText = null;
			
			if (NAVIGATION_MODE_TRANSPORT.equals(navigationMode)) {
				navigationModeText = context.getString(R.string.action_maps_navigate_by_transport);
			} else if (NAVIGATION_MODE_VEHICLE.equals(navigationMode)) {
				navigationModeText = context.getString(R.string.action_maps_navigate_by_vehicle);
			} else if (NAVIGATION_MODE_WALKING.equals(navigationMode)) {
				navigationModeText = context.getString(R.string.action_maps_navigate_by_walking);
			}
			
			return String.format(description, location, navigationModeText);
		}
	}

	@Override
	public Bitmap getIcon() {
		if (NAVIGATION_MODE_TRANSPORT.equals(navigationMode)
				|| NAVIGATION_MODE_VEHICLE.equals(navigationMode)
				|| NAVIGATION_MODE_WALKING.equals(navigationMode)) {
			return ImageUtils.getBitmap(R.drawable.navigate);
		} else {
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return MapsNavigateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return location != null
				&& location.isValid(context)
				&& (NAVIGATION_MODE_TRANSPORT.equals(navigationMode)
					|| NAVIGATION_MODE_VEHICLE.equals(navigationMode)
					|| NAVIGATION_MODE_WALKING.equals(navigationMode));
	}
	
	@Override
	public Action getClone() {
		MapsNavigateWakeAction action = new MapsNavigateWakeAction();
		action.setLocation(location);
		action.setNavigationMode(getNavigationMode());
		return action;
	}

	@Override
	public Action toTemplate() {
		MapsNavigateWakeAction action = new MapsNavigateWakeAction();
		action.setLocation(new LocationVariable());
		action.setNavigationMode(getNavigationMode());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.activities.editors.actions.WazeNavigateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.model.beans.variables.LocationVariable;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class WazeNavigateWakeAction extends WakeAction {

	private static final long serialVersionUID = -1162032262465481459L;

	private static final String CLASS_NAME = WazeNavigateWakeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WazeNavigateWakeAction.class.getName());
	
	@JsonProperty("location")
	private LocationVariable location = new LocationVariable();

	public WazeNavigateWakeAction() {
		super();
	}

	public LocationVariable getLocation() {
		return location;
	}

	public void setLocation(LocationVariable location) {
		this.location = location;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		super.perform(context);
		
		PickersUtils.launchWazeNavigatePicker(context, location.getAddress());
	}
	
	@Override
	public int getNameId() {
		return R.string.action_waze_navigate_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_waze_navigate_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (location == null) {
			return context.getString(R.string.action_waze_navigate_location_invalid);
		} else {
			String description = context.getString(R.string.action_waze_navigate_dynamic_description);
			return String.format(description, location);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.directions);
	}
	
	@Override
	public Class<? extends Activity> getEditor() {
		return WazeNavigateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return location != null
				&& location.isValid(context);
	}
	
	@Override
	public Action getClone() {
		WazeNavigateWakeAction action = new WazeNavigateWakeAction();
		action.setLocation(location);
		return action;
	}

	@Override
	public Action toTemplate() {
		WazeNavigateWakeAction action = new WazeNavigateWakeAction();
		action.setLocation(new LocationVariable());
		return action;
	}

}

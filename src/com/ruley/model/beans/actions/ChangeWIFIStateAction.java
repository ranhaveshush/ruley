/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.wifi.WifiManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeWIFIStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeWIFIStateAction extends Action {

	private static final long serialVersionUID = -3755897594995876042L;
	
	private static final String CLASS_NAME = ChangeWIFIStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeWIFIStateAction.class.getName());

	/**
	 * Holds the wanted wifi state.
	 * 
	 * <P>
	 * WifiManager.WIFI_STATE_ENABLED:<BR/>
	 * Wi-Fi is enabled.<BR/>
	 * WifiManager.WIFI_STATE_DISABLED<BR/>
	 * Wi-Fi is disabled.<BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.net.wifi.WifiManager
	 */
	@JsonProperty("wantedWifiState")
	private int wantedWifiState = WifiManager.WIFI_STATE_ENABLED;

	public int getWantedWifiState() {
		return wantedWifiState;
	}

	public void setWantedWifiState(int wantedWifiState) {
		this.wantedWifiState = wantedWifiState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

		// Changes wifi state
		if (wantedWifiState == WifiManager.WIFI_STATE_ENABLED) {
			wifiManager.setWifiEnabled(true);
			LOGGER.info("WIFI state enabled");
		} else {
			wifiManager.setWifiEnabled(false);
			LOGGER.info("WIFI state disabled");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_wifi_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_wifi_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String wifiState = null;
		switch (wantedWifiState) {
		case WifiManager.WIFI_STATE_ENABLED:
			wifiState = context.getString(R.string.action_change_wifi_state_on);
			break;
		case WifiManager.WIFI_STATE_DISABLED:
			wifiState = context.getString(R.string.action_change_wifi_state_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_wifi_state_dynamic_description);
		return String.format(description, wifiState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedWifiState) {
		case WifiManager.WIFI_STATE_ENABLED:
			return ImageUtils.getBitmap(R.drawable.wifi_on);
		case WifiManager.WIFI_STATE_DISABLED:
			return ImageUtils.getBitmap(R.drawable.wifi_off);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeWIFIStateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedWifiState) {
		case WifiManager.WIFI_STATE_ENABLED:
		case WifiManager.WIFI_STATE_DISABLED:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeWIFIStateAction action = new ChangeWIFIStateAction();
		action.setWantedWifiState(getWantedWifiState());
		return action;
	}

}

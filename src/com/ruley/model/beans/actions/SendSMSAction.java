/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.editors.actions.SendSMSActionEditorActivity;
import com.ruley.app.AppState;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.variables.ContactVariable;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.ToastUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class SendSMSAction extends Action {

	private static final long serialVersionUID = -9164626618825503948L;
	
	private static final String CLASS_NAME = SendSMSAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SendSMSAction.class.getName());

	@JsonProperty("message")
	private String message = null;
	
	@JsonProperty("contact")
	private ContactVariable contact = new ContactVariable();

	public SendSMSAction() {
		super();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ContactVariable getContact() {
		return contact;
	}

	public void setContact(ContactVariable contact) {
		this.contact = contact;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		SmsManager smsManager = SmsManager.getDefault();
		
		String signedMessage = message + context.getString(R.string.action_send_sms_signature);
		ArrayList<String> messageParts = smsManager.divideMessage(signedMessage);
		String phoneNumber = ContactsUtils.getContactPhoneNumber(context, contact.getContactId());
		
		if (phoneNumber != null) {
			smsManager.sendMultipartTextMessage(phoneNumber, null, messageParts, null, null);
			LOGGER.info("SMS sent to " + phoneNumber);
		} else {
			ToastUtils.show(AppState.BUILD_RELEASE, context, "Failed to send SMS", Toast.LENGTH_SHORT);
			LOGGER.info("Failed to send SMS, phoneNumber is null");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_send_sms_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_send_sms_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (contact == null) {
			return context.getString(R.string.action_send_sms_to_missing);
		} else {
			String description = context.getString(R.string.action_send_sms_dynamic_description);
			return String.format(description, contact);
		}
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.sms3);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return SendSMSActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(message)
				&& contact != null
				&& contact.isValid(context);
	}
	
	@Override
	public Action getClone() {
		SendSMSAction action = new SendSMSAction();
		action.setContact(contact);
		action.setMessage(getMessage());
		return action;
	}

	@Override
	public Action toTemplate() {
		SendSMSAction action = new SendSMSAction();
		action.setContact(new ContactVariable());
		action.setMessage(getMessage());
		return action;
	}

}
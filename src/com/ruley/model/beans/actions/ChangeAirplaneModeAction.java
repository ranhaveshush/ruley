/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeAirplaneModeActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeAirplaneModeAction extends Action {

	private static final long serialVersionUID = -7449220062216807323L;
	
	private static final String CLASS_NAME = ChangeAirplaneModeAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ChangeAirplaneModeAction.class.getName());
	
	private static final int NOT_FOUND = -1;

	public static final int AIRPLANE_MODE_ON = 0;
	
	public static final int AIRPLANE_MODE_OFF = 1;

	/**
	 * Holds the wanted airplane mode.
	 * 
	 * For more info:
	 * 
	 * @see android.provider.Settings.System#AIRPLANE_MODE_ON
	 */
	@JsonProperty("wantedAirplaneMode")
	private int wantedAirplaneMode = AIRPLANE_MODE_ON;

	public int getWantedAirplaneMode() {
		return wantedAirplaneMode;
	}

	public void setWantedAirplaneMode(int wantedAirplaneMode) {
		this.wantedAirplaneMode = wantedAirplaneMode;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String airplaneModeName = Settings.System.AIRPLANE_MODE_ON;
		int currAirplaneMode = Settings.System.getInt(context.getContentResolver(), airplaneModeName, NOT_FOUND);
		if (currAirplaneMode == NOT_FOUND) {
			LOGGER.warn(CLASS_NAME + "#perform(...) invalid settings " + airplaneModeName);
			return;
		}
		Settings.System.putInt(context.getContentResolver(), airplaneModeName, wantedAirplaneMode);
		currAirplaneMode = Settings.System.getInt(context.getContentResolver(), airplaneModeName, NOT_FOUND);
		
		switch (currAirplaneMode) {
		case AIRPLANE_MODE_ON:
			LOGGER.info("Airplane mode ON");
			break;
		case AIRPLANE_MODE_OFF:
			LOGGER.info("Airplane mode OFF");
			break;
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_airplane_mode_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_airplane_mode_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String airplaneMode = null;
		switch (wantedAirplaneMode) {
		case AIRPLANE_MODE_ON:
			airplaneMode = context.getString(R.string.action_change_airplane_mode_on);
			break;
		case AIRPLANE_MODE_OFF:
			airplaneMode = context.getString(R.string.action_change_airplane_mode_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_airplane_mode_dynamic_description);
		return String.format(description, airplaneMode);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedAirplaneMode) {
		case AIRPLANE_MODE_ON:
			return ImageUtils.getBitmap(R.drawable.plane_on);
		case AIRPLANE_MODE_OFF:
			return ImageUtils.getBitmap(R.drawable.plane_off);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeAirplaneModeActionEditorActivity.class;
	}
	

	@Override
	public boolean isValid(Context context) {
		switch (wantedAirplaneMode) {
		case AIRPLANE_MODE_ON:
		case AIRPLANE_MODE_OFF:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeAirplaneModeAction action = new ChangeAirplaneModeAction();
		action.setWantedAirplaneMode(getWantedAirplaneMode());
		return action;
	}

}

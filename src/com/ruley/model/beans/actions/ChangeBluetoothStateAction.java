/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeBluetoothStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeBluetoothStateAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ChangeBluetoothStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeBluetoothStateAction.class.getName());

	/**
	 * Holds the wanted bluetooth state.
	 * 
	 * <P>
	 * BluetoothAdapter.STATE_OFF:<BR/>
	 * Indicates the local Bluetooth adapter is off.<BR/>
	 * BluetoothAdapter.STATE_ON<BR/>
	 * Indicates the local Bluetooth adapter is on, and ready for use.<BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.bluetooth.BluetoothAdapter
	 */
	@JsonProperty("wantedBluetoothState")
	private int wantedBluetoothState = BluetoothAdapter.STATE_ON;

	public int getWantedBluetoothState() {
		return wantedBluetoothState;
	}

	public void setWantedBluetoothState(int wantedBluetoothState) {
		this.wantedBluetoothState = wantedBluetoothState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		// Changes bluetooth state
		if (bluetoothAdapter != null) {
			if (wantedBluetoothState == BluetoothAdapter.STATE_ON && !bluetoothAdapter.isEnabled()) {
				bluetoothAdapter.enable();
				LOGGER.info("Bluetooth state enabled");
			} else if (wantedBluetoothState == BluetoothAdapter.STATE_OFF && bluetoothAdapter.isEnabled()) {
				bluetoothAdapter.disable();
				LOGGER.info("Bluetooth state disabled");
			}
		} else {
			LOGGER.info("Bluetooth is not suppored by this hardware platform");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_bluetooth_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_bluetooth_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String bluetoothState = null;
		switch (wantedBluetoothState) {
		case BluetoothAdapter.STATE_ON:
			bluetoothState = context.getString(R.string.action_change_bluetooth_state_on);
			break;
		case BluetoothAdapter.STATE_OFF:
			bluetoothState = context.getString(R.string.action_change_bluetooth_state_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_bluetooth_state_dynamic_description);
		return String.format(description, bluetoothState);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeBluetoothStateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedBluetoothState) {
		case BluetoothAdapter.STATE_ON:
		case BluetoothAdapter.STATE_OFF:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeBluetoothStateAction action = new ChangeBluetoothStateAction();
		action.setWantedBluetoothState(getWantedBluetoothState());
		return action;
	}

}

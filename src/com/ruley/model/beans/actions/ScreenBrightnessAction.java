/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import com.ruley.R;
import com.ruley.activities.editors.actions.ScreenBrightnessActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ScreenBrightnessAction extends Action {

	private static final long serialVersionUID = -9164626618825503948L;
	
	private static final String CLASS_NAME = ScreenBrightnessAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenBrightnessAction.class.getName());
	
	private static final int NOT_FOUND = -1;
	
	public static final int MAX_BRIGHTNESS = 255;
	
	/**
	 * Holds the screen brightness mode:
	 * <P>
	 * Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC:<BR/>
	 * The screen brightness will change automatically.<BR/>
	 * <BR/>
	 * Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL:<BR/>
	 * The screen brightness will be static.
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.provider.Settings.System#SCREEN_BRIGHTNESS_MODE
	 * @see android.provider.Settings.System#SCREEN_BRIGHTNESS_MODE_AUTOMATIC
	 * @see android.provider.Settings.System#SCREEN_BRIGHTNESS_MODE_MANUAL
	 * @see android.provider.Settings.System#SCREEN_BRIGHTNESS
	 */
	@JsonProperty("wantedBrightnessMode")
	private int wantedBrightnessMode = Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
	
	@JsonProperty("brightnessPercentage")
	private int brightnessPercentage = 0;

	public int getBrightnessMode() {
		return wantedBrightnessMode;
	}

	public void setBrightnessMode(int brightnessMode) {
		switch (brightnessMode) {
		case Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC:
		case Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL:
			this.wantedBrightnessMode = brightnessMode;
			break;
		default:
			throw new IllegalArgumentException("Invalid screen brightness mode only.");
		}
	}

	public int getBrightnessPercentage() {
		return brightnessPercentage;
	}

	public void setBrightnessPercentage(int brightnessPercentage) {
		if (0 <= brightnessPercentage && brightnessPercentage <= 100) {
			this.brightnessPercentage = brightnessPercentage;
		}
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");
		
		String screenBrightnessModeName = Settings.System.SCREEN_BRIGHTNESS_MODE;
		int currBrightnessMode = NOT_FOUND;
		
		try {
			currBrightnessMode = Settings.System.getInt(context.getContentResolver(), screenBrightnessModeName);
		} catch (SettingNotFoundException e) {
			LOGGER.error("Error: ", e);
		}
		
		if (wantedBrightnessMode != currBrightnessMode) {
			switch (wantedBrightnessMode) {
			case Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC:
				Settings.System.putInt(
						context.getContentResolver(),
						screenBrightnessModeName,
						Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
				LOGGER.info("Screen brightness mode changed to: SCREEN_BRIGHTNESS_MODE_AUTOMATIC");
				break;
			case Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL:
				float percentage = brightnessPercentage / 100.0F;
				int brightnessValue = Math.round(MAX_BRIGHTNESS * percentage);
				Settings.System.putInt(
						context.getContentResolver(),
						screenBrightnessModeName,
						Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
				Settings.System.putInt(
						context.getContentResolver(),
						Settings.System.SCREEN_BRIGHTNESS, brightnessValue);
				LOGGER.info("Screen brightness mode changed to: SCREEN_BRIGHTNESS_MODE_MANUAL");
				LOGGER.info("Screen brightness changed to: " + brightnessPercentage + "%");
				LOGGER.info("Screen brightness value (0 < < 255): " + brightnessValue);
				break;
			}
		} else {
			LOGGER.info("Screen brightness mode already at the wanted state");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_screen_brightness_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_screen_brightness_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String brightnessModeStr = null;
		switch (wantedBrightnessMode) {
		case Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC:
			brightnessModeStr = context.getString(R.string.action_screen_brightness_mode_automatic);
			break;
		case Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL:
			brightnessModeStr = context.getString(R.string.action_screen_brightness_mode_manual);
			brightnessModeStr += " " + brightnessPercentage + "%";
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_screen_brightness_dynamic_description);
		return String.format(description , brightnessModeStr);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.screen_brightness);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ScreenBrightnessActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return (0 <= brightnessPercentage && brightnessPercentage <= 100)
				&& (wantedBrightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC
					|| wantedBrightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
	}
	
	@Override
	public Action getClone() {
		ScreenBrightnessAction action = new ScreenBrightnessAction();
		action.setBrightnessMode(getBrightnessMode());
		action.setBrightnessPercentage(getBrightnessPercentage());
		return action;
	}

}

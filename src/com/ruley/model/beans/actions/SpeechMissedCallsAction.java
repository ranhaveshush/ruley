/**
 * 
 */
package com.ruley.model.beans.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.CallLog.Calls;

import com.ruley.R;
import com.ruley.activities.editors.actions.SpeechMissedCallsActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.services.TextToSpeechService;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SpeechMissedCallsAction extends Action {
	
	private static final long serialVersionUID = 8403251921046117293L;

	private static final String CLASS_NAME = SpeechMissedCallsAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SpeechMissedCallsAction.class.getName());

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		String[] projection = new String[] {
				Calls.NUMBER,
				Calls.CACHED_NAME
		};
		String selection = new StringBuilder().append(Calls.TYPE)
				.append(" = ? AND ").append(Calls.NEW).append(" = ?")
				.toString();
		String[] selectionArgs = new String[] { Integer.toString(Calls.MISSED_TYPE), "1" };
		String sortOrder = Calls.DEFAULT_SORT_ORDER + " LIMIT 1";
		
		Cursor cursor = context.getContentResolver().query(
				Calls.CONTENT_URI,
				projection,
				selection,
				selectionArgs,
				sortOrder);
		
		// If there are none missed calls, do nothing
		if (cursor.getCount() < 1) {
			cursor.close();
			LOGGER.warn("There are none missed calls");
			return;
		}
		
		while (cursor.moveToNext()) {
			String from = null;
			from = cursor.getString(cursor.getColumnIndex(Calls.CACHED_NAME));
			if (from == null) {
				from = cursor.getString(cursor.getColumnIndex(Calls.NUMBER));
			}
			
			String speak = "Missed call from " + from;
			LOGGER.info(CLASS_NAME + "#handle(...) speak = " + speak);
			
			TextToSpeechService.speek(speak);
		}
		
		cursor.close();
	}
	
	@Override
	public int getNameId() {
		return R.string.action_speech_missed_calls_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_speech_missed_calls_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_speech_missed_calls_dynamic_description);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.speak);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return SpeechMissedCallsActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return true;
	}
	
	@Override
	public Action getClone() {
		SpeechMissedCallsAction action = new SpeechMissedCallsAction();
		return action;
	}

}

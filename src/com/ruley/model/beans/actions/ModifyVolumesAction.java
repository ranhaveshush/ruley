/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.activities.editors.actions.ModifyVolumesActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ModifyVolumesAction extends Action {

	private static final long serialVersionUID = -3013284038569483636L;
	
	private static final String CLASS_NAME = ModifyVolumesAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyVolumesAction.class.getName());

	/**
	 * Holds the audio stream.
	 * 
	 * <P>
	 * AudioManager.STREAM_ALARM:<BR/>
	 * The audio stream for alarms.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_DTMF:<BR/>
	 * The audio stream for DTMF Tones.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_MUSIC:<BR/>
	 * The audio stream for music playback.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_NOTIFICATION:<BR/>
	 * The audio stream for notifications.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_RING:<BR/>
	 * The audio stream for the phone ring.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_SYSTEM:<BR/>
	 * The audio stream for system sounds.<BR/>
	 * <BR/>
	 * AudioManager.STREAM_VOICE_CALL:<BR/>
	 * The audio stream for phone calls.
	 * </P>
	 * 
	 * <P>
	 * Holds the wanted audio volume.<BR/>
	 * Notice: the max volume for every stream is different.
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager
	 * @see android.media.AudioManager#setStreamVolume(int streamType, int index, int flags)
	 * @see android.media.AudioManager#getStreamMaxVolume(int)
	 */
	@JsonProperty("audioStreamsVolumes")
	private Map<Integer, Integer> audioStreamsVolumes = new HashMap<Integer, Integer>() {
		{
			put(AudioManager.STREAM_ALARM, 0);
			put(AudioManager.STREAM_DTMF, 0);
			put(AudioManager.STREAM_MUSIC, 0);
			put(AudioManager.STREAM_NOTIFICATION, 0);
			put(AudioManager.STREAM_RING, 0);
			put(AudioManager.STREAM_SYSTEM, 0);
			put(AudioManager.STREAM_VOICE_CALL, 0);
		}
	};

	/**
	 * Holds the audio flags.
	 * 
	 * <P>
	 * AudioManager.FLAG_ALLOW_RINGER_MODES:<BR/>
	 * Whether to include ringer modes as possible options when changing volume.
	 * <BR/>
	 * <BR/>
	 * AudioManager.FLAG_PLAY_SOUND:<BR/>
	 * Whether to play a sound when changing the volume.<BR/>
	 * <BR/>
	 * AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE:<BR/>
	 * Removes any sounds/vibrate that may be in the queue, or are playing
	 * (related to changing volume).<BR/>
	 * <BR/>
	 * AudioManager.FLAG_SHOW_UI:<BR/>
	 * Show a toast containing the current volume.<BR/>
	 * <BR/>
	 * AudioManager.FLAG_VIBRATE:<BR/>
	 * Whether to vibrate if going into the vibrate ringer mode.
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.media.AudioManager
	 */
	@JsonProperty("flags")
	private int flags = 0;
	
	public Map<Integer, Integer> getVolumes() {
		return audioStreamsVolumes;
	}

	public int getVolume(int audioStream) {
		return audioStreamsVolumes.get(audioStream);
	}

	public void setVolume(int audioStream, int volume) {
		audioStreamsVolumes.put(audioStream, volume);
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);

		// Changes audio stream volume settings
		for (Entry<Integer, Integer> entry : audioStreamsVolumes.entrySet()) {
			int audioStream = entry.getKey();
			int volume = entry.getValue();

			int maxVolume = audioManager.getStreamMaxVolume(audioStream);
			if (volume <= maxVolume) {
				audioManager.setStreamVolume(audioStream, volume, flags);
			}
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_modify_volumes_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_modify_volumes_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		return context.getString(R.string.action_modify_volumes_dynamic_description);
	}

	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.volumes);
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ModifyVolumesActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		if (audioStreamsVolumes == null) {
			return false;
		}
		for (Integer audioStreamVolume : audioStreamsVolumes.values()) {
			if (audioStreamVolume < 0)
				return false;
		}
		return true;
	}
	
	@Override
	public Action getClone() {
		ModifyVolumesAction action = new ModifyVolumesAction();
		Set<Entry<Integer, Integer>> volumes = getVolumes().entrySet();
		for (Entry<Integer, Integer> volume : volumes) {			
			action.setVolume(volume.getKey(), volume.getValue());
		}
		action.setFlags(getFlags());
		return action;
	}

}

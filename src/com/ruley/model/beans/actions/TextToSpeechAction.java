/**
 * 
 */
package com.ruley.model.beans.actions;

import java.util.Locale;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.ruley.R;
import com.ruley.activities.editors.actions.TextToSpeechActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.services.TextToSpeechService;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TextToSpeechAction extends Action {
	
	private static final long serialVersionUID = 3556257620323440563L;

	private static final String CLASS_NAME = TextToSpeechAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(TextToSpeechAction.class.getName());
	
	@JsonProperty("text")
	private String text = null;
	
	@JsonProperty("language")
	private Locale language = Locale.US;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Locale getLanguage() {
		return language;
	}

	public void setLanguage(Locale language) {
		this.language = language;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		TextToSpeechService.speek(text, language);
	}
	
	@Override
	public int getNameId() {
		return R.string.action_text_to_speech_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_text_to_speech_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		if (TextUtils.isEmpty(text)) {
			return context.getString(R.string.action_text_to_speech_text_missing);
		} else {
			String description = context.getString(R.string.action_text_to_speech_dynamic_description);
			return String.format(description, text);
		}
	}
	
	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.speak);
	}
	
	@Override
	public Class<? extends Activity> getEditor() {
		return TextToSpeechActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return language != null && !TextUtils.isEmpty(text);
	}
	
	@Override
	public Action getClone() {
		TextToSpeechAction action = new TextToSpeechAction();
		action.setLanguage(getLanguage());
		action.setText(getText());
		return action;
	}

}

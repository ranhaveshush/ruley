/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.ruley.R;
import com.ruley.activities.editors.actions.WakeLockActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class WakeLockAction extends Action {

	private static final long serialVersionUID = -4703821329828171357L;
	
	private static final String CLASS_NAME = WakeLockAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(WakeLockAction.class.getName());
	
	public static final boolean WAKE_LOCK_ACQUIRE = true;
	
	public static final boolean WAKE_LOCK_RELEASE = false;
	
	/**
	 * Holds the wanted ringer mode.
	 * 
	 * <P>
	 * AudioManager.RINGER_MODE_NORMAL:<BR/>
	 * Ringer mode that may be audible and may vibrate.<BR/>
	 * <BR/>
	 * AudioManager.RINGER_MODE_SILENT:<BR/>
	 * Ringer mode that will be silent and will not vibrate.<BR/>
	 * <BR/>
	 * AudioManager.RINGER_MODE_VIBRATE:<BR/>
	 * Ringer mode that will be silent and will vibrate.<BR/>
	 * <BR/>
	 * </P>
	 * 
	 * For more info:
	 * 
	 * @see android.os.PowerManager
	 * @see android.os.PowerManager#newWakeLock(int levelAndFlags, String tag)
	 */
	@JsonProperty("wantedLevelAndFlags")
	private int wantedLevelAndFlags = PowerManager.PARTIAL_WAKE_LOCK;
	
	@JsonProperty("wakeLockState")
	private boolean wakeLockState = WAKE_LOCK_ACQUIRE;

	public WakeLockAction() {
	}

	public WakeLockAction(int wantedLevelAndFlags) {
		this.wantedLevelAndFlags = wantedLevelAndFlags;
	}

	public int getWantedLevelAndFlags() {
		return wantedLevelAndFlags;
	}

	public void setWantedLevelAndFlags(int wantedLevelAndFlags) {
		this.wantedLevelAndFlags = wantedLevelAndFlags;
	}

	public boolean isWakeLockState() {
		return wakeLockState;
	}

	public void setWakeLockState(boolean wakeLockState) {
		this.wakeLockState = wakeLockState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

		// Changes ringer mode settings
		WakeLock wakeLock = powerManager.newWakeLock(this.wantedLevelAndFlags, CLASS_NAME);
		
		if (wakeLockState == WAKE_LOCK_ACQUIRE) {
			wakeLock.acquire();
			LOGGER.info("Wake Lock Acquired");
		} else if (wakeLockState == WAKE_LOCK_RELEASE) {
			wakeLock.release();
			LOGGER.info("Wake Lock Released");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_wake_lock_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_wake_lock_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String wakeLockFlag = null;
		switch (wantedLevelAndFlags) {
		case PowerManager.PARTIAL_WAKE_LOCK:
			wakeLockFlag = context.getString(R.string.action_wake_lock_partial);
			break;
		case PowerManager.SCREEN_DIM_WAKE_LOCK:
			wakeLockFlag = context.getString(R.string.action_wake_lock_screen_dim);
			break;
		case PowerManager.SCREEN_BRIGHT_WAKE_LOCK:
			wakeLockFlag = context.getString(R.string.action_wake_lock_screen_bright);
			break;
		case PowerManager.FULL_WAKE_LOCK:
			wakeLockFlag = context.getString(R.string.action_wake_lock_full);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_wake_lock_dynamic_description);
		return String.format(description, wakeLockFlag);
	}

	@Override
	public Bitmap getIcon() {
		if (wakeLockState == WAKE_LOCK_ACQUIRE) {
			return ImageUtils.getBitmap(R.drawable.lock);
		} else if (wakeLockState == WAKE_LOCK_RELEASE) {
			return ImageUtils.getBitmap(R.drawable.unlock);
		} else {
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return WakeLockActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return (wakeLockState == WAKE_LOCK_ACQUIRE
					|| wakeLockState == WAKE_LOCK_RELEASE)
				&& (wantedLevelAndFlags == PowerManager.PARTIAL_WAKE_LOCK
					|| wantedLevelAndFlags == PowerManager.SCREEN_DIM_WAKE_LOCK
					|| wantedLevelAndFlags == PowerManager.SCREEN_BRIGHT_WAKE_LOCK
					|| wantedLevelAndFlags == PowerManager.FULL_WAKE_LOCK);
	}
	
	@Override
	public Action getClone() {
		WakeLockAction action = new WakeLockAction();
		action.setWakeLockState(isWakeLockState());
		action.setWantedLevelAndFlags(getWantedLevelAndFlags());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;

import com.ruley.model.beans.device.DeviceState;

/**
 * This class is an Action class that wakes up the device to perform it's task.
 * Usually the task will involve user interaction.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class WakeAction extends Action {
	
	private static final long serialVersionUID = -6644793716804511889L;
	
	private static final String CLASS_NAME = WakeAction.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(WakeAction.class.getName());
	
	private static final int WAKE_TIME_IN_MILLIS = 1000;
	
	public static void wakeDevice(Context context) {
		LOGGER.debug(CLASS_NAME + "#wakeDevice(...)");
		
		boolean isLockPatternEnabled = DeviceState.isLockPatternEnabled();
		if (isLockPatternEnabled) {
			LOGGER.info(CLASS_NAME + "#wakeDevice(...) Lock pattern is Enabled");
		} else {
			LOGGER.info(CLASS_NAME + "#wakeDevice(...) Lock pattern is Disabled");
		}
		
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
				| PowerManager.ACQUIRE_CAUSES_WAKEUP
				| PowerManager.ON_AFTER_RELEASE, CLASS_NAME);
		wakeLock.acquire(WAKE_TIME_IN_MILLIS);
	}

	@Override
	public void perform(Context context) {
		wakeDevice(context);
	}
	
}

/**
 * 
 */
package com.ruley.model.beans.actions.base;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.graphics.Bitmap;

import com.ruley.R;
import com.ruley.model.beans.ICloneable;
import com.ruley.model.beans.ITemplatable;
import com.ruley.model.beans.rules.IRuleComponent;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.utilities.ImageUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.WRAPPER_OBJECT)
public abstract class Action implements IRuleComponent, ICloneable<Action>, ITemplatable<Action> {
	
	private static final long serialVersionUID = -3911000853284596498L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Action.class
			.getName());
	
	@JsonProperty("id")
	private UUID id;
	
	public Action() {
		super();
		this.id = UUID.randomUUID();
	}

	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}

	/**
	 * Performs the action. Every derived class should implemenet this method.
	 * This is where the action logic implemeneted.
	 * 
	 * @param context
	 */
	public abstract void perform(Context context);
	
	@Override
	public Action toTemplate() {
		return getClone();
	}

	@Override
	public boolean equals(Object another) {
		if (another == null) return false;
	    if (another == this) return true;
	    if (!(another instanceof Action)) return false;
	    Action anotherAction = ((Action)another);
		return this.getId().equals(anotherAction.getId());
	}
	
	@Override
	public Bitmap getIcon() {
		return ImageUtils.getBitmap(R.drawable.action);
	}

	@Override
	public String toString() {
		String jsonData = null;
		
		try {
			ObjectMapper objectMapper = JsonHelper.getInstance().getMapper();
			jsonData = objectMapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			 LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}
		
		return jsonData;
	}

}

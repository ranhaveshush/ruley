/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;

import com.ruley.R;
import com.ruley.activities.editors.actions.ChangeFlashlightStateActionEditorActivity;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.utilities.ImageUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
// TODO: check this action on variaty of devices
// TODO: check if this action is needed
public class ChangeFlashlightStateAction extends Action {

	private static final long serialVersionUID = -6180131979747978922L;
	
	private static final String CLASS_NAME = ChangeFlashlightStateAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeFlashlightStateAction.class.getName());
	
	public static final int FLASHLIGHT_ON = 1;
	
	public static final int FLASHLIGHT_OFF = 2;

	/**
	 * Holds the wanted flashlight state.
	 * 
	 * For more info:
	 * 
	 * @see android.hardware.Camera.Parameters
	 */
	@JsonProperty("wantedFlashlightState")
	private int wantedFlashlightState = FLASHLIGHT_ON;

	public int getWantedFlashlightState() {
		return wantedFlashlightState;
	}

	public void setWantedFlashlightState(int wantedFlashlightState) {
		this.wantedFlashlightState = wantedFlashlightState;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		// Checks flashlight availability
		// TODO: you need to filter this action by this boolean,
		// and do not show the user this action
		boolean hasCameraFlash = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

		// Changes flashlight state
		if (hasCameraFlash) {
			Camera camera = Camera.open();
			Parameters parameters = camera.getParameters();
			switch (wantedFlashlightState) {
			case FLASHLIGHT_ON:
				parameters.setFlashMode(Parameters.FLASH_MODE_TORCH);
				camera.setParameters(parameters);
				camera.startPreview();
				LOGGER.info("Flashlight turned ON");
				break;
			case FLASHLIGHT_OFF:
				parameters.setFlashMode(Parameters.FLASH_MODE_OFF);
				camera.setParameters(parameters);
				camera.stopPreview();
				LOGGER.info("Flashlight turned OFF");
				break;
			default:
				throw new IllegalArgumentException("Invalid argument flashlightState " + wantedFlashlightState);
			}
		} else {
			LOGGER.warn("Flashlight is not suppored by this hardware platform");
		}
	}
	
	@Override
	public int getNameId() {
		return R.string.action_change_flashlight_state_name;
	}
	
	@Override
	public int getDescriptionId() {
		return R.string.action_change_flashlight_state_static_description;
	}
	
	@Override
	public String getDescription(Context context) {
		String flashlightState = null;
		switch (wantedFlashlightState) {
		case FLASHLIGHT_ON:
			flashlightState = context.getString(R.string.action_change_flashlight_state_on);
			break;
		case FLASHLIGHT_OFF:
			flashlightState = context.getString(R.string.action_change_flashlight_state_off);
			break;
		default:
			break;
		}
		String description = context.getString(R.string.action_change_flashlight_state_dynamic_description);
		return String.format(description, flashlightState);
	}

	@Override
	public Bitmap getIcon() {
		switch (wantedFlashlightState) {
		case BluetoothAdapter.STATE_ON:
			return ImageUtils.getBitmap(R.drawable.screen_on);
		case BluetoothAdapter.STATE_OFF:
			return ImageUtils.getBitmap(R.drawable.screen_off);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return ChangeFlashlightStateActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		switch (wantedFlashlightState) {
		case BluetoothAdapter.STATE_ON:
		case BluetoothAdapter.STATE_OFF:
			return true;
		default:
			return false;
		}
	}
	
	@Override
	public Action getClone() {
		ChangeFlashlightStateAction action = new ChangeFlashlightStateAction();
		action.setWantedFlashlightState(getWantedFlashlightState());
		return action;
	}

}

/**
 * 
 */
package com.ruley.model.beans.actions;

import org.codehaus.jackson.annotate.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera.CameraInfo;
import android.provider.MediaStore;

import com.ruley.R;
import com.ruley.activities.editors.actions.TakePictureActionEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.actions.base.WakeAction;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class TakePictureWakeAction extends WakeAction {

	private static final long serialVersionUID = -1039523154621907548L;

	private static final String CLASS_NAME = TakePictureWakeAction.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TakePictureWakeAction.class.getName());
	
	/**
	 * Picture capture preview will be displayed when true,
	 * otherwise false.
	 */
	@JsonProperty("preview")
	private boolean preview = true;
	
	/**
	 * Holds the camera side front or back:
	 * <P>
	 * CameraInfo.CAMERA_FACING_BACK<BR/>
	 * CameraInfo.CAMERA_FACING_FRONT<BR/>
	 * </P>
	 * 
	 * @see android.hardware.Camera.CameraInfo#CAMERA_FACING_BACK
	 * @see android.hardware.Camera.CameraInfo#CAMERA_FACING_FRONT
	 */
	@JsonProperty("cameraFacing")
	private int cameraFacing = CameraInfo.CAMERA_FACING_BACK;

	public boolean isPreview() {
		return preview;
	}

	public void setPreview(boolean preview) {
		this.preview = preview;
	}

	public int getCameraFacing() {
		return cameraFacing;
	}

	public void setCameraFacing(int cameraFacing) {
		this.cameraFacing = cameraFacing;
	}

	@Override
	public void perform(Context context) {
		LOGGER.debug(CLASS_NAME + "#perform(...)");

		String imagesDirPath = Config.Paths.getAppStorageDirPath() + Config.Paths.DIR_IMAGES;
		
		if (preview) {
			super.perform(context);
			
			PickersUtils.launchCameraPicker(context,
					MediaStore.ACTION_IMAGE_CAPTURE,
					imagesDirPath);
		} else {
			PickersUtils.launchTakePicPicker(context, cameraFacing,
					imagesDirPath);
		}
	}

	@Override
	public int getNameId() {
		return R.string.action_take_picture_name;
	}

	@Override
	public int getDescriptionId() {
		return R.string.action_take_picture_static_description;
	}

	@Override
	public String getDescription(Context context) {
		String description = context.getString(R.string.action_take_picture_dynamic_description);
		String withPreviewText = context.getString(R.string.action_take_picture_with);
		String withoutPreviewText = context.getString(R.string.action_take_picture_without);
		
		switch (cameraFacing) {
		case CameraInfo.CAMERA_FACING_BACK:
			String cameraFacingBackText = context.getString(R.string.action_take_picture_camera_facing_back);
			if (preview) {
				description = String.format(description, cameraFacingBackText, withPreviewText);
			} else {
				description = String.format(description, cameraFacingBackText, withoutPreviewText);
			}
			break;
		case CameraInfo.CAMERA_FACING_FRONT:
			String cameraFacingFrontText = context.getString(R.string.action_take_picture_camera_facing_front);
			if (preview) {
				description = String.format(description, cameraFacingFrontText, withPreviewText);
			} else {
				description = String.format(description, cameraFacingFrontText, withoutPreviewText);
			}
			break;
		}
		
		return description;
	}

	@Override
	public Bitmap getIcon() {
		switch (cameraFacing) {
		case CameraInfo.CAMERA_FACING_BACK:
			return ImageUtils.getBitmap(R.drawable.camera_back);
		case CameraInfo.CAMERA_FACING_FRONT:
			return ImageUtils.getBitmap(R.drawable.camera_front);
		default:
			return null;
		}
	}

	@Override
	public Class<? extends Activity> getEditor() {
		return TakePictureActionEditorActivity.class;
	}

	@Override
	public boolean isValid(Context context) {
		return (cameraFacing == CameraInfo.CAMERA_FACING_BACK
				|| cameraFacing == CameraInfo.CAMERA_FACING_FRONT);
	}
	
	@Override
	public Action getClone() {
		TakePictureWakeAction action = new TakePictureWakeAction();
		action.setPreview(isPreview());
		action.setCameraFacing(getCameraFacing());
		return action;
	}

}

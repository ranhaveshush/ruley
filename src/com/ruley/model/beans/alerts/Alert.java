/**
 * 
 */
package com.ruley.model.beans.alerts;

import android.view.View.OnClickListener;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class Alert {
	
	public enum Type {
		ALERT_APP_DISABLED,
		ALERT_WIFI_DISABLED,
		ALERT_LOCATION_SERVICES_DISABLED
	}
	
	private Type type = null;
	
	private String text = null;
	
	private String action = null;
	
	private OnClickListener clickListener = null;

	public Alert() {
		super();
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public OnClickListener getClickListener() {
		return clickListener;
	}

	public void setClickListener(OnClickListener clickListener) {
		this.clickListener = clickListener;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other) return true;
		if (other == null || this.getClass() != other.getClass()) return false;
		Alert alert = (Alert) other;
		if (!this.type.equals(alert.type)) {
			return false;
		} else {
			return true;
		}
	}

}
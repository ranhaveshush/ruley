/**
 * 
 */
package com.ruley.model.beans.alerts;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;

import com.ruley.R;
import com.ruley.activities.SettingsActivity;
import com.ruley.app.AppState;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.ActivityRecognitionStateTrigger;
import com.ruley.model.beans.triggers.LocationProximityTrigger;
import com.ruley.model.beans.triggers.LocationStateTrigger;
import com.ruley.model.dao.impl.file.json.RuleDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.utilities.LocationClientUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AlertsManager {
	
	private static final String CLASS_NAME = AlertsManager.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AlertsManager.class.getName());
	
	private IRuleDAO ruleDAO = null;
	
	private ArrayList<Alert> alerts = null;
	
	private AlertsManager() {
		ruleDAO = new RuleDAO();
		alerts = new ArrayList<Alert>();
	}
	
	private static final class SingletonHolder {
		private static final AlertsManager INSTANCE = new AlertsManager();
	}
	
	public static AlertsManager getInstance() {
		return SingletonHolder.INSTANCE;
	}

	public ArrayList<Alert> getAlerts() {
		return alerts;
	}
	
	public Alert createAlert(final Context context, Alert.Type alertType) {
		Alert alert = null;
		
		switch (alertType) {
		case ALERT_APP_DISABLED:
			alert = new Alert();
			alert.setType(Alert.Type.ALERT_APP_DISABLED);
			alert.setText(context.getString(R.string.alert_text_app_disabled));
			alert.setAction(context.getString(R.string.alert_action_app_disabled));
			alert.setClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					context.startActivity(new Intent(context, SettingsActivity.class));
				}
			});
			break;
		case ALERT_WIFI_DISABLED:
			alert = new Alert();
			alert.setType(Alert.Type.ALERT_WIFI_DISABLED);
			alert.setText(context.getString(R.string.alert_text_wifi_disabled));
			alert.setAction(context.getString(R.string.alert_action_wifi_disabled));
			alert.setClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
				}
			});
			break;
		case ALERT_LOCATION_SERVICES_DISABLED:
			alert = new Alert();
			alert.setType(Alert.Type.ALERT_LOCATION_SERVICES_DISABLED);
			alert.setText(context.getString(R.string.alert_text_location_service_disabled));
			alert.setAction(context.getString(R.string.alert_action_location_service_disabled));
			alert.setClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					context.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
				}
			});
			break;
		}
		
		return alert;
	}
	
	public boolean hasAlert(Alert.Type alertType) {
		for (Alert alert : alerts) {
			if (alert.getType() == alertType) {
				return true;
			}
		}
		return false;
	}
	
	public Alert getAlert(Alert.Type alertType) {
		for (Alert alert : alerts) {
			if (alert.getType() == alertType) {
				return alert;
			}
		}
		return null;
	}
	
	public boolean addAlert(Alert alert) {
		return alerts.add(alert);
	}
	
	public boolean removeAlert(Alert alert) {
		return alerts.remove(alert);
	}
	
	public boolean removeAlert(Alert.Type alertType) {
		Alert alert = new Alert();
		alert.setType(alertType);
		return removeAlert(alert);
	}
	
	public void handleAlerts(Context context) {
		long startMillis = System.currentTimeMillis();
		
		// App disabled alert
		// If app is disabled and there are rules, add app disabled alert
		List<Rule> activeRules = ruleDAO.list(true);
		boolean hasActiveRules = activeRules != null && !activeRules.isEmpty();
		if (!AppState.isAppEnabled() && hasActiveRules) {
			if (!hasAlert(Alert.Type.ALERT_APP_DISABLED)) {
				Alert appDisabledAlert = createAlert(context, Alert.Type.ALERT_APP_DISABLED);
				addAlert(appDisabledAlert);
			}
		} else {
			removeAlert(Alert.Type.ALERT_APP_DISABLED);
		}
		
		List<Rule> locationProximityRules = ruleDAO.list(LocationProximityTrigger.class, true);
		List<Rule> locationStateRules = ruleDAO.list(LocationStateTrigger.class, true);
		List<Rule> activityRecognitionStateRules = ruleDAO.list(ActivityRecognitionStateTrigger.class, true);
		boolean hasWifiLocationActiveRules = 
				(locationProximityRules != null && !locationProximityRules.isEmpty())
				|| (locationStateRules != null && !locationStateRules.isEmpty())
				|| (activityRecognitionStateRules != null && !activityRecognitionStateRules.isEmpty());
		
		// Wifi disabled alert
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		// If wifi is disabled and there are rules, add wifi disabled alert
		if (!wifiManager.isWifiEnabled() && hasWifiLocationActiveRules) {
			if (!hasAlert(Alert.Type.ALERT_WIFI_DISABLED)) {
				Alert wifiDisabledAlert = createAlert(context, Alert.Type.ALERT_WIFI_DISABLED);
				addAlert(wifiDisabledAlert);
			}
		} else {
			removeAlert(Alert.Type.ALERT_WIFI_DISABLED);
		}
		
		// Location Services disabled alert
		// If location services is disabled and there are rules, add location services disabled alert
		if (!LocationClientUtils.isLocationServiceEnabled(context) && hasWifiLocationActiveRules) {
			if (!hasAlert(Alert.Type.ALERT_LOCATION_SERVICES_DISABLED)) {
				Alert locationServiceDisabledAlert = createAlert(context, Alert.Type.ALERT_LOCATION_SERVICES_DISABLED);
				addAlert(locationServiceDisabledAlert);
			}
		// If location services is enabled or there are no rules, remove location services disabled alert
		} else {
			removeAlert(Alert.Type.ALERT_LOCATION_SERVICES_DISABLED);
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#handleAlerts(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}

}

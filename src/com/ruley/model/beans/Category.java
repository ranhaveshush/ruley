/**
 * 
 */
package com.ruley.model.beans;

import java.util.Comparator;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class Category implements Comparator<Category> {
	
	private String name = null;
	
	public Category() {
		super();
	}

	public Category(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int compare(Category lhs, Category rhs) {
		return lhs.name.compareTo(rhs.name);
	}

	@Override
	public boolean equals(Object another) {
		if (another == null) return false;
	    if (another == this) return true;
	    if (!(another instanceof Category)) return false;
	    Category anotherCategory = ((Category)another);
		return this.name.equals(anotherCategory.getName());
	}
	
}
 
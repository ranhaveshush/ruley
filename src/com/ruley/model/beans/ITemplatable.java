package com.ruley.model.beans;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 * @param <T>
 */
public interface ITemplatable<T> {
	
	public T toTemplate();

}

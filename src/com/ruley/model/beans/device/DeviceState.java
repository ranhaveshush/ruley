/**
 * 
 */
package com.ruley.model.beans.device;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

import com.ruley.app.MyApplication;

/**
 * Helper class for getting the device current state.
 * 
 * For more info:
 * 
 * @see android.provider.Settings.System
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DeviceState {
	
	private static final String CLASS_NAME = DeviceState.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DeviceState.class.getName());
	
	public static final int TRUE = 1;
	
	public static final int FALSE = 0;
	
	private static enum SettingsScope {
		GLOBAL,
		SYSTEM,
		SECURE
	}
	
	private DeviceState() {
	}
	
	public static boolean isADBEnabled() {
		LOGGER.debug(CLASS_NAME + "#isADBEnabled(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.ADB_ENABLED);
	}
	
	public static boolean isAirplaneModeOn() {
		LOGGER.debug(CLASS_NAME + "#mIsAirplaneModeOn(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.AIRPLANE_MODE_ON);
	}
	
	public static boolean isAlarmAlert() {
		LOGGER.debug(CLASS_NAME + "#isAlarmAlert(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.ALARM_ALERT);
	}
	
	public static boolean isAutoTime() {
		LOGGER.debug(CLASS_NAME + "#isAutoTime(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.AUTO_TIME);
	}
	
	public static boolean isAutoTimeZone() {
		LOGGER.debug(CLASS_NAME + "#isAutoTimeZone(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.AUTO_TIME_ZONE);
	}
	
	public static boolean isBluetoothOn() {
		LOGGER.debug(CLASS_NAME + "#isBluetoothOn(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.BLUETOOTH_ON);
	}
	
	public static boolean isDataRoamingEnabled() {
		LOGGER.debug(CLASS_NAME + "#isDataRoamingEnabled(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.DATA_ROAMING);
	}
	
	@SuppressWarnings("deprecation")
	public static boolean isDimScreen() {
		LOGGER.debug(CLASS_NAME + "#isDimScreen(...)");
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			return getSettings(SettingsScope.SYSTEM, Settings.System.DIM_SCREEN);
		} else {
			return true;
		}
	}
	
	public static boolean isDTMFToneWhenDialing() {
		LOGGER.debug(CLASS_NAME + "#isDTMFToneWhenDialing(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.DTMF_TONE_WHEN_DIALING);
	}
	
	public static boolean isHapticFeedbackEnabled() {
		LOGGER.debug(CLASS_NAME + "#isHapticFeedbackEnabled(...)");
		
		return getSettings(SettingsScope.SYSTEM, Settings.System.HAPTIC_FEEDBACK_ENABLED);
	}
	
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	public static boolean isInstallNonMarketApps() {
		LOGGER.debug(CLASS_NAME + "#isInstallNonMarketApps(...)");
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			return getSettings(SettingsScope.GLOBAL, Settings.Global.INSTALL_NON_MARKET_APPS);
		} else {
			return getSettings(SettingsScope.SECURE, Settings.Secure.INSTALL_NON_MARKET_APPS);
		}
	}
	
	@SuppressWarnings("deprecation")
	@TargetApi(19)
	public static boolean isLocationProvidersAllowed() {
		LOGGER.debug(CLASS_NAME + "#isLocationProvidersAllowed(...)");
		
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
			return getSettings(SettingsScope.SECURE, Settings.Secure.LOCATION_MODE);
		} else {
			return getSettings(SettingsScope.SECURE, Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		}
	}
	
	public static boolean isLockPatternEnabled() {
		LOGGER.debug(CLASS_NAME + "#isLockPatternEnabled(...)");
		
		return getSettings(SettingsScope.SECURE, Settings.Secure.LOCK_PATTERN_ENABLED);
	}
	
	// TODO: continue wrapping the API
	/*public static boolean is() {
		LOG
		
		return getSettings(Settings.System.);
	}*/
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private static boolean getSettings(SettingsScope settingsScope, String settingName) {
		ContentResolver contentResolver = MyApplication.getContext().getContentResolver();
		
		int settingsValue = FALSE;
		
		try {
			if (settingsScope == SettingsScope.SYSTEM) {
				settingsValue = Settings.System.getInt(contentResolver, settingName);
			} else if (settingsScope == SettingsScope.SECURE) {
				settingsValue = Settings.Secure.getInt(contentResolver, settingName);
			} else if (settingsScope == SettingsScope.GLOBAL
					&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
				settingsValue = Settings.Global.getInt(contentResolver, settingName);
			}
		} catch (SettingNotFoundException e) {
			LOGGER.error("Error: ", e);
		}
		
		return (settingsValue == TRUE) ? true : false;
	}
	
	public static boolean isScreenOn() {
		LOGGER.debug(CLASS_NAME + "#isScreenOn(...)");
		
		Context context = MyApplication.getContext();
		
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		return pm.isScreenOn();
	}
	
	public static boolean hasLocationProvider() {
		LOGGER.debug(CLASS_NAME + "#hasGPS(...)");
		
		Context context = MyApplication.getContext();
		
		LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (lm == null) {
			return false;
		}
		List<String> providers = lm.getAllProviders();
		if (providers == null) {
			return false;
		}
		return providers.contains(LocationManager.GPS_PROVIDER)
				|| providers.contains(LocationManager.NETWORK_PROVIDER);
	}

	public static boolean isNetworkConnectionOnline() {
		LOGGER.debug(CLASS_NAME + "#isNetworkConnectionOnline(...)");

		boolean isNetworkConnectionOnline = false;
		
		Context appCtx = MyApplication.getContext();
		try {
			ConnectivityManager cm = (ConnectivityManager) appCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
				isNetworkConnectionOnline = true;
			} else {
				netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
					isNetworkConnectionOnline = true;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
			return false;
		}
		
		return isNetworkConnectionOnline;
	}

}

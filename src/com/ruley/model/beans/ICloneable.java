/**
 * 
 */
package com.ruley.model.beans;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public interface ICloneable<T> {
	
	public T getClone();

}

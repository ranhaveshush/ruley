/**
 * 
 */
package com.ruley.model.beans;

import android.content.Context;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public interface IValidatable {
	
	/**
	 * Return true if valid, otherwise false.
	 * 
	 * @param context
	 * @return
	 */
	public boolean isValid(Context context);

}

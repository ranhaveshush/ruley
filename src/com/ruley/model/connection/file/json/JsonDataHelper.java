/**
 * 
 */
package com.ruley.model.connection.file.json;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.config.Config;
import com.ruley.model.beans.users.base.User;
import com.ruley.utilities.FilesUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class JsonDataHelper {
	
	private static final String CLASS_NAME = JsonDataHelper.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(JsonDataHelper.class.getName());
	
	/**
	 * Returns a list of all the users.
	 * If the users where cached, returns the cached users list (cache functionality deleted).
	 * If the users wheren't cached, caches and returns the users list.
	 * 
	 * @return List of all the users
	 */
	public static List<User> read() {
		LOGGER.debug(CLASS_NAME + "#read(...)");
		
		String jsonFilePath = Config.Paths.getAppStorageDirPath() + Config.Paths.FILE_JSON_DATA;
		
		UsersWrapper usersWrapper = JsonHelper.getInstance().readJSON(jsonFilePath, UsersWrapper.class);
		
		if (usersWrapper != null) {
			return usersWrapper.getUsers();
		}
		
		return null;
	}

	/**
	 * Writes the given users to JSON file.
	 * 
	 * @param users
	 */
	public static void write(List<User> users) {
		LOGGER.debug(CLASS_NAME + "#write(...)");
		
		String jsonFilePath = Config.Paths.getAppStorageDirPath() + Config.Paths.FILE_JSON_DATA;
		
		UsersWrapper usersWrapper = new UsersWrapper();
		usersWrapper.setUsers(users);
		
		JsonHelper.getInstance().writeJSON(jsonFilePath, usersWrapper);
	}

}

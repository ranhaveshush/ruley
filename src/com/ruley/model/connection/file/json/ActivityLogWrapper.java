/**
 * 
 */
package com.ruley.model.connection.file.json;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.ruley.model.beans.rules.ActivityLog;

/**
 * This class is a activity log queue wrapper class,
 * for Jackson to serialize/deserialize activity log java beans to json file
 * and vice versa.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ActivityLogWrapper implements Serializable {
	
	private static final long serialVersionUID = 2825903956172412861L;
	
	@JsonProperty("activityLog")
	List<ActivityLog> activityLog = null;

	public ActivityLogWrapper() {
		super();
	}

	public List<ActivityLog> getActivityLog() {
		return activityLog;
	}

	public void setActivityLog(List<ActivityLog> activityLog) {
		this.activityLog = activityLog;
	}

}

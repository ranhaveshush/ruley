/**
 * 
 */
package com.ruley.model.connection.file.json;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.config.Config;
import com.ruley.model.beans.rules.ActivityLog;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class JsonActivityLogHelper {
	
	private static final String CLASS_NAME = JsonActivityLogHelper.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(JsonActivityLogHelper.class.getName());
	
	/**
	 * Returns activity log.
	 * If activity log is empty, returns empty List.
	 * 
	 * @return Activity log
	 */
	public static List<ActivityLog> read() {
		LOGGER.debug(CLASS_NAME + "#read(...)");
		
		List<ActivityLog> activityLog = null;
		
		String fileJsonActivityLog = Config.Paths.getAppStorageDirPath()
				+ Config.Paths.FILE_JSON_ACTIVITY_LOG;
		
		ActivityLogWrapper activityLogWrapper = JsonHelper.getInstance().readJSON(fileJsonActivityLog, ActivityLogWrapper.class);
		
		if (activityLogWrapper != null
			&& activityLogWrapper.getActivityLog() != null) {
			activityLog = activityLogWrapper.getActivityLog();
		} else {
			activityLog = new ArrayList<ActivityLog>();
		}
		
		return activityLog;
	}

	/**
	 * Writes the given activity log to JSON file.
	 * 
	 * @param activity log
	 */
	public static void write(List<ActivityLog> activityLog) {
		LOGGER.debug(CLASS_NAME + "#write(...)");
		
		String fileJsonActivityLog = Config.Paths.getAppStorageDirPath()
				+ Config.Paths.FILE_JSON_ACTIVITY_LOG;
		
		ActivityLogWrapper activityLogWrapper = new ActivityLogWrapper();
		activityLogWrapper.setActivityLog(activityLog);
		
		JsonHelper.getInstance().writeJSON(fileJsonActivityLog, activityLogWrapper);
	}

}

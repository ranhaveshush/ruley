/**
 * 
 */
package com.ruley.model.connection.file.json;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.ruley.model.beans.users.base.User;

/**
 * This class is a users collection wrapper class,
 * for Jackson to serialize/deserialize users java beans to json file
 * and vice versa.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class UsersWrapper implements Serializable {
	
	private static final long serialVersionUID = 2825903956172412861L;
	
	@JsonProperty("users")
	List<User> users = null;

	public UsersWrapper() {
		super();
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}

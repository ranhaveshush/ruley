/**
 * 
 */
package com.ruley.model.connection.file.json;

import java.io.File;
import java.io.IOException;

import javax.crypto.spec.SecretKeySpec;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.utilities.CryptoUtils;
import com.ruley.utilities.FilesUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class JsonHelper {

	private static final String CLASS_NAME = JsonHelper.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(JsonHelper.class.getName());
	
	private ObjectMapper mapper = null;
	
	private JsonHelper() {
		LOGGER.debug(CLASS_NAME + "#JsonHelper(...)");
		
		initObjectMapper();
	}
	
	private static class SingletonHolder {
		private static final JsonHelper INSTANCE = new JsonHelper();
	}
	
	public static JsonHelper getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public ObjectMapper getMapper() {
		LOGGER.debug(CLASS_NAME + "#getMapper(...)");
		
		return mapper;
	}
	
	/**
	 * Reads JSON file and returns JSON data.
	 * 
	 * @param filePath
	 *            String represents the file path
	 * @param JavaType
	 *            JavaType represents the JSON type of data
	 * @return
	 */
	public synchronized <T> T readJSON(String filePath, Class<T> classType) {
		LOGGER.debug(CLASS_NAME + "#readJSON(...)");

		assert (filePath != null && classType != null);
		
		long startMillis = System.currentTimeMillis();

		File file = new File(filePath);

		if (!file.exists()) {
			LOGGER.warn("File doesn't exists: " + filePath);
			long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			return null;
		}

		T data = null;

		try {
			String encryptedJson = FilesUtils.readFile(file);
			SecretKeySpec sks = CryptoUtils.getKey();
			String decryptedJson = CryptoUtils.decrypt(sks, encryptedJson);
			data = mapper.readValue(decryptedJson, classType);
		} catch (JsonParseException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");

		return data;
	}
	
	/**
	 * Reads JSON file and returns JSON data.
	 * 
	 * @param filePath
	 *            String represents the file path
	 * @param JavaType
	 *            JavaType represents the JSON type of data
	 * @return
	 */
	public synchronized <T> T readJSON(String filePath, JavaType javaType) {
		LOGGER.debug(CLASS_NAME + "#readJSON(...)");

		assert (filePath != null && javaType != null);
		
		long startMillis = System.currentTimeMillis();

		File file = new File(filePath);

		if (!file.exists()) {
			LOGGER.warn("File doesn't exists: " + filePath);
			long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			return null;
		}

		T data = null;

		try {
			String encryptedJson = FilesUtils.readFile(file);
			SecretKeySpec sks = CryptoUtils.getKey();
			String decryptedJson = CryptoUtils.decrypt(sks, encryptedJson);
			data = mapper.readValue(decryptedJson, javaType);
		} catch (JsonParseException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");

		return data;
	}

	/**
	 * Reads JSON file and returns JSON data.
	 * 
	 * @param filePath
	 *            String represents the file path
	 * @param typeReference
	 *            TypeReference represents the JSON type of data
	 * @return
	 */
	public synchronized <T> T readJSON(String filePath, TypeReference<T> typeReference) {
		LOGGER.debug(CLASS_NAME + "#readJSON(...)");

		assert (filePath != null && typeReference != null);
		
		long startMillis = System.currentTimeMillis();

		File file = new File(filePath);

		if (!file.exists()) {
			LOGGER.warn("File doesn't exists: " + filePath);
			long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			return null;
		}

		T data = null;

		try {
			String encryptedJson = FilesUtils.readFile(file);
			SecretKeySpec sks = CryptoUtils.getKey();
			String decryptedJson = CryptoUtils.decrypt(sks, encryptedJson);
			data = mapper.readValue(decryptedJson, typeReference);
		} catch (JsonParseException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");

		return data;
	}

	/**
	 * Writes the given data to JSON file.
	 * 
	 * @param filePath
	 *            String represents the file path
	 * @param data
	 *            Object represents the data to write
	 */
	public synchronized <T> void writeJSON(String filePath, T data) {
		LOGGER.debug(CLASS_NAME + "#writeJSON(...)");

		assert(filePath != null && data != null);
		
		long startMillis = System.currentTimeMillis();

		try {
			String decryptedJson = mapper.writeValueAsString(data);
			SecretKeySpec sks = CryptoUtils.getKey();
			String encryptedJson = CryptoUtils.encrypt(sks, decryptedJson);
			File file = FilesUtils.getOrCreateFile(filePath);
			FilesUtils.writeFile(file, encryptedJson);
		} catch (JsonGenerationException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#read(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}
	
	private synchronized void initObjectMapper() {
		LOGGER.debug(CLASS_NAME + "#initObjectMapper(...)");
		
		mapper = new ObjectMapper();
		mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);
		mapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.setVisibility(JsonMethod.ALL, Visibility.NONE);
	}

}

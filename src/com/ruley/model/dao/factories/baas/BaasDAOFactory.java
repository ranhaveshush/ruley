/**
 * 
 */
package com.ruley.model.dao.factories.baas;

import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActionDAO;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IDeviceStateDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BaasDAOFactory extends DAOFactory {

	@Override
	public IUserDAO getUserDAO() {
		return null;
	}

	@Override
	public IRuleDAO getRuleDAO() {
		return null;
	}

	@Override
	public ITriggerDAO getTriggerDAO() {
		return null;
	}

	@Override
	public IActionDAO getActionDAO() {
		return null;
	}

	@Override
	public IDeviceStateDAO getDeviceStateDAO() {
		return null;
	}

	@Override
	public IActivityLogDAO getActivityLogDAO() {
		return null;
	}

}

/**
 * 
 */
package com.ruley.model.dao.factories.file;

import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.impl.file.json.ActionDAO;
import com.ruley.model.dao.impl.file.json.ActivityLogDAO;
import com.ruley.model.dao.impl.file.json.DeviceStateDAO;
import com.ruley.model.dao.impl.file.json.RuleDAO;
import com.ruley.model.dao.impl.file.json.TriggerDAO;
import com.ruley.model.dao.impl.file.json.UserDAO;
import com.ruley.model.dao.logic.IActionDAO;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IDeviceStateDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class JsonDAOFactory extends DAOFactory {

	@Override
	public IUserDAO getUserDAO() {
		return new UserDAO();
	}

	@Override
	public IRuleDAO getRuleDAO() {
		return new RuleDAO();
	}

	@Override
	public ITriggerDAO getTriggerDAO() {
		return new TriggerDAO();
	}

	@Override
	public IActionDAO getActionDAO() {
		return new ActionDAO();
	}

	@Override
	public IDeviceStateDAO getDeviceStateDAO() {
		return new DeviceStateDAO();
	}
	
	public IActivityLogDAO getActivityLogDAO() {
		return new ActivityLogDAO();
	}

}

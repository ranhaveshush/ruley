/**
 * 
 */
package com.ruley.model.dao.factories;

import com.ruley.model.dao.factories.file.JsonDAOFactory;
import com.ruley.model.dao.logic.IActionDAO;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IDeviceStateDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public abstract class DAOFactory {

	/*
	 * List of data storage types supported.
	 */

	public static final int JSON = 0;
	
	/*
	 * List of DAO types supported by the factory There will be a method for
	 * each DAO that can be created. The conrete factories will have to
	 * implement there methods.
	 */
	
	public abstract IUserDAO getUserDAO();

	public abstract IRuleDAO getRuleDAO();

	public abstract ITriggerDAO getTriggerDAO();

	public abstract IActionDAO getActionDAO();
	
	public abstract IDeviceStateDAO getDeviceStateDAO();
	
	public abstract IActivityLogDAO getActivityLogDAO();

	/**
	 * Returns the requested dao factory by given constant representing the
	 * factory.
	 * 
	 * @param context
	 * @param whichFactory
	 * @return
	 */
	public static DAOFactory getFactory(int whichFactory) {
		switch (whichFactory) {
		case JSON:
			return new JsonDAOFactory();
		default:
			throw new IllegalArgumentException("Invalid factory type argument");
		}
	}
	
	/**
	 * Returns the used dao factory implementation type.
	 * 
	 * @return Integer represnts the dao factory implementation type.
	 */
	public static int getFactoryType() {
		return JSON;
	}

}

/**
 * 
 */
package com.ruley.model.dao.logic;

import java.util.List;
import java.util.UUID;

import com.ruley.model.beans.actions.base.Action;

/**
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public interface IActionDAO {

	/**
	 * Returns all the actions.
	 * 
	 * @return List of all the actions
	 */
	public List<Action> list();

	/**
	 * Returns a specific action by given action's id.
	 * 
	 * @param actionId
	 *            UUID represents the action's id
	 * @return Action
	 */
	public Action get(final UUID actionId);

	/**
	 * Stores (Inserts or Updates) a action.
	 * 
	 * @param action
	 *            Action the action to store
	 */
	public void store(final Action action);

	/**
	 * Inserts a action.
	 * 
	 * @param action
	 *            Action the action to insert
	 */
	public void insert(Action action);

	/**
	 * Updates a action.
	 * 
	 * @param action
	 *            Action the action to update
	 */
	public void update(final Action action);

	/**
	 * Deletes a action.
	 * 
	 * @param action
	 *            Action the action to delete
	 */
	public void delete(Action action);

}

/**
 * 
 */
package com.ruley.model.dao.logic;

import java.util.Map;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public interface IDeviceStateDAO {

	/**
	 * Returns all device states.
	 * 
	 * @return Map of all the states
	 */
	public Map<String, Integer> list();

	/**
	 * Returns specific state for a class name.
	 * 
	 * @param className
	 *            String represents the fully qualified class name
	 * @return Integer represents the state value
	 */
	public Integer get(String className);

	/**
	 * Updates the specific state for a class name.
	 * 
	 * @param className
	 *            String represents the fully qualified class name
	 * @param stateValue
	 *            Integer represents the state value to update
	 */
	public void set(String className, int stateValue);

}

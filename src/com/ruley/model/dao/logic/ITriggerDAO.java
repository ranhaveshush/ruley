/**
 * 
 */
package com.ruley.model.dao.logic;

import java.util.List;
import java.util.UUID;

import com.ruley.model.beans.triggers.base.Trigger;

/**
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public interface ITriggerDAO {

	/**
	 * Returns all the triggers.
	 * 
	 * @return List of all the triggers
	 */
	public List<Trigger> list();

	/**
	 * Returns all the active triggers.
	 * 
	 * @param isActive
	 *            Boolean represents triggers active or incative state
	 * @return List of all the active triggers
	 */
	public List<Trigger> list(final boolean isActive);

	/**
	 * Returns triggers by given trigger's class.
	 * 
	 * @param triggerClass
	 *            Class represents the trigger's class
	 * @return List of all the triggers with the type triggerClass
	 */
	public List<Trigger> list(Class<? extends Trigger> triggerClass);

	/**
	 * Returns all the active triggers of specific trigger's class.
	 * 
	 * @param trigger
	 *            Class Class represents the trigger's class
	 * @param isActive
	 *            Boolean represents triggers active or incative state
	 * @return List of all the active triggers of specific trigger class
	 */
	public List<Trigger> list(Class<? extends Trigger> triggerClass,
			final boolean isActive);

	/**
	 * Returns a specific trigger by given trigger's id.
	 * 
	 * @param triggerId
	 *            UUID represents the trigger's id
	 * @return Trigger
	 */
	public Trigger get(final UUID triggerId);

	/**
	 * Stores (Inserts or Updates) a trigger.
	 * 
	 * @param trigger
	 *            Trigger the tirgger to store
	 */
	public void store(Trigger trigger);

	/**
	 * Inserts a trigger.
	 * 
	 * @param trigger
	 *            Trigger the trigger to insert
	 */
	public void insert(Trigger trigger);

	/**
	 * Updates a trigger.
	 * 
	 * @param trigger
	 *            Trigger the trigger to update
	 */
	public void update(final Trigger trigger);

	/**
	 * Deletes a specific trigger by given trigger's id.
	 * 
	 * @param trigger
	 *            Trigger the trigger to delete
	 */
	public void delete(Trigger trigger);

}

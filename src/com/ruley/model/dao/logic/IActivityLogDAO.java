/**
 * 
 */
package com.ruley.model.dao.logic;

import java.util.List;
import java.util.UUID;

import com.ruley.model.beans.rules.ActivityLog;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public interface IActivityLogDAO {

	/**
	 * Returns all the activity log.
	 * 
	 * @return List represents the activity log
	 */
	public List<ActivityLog> list();

	/**
	 * Returns all the activity log.
	 * 
	 * @param ruleId
	 *            UUID represents the unique rule id
	 * @return List represents the activity log
	 */
	public List<ActivityLog> list(UUID ruleId);

	/**
	 * Returns all the activity log.
	 * 
	 * @param ruleId
	 *            UUID represents the unique rule id
	 * @param maxLines
	 *            Integer represents the max lines to return
	 * @return List represents the activity log
	 */
	public List<ActivityLog> list(UUID ruleId, int maxLines);

	/**
	 * Logs a message
	 * 
	 * @param UUID
	 *            UUID represents the component to log
	 * @param String
	 *            String represents the rule name
	 * @param action
	 *            String represents the rule's action to log
	 */
	public void log(UUID uuid, String ruleName, String action);
	
	/**
	 * Clears all the activity log.
	 * 
	 */
	public void clear();

}

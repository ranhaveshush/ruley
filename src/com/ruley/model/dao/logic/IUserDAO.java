/**
 * 
 */
package com.ruley.model.dao.logic;

import java.util.List;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.base.User;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public interface IUserDAO {

	/**
	 * Returns all the users.
	 * When there aren't any users returns null.
	 * 
	 * @return List of all the users
	 */
	public List<User> list();

	/**
	 * Returns a specific user by given user's class.
	 * 
	 * @param userClass
	 *            Class represents the user's class
	 * @return User
	 */
	public User get(Class<? extends User> userClass);

	/**
	 * Returns a specific user by given user's identifier.
	 * 
	 * @param user
	 *            User represents the user to get
	 * @return User
	 */
	public User get(final User user);

	/**
	 * Returns a specific user by given user's rule.
	 * 
	 * @param rule
	 *            Rule represents the user's rule
	 * @return User
	 */
	public User get(final Rule rule);

	/**
	 * Stores (Inserts or Updates) a user.
	 * 
	 * @param user
	 *            User the user to store
	 */
	public void store(final User user);

	/**
	 * Inserts a user.
	 * 
	 * @param user
	 *            User the user to insert
	 */
	public void insert(final User user);

	/**
	 * Updates a user.
	 * 
	 * @param user
	 *            User the user to update
	 */
	public void update(final User user);

	/**
	 * Deletes a specific user.
	 * 
	 * @param user
	 *            User the user to delete
	 */
	public void delete(final User user);

}

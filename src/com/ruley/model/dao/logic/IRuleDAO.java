/**
 * 
 */
package com.ruley.model.dao.logic;

import java.util.List;
import java.util.UUID;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.base.User;

/**
 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
 * 
 */
public interface IRuleDAO {

	/**
	 * Returns all the rules.
	 * 
	 * @return List of all the rules, if none found returns null
	 */
	public List<Rule> list();

	/**
	 * Returns all the active rules.
	 * 
	 * @param isActive
	 *            Boolean represents the rule active state
	 * @return List of all the active rules, if none found returns null
	 */
	public List<Rule> list(final boolean isActive);

	/**
	 * Returns all the active and satisfied rules.
	 * 
	 * @param isActive
	 *            Boolean represents the rule's active state
	 * @param isSatisfied
	 *            Boolean represents the rule's satisfaction state
	 * @return List of all the active and satisfied rules, if none found returns null
	 */
	public List<Rule> list(final boolean isActive, final boolean isSatisfied);

	/**
	 * Returns all the active rules contains the given trigger.
	 * 
	 * @param triggerClass
	 *            Class represents the trigger's class
	 * @param isActive
	 *            Boolean represents the rule's active state
	 * @return List of all the active and satisfied rules, if none found returns null
	 */
	public List<Rule> list(Class<? extends Trigger> triggerClass,
			boolean isActive);

	/**
	 * Returns all the active and satisfied rules contains the given trigger.
	 * 
	 * @param triggerClass
	 *            Class represents the trigger's class
	 * @param isActive
	 *            Boolean represents the rule's active state
	 * @param isSatisfied
	 *            Boolean represents the rule's satisfaction state
	 * @return List of all the active and satisfied rules, if none found returns null
	 */
	public List<Rule> list(Class<? extends Trigger> triggerClass,
			final boolean isActive, final boolean isSatisfied);

	/**
	 * Returns a specific rule by given rule.
	 * 
	 * @param rule
	 *            Rule represents the rule to get
	 * @return Rule if found returns the rule, otherwise null
	 */
	public Rule get(final Rule rule);

	/**
	 * Returns a specific rule by given rule's id.
	 * 
	 * @param ruleId
	 *            UUID represents the rule's id to get
	 * @return Rule if found returns the rule, otherwise null
	 */
	public Rule get(final UUID ruleId);

	/**
	 * Returne a specific rule by given trigger's id
	 * 
	 * @param triggerId
	 *            UUID represents the trigger's id
	 * @param isActive
	 *            Boolean represents the rule's active state
	 * @param isSatisfied
	 *            Boolean represents the rule's satisfaction state
	 * @return Rule if found returns the rule, otherwise null
	 */
	public Rule get(UUID triggerId, boolean isActive, boolean isSatisfied);

	/**
	 * Stores (Inserts or Updates) a rule.
	 * 
	 * @param rule
	 *            Rule the rule to store
	 * @param userClass
	 *            Class<User> the user to store the rule class
	 */
	public void store(Class<? extends User> userClass, final Rule rule);

	/**
	 * Inserts a rule.
	 * 
	 * @param rule
	 *            Rule the rule to insert
	 */
	public void insert(final Rule rule);

	/**
	 * Updates a rule.
	 * 
	 * @param rule
	 *            Rule the rule to update
	 */
	public void update(final Rule rule);

	/**
	 * Deletes a specific rule by given rule's id.
	 * 
	 * @param rule
	 *            Rule the rule to delete
	 */
	public void delete(final Rule rule);

}

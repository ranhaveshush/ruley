/**
 * 
 */
package com.ruley.model.dao.impl.file.json;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import android.text.TextUtils;

import com.parse.ParseUser;
import com.ruley.model.beans.rules.ActivityLog;
import com.ruley.model.connection.file.json.JsonActivityLogHelper;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.AnalyticsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ActivityLogDAO implements IActivityLogDAO {
	
	private static final int ACTIVITY_LOG_CAPACITY = 100;

	public List<ActivityLog> list() {
		List<ActivityLog> activity = JsonActivityLogHelper.read();

		if (activity == null) {
			activity = new ArrayList<ActivityLog>();
		}

		return activity;
	}

	public List<ActivityLog> list(UUID ruleId) {

		assert (ruleId != null);
		
		List<ActivityLog> activity = new ArrayList<ActivityLog>();
		List<ActivityLog> temp = JsonActivityLogHelper.read();

		if (temp != null) {
			for (ActivityLog activityLog : temp) {
				if (activityLog.getRuleId().equals(ruleId)) {
					activity.add(activityLog);
				}
			}
		}

		return activity;
	}

	public List<ActivityLog> list(UUID ruleId, int maxLines) {
		
		assert(ruleId != null && maxLines > 0);
		
		List<ActivityLog> activity = new ArrayList<ActivityLog>();
		List<ActivityLog> temp = JsonActivityLogHelper.read();

		if (temp != null) {
			int activityLogLines = 0;
			for (ActivityLog activityLog : temp) {
				if (activityLog.getRuleId().equals(ruleId)) {
					activity.add(activityLog);
					if (++activityLogLines >= maxLines) {
						break;
					}
				}
			}
		}

		return activity;
	}

	public void log(UUID ruleId, String ruleName, String action) {

		assert(ruleId != null
				&& !TextUtils.isEmpty(ruleName)
				&& !TextUtils.isEmpty(action));

		ParseUser currentUser = ParseUser.getCurrentUser();
		String userName = currentUser.getString(UserParse.FIELD_NAME) + ", "
				+ currentUser.getUsername();
		
		Calendar now = Calendar.getInstance(ActivityLog.DEFAULT_LOCALE);
		
		ActivityLog activityLog = new ActivityLog();
		activityLog.setUserName(userName);
		activityLog.setRuleId(ruleId);
		activityLog.setRuleName(ruleName);
		activityLog.setAction(action);
		activityLog.setTimestamp(now);

		List<ActivityLog> activity = JsonActivityLogHelper.read();
		
		if (activity.size() >= ACTIVITY_LOG_CAPACITY) {
			activity.remove(ACTIVITY_LOG_CAPACITY - 1);
		}

		activity.add(0, activityLog);
		
		JsonActivityLogHelper.write(activity);
		
		AnalyticsUtils.sendActivityLogEvent(activityLog);
	}

	@Override
	public void clear() {
		JsonActivityLogHelper.write(new ArrayList<ActivityLog>());
	}

}

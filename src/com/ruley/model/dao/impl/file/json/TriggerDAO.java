/**
 * 
 */
package com.ruley.model.dao.impl.file.json;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.connection.file.json.JsonDataHelper;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TriggerDAO implements ITriggerDAO {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TriggerDAO.class.getName());

	@Override
	public List<Trigger> list() {
		LOGGER.debug("TriggerDAO#list(...)");
		
		List<Trigger> foundTriggers = new ArrayList<Trigger>();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		
		List<Rule> foundRules = ruleDAO.list();
		
		if (foundRules != null && !foundRules.isEmpty()) {
			for (Rule foundRule : foundRules) {
				foundTriggers.addAll(foundRule.getTriggers());
			}
		}
		
		return foundTriggers;
	}

	@Override
	public List<Trigger> list(boolean isActive) {
		LOGGER.debug("TriggerDAO#list(...)");
		
		List<Trigger> foundTriggers = new ArrayList<Trigger>();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		
		List<Rule> activeRules = ruleDAO.list(isActive);
		
		if (activeRules != null && !activeRules.isEmpty()) {
			for (Rule activeRule : activeRules) {
				foundTriggers.addAll(activeRule.getTriggers());
			}
		}
		
		return foundTriggers;
	}

	@Override
	public List<Trigger> list(Class<? extends Trigger> triggerClass) {
		LOGGER.debug("TriggerDAO#list(...)");
		
		assert(triggerClass != null);
		
		List<Trigger> foundTriggers = new ArrayList<Trigger>();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		
		List<Rule> foundRules = ruleDAO.list();
		
		if (foundRules != null && !foundRules.isEmpty()) {
			for (Rule currRule : foundRules) {
				for (Trigger currTrigger : currRule.getTriggers()) {
					if (triggerClass.isInstance(currTrigger)) {
						foundTriggers.add(currTrigger);
					}
				}
			}
		}
		
		return foundTriggers;
	}

	@Override
	public List<Trigger> list(Class<? extends Trigger> triggerClass, boolean isActive) {
		LOGGER.debug("TriggerDAO#list(...)");
		
		assert(triggerClass != null);
		
		List<Trigger> foundTriggers = new ArrayList<Trigger>();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		
		List<Rule> foundRules = ruleDAO.list(triggerClass, isActive);
		
		if (foundRules != null && !foundRules.isEmpty()) {
			for (Rule currRule : foundRules) {
				for (Trigger currTrigger : currRule.getTriggers()) {
					if (triggerClass.isInstance(currTrigger)) {
						foundTriggers.add(currTrigger);
					}
				}
			}
		}
		
		return foundTriggers;
	}

	@Override
	public Trigger get(UUID triggerId) {
		LOGGER.debug("TriggerDAO#get(...)");
		
		assert(triggerId != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					for (Trigger currTrigger : currRule.getTriggers()) {
						if (triggerId.equals(currTrigger.getId())) {
							return currTrigger;
						}
					}
				}
			}
		}
		
		return null;
	}

	@Override
	public void store(Trigger trigger) {
		LOGGER.debug("TriggerDAO#store(...)");
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			List<Trigger> ruleTriggers = null;
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					ruleTriggers = currRule.getTriggers();
					if (ruleTriggers.remove(trigger)) {
						ruleTriggers.add(trigger);
						JsonDataHelper.write(users);
						return;
					}
				}
			}
		}
		
		LOGGER.error(TriggerDAO.class.getName() + " this trigger is not exists");
	}

	@Override
	public void insert(Trigger trigger) {
		LOGGER.warn(TriggerDAO.class.getName() + " # insert(...) method not implemented");
		
		assert(false);
	}

	@Override
	public void update(Trigger trigger) {
		LOGGER.warn(TriggerDAO.class.getName() + " # update(...) method not implemented");
		
		assert(false);
	}

	@Override
	public void delete(Trigger trigger) {
		LOGGER.debug("TriggerDAO#delete(...)");
		
		assert(trigger != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.getTriggers().remove(trigger)) {
						JsonDataHelper.write(users);
						return;
					}
				}
			}
		}
	}

}

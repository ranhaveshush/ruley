/**
 * 
 */
package com.ruley.model.dao.impl.file.json;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.connection.file.json.JsonDataHelper;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class UserDAO implements IUserDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDAO.class
			.getName());

	@Override
	public List<User> list() {
		LOGGER.debug("UserDAO#list(...)");
		
		return JsonDataHelper.read();
	}

	@Override
	public User get(Class<? extends User> userClass) {
		LOGGER.debug("UserDAO#get(...)");
		
		assert(userClass != null);
		
		List<User> users = JsonDataHelper.read();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				if (userClass.isInstance(currUser)) {
					return currUser;
				}
			}
		}
		
		return null;
	}

	@Override
	public User get(User user) {
		LOGGER.debug("UserDAO#get(...)");
		
		assert(user != null);
		
		List<User> users = JsonDataHelper.read();
		
		if (users != null && !users.isEmpty()) {
			int userPos = users.indexOf(user);
			
			if (userPos != -1) {
				return users.get(userPos);
			}
		}
		
		return null;
	}

	@Override
	public User get(Rule rule) {
		LOGGER.debug("UserDAO#get(...)");
		
		assert(rule != null);
		
		List<User> users = JsonDataHelper.read();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				if (currUser.getRules().contains(rule)) {
					return currUser;
				}
			}
		}
		
		return null;
	}

	@Override
	public void store(User user) {
		LOGGER.debug("UserDAO#store(...)");
		
		assert(user != null);
		
		List<User> users = JsonDataHelper.read();
		
		int userPos;
		
		if (users == null) {
			users = new ArrayList<User>();
			users.add(user);
		} else if (users.isEmpty()) {
			users.add(user);
		} else if ((userPos = users.indexOf(user)) != -1) {
			users.remove(userPos);
			users.add(user);
		} else {
			users.add(user);
		}
		
		JsonDataHelper.write(users);
	}

	@Override
	public void insert(User user) {
		LOGGER.warn(UserDAO.class.getName() + " # insert(...) method not implemented");
		
		assert(false);
	}

	@Override
	public void update(User user) {
		LOGGER.warn(UserDAO.class.getName() + " # update(...) method not implemented");
		
		assert(false);
	}

	@Override
	public void delete(User user) {
		LOGGER.debug("UserDAO#delete(...)");
		
		assert(user != null);
		
		List<User> users = JsonDataHelper.read();
		
		if (users != null && !users.isEmpty() && users.remove(user)) {
			JsonDataHelper.write(users);
		}
	}

}

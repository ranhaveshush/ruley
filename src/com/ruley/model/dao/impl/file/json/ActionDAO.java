/**
 * 
 */
package com.ruley.model.dao.impl.file.json;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.connection.file.json.JsonDataHelper;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActionDAO;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ActionDAO implements IActionDAO {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ActionDAO.class.getName());

	@Override
	public List<Action> list() {
		LOGGER.debug("ActionDAO#list(...)");
		
		List<Action> foundActions = null;
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			foundActions = new ArrayList<Action>();
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					foundActions.addAll(currRule.getActions());
				}
			}
		}
		
		return foundActions;
	}

	@Override
	public Action get(UUID actionId) {
		LOGGER.debug("ActionDAO#get(...)");
		
		assert(actionId != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					for (Action currAction : currRule.getActions()) {
						if (actionId.equals(currAction.getId())) {
							return currAction;
						}
					}
				}
			}
		}
		
		return null;
	}

	@Override
	public void store(Action action) {
		LOGGER.debug("ActionDAO#store(...)");
		
		assert(action != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			List<Action> ruleActions = null;
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					ruleActions = currRule.getActions();
					if (ruleActions.remove(action)) {
						ruleActions.add(action);
						JsonDataHelper.write(users);
						return;
					}
				}
			}
		}
		
		LOGGER.error(ActionDAO.class.getName() + " this action is not exists");
	}

	@Override
	public void insert(Action action) {
		LOGGER.warn(ActionDAO.class.getName() + " # insert(...) method not implemented");
		
		assert(false);
	}

	@Override
	public void update(Action action) {
		LOGGER.warn(ActionDAO.class.getName() + " # update(...) method not implemented");
		
		assert(false);
	}

	@Override
	public void delete(Action action) {
		LOGGER.debug("ActionDAO#delete(...)");
		
		assert(action != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.getActions().remove(action)) {
						JsonDataHelper.write(users);
						return;
					}
				}
			}
		}
	}

}

/**
 * 
 */
package com.ruley.model.dao.impl.file.json;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.connection.file.json.JsonDataHelper;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RuleDAO implements IRuleDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RuleDAO.class
			.getName());

	@Override
	public List<Rule> list() {
		LOGGER.debug("RuleDAO#list(...)");
		
		List<Rule> foundRules = null;
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			foundRules = new ArrayList<Rule>();
			
			for (User currUser : users) {
				foundRules.addAll(currUser.getRules());
			}
		}
		
		return foundRules;
	}

	@Override
	public List<Rule> list(boolean isActive) {
		LOGGER.debug("RuleDAO#list(...)");
		
		List<Rule> foundRules = null;
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			foundRules = new ArrayList<Rule>();
			
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.isActive() == isActive) {
						foundRules.add(currRule);
					}
				}
			}
		}
		
		return foundRules;
	}

	@Override
	public List<Rule> list(boolean isActive, boolean isSatisfied) {
		LOGGER.debug("RuleDAO#list(...)");
		
		List<Rule> foundRules = null;
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			foundRules = new ArrayList<Rule>();
			
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.isSatisfied() == isSatisfied
							&& currRule.isActive() == isActive) {
						foundRules.add(currRule);
					}
				}
			}
		}
		
		return foundRules;
	}
	
	@Override
	public List<Rule> list(Class<? extends Trigger> triggerClass, boolean isActive) {
		LOGGER.debug("RuleDAO#list(...)");
		
		assert(triggerClass != null);
		
		List<Rule> foundRules = null;
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			foundRules = new ArrayList<Rule>();
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.isActive() == isActive) {
						for (Trigger currTrigger : currRule.getTriggers()) {
							if (triggerClass.isInstance(currTrigger)) {
								foundRules.add(currRule);
								break;
							}
						}
					}
				}
			}
		}
		
		return foundRules;
	}
	
	@Override
	public List<Rule> list(Class<? extends Trigger> triggerClass, boolean isActive, boolean isSatisfied) {
		LOGGER.debug("RuleDAO#list(...)");
		
		assert(triggerClass != null);
		
		List<Rule> foundRules = null;
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			foundRules = new ArrayList<Rule>();
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.isSatisfied() == isSatisfied && currRule.isActive() == isActive) {
						for (Trigger currTrigger : currRule.getTriggers()) {
							if (triggerClass.isInstance(currTrigger)) {
								foundRules.add(currRule);
								break;
							}
						}
					}
				}
			}
		}
		
		return foundRules;
	}

	@Override
	public Rule get(Rule rule) {
		LOGGER.debug("RuleDAO#get(...)");
		
		assert(rule != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			List<Rule> userRules = null;
			for (User currUser : users) {
				userRules = currUser.getRules();
				int rulePos = userRules.indexOf(rule);
				if (rulePos != -1) {
					return userRules.get(rulePos);
				}
			}
		}
		
		return null;
	}

	@Override
	public Rule get(UUID ruleId) {
		LOGGER.debug("RuleDAO#get(...)");
		
		assert(ruleId != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (ruleId.equals(currRule.getId())) {
						return currRule;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public Rule get(UUID triggerId, boolean isActive, boolean isSatisfied) {
		LOGGER.debug("RuleDAO#get(...)");
		
		assert(triggerId != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				for (Rule currRule : currUser.getRules()) {
					if (currRule.isSatisfied() == isSatisfied && currRule.isActive() == isActive) {
						for (Trigger currTrigger : currRule.getTriggers()) {
							if (triggerId.equals(currTrigger.getId())) {
								return currRule;
							}
						}
					}
				}
			}
		}
		
		return null;
	}

	@Override
	public void store(Class<? extends User> userClass, Rule rule) {
		LOGGER.debug("RuleDAO#store(...)");
		
		assert(userClass != null && rule != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		User user = userDAO.get(userClass);
		if (user == null) {
			return;
		}
		
		List<Rule> userRules = user.getRules();
		int rulePos = userRules.indexOf(rule);
		// If user does have the rule, update the rule
		if (rulePos != -1) {
			userRules.remove(rulePos);
			userRules.add(rulePos, rule);
		// If user doesn't have the rule, add the rule
		} else {
			user.getRules().add(0, rule);
		}
		
		userDAO.store(user);
	}

	@Override
	public void insert(Rule rule) {
		LOGGER.warn(RuleDAO.class.getName() + " # insert(Rule rule) method not implemented");
		assert(false);
	}

	@Override
	public void update(Rule rule) {
		LOGGER.warn(RuleDAO.class.getName() + " # update(Rule rule) method not implemented");
		assert(false);
	}

	@Override
	public void delete(Rule rule) {
		LOGGER.debug("RuleDAO#delete(...)");
		
		assert(rule != null);
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<User> users = userDAO.list();
		
		// If default user does have the rule, delete the rule
		if (users != null && !users.isEmpty()) {
			for (User currUser : users) {
				if (currUser.getRules().remove(rule)) {
					JsonDataHelper.write(users);
					return;
				}
			}
		}
	}

}

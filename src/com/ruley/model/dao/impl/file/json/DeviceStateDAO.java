/**
 * 
 */
package com.ruley.model.dao.impl.file.json;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.config.Config;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.model.dao.logic.IDeviceStateDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DeviceStateDAO implements IDeviceStateDAO {
	
	private static final String CLASS_NAME = DeviceStateDAO.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DeviceStateDAO.class.getName());
	
	private static final Integer NOT_FOUND = -1;

	@Override
	public Map<String, Integer> list() {
		LOGGER.debug(CLASS_NAME + "#list(...)");
		
		String fileJsonDeviceState = Config.Paths.getAppStorageDirPath()
		+ Config.Paths.FILE_JSON_DEVICE_STATE;
		
		return JsonHelper.getInstance().readJSON(fileJsonDeviceState, new TypeReference<Map<String, Integer>>() {});
	}

	@Override
	public Integer get(String className) {
		LOGGER.debug(CLASS_NAME + "#get(...)");
		
		String fileJsonDeviceState = Config.Paths.getAppStorageDirPath()
				+ Config.Paths.FILE_JSON_DEVICE_STATE;
		
		Integer stateValue = NOT_FOUND;
		
		Map<String, Integer> deviceState = JsonHelper.getInstance()
				.readJSON(fileJsonDeviceState, new TypeReference<HashMap<String, Integer>>() {});
		if (deviceState != null) {
			Integer tempValue = deviceState.get(className);
			stateValue = (tempValue == null) ? NOT_FOUND : tempValue;
		}
		
		return stateValue;
	}

	@Override
	public void set(String className, int stateValue) {
		LOGGER.debug(CLASS_NAME + "#set(...)");
		
		String fileJsonDeviceState = Config.Paths.getAppStorageDirPath()
				+ Config.Paths.FILE_JSON_DEVICE_STATE;
		
		Map<String, Integer> deviceState = JsonHelper.getInstance()
				.readJSON(fileJsonDeviceState, new TypeReference<HashMap<String, Integer>>() {});
		
		if (deviceState == null) {
			deviceState = new HashMap<String, Integer>();
		}
		
		deviceState.put(className, stateValue);
		JsonHelper.getInstance().writeJSON(fileJsonDeviceState, deviceState);
	}

}

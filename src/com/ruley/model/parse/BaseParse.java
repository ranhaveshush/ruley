/**
 * 
 */
package com.ruley.model.parse;

import java.io.Serializable;
import java.util.List;

import com.parse.DeleteCallback;
import com.parse.ParseObject;
import com.parse.SaveCallback;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class BaseParse extends ParseObject implements Serializable {
	
	private static final long serialVersionUID = -5121223002675374744L;
	
	public static final String FIELD_OBJECT_ID = "objectId";

	public static final String FIELD_CREATED_AT = "createdAt";
	
	public static final String FIELD_UPDATED_AT = "updatedAt";

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		String objectId = getObjectId();
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		BaseParse other = (BaseParse) obj;
		String otherObjectId = other.getObjectId();
		if (otherObjectId != null && this.getObjectId() != null) {
			return this.getObjectId().equals(otherObjectId);
		} else {
			return false;
		}
	}
	
	public static <T extends ParseObject> void saveAllInBackground(List<T> objects, SaveCallback callback) {
		ParseObject.saveAllInBackground(objects, callback);
	}
	
	public static <T extends ParseObject> void deleteAllInBackground(List<T> objects, DeleteCallback callback) {
		ParseObject.deleteAllInBackground(objects, callback);
	}
	
}

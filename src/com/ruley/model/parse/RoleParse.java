/**
 * 
 */
package com.ruley.model.parse;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RoleParse {
	
	private RoleParse() {
	}
	
	public static final String ROLE_ADMINS = "Administrators";
	
	public static final String ROLE_MODERATORS = "Moderators";

}

/**
 * 
 */
package com.ruley.model.parse;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.content.Context;

import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.FindCallback;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class UserParse {
	
	private UserParse() {
	}
	
	public static final String FIELD_OBJECT_ID = "objectId";

	public static final String FIELD_CREATED_AT = "createdAt";
	
	public static final String FIELD_UPDATED_AT = "updatedAt";
	
	public static final String FIELD_NAME = "name";
	
	public static final String RELATION_SAVED_RULES = "savedRules";
	
	public static final String RELATION_UPLOADED_TEMPLATE_RULES = "uploadedTemplateRules";
	
	public static final String RELATION_DOWNLOADED_TEMPLATE_RULES = "downloadedTemplateRules";
	
	public static final String RELATION_LIKED_TEMPLATE_RULES = "likedTemplateRules";
	
	private static Set<UUID> sLikedRulesIds = new HashSet<UUID>();
	
	public static void cacheLikedRulesIds(Context context) {
		ParseUtils.findTemplateRules(context, UserParse.RELATION_LIKED_TEMPLATE_RULES, new FindCallback<TemplateRule>() {
			@Override
			public void done(List<TemplateRule> templateRules) {
				clearLikedRulesIds();
				for (TemplateRule templateRule : templateRules) {
					UserParse.sLikedRulesIds.add(templateRule.getId());
				}
			}
		});
	}
	
	public static void clearLikedRulesIds() {
		UserParse.sLikedRulesIds.clear();
	}
	
	public static void addLikedRule(UUID id) {
		UserParse.sLikedRulesIds.add(id);
	}
	
	public static void removeLikedRule(UUID id) {
		UserParse.sLikedRulesIds.remove(id);
	}
	
	public static boolean isLiked(UUID id) {
		return UserParse.sLikedRulesIds.contains(id);
	}

}

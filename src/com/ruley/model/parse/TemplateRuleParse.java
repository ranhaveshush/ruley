/**
 * 
 */
package com.ruley.model.parse;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.text.TextUtils;

import com.parse.ParseClassName;
import com.parse.ParseUser;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.connection.file.json.JsonHelper;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@ParseClassName("TemplateRule")
public class TemplateRuleParse extends BaseParse {
	
	private static final long serialVersionUID = 5124701728697959963L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TemplateRuleParse.class.getName());
	
	public static final String FIELD_RULE_ID = "ruleId";
	
	public static final String FIELD_RULE_JSON = "ruleJson";
	
	public static final String FIELD_IS_FEATURED = "isFeatured";
	
	public static final String FIELD_NUM_OF_DOWNLOADS = "numOfDownloads";

	public static final String FIELD_NUM_OF_LIKES = "numOfLikes";
	
	public static final String FIELD_CREATED_BY = "createdBy";
	
	private TemplateRule mTemplateRule = null;
	
	public TemplateRuleParse() {
		super();
	}
	
	public TemplateRuleParse(TemplateRule templateRule) {
		super();
		
		setRuleId(templateRule.getId());
		setTemplateRule(templateRule);
		setIsFeatured(false);
		setNumOfDownloads(0);
		setNumOfLikes(0);
		setCreatedBy(ParseUser.getCurrentUser());
	}
	
	public UUID getRuleId() {
		String ruleId = getString(FIELD_RULE_ID);
		return UUID.fromString(ruleId);
	}
	
	public void setRuleId(UUID ruleId) {
		put(FIELD_RULE_ID, ruleId.toString());
	}
	
	public TemplateRule getTemplateRule() {
		if (mTemplateRule == null) {
			try {
				String ruleJson = getString(FIELD_RULE_JSON);
				ObjectMapper mapper = JsonHelper.getInstance().getMapper();
				mTemplateRule = mapper.readValue(ruleJson, TemplateRule.class);
			} catch (JsonParseException e) {
				LOGGER.error("Error: ", e);
			} catch (JsonMappingException e) {
				LOGGER.error("Error: ", e);
			} catch (IOException e) {
				LOGGER.error("Error: ", e);
			}
		}
		
		try {
			// TODO: this is a workaround (see Issue#247)
			String createdByName = mTemplateRule.getCreatedByName();
			if (TextUtils.isEmpty(createdByName)) {
				String authorUserName = getCreatedBy().getString(UserParse.FIELD_NAME);
				mTemplateRule.setCreatedByName(authorUserName);
			}
		} catch (Exception e) {
		}
		
		return mTemplateRule;
	}

	public void setTemplateRule(TemplateRule templateRule) {
		if (templateRule != null) {
			put(FIELD_RULE_JSON, templateRule.toString());
			mTemplateRule = templateRule;
		}
	}
	
	public boolean getIsFeatured() {
		return getBoolean(FIELD_IS_FEATURED);
	}
	
	public void setIsFeatured(boolean isFeatured) {
		put(FIELD_IS_FEATURED, isFeatured);
	}

	public int getNumOfDownloads() {
		return getInt(FIELD_NUM_OF_DOWNLOADS);
	}

	public void setNumOfDownloads(int numOfDownloads) {
		if (numOfDownloads >= 0) {
			put(FIELD_NUM_OF_DOWNLOADS, numOfDownloads);
		}
	}

	public int getNumOfLikes() {
		return getInt(FIELD_NUM_OF_LIKES);
	}

	public void setNumOfLikes(int numOfLikes) {
		if (numOfLikes >= 0) {
			put(FIELD_NUM_OF_LIKES, numOfLikes);
		}
	}
	
	public ParseUser getCreatedBy() {
		return getParseUser(FIELD_CREATED_BY);
	}

	public void setCreatedBy(ParseUser user) {
		put(FIELD_CREATED_BY, user);
	}
	
	public void incrementNumOfDownloads() {
		increment(FIELD_NUM_OF_DOWNLOADS);
	}
	
	public void incrementNumOfDownloads(int amount) {
		increment(FIELD_NUM_OF_DOWNLOADS, amount);
	}
	
	public void incrementNumOfLikes() {
		increment(FIELD_NUM_OF_LIKES);
	}
	
	public void incrementNumOfLikes(int amount) {
		increment(FIELD_NUM_OF_LIKES, amount);
	}
	
	public boolean isLiked() {
		return UserParse.isLiked(getRuleId());
	}
	
}

/**
 * 
 */
package com.ruley.model.parse;

import java.io.IOException;
import java.util.UUID;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.parse.ParseClassName;
import com.parse.ParseUser;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.connection.file.json.JsonHelper;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@ParseClassName("Rule")
public class RuleParse extends BaseParse {
	
	private static final long serialVersionUID = 5124701728697959963L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(RuleParse.class.getName());
	
	public static final String FIELD_RULE_ID = "ruleId";
	
	public static final String FIELD_RULE_JSON = "ruleJson";
	
	public static final String FIELD_OWNED_BY = "ownedBy";
	
	private Rule mRule = null;
	
	public RuleParse() {
		super();
	}
	
	public RuleParse(Rule rule) {
		super();
		
		setRuleId(rule.getId());
		setRule(rule);
		setOwnedBy(ParseUser.getCurrentUser());
	}
	
	public UUID getRuleId() {
		String ruleId = getString(FIELD_RULE_ID);
		return UUID.fromString(ruleId);
	}
	
	public void setRuleId(UUID ruleId) {
		put(FIELD_RULE_ID, ruleId.toString());
	}
	
	public Rule getRule() {
		if (mRule == null) {
			try {
				String ruleJson = getString(FIELD_RULE_JSON);
				ObjectMapper mapper = JsonHelper.getInstance().getMapper();
				mRule = mapper.readValue(ruleJson, Rule.class);
			} catch (JsonParseException e) {
				LOGGER.error("Error: ", e);
			} catch (JsonMappingException e) {
				LOGGER.error("Error: ", e);
			} catch (IOException e) {
				LOGGER.error("Error: ", e);
			}
		}
		
		return mRule;
	}

	public void setRule(Rule rule) {
		if (rule != null) {
			put(FIELD_RULE_JSON, rule.toString());
			mRule = rule;
		}
	}
	
	public ParseUser getOwnedBy() {
		return getParseUser(FIELD_OWNED_BY);
	}

	public void setOwnedBy(ParseUser user) {
		put(FIELD_OWNED_BY, user);
	}
	
	public boolean isLiked() {
		return UserParse.isLiked(getRuleId());
	}
	
}

/**
 * 
 */
package com.ruley.loaders;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RulesLoader extends AsyncTaskLoader<List<Rule>> {
	
	private static final String CLASS_NAME = RulesLoader.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(RulesLoader.class.getName());

	private List<Rule> mRules = null;

	public RulesLoader(Context context) {
		super(context);
	}

	/**
	 * This is where the bulk of our work is done. This function is called in a
	 * background thread and should generate a new set of data to be published
	 * by the loader.
	 */
	@Override
	public List<Rule> loadInBackground() {
		LOGGER.debug(CLASS_NAME + "#loadInBackground(...)");
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		User defaultUser = userDAO.get(DefaultUser.class);
		return mRules = (defaultUser != null) ? defaultUser.getRules() : new ArrayList<Rule>();
	}

	/**
	 * Handles a request to start the Loader.
	 */
	@Override
	protected void onStartLoading() {
		LOGGER.debug(CLASS_NAME + "#onStartLoading(...)");
		
		if (mRules != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(mRules);
		}

		if (takeContentChanged() || mRules == null) {
			// If the data has changed since the last time it was loaded
			// or is not currently available, start a load.
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		LOGGER.debug(CLASS_NAME + "#onStopLoading(...)");
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/**
	 * Handles a request to cancel a load.
	 */
	@Override
	public void onCanceled(List<Rule> rules) {
		LOGGER.debug(CLASS_NAME + "#onCanceled(...)");
		super.onCanceled(rules);
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		LOGGER.debug(CLASS_NAME + "#onReset(...)");
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		// At this point we can release the resources associated with 'mRules'
		// if needed.
		if (mRules != null) {
			mRules = null;
		}
	}

}

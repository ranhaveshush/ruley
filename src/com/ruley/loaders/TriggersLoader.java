/**
 * 
 */
package com.ruley.loaders;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.ITriggerDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
// FIMXE: if not use, delete it
public class TriggersLoader extends AsyncTaskLoader<List<Trigger>> {

	private static final Logger LOGGER = LoggerFactory.getLogger(TriggersLoader.class.getName());

	private List<Trigger> triggers = null;

	public TriggersLoader(Context context, UUID ruleUuId) {
		super(context);
	}

	/**
	 * This is where the bulk of our work is done. This function is called in a
	 * background thread and should generate a new set of data to be published
	 * by the loader.
	 */
	@Override
	public List<Trigger> loadInBackground() {
		LOGGER.debug("loadInBackground(...)");
		
		ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		return triggers = triggerDAO.list();
	}

	/**
	 * Handles a request to start the Loader.
	 */
	@Override
	protected void onStartLoading() {
		LOGGER.debug("onStartLoading(...)");
		if (triggers != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(triggers);
		}

		if (takeContentChanged() || triggers == null) {
			// If the data has changed since the last time it was loaded
			// or is not currently available, start a load.
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		LOGGER.debug("onStopLoading(...)");
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/**
	 * Handles a request to cancel a load.
	 */
	@Override
	public void onCanceled(List<Trigger> triggers) {
		LOGGER.debug("onCanceled(...)");
		super.onCanceled(triggers);
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		LOGGER.debug("onReset(...)");
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		// At this point we can release the resources associated with 'triggers'
		// if needed.
		if (triggers != null) {
			triggers = null;
		}
	}

}

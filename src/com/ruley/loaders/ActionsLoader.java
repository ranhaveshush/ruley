/**
 * 
 */
package com.ruley.loaders;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActionDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
// FIMXE: if not use, delete it
public class ActionsLoader extends AsyncTaskLoader<List<Action>> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActionsLoader.class.getName());

	private List<Action> actions = null;

	public ActionsLoader(Context context) {
		super(context);
	}

	/**
	 * This is where the bulk of our work is done. This function is called in a
	 * background thread and should generate a new set of data to be published
	 * by the loader.
	 */
	@Override
	public List<Action> loadInBackground() {
		LOGGER.debug("loadInBackground(...)");
		
		IActionDAO actionDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getActionDAO();
		return actions = actionDAO.list();
	}

	/**
	 * Handles a request to start the Loader.
	 */
	@Override
	protected void onStartLoading() {
		LOGGER.debug("onStartLoading(...)");
		if (actions != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(actions);
		}

		if (takeContentChanged() || actions == null) {
			// If the data has changed since the last time it was loaded
			// or is not currently available, start a load.
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		LOGGER.debug("onStopLoading(...)");
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/**
	 * Handles a request to cancel a load.
	 */
	@Override
	public void onCanceled(List<Action> actions) {
		LOGGER.debug("onCanceled(...)");
		super.onCanceled(actions);
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		LOGGER.debug("onReset(...)");
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		// At this point we can release the resources associated with 'actions'
		// if needed.
		if (actions != null) {
			actions = null;
		}
	}

}

/**
 * 
 */
package com.ruley.utilities;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ruley.R;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.rules.ActivityLog;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AnalyticsUtils {
	
	public enum TrackerName {
		DEV_VERSION_APP_TRACKER,
		BETA_VERSION_APP_TRACKER,
		PROD_VERSION_APP_TRACKER
	}
	
	private Context mAppCtx = null;
	
	private HashMap<TrackerName, Tracker> mTrackers = null;
	
	private AnalyticsUtils() {
		mAppCtx = MyApplication.getContext();
		mTrackers = new HashMap<TrackerName, Tracker>();
	}
	
	private static final class SingletonHolder {
		private static final AnalyticsUtils INSTANCE = new AnalyticsUtils();
	}
	
	public static AnalyticsUtils getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public synchronized Tracker getTracker(TrackerName trackerName) {
		if (!mTrackers.containsKey(trackerName)) {
			GoogleAnalytics analytics = GoogleAnalytics.getInstance(mAppCtx);
			Tracker tracker = null;
			switch (trackerName) {
			case PROD_VERSION_APP_TRACKER:
				tracker = analytics.newTracker(R.xml.production_app_tracker);
				break;
			case BETA_VERSION_APP_TRACKER:
				tracker = analytics.newTracker(R.xml.beta_app_tracker);
				break;
			case DEV_VERSION_APP_TRACKER:
				tracker = analytics.newTracker(R.xml.development_app_tracker);
				break;
			default:
				throw new IllegalArgumentException("Invalid tracker name " + trackerName);
			}
			// Enable Advertising Features
			tracker.enableAdvertisingIdCollection(true);
			mTrackers.put(trackerName, tracker);
		}
		
		return mTrackers.get(trackerName);
	}
	
	public static TrackerName getDefaultTrackerName() {
		int buildMode = AppState.getBuildMode();
		if (buildMode == AppState.BUILD_RELEASE) {
			return TrackerName.PROD_VERSION_APP_TRACKER;
		} else if (buildMode == AppState.BUILD_BETA) {
		return TrackerName.PROD_VERSION_APP_TRACKER;
		} else if (buildMode == AppState.BUILD_DEBUG) {
			return TrackerName.DEV_VERSION_APP_TRACKER;
		} else {
			return TrackerName.DEV_VERSION_APP_TRACKER;
		}
	}
	
	public static void sendSignupEvent(String accountType) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
		.setCategory("Account")
		.setAction("Signup")
		.setLabel(accountType)
		.build());
	}
	
	public static void sendLoginEvent(String accountType) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
		.setCategory("Account")
		.setAction("Login")
		.setLabel(accountType)
		.build());
	}
	
	public static void sendLogoutEvent(String accountType) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
		.setCategory("Account")
		.setAction("Logout")
		.setLabel(accountType)
		.build());
	}
	
	public static void sendScreenViewEvent(Activity activity) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Set screen name.
		tracker.setScreenName(activity.getClass().getSimpleName());
        // Send a screen view.
		tracker.send(new HitBuilders.AppViewBuilder().build());
	}
	
	public static void sendScreenViewEvent(String screenName) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Set screen name.
		tracker.setScreenName(screenName);
        // Send a screen view.
		tracker.send(new HitBuilders.AppViewBuilder().build());
	}
	
	public static void sendActivityLogEvent(ActivityLog activityLog) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
		.setCategory("ActivityLog")
		.setAction(activityLog.toMessage())
		.setLabel(activityLog.getUserName())
		.build());
	}
	
	public static void sendRuleShared() {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
	    .setCategory("Rules Store")
	    .setAction("Shared")
	    .setLabel("Rule")
	    .setValue(1)
	    .build());
	}
	
	public static void sendRuleDownloaded() {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
	    .setCategory("Rules Store")
	    .setAction("Downloaded")
	    .setLabel("Rule")
	    .setValue(1)
	    .build());
	}
	
	public static void sendRuleLiked() {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
	    .setCategory("Rules Store")
	    .setAction("Liked")
	    .setLabel("Rule")
	    .setValue(1)
	    .build());
	}
	
	public static void sendRuleDisliked() {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
	    .setCategory("Rules Store")
	    .setAction("Disliked")
	    .setLabel("Rule")
	    .setValue(1)
	    .build());
	}
	
	public static void sendRulePerformed() {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send a custom event, ActivityLog
		tracker.send(new HitBuilders.EventBuilder()
	    .setCategory("Rules")
	    .setAction("Performed")
	    .setLabel("Rule")
	    .setValue(1)
	    .build());
	}
	
	/**
	 * Sends social interaction to Google Analytics.
	 * 
	 * @param network
	 *            - The social network with which the user is interacting (e.g.
	 *            Facebook, Google+, Twitter, etc.).
	 * @param action
	 *            - The social action taken (e.g. Like, Share, +1, etc.).
	 * @param target
	 *            - The content on which the social action is being taken (i.e.
	 *            a specific article or video).
	 */
	public static void sendSocialInteraction(String network, String action, String target) {
		// Gets the default tracker
		Tracker tracker = AnalyticsUtils.getInstance().getTracker(getDefaultTrackerName());
		// Send social interaction
		tracker.send(new HitBuilders.SocialBuilder()
		.setNetwork(network)
		.setAction(action)
		.setTarget(target)
		.build());
	}

}

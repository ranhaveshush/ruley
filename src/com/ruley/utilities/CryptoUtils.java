/**
 * 
 */
package com.ruley.utilities;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.text.TextUtils;
import android.util.Base64;

import com.ruley.config.Config;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class CryptoUtils {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CryptoUtils.class.getName());
	
	public static final String DELIMETER = "#";
	
	private CryptoUtils() {
	}
	
	public static String encrypt(SecretKeySpec sks, String clearText) {
		byte[] encodedBytes = null;
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, sks);
			encodedBytes = cipher.doFinal(clearText.getBytes());
		} catch (Exception e) {
			LOGGER.error("AES encryption error: ", e);
		}
		return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
	}

	public static String decrypt(SecretKeySpec sks, String encryptedText) {
		byte[] decodedBytes = null;
		try {
			decodedBytes = Base64.decode(encryptedText, Base64.DEFAULT);
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, sks);
			decodedBytes = cipher.doFinal(decodedBytes);
		} catch (Exception e) {
			LOGGER.error("AES decryption error: ", e);
		}
		return new String(decodedBytes);
	}
	
	public static String encryptWithAttachedKey(SecretKeySpec sks, String clearText) {
		String encryptedText = encrypt(sks, clearText);
		return secretKeyToString(sks) + DELIMETER + encryptedText;
	}
	
	public static String decryptWithAttachedKey(String encryptedTextWithKey) {
		String[] parts = encryptedTextWithKey.split(DELIMETER);
		String sksStr = parts[0];
		String encryptedText = parts[1];
		SecretKeySpec sks = stringToSecretKey(sksStr);
		return decrypt(sks, encryptedText);
	}
	
	public static String secretKeyToString(SecretKeySpec sks) {
		if (sks == null) {
			return null;
		}
		return Base64.encodeToString(sks.getEncoded(), Base64.DEFAULT);
	}
	
	public static SecretKeySpec stringToSecretKey(String sksStr) {
		if (sksStr == null) {
			return null;
		}
		byte[] key = Base64.decode(sksStr, Base64.DEFAULT);
		return new SecretKeySpec(key, "AES");
	}
	
	public static SecretKeySpec getKey() {
		String sksStr = PrefsUtils.getKeyMaster();
		if (TextUtils.isEmpty(sksStr)) {
			String fileMaster = Config.Paths.getAppStorageDirPath() + Config.Paths.FILE_MASTER;
			sksStr = FilesUtils.readFile(fileMaster);
			if (TextUtils.isEmpty(sksStr)) {
				SecretKeySpec sks = genKey();
				sksStr = secretKeyToString(sks);
				FilesUtils.writeFile(fileMaster, sksStr);
			}
			PrefsUtils.setKeyMaster(sksStr);
		}
		return stringToSecretKey(sksStr);
	}
	
	// Sets up secret key spec for 128-bit,
	// AES encryption and decryption
	private static SecretKeySpec genKey() {
		SecretKeySpec sks = null;
		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			sr.setSeed("any data used as random seed".getBytes());
			KeyGenerator kg = KeyGenerator.getInstance("AES");
			kg.init(128);
			sks = new SecretKeySpec((kg.generateKey()).getEncoded(), "AES");
		} catch (Exception e) {
			LOGGER.error("AES secret key spec error: ", e);
		}
		return sks;
	}

}

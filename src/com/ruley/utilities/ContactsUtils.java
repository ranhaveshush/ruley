/**
 * 
 */
package com.ruley.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ContactsUtils {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ContactsUtils.class.getName());
	
	private ContactsUtils() {
	}
	
	public static String getContactId(Context context, String phoneNumber) {
		// Validates arguments
		if (TextUtils.isEmpty(phoneNumber)) {
			return null;
		}
		
		String contactId = null;

        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        Cursor cursor = contentResolver.query(uri, new String[] {ContactsContract.PhoneLookup._ID}, null, null, null);

        if (cursor != null && cursor.moveToNext()) {
           contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
           cursor.close();
        }
        
        return contactId;
	}
	
	public static String getContactName(Context context, String contactId) {
		// Validates arguments
		if (TextUtils.isEmpty(contactId)) {
			return null;
		}
		
		String contactName = null;
		
		Uri contactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, contactId);

		Cursor cursor = context.getContentResolver().query(contactUri, null, null,
				null, null);
		if (cursor != null && cursor.moveToFirst()) {
			contactName = cursor.getString(cursor
					.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
		}
		cursor.close();

		return contactName;
	}

	public static String getContactPhoneNumber(Context context, String contactId) {
		// Validates arguments
		if (TextUtils.isEmpty(contactId)) {
			return null;
		}
				
		String contactNumber = null;

		// Using the contact Id to get contact phone number
		Cursor cursorPhone = context.getContentResolver().query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				new String[] { ContactsContract.CommonDataKinds.Phone.NUMBER },
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND "
						+ ContactsContract.CommonDataKinds.Phone.TYPE + " = "
						+ ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
				new String[] { contactId }, null);
		if (cursorPhone != null && cursorPhone.moveToFirst()) {
			contactNumber = cursorPhone
					.getString(cursorPhone
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
		}
		cursorPhone.close();

		return contactNumber;
	}

	public static Bitmap getContactPhoto(Context context, String contactId) {
		// Validates arguments
		if (TextUtils.isEmpty(contactId)) {
			return null;
		}
		
		Bitmap photo = null;

		try {
			InputStream inputStream = ContactsContract.Contacts
					.openContactPhotoInputStream(context.getContentResolver(),
							ContentUris.withAppendedId(
									ContactsContract.Contacts.CONTENT_URI,
									Long.valueOf(contactId)));

			if (inputStream != null) {
				photo = BitmapFactory.decodeStream(inputStream);
				inputStream.close();
			}
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}

		return photo;
	}
	
	public static List<String> getPhoneNumbers(Context context, List<String> contactsIds) {
		List<String> phoneNumbers = new ArrayList<String>();
		
		for (String contactId : contactsIds) {
			phoneNumbers.add( getContactPhoneNumber(context, contactId) );
		}
		
		return phoneNumbers;
	}

	public static List<String> getContacstIds(Context context, List<String> contactsNames) {
		List<String> contactsIds = null;
		
		// Generates arguments question marks placeholders
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for (int i=0; i < contactsNames.size(); ++i) {
			sb.append("?,");
		}
		sb.deleteCharAt(sb.length()-1); // Deletes the last comma ,
		sb.append(")");
		String argsPlaceholders = sb.toString();
		
		Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
				new String[] {ContactsContract.Contacts._ID},
				ContactsContract.Contacts.DISPLAY_NAME + " IN " + argsPlaceholders,
				contactsNames.toArray(new String[contactsNames.size()]), null);
		
		if (cursor != null) {
			contactsIds = new ArrayList<String>();
			while (cursor.moveToNext()) {
				contactsIds.add(
						cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID)) );
			}
			
			cursor.close();
		}
		
		return contactsIds;
	}
	
	public static String getContactName(Context context, int contactId) {
		String personName = null;
		
		String[] projection = new String[] {
				ContactsContract.Contacts.DISPLAY_NAME
		};
		String selection = ContactsContract.Contacts._ID + " = ?";
		String[] selectionArgs = new String[] {
				String.valueOf(contactId)
		};
		
		Cursor cursor = context.getContentResolver().query(
				Uri.parse("content://contacts/people"),
				projection,
				selection,
				selectionArgs,
				null);
		
		if (cursor != null) {
			if (cursor.moveToFirst()) {
				personName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
			}
			
			cursor.close();
		}
		
		return personName;
	}
	
	public static String getContactPhoneNumber(Context context, int contactId) {
		String phoneNumber = getContactSuperPrimaryPhoneNumber(context, contactId);
		if (!TextUtils.isEmpty(phoneNumber)) {
			return phoneNumber;
		}
		phoneNumber = getContactPrimaryPhoneNumber(context, contactId);
		if (!TextUtils.isEmpty(phoneNumber)) {
			return phoneNumber;
		}
		return getContactAnyPhoneNumber(context, contactId);
	}
	
	public static String getContactSuperPrimaryPhoneNumber(Context context, int contactId) {
		String phoneNumber = null;
		
		Uri Uri = ContactsContract.Data.CONTENT_URI;
		String[] projection = new String[] {
				ContactsContract.CommonDataKinds.Phone.NUMBER
		};
		String selection = ContactsContract.Data.CONTACT_ID + "=? AND "
				+ ContactsContract.Data.MIMETYPE + "='"
				+ ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
				+ "' AND " + ContactsContract.Data.IS_SUPER_PRIMARY + "=1";
		String[] selectionArgs = new String[] {
				String.valueOf(contactId)
		};
		
		Cursor cursor = null;
		try {
			cursor = context.getContentResolver().query(
					Uri,
					projection,
					selection,
					selectionArgs,
					null);
			
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		
		return phoneNumber;
	}
	
	public static String getContactPrimaryPhoneNumber(Context context, int contactId) {
		String phoneNumber = null;
		
		Uri Uri = ContactsContract.Data.CONTENT_URI;
		String[] projection = new String[] {
				ContactsContract.CommonDataKinds.Phone.NUMBER
		};
		String selection = ContactsContract.Data.CONTACT_ID + "=? AND "
				+ ContactsContract.Data.MIMETYPE + "='"
				+ ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
				+ "' AND " + ContactsContract.Data.IS_PRIMARY + "=1";
		String[] selectionArgs = new String[] {
				String.valueOf(contactId)
		};
		
		Cursor cursor = null;
		try {
			cursor = context.getContentResolver().query(
					Uri,
					projection,
					selection,
					selectionArgs,
					null);
			
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		
		return phoneNumber;
	}
	
	public static String getContactAnyPhoneNumber(Context context, int contactId) {
		String phoneNumber = null;
		
		Uri Uri = ContactsContract.Data.CONTENT_URI;
		String[] projection = new String[] {
				ContactsContract.CommonDataKinds.Phone.NUMBER
		};
		String selection = ContactsContract.Data.CONTACT_ID + "=? AND "
				+ ContactsContract.Data.MIMETYPE + "='"
				+ ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
				+ "'";
		String[] selectionArgs = new String[] {
				String.valueOf(contactId)
		};
		
		Cursor cursor = null;
		try {
			cursor = context.getContentResolver().query(
					Uri,
					projection,
					selection,
					selectionArgs,
					null);
			
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				}
			}
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		
		return phoneNumber;
	}

}

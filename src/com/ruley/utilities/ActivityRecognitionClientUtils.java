/**
 * 
 */
package com.ruley.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.DetectedActivity;
import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ActivityRecognitionClientUtils implements ConnectionCallbacks, OnConnectionFailedListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityRecognitionClientUtils.class.getName());
	
	private static final int REQUEST_CODE_CONNECTION_FAILURE_RESOLUTION = 3001;
	
	// Constants that define the activity detection interval
    public static final int MILLISECONDS_PER_SECOND = 1000;
    public static final int DETECTION_INTERVAL_SECONDS = 30;
    public static final int DETECTION_INTERVAL_MILLISECONDS = MILLISECONDS_PER_SECOND * DETECTION_INTERVAL_SECONDS;
	
    /**
     * @author Ran Haveshush, ran.haveshush.dev@gmail.com
     *
     */
    public interface Listener {
    	
    	/**
    	 * Invoked when finish registeration to user activity recognition.
    	 */
    	void onStartActivityRecognition();
    	
    	/**
    	 * Invoked when finish unregisteration to user activity recognition.
    	 */
    	void onStopActivityRecognition();
    	
    	/**
		 * Invoked when error occured.
		 */
		public void onError(int errorMessageResourceId);
    }
    
    private static final Map<Integer, String> activitiesMap = Collections.unmodifiableMap(new HashMap<Integer, String>() {
    	{
    		put(DetectedActivity.IN_VEHICLE, "in_vehicle");
    		put(DetectedActivity.ON_BICYCLE, "on_bicycle");
    		put(DetectedActivity.ON_FOOT, "on_foot");
    		put(DetectedActivity.STILL, "still");
    		put(DetectedActivity.UNKNOWN, "unknown");
    		put(DetectedActivity.TILTING, "tilting");
    		put(DetectedActivity.WALKING, "walking");
    		put(DetectedActivity.RUNNING, "running");
    	}
    });
	
	private static boolean inProgress = false;
	
	private enum RequestType {
		START_ACTIVITY_RECOGNITION,
		STOP_ACTIVITY_RECOGNITION
	};
	
	private Listener listener = null;
	
	private RequestType requestType = null;
	
	private ActivityRecognitionClient activityRecognitionClient = null;
	
	private PendingIntent pendingIntent = null;
	
	public static String getNameFromType(int activityType) {
		return activitiesMap.get(activityType);
	}
	
	public void startActivityRecognition(Context context, PendingIntent pendingIntent, Listener listener) {
		LOGGER.debug("ActivityRecognitionClientUtils#startActivityRecognition(...)");
		
		this.listener = listener;
		this.pendingIntent = pendingIntent;
		requestType = RequestType.START_ACTIVITY_RECOGNITION;
		activityRecognitionClient = new ActivityRecognitionClient(context, this, this);
		
		if (!inProgress) {
			inProgress = true;
			activityRecognitionClient.connect();
		} else {
			activityRecognitionClient.disconnect();
			inProgress = false;
			activityRecognitionClient.connect();
		}
	}
	
	public void stopActivityRecognition(Context context, PendingIntent pendingIntent, Listener listener) {
		LOGGER.debug("ActivityRecognitionClientUtils#stopActivityRecognition(...)");
		
		this.listener = listener;
		this.pendingIntent = pendingIntent;
		requestType = RequestType.STOP_ACTIVITY_RECOGNITION;
		activityRecognitionClient = new ActivityRecognitionClient(context, this, this);
		
		if (!inProgress) {
			inProgress = true;
			activityRecognitionClient.connect();
		} else {
			activityRecognitionClient.disconnect();
			inProgress = false;
			activityRecognitionClient.connect();
		}
	}

	@Override
	public void onConnected(Bundle args) {
		LOGGER.debug("ActivityRecognitionClientUtils#onConnected(...)");
		
		switch (requestType) {
		case START_ACTIVITY_RECOGNITION:
			activityRecognitionClient.requestActivityUpdates(DETECTION_INTERVAL_MILLISECONDS, pendingIntent);
			LOGGER.info("Request activity updates");
			listener.onStartActivityRecognition();
			inProgress = false;
			activityRecognitionClient.disconnect();
			break;
		case STOP_ACTIVITY_RECOGNITION:
			activityRecognitionClient.removeActivityUpdates(pendingIntent);
			LOGGER.info("Remove activity updates");
			listener.onStopActivityRecognition();
			inProgress = false;
			activityRecognitionClient.disconnect();
			break;
		}
	}
	
	@Override
	public void onDisconnected() {
		LOGGER.debug("ActivityRecognitionClientUtils#onDisconnected(...)");
		
		inProgress = false;
		activityRecognitionClient = null;
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		LOGGER.debug("ActivityRecognitionClientUtils#onConnectionFailed(...)");
		
		inProgress = false;
		
		if (connectionResult.hasResolution()) {
			LOGGER.warn("There is a resolution for the error, launch resolution dialog");
			Context appCtx = MyApplication.getContext();
			try {
                connectionResult.startResolutionForResult((Activity)appCtx, REQUEST_CODE_CONNECTION_FAILURE_RESOLUTION);
            } catch (IntentSender.SendIntentException e) {
                LOGGER.error("Error: ", e);
            }
		} else {
			LOGGER.error("There isn't a resolution for the error, launch error dialog");
			listener.onError(connectionResult.getErrorCode());
		}
	}

}


/**
 * 
 */
package com.ruley.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AppsUtils {
	
	private static final String CLASS_NAME = AppsUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AppsUtils.class.getName());
	
	private static PackageManager PACKAGE_MANAGER = MyApplication.getContext()
			.getApplicationContext().getPackageManager();
	
	private static Comparator<ApplicationInfo> appInfoComparator = new Comparator<ApplicationInfo>() {

		@Override
		public int compare(ApplicationInfo lhs, ApplicationInfo rhs) {
			String lhsName = PACKAGE_MANAGER.getApplicationLabel(lhs).toString();
			String rhsName = PACKAGE_MANAGER.getApplicationLabel(rhs).toString();
			return lhsName.compareToIgnoreCase(rhsName);
		}
		
	};
	
	private AppsUtils() {
	}
	
	public static boolean isRunningInForeground(String appPackageName) {
		Context appCtx = MyApplication.getContext();

		ActivityManager activityManager = (ActivityManager) appCtx.getSystemService(Context.ACTIVITY_SERVICE);
		if (activityManager == null) {
			return false;
		}

		List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(1);
		if (tasks == null || tasks.isEmpty()) {
			return false;
		}

		String topActivityName = tasks.get(0).topActivity.getPackageName();
		return topActivityName.equalsIgnoreCase(appPackageName);
	}
	
	public static List<ApplicationInfo> getLaunchableInstalledApps() {
		LOGGER.debug(CLASS_NAME + "#getLaunchableInstalledApps(...)");
		
		long startMillis, endMillis;
		startMillis = System.currentTimeMillis();
		
		List<ApplicationInfo> installedApps = PACKAGE_MANAGER.getInstalledApplications(
				PackageManager.GET_META_DATA|PackageManager.GET_SHARED_LIBRARY_FILES);
		
		List<ApplicationInfo> launchableInstalledApps = new ArrayList<ApplicationInfo>();
		for (ApplicationInfo appInfo : installedApps) {
			if (isAppLaunchable(appInfo)) {
				launchableInstalledApps.add(appInfo);
			}
		}
		
		// Sorts the launchableInstalledApps
		Collections.sort(launchableInstalledApps, appInfoComparator);
		
		endMillis = System.currentTimeMillis();
		LOGGER.warn("List launchable installed apps took: " + (endMillis - startMillis));
		
		return launchableInstalledApps;
	}
	
	public static boolean isAppLaunchable(ApplicationInfo appInfo) {
		LOGGER.debug(CLASS_NAME + "#isAppLaunchable(...)");
		
		// Validates that the installed app is launchable
		Intent launchIntent = PACKAGE_MANAGER.getLaunchIntentForPackage(appInfo.packageName);
		if (launchIntent != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void launchApp(Context context, String appPackage) {
		LOGGER.debug(CLASS_NAME + "#launchApp(...)");
		
		Intent intent = PACKAGE_MANAGER.getLaunchIntentForPackage(appPackage);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(intent)) {
			context.startActivity(intent);
		}
	}
	
	public static Bitmap getAppIcon(String appPackage) {
		LOGGER.debug(CLASS_NAME + "#getAppIcon(...)");
		
		if (appPackage == null) {
			return null;
		}
		
		try {
			ApplicationInfo appInfo = PACKAGE_MANAGER.getApplicationInfo(appPackage, 0);
			Drawable icon = appInfo.loadIcon(PACKAGE_MANAGER);
			Bitmap bitmap = ImageUtils.toBitmap(icon);
			return ImageUtils.grayscale(bitmap);
		} catch (NameNotFoundException e) {
			LOGGER.error("Error: ", e);
		}
		
		return null;
	}

}

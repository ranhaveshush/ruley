/**
 * 
 */
package com.ruley.utilities;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.URLName;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.AsyncTask;

import com.sun.mail.smtp.SMTPTransport;
import com.sun.mail.util.BASE64EncoderStream;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class GMailOauthSender {
	
	private static final String CLASS_NAME = GMailOauthSender.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GMailOauthSender.class.getName());

	private static final String HOST = "smtp.gmail.com";
	
	private static final int PORT = 587;
	
	private Session session;

	public synchronized void sendMail(
			String subject,
			String body,
			String user,
			String recipients,
			String oauthToken) {
		
		new TempAsyncTask().execute(subject, body, user, recipients, oauthToken);
	}
	
	public class TempAsyncTask extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... params) {
			LOGGER.debug(CLASS_NAME + "#doInBackground(...)");
			
			String subject = params[0];
			String body = params[1];
			String user = params[2];
			String recipients = params[3];
			String oauthToken = params[4];
			
			Properties props = new Properties();
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.starttls.required", "true");
			props.put("mail.smtp.sasl.enable", "false");
			session = Session.getInstance(props);
			session.setDebug(true);

			try {
				final URLName unusedUrlName = null;
				SMTPTransport transport = new SMTPTransport(session, unusedUrlName);
				// If the password is non-null, SMTP tries to do AUTH LOGIN.
				final String emptyPassword = null;
				transport.connect(HOST, PORT, user, emptyPassword);

				String responseStr = String.format("user=%s\1auth=Bearer %s\1\1", user, oauthToken);
				byte[] response = responseStr.getBytes();
				response = BASE64EncoderStream.encode(response);

				transport.issueCommand("AUTH XOAUTH2 " + new String(response), 235);

				MimeMessage message = new MimeMessage(session);
				DataHandler handler = new DataHandler(new ByteArrayDataSource(body.getBytes(), "text/html"));
				message.setSender(new InternetAddress(user));
				message.setSubject(subject);
				message.setDataHandler(handler);
				if (recipients.indexOf(',') > 0) {
					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipients));
				} else {
					message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipients));
				}
				
				transport.sendMessage(message, message.getAllRecipients());
			} catch (AddressException e) {
				LOGGER.error("Error: ", e);
			} catch (SendFailedException e) {
				LOGGER.error("Error: ", e);
			} catch (MessagingException e) {
				LOGGER.error("Error: ", e);
			} catch (Exception e) {
				LOGGER.error("Error: ", e);
			}
			
			return null;
		}
		
	}
}

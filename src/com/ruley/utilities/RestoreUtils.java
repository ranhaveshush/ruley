/**
 * 
 */
package com.ruley.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RestoreUtils {
	
	private static final String CLASS_NAME = RestoreUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RestoreUtils.class.getName());
	
	private RestoreUtils() {
	}
	
	/**
	 * Restore type enum, Inner class representing all the available
	 * restore operations.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public enum RestoreType {
		MERGE,
		DELETE_BEFORE_RESTORE
	}
	
	/**
	 * Restores user's rules from JSON backup file.
	 * 
	 * @param context
	 * @param filePath	String represents the path to the backup file
	 * @param restoreType	Enum RestoreType represents the type of restore operation to execute
	 * @return	List of Rules
	 * 
	 * <P>
	 * Restore options:<BR/>
	 * RestoreType.MERGE<BR/>
	 * RestoreType.DELETE_BEFORE_RESTORE
	 */
	public static List<Rule> restoreRulesFromJSON(Context context, String filePath, RestoreType restoreType) {
		LOGGER.debug(CLASS_NAME + "#restoreRulesFromJSON(...)");
		
		List<Rule> rulesToRestore = JsonHelper.getInstance()
				.readJSON(filePath, new TypeReference<List<Rule>>() {});
		
		return restoreRules(context, restoreType, rulesToRestore);
	}
	
	/**
	 * Restores user's rules from JSON backup file.
	 * 
	 * @param context
	 * @param inputStream	InputStream represents the the backup file
	 * @param restoreType	Enum RestoreType represents the type of restore operation to execute
	 * @return	List of Rules
	 * 
	 * <P>
	 * Restore options:<BR/>
	 * RestoreType.MERGE<BR/>
	 * RestoreType.DELETE_BEFORE_RESTORE
	 */
	public static List<Rule> restoreRulesFromJSON(Context context, InputStream inputStream, RestoreType restoreType) {
		LOGGER.debug(CLASS_NAME + "#restoreRulesFromJSON(...)");
		
		List<Rule> rulesToRestore = null;
		
		try {
			ObjectMapper mapper = JsonHelper.getInstance().getMapper();
			rulesToRestore = mapper.readValue(inputStream, new TypeReference<List<Rule>>() {});
		} catch (JsonParseException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}
		
		return restoreRules(context, restoreType, rulesToRestore);
	}

	public static List<Rule> restoreRules(Context context, RestoreType restoreType, List<Rule> rulesToRestore) {
		long startMillis = System.currentTimeMillis();
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		User defaultUser = userDAO.get(DefaultUser.class);
		List<Rule> defaultUserRules = defaultUser.getRules();
		
		if (rulesToRestore != null && !rulesToRestore.isEmpty()) {
			switch (restoreType) {
			// Merges user's current rules with rules to restore
			case MERGE:
				// Activates and restores user's rules from backup
				for (Rule ruleToRestore : rulesToRestore) {
					if (defaultUserRules.contains(ruleToRestore)) { continue; }
					if (ruleToRestore.isActive()) {
						ruleToRestore.setActive(context, true, null);
					}
					defaultUserRules.add(ruleToRestore);
				}
				break;
			// Deletes the previous default user's rules,
			// Then resotres the rules from backup
			case DELETE_BEFORE_RESTORE:
				// Deletes the previous default user's rules
				while (!defaultUserRules.isEmpty()) {
					Rule ruleToDelete = defaultUserRules.remove(0);
					if (ruleToDelete.isActive()) {
						ruleToDelete.setActive(context, false, null);
					}
				}
				// Activates and restores rules from backup
				for (Rule ruleToRestore : rulesToRestore) {
					if (ruleToRestore.isActive()) {
						ruleToRestore.setActive(context, true, null);
					}
					defaultUserRules.add(ruleToRestore);
				}
				break;
			default:
				throw new IllegalArgumentException(CLASS_NAME + " invalid restoreType argument");
			}
		}
		
		userDAO.store(defaultUser);
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#restoreRulesFromJSON(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
		
		return defaultUserRules;
	}

}

/**
 * 
 */
package com.ruley.utilities;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PackageInfoUtils {
	
	public static int INVALID_VERSION_CODE = -1;
	
	private PackageInfoUtils() {
	}
	
	public static String getAppVersionName(Context context) {
		String appVersionName = null;
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			appVersionName = packageInfo.versionName;
		} catch (NameNotFoundException e) {
		}
		return appVersionName;
	}
	
	public static int getAppVersionCode(Context context) {
		int appVersionCode = INVALID_VERSION_CODE;
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			appVersionCode = packageInfo.versionCode;
		} catch (NameNotFoundException e) {
		}
		return appVersionCode;
	}

}

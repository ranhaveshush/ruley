/**
 * 
 */
package com.ruley.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseRole;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.ruley.R;
import com.ruley.app.AppState;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.parse.BaseParse;
import com.ruley.model.parse.RoleParse;
import com.ruley.model.parse.RuleParse;
import com.ruley.model.parse.TemplateRuleParse;
import com.ruley.model.parse.UserParse;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ParseUtils {
	
	private static final String CLASS_NAME = ParseUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ParseUtils.class.getName());
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface DoneCallback {
		
		public void done();
		
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface FindCallback<T> {
		
		public void done(List<T> items);
		
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 * @param <T>
	 */
	public interface UpdateCallback<T> {
		
		public void done(T item);
		
	}
	
	private ParseUtils() {
	}
	
	public static ParseACL getDefaultParseACL() {
		ParseACL defaultACL = new ParseACL();
		defaultACL.setPublicReadAccess(true);
		defaultACL.setRoleWriteAccess(RoleParse.ROLE_ADMINS, true);
		return defaultACL;
	}

	public static ParseACL getUserParseACL(ParseUser currentUser) {
		ParseACL parseUserACL = new ParseACL(currentUser);
		parseUserACL.setPublicReadAccess(true);
		return parseUserACL;
	}
	
	public static ParseACL getRuleParseACL() {
		ParseACL ruleParseACL = new ParseACL(ParseUser.getCurrentUser());
		ruleParseACL.setRoleReadAccess(RoleParse.ROLE_MODERATORS, true);
		ruleParseACL.setRoleWriteAccess(RoleParse.ROLE_MODERATORS, true);
		return ruleParseACL;
	}
	
	public static ParseACL getTempalteRuleParseACL() {
		ParseACL templateRuleParseACL = new ParseACL();
		templateRuleParseACL.setPublicReadAccess(true);
		templateRuleParseACL.setRoleWriteAccess(RoleParse.ROLE_MODERATORS, true);
		templateRuleParseACL.setWriteAccess(ParseUser.getCurrentUser(), true);
		return templateRuleParseACL;
	}
	
	/**
	 * Initializes parse BaaS.
	 */
	public static void initParse(Context appCtx) {
		// Registers ParseObject subclasses
		ParseObject.registerSubclass(RuleParse.class);
		ParseObject.registerSubclass(TemplateRuleParse.class);

		// Initialize Parse
		Parse.initialize(appCtx,
				appCtx.getString(R.string.parse_app_id),
				appCtx.getString(R.string.parse_client_key));
		
		// Initialize Parse Facebook integration
		ParseFacebookUtils.initialize(appCtx.getString(R.string.facebook_app_id));
		
		Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
		
		// Sets the default Access Control List
		ParseACL.setDefaultACL(getDefaultParseACL(), true);
	}
	
	public static boolean isRuleyTeamUser() {
		ParseUser currentUser = ParseUser.getCurrentUser();
		
		// Validates current user
		if (currentUser == null) {
			return false;
		}
		
		String currentUserEmail = currentUser.getEmail();
		if (!TextUtils.isEmpty(currentUserEmail)) {
			List<String> ruleyTeamUsers = new ArrayList<String>(Arrays.asList(
					"ran.haveshush@gmail.com", "ruley.team@gmail.com"));
			
			if (ruleyTeamUsers.contains(currentUserEmail)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean isParseUser(ParseUser currentUser) {
		return !TextUtils.isEmpty(currentUser.getEmail());
	}
	
	public static boolean isFacebookUser(ParseUser currentUser) {
		return currentUser != null && ParseFacebookUtils.isLinked(currentUser);
	}
	
	public static boolean isParseUserLoggedIn() {
		LOGGER.debug(CLASS_NAME + "#isParseUserLoggedIn(...)");
		
		return getCurrentUserAccoutType() != null;
	}
	
	public static String getCurrentUserAccoutType() {
		LOGGER.debug(CLASS_NAME + "#getCurrentUserAccoutType(...)");
		
		ParseUser currentUser = ParseUser.getCurrentUser();
		// If not signed up, return
		if (currentUser == null || TextUtils.isEmpty(currentUser.getObjectId())) {
			LOGGER.warn(CLASS_NAME + "#getCurrentUserAccoutType(...) current user is NULL");
			return null;
		}
		
		String accountType = null;
		
		// If Parse user
		if (isParseUser(currentUser)) {
			accountType = "Parse";
		// If Facebook user
		} else if (isFacebookUser(currentUser)) {
			accountType = "Facebook";
		}
		
		return accountType;
	}
	
	/**
	 * Gets the user rules.
	 * 
	 * @param context
	 * @param parseRelationName
	 */
	public static void findRules(final Context context, String parseRelationName, final FindCallback<Rule> findCallback) {
		LOGGER.debug(CLASS_NAME + "#findRules(...) from relation " + parseRelationName);

		// Gets the current user rules relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		
		final ParseRelation<RuleParse> rulesRelation = currentUser.getRelation(parseRelationName);
		ParseQuery<RuleParse> query = rulesRelation.getQuery();
		query.findInBackground(new com.parse.FindCallback<RuleParse>() {
			@Override
			public void done(List<RuleParse> rules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If previously rules found
				} else if (rules != null && !rules.isEmpty()) {
					List<Rule> foundRules = new ArrayList<Rule>();
					for (RuleParse ruleParse : rules) {
						Rule rule = ruleParse.getRule();
						LOGGER.info("findRules(...) found " + rule.getName());
						foundRules.add(rule);
					}
					if (findCallback != null) {
						findCallback.done(foundRules);
					}
				}
			}
		});
	}
	
	/**
	 * Override the user rules.
	 * Deletes all the previously backed up rules and backs up the given ones.
	 * 
	 * @param context
	 * @param selectedRules
	 * @param parseRelationName
	 */
	public static void overrideRules(final Context context,
			final List<Rule> selectedRules, String parseRelationName, final DoneCallback doneCallback) {
		LOGGER.debug(CLASS_NAME + "#overrideRules(...) in relation " + parseRelationName);

		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		// Constructs the rules to override
		ParseACL ruleParseACL = getRuleParseACL();
		final List<RuleParse> rulesToOverride = new ArrayList<RuleParse>();
		for (Rule rule : selectedRules) {
			RuleParse ruleParse = new RuleParse(rule);
			ruleParse.setACL(ruleParseACL);
			ruleParse.saveInBackground();
			rulesToOverride.add(ruleParse);
		}
		// Gets the current user rules relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final ParseRelation<RuleParse> rulesRelation = currentUser.getRelation(parseRelationName);
		ParseQuery<RuleParse> query = rulesRelation.getQuery();
		query.findInBackground(new com.parse.FindCallback<RuleParse>() {
			@Override
			public void done(List<RuleParse> rules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If previously backed up rules found, delete all
				} else if (rules != null && !rules.isEmpty()) {
					RuleParse.deleteAllInBackground(rules, new DeleteCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							} else {								
								LOGGER.info("overrideRules(...) all rules were deleted");
								// Adds all the rules to override
								RuleParse.saveAllInBackground(rulesToOverride, new SaveCallback() {
									@Override
									public void done(ParseException e) {
										if (e != null) {
											LOGGER.error("Error: ", e);
											ToastUtils.show(AppState.BUILD_RELEASE,
													context,
													R.string.toast_connection_failed, Toast.LENGTH_SHORT);
											return;
										}
										LOGGER.info("overrideRules(...) selected rules were saved");
										for (RuleParse ruleToOverride : rulesToOverride) {
											rulesRelation.add(ruleToOverride);
											Rule rule = ruleToOverride.getRule();
											LOGGER.info("overrideRules(...) add rule to relation " + rule.getName());
										}
										currentUser.saveInBackground(new SaveCallback() {
											@Override
											public void done(ParseException e) {
												if (e != null) {
													LOGGER.error("Error: ", e);
													ToastUtils.show(AppState.BUILD_RELEASE,
															context,
															R.string.toast_connection_failed, Toast.LENGTH_SHORT);
													return;
												}
												if (doneCallback != null) {
													doneCallback.done();
												}
											}
										});
									}
								});
							}
						}
					});
				// If previously backed up rules found,
				// Adds all the rules to override
				} else {
					RuleParse.saveAllInBackground(rulesToOverride, new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							LOGGER.info("overrideRules(...) selected rules were saved");
							for (RuleParse ruleToOverride : rulesToOverride) {
								rulesRelation.add(ruleToOverride);
								Rule rule = ruleToOverride.getRule();
								LOGGER.info("overrideRules(...) add rule to relation " + rule.getName());
							}
							currentUser.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										LOGGER.error("Error: ", e);
										ToastUtils.show(AppState.BUILD_RELEASE,
												context,
												R.string.toast_connection_failed, Toast.LENGTH_SHORT);
										return;
									}
									if (doneCallback != null) {
										doneCallback.done();
									}
								}
							});
						}
					});
				}
			}
		});
	}
	
	/**
	 * Adds the given rules to the user rules.
	 * Updates same rules with given rules,
	 * and keeps different rules intact. 
	 * 
	 * @param context
	 * @param selectedRules
	 * @param parseRelationName
	 */
	public static void addRules(final Context context,
			final List<Rule> selectedRules, String parseRelationName, final DoneCallback doneCallback) {
		LOGGER.debug(CLASS_NAME + "#addRules(...) to relation " + parseRelationName);
		
		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		// Constructs the rules to add
		ParseACL ruleParseACL = getRuleParseACL();
		final List<RuleParse> rulesToAdd = new ArrayList<RuleParse>();
		for (Rule rule : selectedRules) {
			RuleParse ruleParse = new RuleParse(rule);
			ruleParse.setACL(ruleParseACL);
			ruleParse.saveInBackground();
			rulesToAdd.add(ruleParse);
		}
		
		List<String> selectedRulesIds = getRulesIds(selectedRules);
		
		// Gets the current user rules relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final ParseRelation<RuleParse> rulesRelation = currentUser.getRelation(parseRelationName);
		ParseQuery<RuleParse> query = rulesRelation.getQuery();
		query.whereContainedIn(RuleParse.FIELD_RULE_ID, selectedRulesIds);
		query.findInBackground(new com.parse.FindCallback<RuleParse>() {
			@Override
			public void done(List<RuleParse> rules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If previously backed up rules found,
				// deletes the found ones already contained in the rules to add,
				// then adds all the rules to add
				} else if (rules != null && !rules.isEmpty()) {
					RuleParse.deleteAllInBackground(rules, new DeleteCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							LOGGER.info("addRules(...) selected rules to add were deleted");
							RuleParse.saveAllInBackground(rulesToAdd, new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										LOGGER.error("Error: ", e);
										ToastUtils.show(AppState.BUILD_RELEASE,
												context,
												R.string.toast_connection_failed, Toast.LENGTH_SHORT);
										return;
									}
									for (RuleParse ruleToAdd : rulesToAdd) {
										rulesRelation.add(ruleToAdd);
										Rule rule = ruleToAdd.getRule();
										LOGGER.info("addRules(...) " + ruleToAdd.getObjectId() + " \"" + rule.getName() + "\" added to saved rules relation");
									}
									LOGGER.info("addRules(...) selected rules to add were saved");
									currentUser.saveInBackground(new SaveCallback() {
										@Override
										public void done(ParseException e) {
											if (e != null) {
												LOGGER.error("Error: ", e);
												ToastUtils.show(AppState.BUILD_RELEASE,
														context,
														R.string.toast_connection_failed, Toast.LENGTH_SHORT);
												return;
											}
											if (doneCallback != null) {
												doneCallback.done();
											}
										}
									});
								}
							});
						}
					});
				// If previously backed up rules not found,
				// Adds all the rules to add
				} else {
					RuleParse.saveAllInBackground(rulesToAdd, new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							for (RuleParse ruleToAdd : rulesToAdd) {
								rulesRelation.add(ruleToAdd);
								Rule rule = ruleToAdd.getRule();
								LOGGER.info("addRules(...) " + ruleToAdd.getObjectId() + " \"" + rule.getName() + "\" added to saved rules relation");
							}
							LOGGER.info("addRules(...) selected rules to add were saved");
							currentUser.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										LOGGER.error("Error: ", e);
										ToastUtils.show(AppState.BUILD_RELEASE,
												context,
												R.string.toast_connection_failed, Toast.LENGTH_SHORT);
										return;
									}
									if (doneCallback != null) {
										doneCallback.done();
									}
								}
							});
						}
					});
				}
			}
		});
	}
	
	/**
	 * Deletes the given rules from the user rules.
	 * 
	 * @param context
	 * @param selectedRules
	 * @param parseRelationName TODO
	 */
	public static void removeRules(final Context context,
			final Collection<? extends RuleParse> selectedRules, String parseRelationName,
			final DoneCallback doneCallback) {
		LOGGER.debug(CLASS_NAME + "#removeRules(...) from relation " + parseRelationName);
		
		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		List<String> selectedRulesIds = getRulesParseIds(selectedRules);
		
		// Gets the current user rules relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final ParseRelation<RuleParse> rulesRelation = currentUser.getRelation(parseRelationName);
		ParseQuery<RuleParse> query = rulesRelation.getQuery();
		query.whereContainedIn(RuleParse.FIELD_RULE_ID, selectedRulesIds);
		query.findInBackground(new com.parse.FindCallback<RuleParse>() {
			@Override
			public void done(List<RuleParse> rules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If rules found, delete them
				} else if (rules != null && !rules.isEmpty()) {
					RuleParse.deleteAllInBackground(rules, new DeleteCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							LOGGER.info("removeRules(...) selected rules to remove were deleted");
							if (doneCallback != null) {
								doneCallback.done();
							}
						}
					});
				}
			}
		});
	}
	
	/**
	 * Gets the user rules.
	 * 
	 * @param context
	 * @param parseRelationName
	 */
	public static void findTemplateRules(final Context context, String parseRelationName, final FindCallback<TemplateRule> findCallback) {
		LOGGER.debug(CLASS_NAME + "#findTemplateRules(...) from relation " + parseRelationName);

		// Gets the current user rules relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		
		final ParseRelation<TemplateRuleParse> rulesRelation = currentUser.getRelation(parseRelationName);
		ParseQuery<TemplateRuleParse> query = rulesRelation.getQuery();
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> templateRules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If previously rules found
				} else if (templateRules != null && !templateRules.isEmpty()) {
					List<TemplateRule> foundTemplateRules = new ArrayList<TemplateRule>();
					for (TemplateRuleParse templateRuleParse : templateRules) {
						TemplateRule templateRule = templateRuleParse.getTemplateRule();
						LOGGER.info("findTemplateRules(...) found " + templateRule.getName());
						foundTemplateRules.add(templateRule);
					}
					if (findCallback != null) {
						findCallback.done(foundTemplateRules);
					}
				}
			}
		});
	}
	
	/**
	 * Deletes the given template rules from the user rules.
	 * 
	 * @param context
	 * @param selectedTemplateRules
	 * @param parseRelationName TODO
	 */
	public static void removeTemplateRules(final Context context,
			final List<? extends TemplateRuleParse> selectedTemplateRules, String parseRelationName,
			final DoneCallback doneCallback) {
		LOGGER.debug(CLASS_NAME + "#removeTemplateRules(...) from relation " + parseRelationName);
		
		// Validates selected rules
		if (selectedTemplateRules == null || selectedTemplateRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		
		List<String> selectedRulesIds = getTemplateRulesParseIds(selectedTemplateRules);
		
		// Gets the current user rules relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final ParseRelation<TemplateRuleParse> relation = currentUser.getRelation(parseRelationName);
		ParseQuery<TemplateRuleParse> query = relation.getQuery();
		query.whereContainedIn(TemplateRuleParse.FIELD_RULE_ID, selectedRulesIds);
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> rules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If rules found, delete them
				} else if (rules != null && !rules.isEmpty()) {
					TemplateRuleParse.deleteAllInBackground(rules, new DeleteCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							LOGGER.info("removeTemplateRules(...) selected rules to remove were deleted");
							if (doneCallback != null) {
								doneCallback.done();
							}
						}
					});
				}
			}
		});
	}
	
	/**
	 * Deletes the given objects.
	 * 
	 * @param context
	 * @param selectedObjects
	 * @param parseRelationName TODO
	 */
	public static void removeObjects(final Context context,
			final Collection<? extends ParseObject> selectedObjects, String parseRelationName,
			final DoneCallback doneCallback) {
		LOGGER.debug(CLASS_NAME + "#removeObjects(...) from relation " + parseRelationName);
		
		// Validates selected objects
		if (selectedObjects == null || selectedObjects.isEmpty()) {
			LOGGER.warn("WARN: invalid selected objects");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		List<String> selectedObjectsIds = getParseObjectsIds(selectedObjects);
		
		// Gets the current user objects relation
		final ParseUser currentUser = ParseUser.getCurrentUser();
		final ParseRelation<ParseObject> relation = currentUser.getRelation(parseRelationName);
		ParseQuery<ParseObject> query = relation.getQuery();
		query.whereContainedIn(BaseParse.FIELD_OBJECT_ID, selectedObjectsIds);
		query.findInBackground(new com.parse.FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> objects, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If objects found, delete them
				} else if (objects != null && !objects.isEmpty()) {
					ParseObject.deleteAllInBackground(objects, new DeleteCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							LOGGER.info("removeObjects(...) selected objects to remove were deleted");
							if (doneCallback != null) {
								doneCallback.done();
							}
						}
					});
				}
			}
		});
	}
	
	public static void shareTemplateRule(final Context context, final TemplateRule templateRule, final DoneCallback callback) {
		LOGGER.debug(CLASS_NAME + "#shareTemplateRule(...)");
		
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		
		final String ruleId = templateRule.getId().toString();
		ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
		query.whereEqualTo(TemplateRuleParse.FIELD_RULE_ID, ruleId);
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> tempalteRules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If new template rule, share it
				} else if (tempalteRules == null || tempalteRules.isEmpty()) {
					// Create parse template rule from template rule
					final TemplateRuleParse templateRuleParse = new TemplateRuleParse(templateRule);
					ParseACL templateRuleParseACL = getTempalteRuleParseACL();
					templateRuleParse.setACL(templateRuleParseACL);
					templateRuleParse.incrementNumOfDownloads(+1);
					if (templateRuleParse.isLiked()) {
						templateRuleParse.incrementNumOfLikes(+1);
					}
					templateRuleParse.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							// Updates the current user uploaded template rules relation
							ParseUser currentUser = ParseUser.getCurrentUser();
							ParseRelation<TemplateRuleParse> uploadedRulesRelation = currentUser
									.getRelation(UserParse.RELATION_UPLOADED_TEMPLATE_RULES);
							uploadedRulesRelation.add(templateRuleParse);
							// Updates the current user liked template rules relation
							// If the template rule is liked, add it to the liked rules relation
							if (templateRuleParse.isLiked()) {
								ParseRelation<ParseObject> likedRulesRelation = currentUser
										.getRelation(UserParse.RELATION_LIKED_TEMPLATE_RULES);
								likedRulesRelation.add(templateRuleParse);
							}
							currentUser.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										LOGGER.error("Error: ", e);
										ToastUtils.show(AppState.BUILD_RELEASE,
												context,
												R.string.toast_connection_failed, Toast.LENGTH_SHORT);
										return;
									}
									ToastUtils.show(AppState.BUILD_RELEASE,
											context,
											R.string.toast_share_new_rule, Toast.LENGTH_SHORT);
									// Updates the activity log and analytics
									String action = context.getString(R.string.rule_uploaded);
									IActivityLogDAO activityLogDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
									activityLogDAO.log(templateRule.getId(), templateRule.getName(), action);
									AnalyticsUtils.sendRuleShared();
									if (callback != null) {
										callback.done();
									}
								}
							});
						}
					});
				// If existing template rule, update it
				} else {
					// Gets the parse template rule from template rule
					final TemplateRuleParse templateRuleParse = tempalteRules.get(0);
					templateRuleParse.setTemplateRule(templateRule);
					templateRuleParse.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							// Updates the current user uploaded template rules relation
							ParseUser currentUser = ParseUser.getCurrentUser();
							ParseRelation<TemplateRuleParse> uploadedRulesRelation = currentUser
									.getRelation(UserParse.RELATION_UPLOADED_TEMPLATE_RULES);
							uploadedRulesRelation.remove(templateRuleParse);
							uploadedRulesRelation.add(templateRuleParse);
							currentUser.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										LOGGER.error("Error: ", e);
										ToastUtils.show(AppState.BUILD_RELEASE,
												context,
												R.string.toast_connection_failed, Toast.LENGTH_SHORT);
										return;
									}
									ToastUtils.show(AppState.BUILD_RELEASE,
											context,
											R.string.toast_update_shared_rule,
											Toast.LENGTH_SHORT);
									// Updates the activity log and analytics
									String action = context.getString(R.string.rule_updated);
									IActivityLogDAO activityLogDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
									activityLogDAO.log(templateRule.getId(), templateRule.getName(), action);
									if (callback != null) {
										callback.done();
									}
								}
							});
						}
					});
				}
			}
		});
	}
	
	public static void updateTemplateRuleDownloaded(final Context context, final TemplateRule downloadedTemplateRule, final UpdateCallback<TemplateRuleParse> updateCallback) {
		LOGGER.debug(CLASS_NAME + "#updateTemplateRuleDownloaded(...)");
		
		// If downloaded template rule were created by the current user,
		// don't increment the remote template rule's number of downloads counter
		if (doesTemplateRuleMine(context, downloadedTemplateRule)) {
			return;
		}
		// If downloaded template rule weren't created by the current user,
		// increment the remote template rule's number of downloads counter
		final String ruleId = downloadedTemplateRule.getId().toString();
		ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
		query.whereEqualTo(TemplateRuleParse.FIELD_RULE_ID, ruleId);
		query.include(TemplateRuleParse.FIELD_CREATED_BY);
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> templateRules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If template rule not found
				} else if (templateRules == null || templateRules.isEmpty()) {
					LOGGER.warn("WARN: not found rule with id: " + downloadedTemplateRule.getId());
				// If template rule found, update it with download
				} else {
					final TemplateRuleParse foundTemplateRuleParse = templateRules.get(0);
					foundTemplateRuleParse.incrementNumOfDownloads();
					foundTemplateRuleParse.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							if (updateCallback != null) {
								updateCallback.done(foundTemplateRuleParse);
							}
						}
					});
				}
			}
		});
	}
	
	public static boolean doesTemplateRuleMine(Context context, TemplateRule templateRule) {
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return false;
		}
		// Gets current user name
		String currentUsername = ParseUser.getCurrentUser().getUsername();
		// Gets template rule's creator user name
		String ruleCreatorUsername = templateRule.getCreatedBy();
		if (TextUtils.isEmpty(ruleCreatorUsername)) {
			ruleCreatorUsername = context.getString(R.string.created_by_not_available);
		}
		// If the current user is the template rule's creator
		if (currentUsername.equalsIgnoreCase(ruleCreatorUsername)) {
			return true;
		// If the current user is not the template rule's creator
		} else {
			return false;
		}
	}
	
	public static void toggleTemplateRuleLiked(final Context context,
			final TemplateRule templateRule, final boolean isLiked,
			final DoneCallback doneCallback) {
		LOGGER.debug(CLASS_NAME + "#toggleTemplateRuleLiked(...)");
		
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		
		// Toggles remote template rule's liked state
		String ruleId = templateRule.getId().toString();
		ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
		query.whereEqualTo(TemplateRuleParse.FIELD_RULE_ID, ruleId);
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> templateRules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If template rule not found
				} else if (templateRules == null || templateRules.isEmpty()) {
					LOGGER.warn("WARN: not found rule with id: " + templateRule.getId());
				// If template rule found, toggle it's like state
				} else {
					// Gets the found template rule
					final TemplateRuleParse foundTemplateRuleParse = templateRules.get(0);
					// Gets the current user like template rules relation
					ParseUser currentUser = ParseUser.getCurrentUser();
					ParseRelation<TemplateRuleParse> likedRulesRelation = currentUser.getRelation(
									UserParse.RELATION_LIKED_TEMPLATE_RULES);
					if (isLiked) {
						likedRulesRelation.add(foundTemplateRuleParse);
					} else {
						likedRulesRelation.remove(foundTemplateRuleParse);
					}
					currentUser.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								LOGGER.error("Error: ", e);
								ToastUtils.show(AppState.BUILD_RELEASE,
										context,
										R.string.toast_connection_failed, Toast.LENGTH_SHORT);
								return;
							}
							if (isLiked) {
								foundTemplateRuleParse.incrementNumOfLikes(+1);
							} else {
								foundTemplateRuleParse.incrementNumOfLikes(-1);
							}
							foundTemplateRuleParse.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										LOGGER.error("Error: ", e);
										ToastUtils.show(AppState.BUILD_RELEASE,
												context,
												R.string.toast_connection_failed, Toast.LENGTH_SHORT);
										return;
									}
									// Updates the activity log and analytics
									String action = null;
									if (isLiked) {
										AnalyticsUtils.sendRuleLiked();
										action = context.getString(R.string.rule_liked);
									} else {
										AnalyticsUtils.sendRuleDisliked();
										action = context.getString(R.string.rule_disliked);
									}
									IActivityLogDAO activityLogDAO = DAOFactory.getFactory(
											DAOFactory.getFactoryType()).getActivityLogDAO();
									activityLogDAO.log(templateRule.getId(), templateRule.getName(), action);
									if (doneCallback != null) {						
										doneCallback.done();
									}
								}
							});
						}
					});
				}
			}
		});
	}
	
	public static void deleteTemplateRules(final Context context, List<Rule> selectedRules) {
		LOGGER.debug(CLASS_NAME + "#deleteTemplateRules(...)");

		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		// Gets selected template rules ids
		List<String> selectedTemplateRulesIds = new ArrayList<String>();
		for (Rule selectedRule : selectedRules) {
			if (!selectedRule.isMine()) {
				selectedTemplateRulesIds.add(selectedRule.getId().toString());
			}
		}
		// If there aren't any selected template rule, do nothing
		if (selectedTemplateRulesIds.isEmpty()) {
			return;
		}
		// If there are selected template rule, update remote user relations
		ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
		query.whereContainedIn(TemplateRuleParse.FIELD_RULE_ID, selectedTemplateRulesIds);
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> templateRules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If template rules not found
				} else if (templateRules == null || templateRules.isEmpty()) {
					LOGGER.warn("WARN: not found any rule");
				// If template rules found, delete it from user
				} else {
					ParseUser currentUser = ParseUser.getCurrentUser();
					// Gets the current user liked template rules
					ParseRelation<TemplateRuleParse> likedRulesRelation = currentUser.getRelation(
									UserParse.RELATION_LIKED_TEMPLATE_RULES);
					// Gets the current user downloads template rules
					ParseRelation<TemplateRuleParse> downloadedRulesRelation = currentUser.getRelation(
							UserParse.RELATION_DOWNLOADED_TEMPLATE_RULES);
					for (TemplateRuleParse templateRule : templateRules) {
						likedRulesRelation.remove(templateRule);
						downloadedRulesRelation.remove(templateRule);
					}
					currentUser.saveInBackground();
				}
			}
		});
	}
	
	public static void restoreTemplateRule(final Context context, final Rule rule) {
		LOGGER.debug(CLASS_NAME + "#restoreTemplateRule(...)");
		
		// Validates rule
		if (rule == null) {
			LOGGER.warn("WARN: invalid rules");
			return;
		}
		// Validates current user
		if (getCurrentUserAccoutType() == null) {
			LOGGER.warn("WARN: invalide currentUser, aboarding...");
			return;
		}
		
		String ruleToRestoreId = rule.getId().toString();
		ParseQuery<TemplateRuleParse> query = new ParseQuery<TemplateRuleParse>(TemplateRuleParse.class);
		query.whereEqualTo(TemplateRuleParse.FIELD_RULE_ID, ruleToRestoreId);
		query.findInBackground(new com.parse.FindCallback<TemplateRuleParse>() {
			@Override
			public void done(List<TemplateRuleParse> templateRules, ParseException e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							context,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If template rules not found
				} else if (templateRules == null || templateRules.isEmpty()) {
					LOGGER.warn("WARN: not found any rule");
				// If template rules found, delete it from user
				} else {
					ParseUser currentUser = ParseUser.getCurrentUser();
					if (rule.isLiked()) {
						// Gets the current user liked template rules
						ParseRelation<TemplateRuleParse> likedRulesRelation = currentUser
								.getRelation(UserParse.RELATION_LIKED_TEMPLATE_RULES);
						likedRulesRelation.add(templateRules.get(0));
					}
					// Gets the current user downloads template rules
					ParseRelation<TemplateRuleParse> downloadedRulesRelation = currentUser.getRelation(
							UserParse.RELATION_DOWNLOADED_TEMPLATE_RULES);
					downloadedRulesRelation.add(templateRules.get(0));
					currentUser.saveInBackground();
				}
			}
		});
	}

	// TODO: delete
	public static final void createRoles() {
		ParseRole admins = new ParseRole(RoleParse.ROLE_ADMINS);
		ParseACL adminsACL = new ParseACL();
		adminsACL.setPublicReadAccess(true);
		adminsACL.setPublicWriteAccess(true);
		admins.setACL(adminsACL);
		admins.saveInBackground();
		
		ParseRole moderators = new ParseRole(RoleParse.ROLE_MODERATORS);
		ParseACL moderatorsACL = new ParseACL();
		moderatorsACL.setPublicReadAccess(true);
		moderatorsACL.setPublicWriteAccess(true);
		moderators.setACL(moderatorsACL);
		moderators.saveInBackground();
	}
	
	// TODO: delete
	public static final void configAdminsRole() {
		ParseQuery<ParseRole> query1 = ParseQuery.getQuery(ParseRole.class);
		query1.getInBackground("jJAnFqA3vE", new GetCallback<ParseRole>() {
			@Override
			public void done(ParseRole role1, ParseException e) {
				if (e == null) {
					role1.getUsers().add(ParseUser.getCurrentUser());
					role1.saveInBackground();
				}
			}
		});
	}
	
	// TODO: delete
	public static final void configModeratorsRole() {
		ParseQuery<ParseRole> query1 = ParseQuery.getQuery(ParseRole.class);
		query1.getInBackground("1kivZErnz0", new GetCallback<ParseRole>() {
			@Override
			public void done(final ParseRole role1, ParseException e) {
				if (e == null) {
					role1.getUsers().add(ParseUser.getCurrentUser());
					ParseQuery<ParseRole> query2 = ParseQuery.getQuery(ParseRole.class);
					query2.getInBackground("jJAnFqA3vE", new GetCallback<ParseRole>() {
						@Override
						public void done(ParseRole role2, ParseException e) {
							if (e == null) {
								role1.getRoles().add(role2);
								role1.saveInBackground();
							}
						}
					});
				}
			}
		});
	}
	
	private static List<String> getRulesIds(final List<Rule> selectedRules) {
		List<String> selectedRulesIds = new ArrayList<String>();
		
		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return selectedRulesIds;
		}
		// Gets selected rules ids
		for (Rule selectedRule : selectedRules) {
			selectedRulesIds.add(selectedRule.getId().toString());
		}
		
		return selectedRulesIds;
	}
	
	private static List<String> getRulesParseIds(final Collection<? extends RuleParse> selectedRules) {
		List<String> selectedRulesIds = new ArrayList<String>();
		
		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return selectedRulesIds;
		}
		// Gets selected rules ids
		for (RuleParse selectedRule : selectedRules) {
			selectedRulesIds.add(selectedRule.getRuleId().toString());
		}
		
		return selectedRulesIds;
	}
	
	private static List<String> getTemplateRulesParseIds(final List<? extends TemplateRuleParse> selectedRules) {
		List<String> selectedRulesIds = new ArrayList<String>();
		
		// Validates selected rules
		if (selectedRules == null || selectedRules.isEmpty()) {
			LOGGER.warn("WARN: invalid selected rules");
			return selectedRulesIds;
		}
		// Gets selected rules ids
		for (TemplateRuleParse selectedRule : selectedRules) {
			selectedRulesIds.add(selectedRule.getRuleId().toString());
		}
		
		return selectedRulesIds;
	}
	
	private static List<String> getParseObjectsIds(final Collection<? extends ParseObject> selectedObjects) {
		List<String> selectedObjectsIds = new ArrayList<String>();
		
		// Validates selected objects
		if (selectedObjects == null || selectedObjects.isEmpty()) {
			LOGGER.warn("WARN: invalid selected objects");
			return selectedObjectsIds;
		}
		// Gets selected objects ids
		for (ParseObject selectedObject : selectedObjects) {
			selectedObjectsIds.add(selectedObject.getObjectId());
		}
		
		return selectedObjectsIds;
	}

}

/**
 * 
 */
package com.ruley.utilities;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ruley.model.beans.rules.Rule;
import com.ruley.model.connection.file.json.JsonHelper;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class BackupUtils {
	
	private static final String CLASS_NAME = BackupUtils.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BackupUtils.class.getName());
	
	private BackupUtils() {
	}

	public static void backupRulesToJSON(List<Rule> rules, String filePath) {
		LOGGER.debug(CLASS_NAME + "#backupRulesToJSON(...)");
		
		long startMillis = System.currentTimeMillis();

		JsonHelper.getInstance().writeJSON(filePath, rules);
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#backupRulesToJSON(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}

}

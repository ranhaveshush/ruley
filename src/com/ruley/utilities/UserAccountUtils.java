/**
 * 
 */
package com.ruley.utilities;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class UserAccountUtils {

	private static final String CLASS_NAME = UserAccountUtils.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(UserAccountUtils.class.getName());
	
	public static final String ACCOUNT_TYPE_GOOGLE = "com.google";

	private UserAccountUtils() {
	}
	
	/**
	 * Returns the default Google account.
	 * 
	 * @return Account represents the default Google account.
	 */
	public static Account getDefaultAccount() {
		LOGGER.debug(CLASS_NAME + "#getDefaultAccount(...)");
		
		Account defaultAccount = null;
		
		List<Account> accounts = getAccounts(UserAccountUtils.ACCOUNT_TYPE_GOOGLE);
		if (accounts != null && !accounts.isEmpty()) {
			defaultAccount = accounts.get(0);
		}
		
		return defaultAccount;
	}

	/**
	 * Returns all the accounts by given account type.
	 * 
	 * @param accountType
	 *            String represents the account type (e.g. "com.google")
	 * @return List of all the accounts from a given account type, if there are
	 *         none accounts by that account type returns empty list.
	 */
	public static List<Account> getAccounts(String accountType) {
		LOGGER.debug(CLASS_NAME + "#getAccounts(...)");

		List<Account> accounts = new ArrayList<Account>();

		Context appCtx = MyApplication.getContext();
		
		AccountManager accountManager = AccountManager.get(appCtx);
		Account[] userAccounts = accountManager.getAccountsByType(accountType);
		for (Account userAccount : userAccounts) {
			accounts.add(userAccount);
		}

		return accounts;
	}

	/**
	 * Returns an account by given account type and account name.
	 *
	 * @param accountManager
	 * @param accountType
	 *            String represents the account type (e.g. "com.google")
	 * @param accountName
	 *            String represents the account name
	 * @return Account the wanted account if found, otherwise returns null
	 */
	public static Account getAccount(AccountManager accountManager, String accountType, String accountName) {
		LOGGER.debug(CLASS_NAME + "#getAccount(...)");

		Account[] userAccounts = accountManager.getAccountsByType(accountType);
		for (Account userAccount : userAccounts) {
			if (accountName.equals(userAccount.name)) {
				return userAccount;
			}
		}

		return null;
	}

}

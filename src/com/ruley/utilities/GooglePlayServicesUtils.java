/**
 * 
 */
package com.ruley.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.ruley.R;
import com.ruley.app.AppState;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class GooglePlayServicesUtils {
	
	private static final String CLASS_NAME = GooglePlayServicesUtils.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GooglePlayServicesUtils.class.getName());
	
	public static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 9000;
	
	private GooglePlayServicesUtils() {
	}

	public static boolean isGooglePlayServicesAvailable(Activity activity) {
		LOGGER.debug(CLASS_NAME + "#mIsGooglePlayServicesAvailable(...)");
		
		// Checks that Google Play Services is available
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		return GooglePlayServicesConnectionResolution(resultCode, activity);
	}
	
	public static void handleActivityResult(int requestCode, int resultCode, Activity activity) {
		if (requestCode == REQUEST_CODE_RECOVER_PLAY_SERVICES) {
			if (resultCode == Activity.RESULT_OK) {
				boolean isGooglePlayServicesAvailable = GooglePlayServicesUtils.isGooglePlayServicesAvailable(activity);
				if (!isGooglePlayServicesAvailable) {
					activity.setResult(Activity.RESULT_CANCELED);
					activity.finish();
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				ToastUtils.show(AppState.BUILD_RELEASE, activity, R.string.toast_google_play_services_missing, Toast.LENGTH_LONG);
				activity.setResult(Activity.RESULT_CANCELED);
				activity.finish();
			}
		}
	}
	
	private static boolean GooglePlayServicesConnectionResolution(int errorCode, Activity activity) {
		String errorString = GooglePlayServicesUtil.getErrorString(errorCode);
		LOGGER.info("Google Play Services: " + errorString);
		
		boolean isGooglePlayServicesAvailable = false;
		
		switch (errorCode) {
		case ConnectionResult.SUCCESS:
			isGooglePlayServicesAvailable = true;
			break;
		case ConnectionResult.SERVICE_MISSING:
		case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
		case ConnectionResult.SERVICE_DISABLED:
			showErrorDialog(errorCode, activity);
			isGooglePlayServicesAvailable = false;
			break;
		case ConnectionResult.SIGN_IN_REQUIRED:
		case ConnectionResult.RESOLUTION_REQUIRED:
			// TODO: implement
//			ConnectionResult.startResolutionForResult();
			isGooglePlayServicesAvailable = false;
			break;
		case ConnectionResult.INVALID_ACCOUNT:
		case ConnectionResult.NETWORK_ERROR:
		case ConnectionResult.INTERNAL_ERROR:
		case ConnectionResult.SERVICE_INVALID:
		case ConnectionResult.DEVELOPER_ERROR:
		case ConnectionResult.LICENSE_CHECK_FAILED:
			activity.finish();
			isGooglePlayServicesAvailable = false;
			break;
		default:
			LOGGER.error(CLASS_NAME + "#GooglePlayServicesConnectionResolution(...) invalid argument resultCode: " + errorCode);
			isGooglePlayServicesAvailable = false;
		}
		
		if (!isGooglePlayServicesAvailable) {
			ToastUtils.show(AppState.BUILD_RELEASE, activity, R.string.toast_google_play_services_missing, Toast.LENGTH_LONG);
		}
		
		return isGooglePlayServicesAvailable;
	}

	private static void showErrorDialog(int errorCode, final Activity activity) {
		GooglePlayServicesUtil.getErrorDialog(errorCode, activity,
				REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
	}
}

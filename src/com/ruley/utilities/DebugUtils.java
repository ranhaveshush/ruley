/**
 * 
 */
package com.ruley.utilities;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.ruley.model.beans.actions.RespondReceivedSmsesAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DebugUtils {
	
	private static final String CLASS_NAME = RespondReceivedSmsesAction.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RespondReceivedSmsesAction.class.getName());
	
	private DebugUtils() {
	}
	
	public static void dumpContentResolverQuery(Context context, Uri uri,
			String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		
		LOGGER.info("Uri: " + uri);
		LOGGER.info("Projection: " + Arrays.toString(projection));
		LOGGER.info("Selection: " + selection);
		LOGGER.info("SelectionArgs: " + Arrays.toString(selectionArgs));
		LOGGER.info("sortOrder: " + sortOrder);
		
		Cursor cursor = context.getContentResolver().query(uri, projection,
				selection, selectionArgs, sortOrder);
		
		try {
			dumpCursor(cursor);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}
	
	public static void dumpCursor(Cursor cursor) {
		// Validates cursor
		if (cursor == null || !cursor.moveToFirst()) {
			return;
		}
		
		dumpCursorSchema(cursor);
		
		while(cursor.moveToNext()) {
			dumpCursorRecord(cursor);
		}
	}
	
	public static void dumpCursorSchema(Cursor cursor) {
		// Validates cursor
		if (cursor == null) {
			return;
		}
		
		for (int columnIndex=0; columnIndex < cursor.getColumnCount(); ++columnIndex) {
			String columnName = cursor.getColumnName(columnIndex);
			int columnType = cursor.getType(columnIndex);
			LOGGER.info("#" + columnIndex + " " + cursorColumnTypeToString(columnType) + " "  + columnName);
		}
	}
	
	public static String cursorColumnTypeToString(int columnType) {
		switch (columnType) {
		case Cursor.FIELD_TYPE_STRING:
			return "STRING";
		case Cursor.FIELD_TYPE_INTEGER:
			return "INTEGER";
		case Cursor.FIELD_TYPE_FLOAT:
			return "FLOAT";
		case Cursor.FIELD_TYPE_BLOB:
			return "BLOB";
		case Cursor.FIELD_TYPE_NULL:
			return "NULL";
		default:
			throw new IllegalStateException("Invalid cursor column type"); 
		}
	}
	
	public static void dumpCursorRecord(Cursor cursor) {
		// Validates cursor
		if (cursor == null) {
			return;
		}
		
		LOGGER.info(cursorRecordToString(cursor));
	}
	
	public static String cursorRecordToString(Cursor cursor) {
		// Validates cursor
		if (cursor == null) {
			return null;
		}
		
		String record = "";
		
		for (int columnIndex=0; columnIndex < cursor.getColumnCount(); ++columnIndex) {
			String columnName = cursor.getColumnName(columnIndex);
			record += columnName + "=";
			
			int columnType = cursor.getType(columnIndex);
			switch (columnType) {
			case Cursor.FIELD_TYPE_STRING:
				record += cursor.getString(columnIndex);
				break;
			case Cursor.FIELD_TYPE_INTEGER:
				record += cursor.getInt(columnIndex);
				break;
			case Cursor.FIELD_TYPE_FLOAT:
				record += cursor.getFloat(columnIndex);
				break;
			case Cursor.FIELD_TYPE_BLOB:
				record += cursor.getBlob(columnIndex);
				break;
			case Cursor.FIELD_TYPE_NULL:
				record += "null";
				break;
			default:
				throw new IllegalStateException("Invalid cursor column type");
			}
			
			// If isn't the last column concatenate comma
			if (columnIndex + 1 < cursor.getColumnCount()) {
				record += ", ";
			}
		}
		
		return record;
	}

}

/**
 * 
 */
package com.ruley.utilities;

import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.app.AppState;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToastUtils {
	
	private static final int LONG_DELAY = 3500; // 3.5 seconds
	private static final int SHORT_DELAY = 2000; // 2 seconds
	
	private ToastUtils() {
	}
	
	public static void show(int level, Context context, int resourceId, int duration) {
		if (AppState.getBuildMode() <= level) {
			Toast.makeText(context, resourceId, duration).show();
		}
	}

	public static void show(int level, Context context, String toastText, int duration) {
		if (AppState.getBuildMode() <= level) {
			Toast.makeText(context, toastText, duration).show();
		}
	}
	
	public static void show(int level, Context context, View parent, int durationType,
			int toastResourceId, int textStringResourceId, int buttonStringResourceId,
			final OnClickListener onClickListener) {
		
		if (AppState.getBuildMode() <= level) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View toastView = inflater.inflate(toastResourceId, null);
			TextView tvText = (TextView) toastView.findViewById(R.id.tvText);
			final TextView tvBack = (TextView) toastView.findViewById(R.id.tvBack);
			
			tvText.setText(textStringResourceId);
			tvBack.setText(buttonStringResourceId);
			
			// Shows the popup window
			final PopupWindow toastPopup = new PopupWindow(toastView);
			//toastPopup.setAnimationStyle(R.anim.abc_fade_out);
			int screenDensity = (int) context.getResources().getDisplayMetrics().density;
			toastPopup.setHeight(screenDensity * 62);
			toastPopup.setWidth((int) Math.min(screenDensity * 400, parent.getWidth() * 0.9f));
			toastPopup.showAtLocation(parent, Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, (int)(screenDensity * 15));
			
			// Dismisses the popup window after duration millis
			final Handler handler = new Handler();
			final Runnable toastPopupDismiss = new Runnable() {
				@Override
				public void run() {
					toastPopup.dismiss();
				}
			};
			handler.postDelayed(toastPopupDismiss, getToastDuration(durationType));
			
			// TODO: This is crushing the app when the parent activity
			// is no longer existing.
//			// Handles back press
//			tvBack.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					toastPopup.dismiss();
//					handler.removeCallbacks(toastPopupDismiss);
//					onClickListener.onClick(tvBack);
//				}
//			});
		}
	}
	
	public static int getToastDuration(int durationType) {
		switch (durationType) {
		case Toast.LENGTH_SHORT:
			return SHORT_DELAY;
		case Toast.LENGTH_LONG:
			return LONG_DELAY;
		default:
			throw new IllegalArgumentException("Invalid durationType not Toast.LENGTH_SHORT/Toast.LENGTH_LONG");
		}
	}

}

/**
 * 
 */
package com.ruley.utilities;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationClient.OnAddGeofencesResultListener;
import com.google.android.gms.location.LocationClient.OnRemoveGeofencesResultListener;
import com.google.android.gms.location.LocationStatusCodes;
import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;

public class LocationClientUtils implements ConnectionCallbacks, OnConnectionFailedListener, OnAddGeofencesResultListener, OnRemoveGeofencesResultListener {
	
	private static final String CLASS_NAME = LocationClientUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(LocationClientUtils.class.getName());
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		/**
		 * Invoked when device last location is known. 
		 */
		public void onKnownLastLocation(Location location);
		
		/**
		 * Invoked when finish registeration to mGeofences services.
		 */
		public void onAddedGeofences();
		
		/**
		 * Invoked when finish unregisteration to mGeofences services.
		 */
		public void onRemovedGeofencesByPendingIntent();
		
		/**
		 * Invoked when error occured.
		 */
		public void onError(int errorMessageResourceId);
	}

	private static final int REQUEST_CODE_CONNECTION_FAILURE_RESOLUTION = 2001;
	
	private static boolean mInProgress = false;
	
	private enum RequestType {
		GET_LAST_LOCATION,
		ADD_GEOFENCE,
		REMOVE_GEOFENCE_INTENT
	};
	
	private LocationClientUtils.Listener mListener = null;
	
	private RequestType mRequestType = null;
	
	private LocationClient mLocationClient = null;
	
	private PendingIntent mPendingIntent = null;
	
	private List<Geofence> mGeofences = null;
	
	public static boolean isProviderEnabled(Context context, String provider) {
		LOGGER.debug(CLASS_NAME + "#isProviderEnabled(...)");
		
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (locationManager == null) {
			LOGGER.info("LocationManager not available");
			return false;
		} else {
			boolean isProviderEnabled = locationManager.isProviderEnabled(provider);
			LOGGER.info(provider + " is " + ((isProviderEnabled) ? " enabled" : " disbaled"));
			return isProviderEnabled;
		}
	}
	
	public static boolean isLocationServiceEnabled(Context context) {
		LOGGER.debug(CLASS_NAME + "#islocationServicesEnabled(...)");
		
		boolean isGPSProviderEnabled = isProviderEnabled(context, LocationManager.GPS_PROVIDER);
		if (!isGPSProviderEnabled) {
			boolean isNetworkProviderEnabled = isProviderEnabled(context, LocationManager.NETWORK_PROVIDER);
			if (!isNetworkProviderEnabled) {
				return false;
			}
		}
		
		return true;
	}
	
	public void getLastLocation(Context context, LocationClientUtils.Listener listener) {
		LOGGER.debug(CLASS_NAME + "#getLastLocation(...)");
		
		// Validates arguments
		if (context == null || listener == null) {
			throw new IllegalArgumentException(
					CLASS_NAME + "#getLastLocation(...) - Invalid null arguments.");
		}
		
		this.mListener = listener;
		mRequestType = RequestType.GET_LAST_LOCATION;
		mLocationClient = new LocationClient(context, this, this);
		
		if (!mInProgress) {
			mInProgress = true;
			mLocationClient.connect();
		} else {
			mLocationClient.disconnect();
			mInProgress = false;
			mLocationClient.connect();
		}
	}
	
	public void addGeofences(Context context, List<Geofence> geofences, PendingIntent pendingIntent, LocationClientUtils.Listener listener) {
		LOGGER.debug(CLASS_NAME + "#addGeofences(...)");
		
		// Validates arguments
		if (context == null || geofences == null || geofences.isEmpty()
			|| pendingIntent == null || listener == null) {
			throw new IllegalArgumentException(
					CLASS_NAME + "#addGeofences(...) - Invalid null arguments.");
		}
		
		this.mListener = listener;
		this.mGeofences = geofences;
		this.mPendingIntent = pendingIntent;
		mRequestType = RequestType.ADD_GEOFENCE;
		mLocationClient = new LocationClient(context, this, this);
		
		if (!mInProgress) {
			mInProgress = true;
			mLocationClient.connect();
		} else {
			mLocationClient.disconnect();
			mInProgress = false;
			mLocationClient.connect();
		}
	}
	
	public void removeGeofences(Context context, PendingIntent pendingIntent, LocationClientUtils.Listener listener) {
		LOGGER.debug(CLASS_NAME + "#removeGeofences(...)");
		
		// Validates arguments
		if (context == null || pendingIntent == null || listener == null) {
			throw new IllegalArgumentException(
					CLASS_NAME + "#removeGeofences(...) - Invalid null arguments.");
		}
		
		this.mListener = listener;
		this.mPendingIntent = pendingIntent;
		mRequestType = RequestType.REMOVE_GEOFENCE_INTENT;
		mLocationClient = new LocationClient(context, this, this);
		
		if (!mInProgress) {
			mInProgress = true;
			mLocationClient.connect();
		} else {
			mLocationClient.disconnect();
			mInProgress = false;
			mLocationClient.connect();
		}
	}

	@Override
	public void onConnected(Bundle args) {
		LOGGER.debug(CLASS_NAME + "#onConnected(...)");
		
		switch (mRequestType) {
		case GET_LAST_LOCATION:
			mListener.onKnownLastLocation(mLocationClient.getLastLocation());
			break;
		case ADD_GEOFENCE:
			mLocationClient.addGeofences(mGeofences, mPendingIntent, this);
			break;
		case REMOVE_GEOFENCE_INTENT:
			mLocationClient.removeGeofences(mPendingIntent, this);
			break;
		}
	}
	
	@Override
	public void onDisconnected() {
		LOGGER.debug(CLASS_NAME + "#onDisconnected(...)");
		
		mInProgress = false;
		mLocationClient = null;
	}

	@Override
	public void onAddGeofencesResult(int statusCode, String[] geofenceRequestIds) {
		LOGGER.debug(CLASS_NAME + "#onAddGeofencesResult(...)");
		
		switch (statusCode) {
		case LocationStatusCodes.SUCCESS:
			LOGGER.info("Adding Geofence was successful");
			mListener.onAddedGeofences();
			break;
		case LocationStatusCodes.ERROR:
			LOGGER.error("Adding Geofence was failed");
			mListener.onError(R.string.location_status_codes_error);
			break;
		case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE:
			LOGGER.error("Adding Geofence was failed");
			mListener.onError(R.string.location_status_codes_geofence_not_available);
			break;
		case LocationStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
			LOGGER.error("Adding Geofence was failed");
			mListener.onError(R.string.location_status_codes_too_many_geofences);
			break;
		case LocationStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
			LOGGER.error("Adding Geofence was failed");
			mListener.onError(R.string.location_status_codes_too_many_pending_intents);
			break;
		}
		
        mInProgress = false;
        mLocationClient.disconnect();
	}
	
	@Override
	public void onRemoveGeofencesByPendingIntentResult(int statusCode, PendingIntent pendingIntent) {
		LOGGER.debug(CLASS_NAME + "#onRemoveGeofencesByPendingIntentResult(...)");
		
		switch (statusCode) {
		case LocationStatusCodes.SUCCESS:
			LOGGER.info("Removing Geofence was successful");
			mListener.onRemovedGeofencesByPendingIntent();
			break;
		case LocationStatusCodes.ERROR:
			LOGGER.error("Removing Geofence was failed");
			mListener.onError(R.string.location_status_codes_error);
			break;
		case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE:
			LOGGER.error("Removing Geofence was failed");
			mListener.onError(R.string.location_status_codes_geofence_not_available);
			break;
		}
		
		mInProgress = false;
		mLocationClient.disconnect();
	}
	
	@Override
	public void onRemoveGeofencesByRequestIdsResult(int statusCode, String[] requestIds) {
		LOGGER.debug(CLASS_NAME + "#onRemoveGeofencesByRequestIdsResult(...)");
		
		switch (statusCode) {
		case LocationStatusCodes.SUCCESS:
			LOGGER.info("Removing Geofence was successful");
			mListener.onRemovedGeofencesByPendingIntent();
			break;
		case LocationStatusCodes.ERROR:
			LOGGER.error("Removing Geofence was failed");
			mListener.onError(R.string.location_status_codes_error);
			break;
		case LocationStatusCodes.GEOFENCE_NOT_AVAILABLE:
			LOGGER.error("Removing Geofence was failed");
			mListener.onError(R.string.location_status_codes_geofence_not_available);
			break;
		}
		
		mInProgress = false;
		mLocationClient.disconnect();
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		LOGGER.debug(CLASS_NAME + "#onConnectionFailed(...)");
		
		mInProgress = false;
		
		if (connectionResult.hasResolution()) {
			LOGGER.warn("There is a resolution for the error, launch resolution dialog");
			Context appCtx = MyApplication.getContext();
			try {
                connectionResult.startResolutionForResult((Activity)appCtx, REQUEST_CODE_CONNECTION_FAILURE_RESOLUTION);
            } catch (IntentSender.SendIntentException e) {
                LOGGER.error("Error: ", e);
            }
		} else {
			LOGGER.error("There isn't a resolution for the error, launch error dialog");
			mListener.onError(connectionResult.getErrorCode());
		}
	}
	
	/**
	 * Workaround to the listening to the location services disabled event.
	 * There isn't any energy efficient solution so this is the one.
	 */
	public void listenLocationServicesDisabled(Context context, LocationClientUtils.Listener listener) {
		LOGGER.debug(CLASS_NAME + "#listenLocationServicesDisabled(...)");
		
		final Geofence dummyGeofence = new Geofence.Builder()
		.setRequestId("0") // The geofence request id is the trigger's UUID
		.setCircularRegion(0D, 0D, 1F)
		.setExpirationDuration(Geofence.NEVER_EXPIRE)
		.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
		.build();
		ArrayList<Geofence> dummyGeofences = new ArrayList<Geofence>() {
			{
				add(dummyGeofence);
			}
		};
		
		Intent intent = new Intent(Config.Action.ACTION_LOCATION_SERVICE_DISABLED);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.LOCATION_SERVICE_DISABLED_REQUEST_CODE,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		if (pendingIntent != null) {
			addGeofences(context, dummyGeofences, pendingIntent, listener);
		}
	}
	
	/**
	 * Workaround to the listening to the location services disabled event.
	 * There isn't any energy efficient solution so this is the one.
	 */
	public void unlistenLocationServicesDisabled(Context context, LocationClientUtils.Listener listener) {
		LOGGER.debug(CLASS_NAME + "#unlistenLocationServicesDisabled(...)");
		
		Intent intent = new Intent(Config.Action.ACTION_LOCATION_SERVICE_DISABLED);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				Config.PendingIntent.LOCATION_SERVICE_DISABLED_REQUEST_CODE, intent,
				PendingIntent.FLAG_NO_CREATE | PendingIntent.FLAG_CANCEL_CURRENT);
		
		if (pendingIntent != null) {
			removeGeofences(context, pendingIntent, listener);
		}
	}

}

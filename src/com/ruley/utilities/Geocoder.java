/**
 * 
 */
package com.ruley.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.location.Address;
import android.os.AsyncTask;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class Geocoder {

	private static final String URL_GEOCODER_SERVICE = "https://maps.googleapis.com/maps/api/geocode/json?";

	private static final String CLASS_NAME = Geocoder.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(Geocoder.class.getName());
	
	private static final Locale LOCALE = new Locale("IW", "IL");

	private static final String CHARSET = "UTF-8";
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		public void onFinishAutoCompleteAddress(List<Address> addresses);
		
		public void onFinishGeocoding(List<Address> addresses);
		
		public void onFinishReverseGeocoding(List<Address> addresses);
	}
	
	private Listener mListener = null;
	
	public Geocoder(Listener Listener) {
		super();
		mListener = Listener;
	}
	
	public void autoCompleteAddresses(String... addresses) {
		LOGGER.debug(CLASS_NAME + "#autoCompleteAddresses(...)");
		
		try {
			new AutoCompleteAddressesTask().execute(addresses);
		} catch (Exception e) {
			mListener.onFinishAutoCompleteAddress(null);
		}
	}
	
	public void geocoding(String address) {
		LOGGER.debug(CLASS_NAME + "#geocoding(...)");
		
		try {
			new Geocoding().execute(address);
		} catch (Exception e) {
			mListener.onFinishGeocoding(null);
		}
	}
	
	public void reverseGeocoding(double latitude, double longitude) {
		LOGGER.debug(CLASS_NAME + "#reverseGeocoding(...)");
		
		try {
			new ReverseGeocoding().execute(latitude, longitude);
		} catch (Exception e) {
			mListener.onFinishReverseGeocoding(null);
		}
	}
	
	/**
	 * Auto complete async task to geocode addresses.
	 * 
	 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
	 * 
	 */
	private class AutoCompleteAddressesTask extends
			AsyncTask<String, Void, List<Address>> {

		private static final String TAG = "AutoCompleteAddressesTask";

		/**
		 * Max results per autocomplete.
		 */
		private static final int MAX_RESULTS = 3;

		@Override
		protected List<Address> doInBackground(String... addresses) {
			LOGGER.debug(TAG, "doInBackground(String... addresses)");

			String toAutoCompleteAddressStr = addresses[0];
			return getFromLocationName(toAutoCompleteAddressStr, LOCALE, MAX_RESULTS);
		}

		@Override
		protected void onPostExecute(List<Address> addresses) {
			LOGGER.debug(TAG, "onPostExecute(List<Address> autoCompleteAddressesSuggestions)");

			mListener.onFinishAutoCompleteAddress(addresses);
		}
	}
	
	/**
	 * Geocoding address to latitude and longitude.
	 * 
	 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
	 * 
	 */
	private class Geocoding extends AsyncTask<String, Void, List<Address>> {

		private static final String TAG = "Geocoding";

		private static final int MAX_RESULTS = 1;

		@Override
		protected List<Address> doInBackground(String... params) {
			LOGGER.debug(TAG, "doInBackground(String... params)");

			String address = params[0];
			return getFromLocationName(address, LOCALE, MAX_RESULTS);
		}

		@Override
		protected void onPostExecute(List<Address> addresses) {
			LOGGER.debug(TAG, "onPostExecute(String address)");

			mListener.onFinishGeocoding(addresses);
		}

	}
	
	/**
	 * Reverse geocoding an latitude longitude to readable address.
	 * 
	 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
	 * 
	 */
	private class ReverseGeocoding extends
			AsyncTask<Double, Void, List<Address>> {

		private static final String TAG = "ReverseGeocoding";

		private static final int MAX_RESULTS = 1;

		@Override
		protected List<Address> doInBackground(Double... params) {
			LOGGER.debug(TAG, "doInBackground(Double params)");

			double latitude = params[0];
			double longitude = params[1];
			return getFromLocation(latitude, longitude, LOCALE, MAX_RESULTS);
		}

		@Override
		protected void onPostExecute(List<Address> addresses) {
			LOGGER.debug(TAG, "onPostExecute(String address)");

			mListener.onFinishReverseGeocoding(addresses);
		}

	}

	public List<Address> getFromLocationName(String locationName,
			Locale locale, int maxResults) {
		LOGGER.debug(CLASS_NAME + "#getFromLocationName(...)");

		List<Address> addresses = null;

		try {
			// Escapes special characters
			String address = URLEncoder.encode(locationName, CHARSET).replace(" ", "+");

			StringBuilder sbUrl = new StringBuilder(URL_GEOCODER_SERVICE);
			sbUrl.append("address=").append(address);
			sbUrl.append("&sensor=true");
			sbUrl.append("&language=").append(locale.toString());

			String url = sbUrl.toString();
			LOGGER.debug(CLASS_NAME + "#getFromLocationName(...) url = " + url);

			addresses = geocoderCore(url, maxResults);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Error: ", e);
		}

		return addresses;
	}

	public List<Address> getFromLocation(double latitude,
			double longitude, Locale locale, int maxResults) {
		LOGGER.debug(CLASS_NAME + "#getFromLocation(...)");

		StringBuilder sbUrl = new StringBuilder(URL_GEOCODER_SERVICE);
		sbUrl.append("latlng=").append(latitude).append(",").append(longitude);
		sbUrl.append("&sensor=true");
		sbUrl.append("&language=").append(locale.toString());

		String url = sbUrl.toString();
		LOGGER.info(CLASS_NAME + "#getFromLocationName(...) url = " + url);

		return geocoderCore(url, maxResults);
	}

	/**
	 * This method is the core method of the geocoder. This method communicates
	 * with geocoder service and returns a list of addresses.
	 * 
	 * @param urlStr
	 *            String represents the url to the geocoder service
	 * @param maxResults
	 *            Integer represents the max result to retrieve
	 * @return List of addresses
	 */
	private List<Address> geocoderCore(String urlStr, int maxResults) {

		List<Address> addresses = null;

		StringBuilder sbResponse = new StringBuilder();

		try {
			URL url = new URL(urlStr);
			InputStream is = url.openStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ((line = br.readLine()) != null) {
				sbResponse.append(line);
			}
			JSONObject jsonResponse = new JSONObject(sbResponse.toString());
			LOGGER.debug(CLASS_NAME
					+ "#getFromLocationName(...) jsonResponse = "
					+ jsonResponse);
			addresses = extractResponse(jsonResponse, maxResults);
		} catch (ClientProtocolException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} catch (JSONException e) {
			LOGGER.error("Error: ", e);
		}

		return addresses;
	}

	/**
	 * @see https
	 *      ://developers.google.com/maps/documentation/geocoding/#StatusCodes
	 * 
	 * @param jsonResponse
	 * @return
	 */
	private List<Address> extractResponse(JSONObject jsonResponse,
			int maxResults) {
		LOGGER.debug(CLASS_NAME + "#extractResponse(...)");

		try {
			String status = jsonResponse.getString("status");
			if ("OK".equals(status)) {
				LOGGER.debug(CLASS_NAME
						+ "#extractRsponse(...) "
						+ status
						+ ": indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.");
				List<Address> addresses = new ArrayList<Address>();
				JSONArray results = jsonResponse.getJSONArray("results");
				for (int i = 0; i < results.length() && i < maxResults; ++i) {
					// Gets address info from json
					JSONObject result = results.getJSONObject(i);
					String formattedAddress = result
							.getString("formatted_address");
					JSONObject geometry = result.getJSONObject("geometry");
					JSONObject location = geometry.getJSONObject("location");
					double latitude = location.getDouble("lat");
					double longitude = location.getDouble("lng");
					// Constructs address according to json info
					Address address = new Address(Locale.getDefault());
					address.setLatitude(latitude);
					address.setLongitude(longitude);
					address.setAddressLine(0, formattedAddress);
					addresses.add(address);
				}
				return addresses;
			} else if ("ZERO_RESULTS".equals(status)) {
				LOGGER.debug(CLASS_NAME
						+ "#extractRsponse(...) "
						+ status
						+ ": indicates that the geocode was successful but returned no results. This may occur if the geocode was passed a non-existent address or a latlng in a remote location.");
				return new ArrayList<Address>();
			} else if ("OVER_QUERY_LIMIT".equals(status)) {
				LOGGER.debug(CLASS_NAME + "#extractRsponse(...) " + status
						+ ": indicates that you are over your quota.");
			} else if ("REQUEST_DENIED".equals(status)) {
				LOGGER.debug(CLASS_NAME
						+ "#extractRsponse(...) "
						+ status
						+ ": indicates that your request was denied, generally because of lack of a sensor parameter.");
			} else if ("INVALID_REQUEST".equals(status)) {
				LOGGER.debug(CLASS_NAME
						+ "#extractRsponse(...) "
						+ status
						+ ": generally indicates that the query (address or latlng) is missing.");
			} else if ("UNKNOWN_ERROR".equals(status)) {
				LOGGER.debug(CLASS_NAME
						+ "#extractRsponse(...) "
						+ status
						+ ": indicates that the request could not be processed due to a server error. The request may succeed if you try again.");
			}
			String errorMessage = jsonResponse.getString("error_message");
			if (errorMessage != null) {
				LOGGER.debug(CLASS_NAME + "#extractResponse(...) Error: "
						+ errorMessage);
			}
		} catch (JSONException e) {
			LOGGER.error("Error: ", e);
		}

		return null;
	}

}

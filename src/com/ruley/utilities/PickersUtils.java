/**
 * 
 */
package com.ruley.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;

import com.ruley.activities.pickers.AppPickerActivity;
import com.ruley.activities.pickers.TakePictureActivity;
import com.ruley.activities.pickers.TakeVideoActivity;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PickersUtils {
	
	private static final String CLASS_NAME = PickersUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PickersUtils.class.getName());
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_hhmmss");
	
	private PickersUtils() {
	}
	
	public static void launchContactPicker(Activity activity, int requestCode) {
		LOGGER.debug(CLASS_NAME + "#launchContactPicker(...)");
		
		Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
				ContactsContract.Contacts.CONTENT_URI);
		try {
			activity.startActivityForResult(contactPickerIntent, requestCode);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	public static void launchAppPicker(Activity activity, int requestCode) {
		LOGGER.debug(CLASS_NAME + "#launchAppPicker(...)");
		
		Intent appPickerIntent = new Intent(activity, AppPickerActivity.class);
		try {
			activity.startActivityForResult(appPickerIntent, requestCode);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	public static void launchImagePicker(Activity activity, int requestCode) {
		LOGGER.debug(CLASS_NAME + "#launchImagePicker(...)");
		
		Intent imgIntent = new Intent(Intent.ACTION_PICK);
		imgIntent.setType("image/*");
		try {
			activity.startActivityForResult(Intent.createChooser(imgIntent, "Complete action using"), requestCode);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	public static void launchCameraPicker(Context context, String action, String directoryPath) {
		LOGGER.debug(CLASS_NAME + "#launchCameraPicker(...)");
		
		String fileNamePrefix = null;
		String fileType = null;
		if (MediaStore.ACTION_IMAGE_CAPTURE.equals(action) || MediaStore.ACTION_IMAGE_CAPTURE_SECURE.equals(action)) {
			fileNamePrefix = "pic_";
			fileType = ".jpg";
		} else if (MediaStore.ACTION_VIDEO_CAPTURE.equals(action)) {
			fileNamePrefix = "vid_";
			fileType = ".mp4";
		} else {
			LOGGER.debug(CLASS_NAME + "#launchCameraPicker(...) illegal action argument: " + action);
			return;
		}
		
		// Constructs the picture file path
		StringBuilder sb = new StringBuilder();
		sb.append(directoryPath);
		sb.append(fileNamePrefix);
		sb.append(DATE_FORMAT.format(new Date()));
		sb.append(fileType);
		String filePath = sb.toString();
		
        try {
        	File file = FilesUtils.getOrCreateFile(filePath);
            Uri fileUri = Uri.fromFile(file);
			Intent cameraIntent = new Intent(action);
			cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
			cameraIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(cameraIntent);
        } catch (IOException e) {
        	LOGGER.error("Error: ", e);
        }
	}
	
	public static void launchTakePicPicker(Context context, int cameraFacing, String directoryPath) {
		LOGGER.debug(CLASS_NAME + "#launchTakePicPicker(...)");
		
		Intent takePicPickerIntent = new Intent(context, TakePictureActivity.class);
		takePicPickerIntent.putExtra(TakePictureActivity.EXTRA_CAMERA_FACING, cameraFacing);
		takePicPickerIntent.putExtra(TakePictureActivity.EXTRA_DIRECTORY_PATH, directoryPath);
		takePicPickerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			context.startActivity(takePicPickerIntent);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	public static void launchTakeVidPicker(Context context, int cameraFacing, String directoryPath) {
		LOGGER.debug(CLASS_NAME + "#launchTakeVidPicker(...)");
		
		Intent takeVidPickerIntent = new Intent(context, TakeVideoActivity.class);
		takeVidPickerIntent.putExtra(TakePictureActivity.EXTRA_CAMERA_FACING, cameraFacing);
		takeVidPickerIntent.putExtra(TakePictureActivity.EXTRA_DIRECTORY_PATH, directoryPath);
		takeVidPickerIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			context.startActivity(takeVidPickerIntent);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	public static void launchEmailPicker(Context context, String to, String subject, String text) {
		LOGGER.debug(CLASS_NAME + "#launchEmailPicker(...)");
		
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, 
				Uri.fromParts("mailto", to, null));
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, text);
		emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(emailIntent)) {
			context.startActivity(emailIntent);
		}
	}
	
	public static void launchEmailChooser(Context context,String chooserTitle, String to, String subject, String text, Uri uri) {
		LOGGER.debug(CLASS_NAME + "#launchEmailChooser(...)");
		
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("message/rfc822");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, text);
		emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
		emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(emailIntent)) {
			context.startActivity(Intent.createChooser(emailIntent, chooserTitle));
		}
	}
	
	public static void launchFolderPicker(Context context) {
		LOGGER.debug(CLASS_NAME + "#launchFolderPicker(...)");
		
		Intent intent = new Intent();
		/* intent.setType("text/directory/*"); */
		intent.setAction(Intent.ACTION_PICK);
		
		if (IntentUtils.isIntentSafe(intent)) {
			context.startActivity(Intent.createChooser(intent, "Select Folder"));
		}
	}
	
	public static void launchWebSearchPicker(Context context, String searchQuery) {
		LOGGER.debug(CLASS_NAME + "#launchWebSearchPicker(...)");
		
		Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
		intent.putExtra(SearchManager.QUERY, searchQuery);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(intent)) {
			context.startActivity(intent);
		}
	}
	
	public static void launchBrowserPicker(Context context, String url) {
		LOGGER.debug(CLASS_NAME + "#launchBrowserPicker(...)");
		
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(intent)) {
			context.startActivity(intent);
		}
	}
	
	public static void launchDialerPicker(Context context, String phoneNumber) {
		LOGGER.debug(CLASS_NAME + "#launchDialerPicker(...)");
		
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel:" + phoneNumber));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(intent)) {
			context.startActivity(intent);
		}
	}
	
	public static void launchMapsNavigatePicker(Context context, String address, String navigationMode) {
		LOGGER.debug(CLASS_NAME + "#launchMapsNavigatePicker(...)");
		
		/* "http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345" */
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
				Uri.parse("google.navigation:q=" + address + "&mode=" + navigationMode));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		if (IntentUtils.isIntentSafe(intent)) {
			context.startActivity(intent);
		}
	}
	
	public static void launchWazeNavigatePicker(Context context, String address) {
		LOGGER.debug(CLASS_NAME + "#launchWazeNavigatePicker(...)");
		
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("waze://?q=" + address));
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (ActivityNotFoundException e) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
			context.startActivity(intent);
		}
	}

}

/**
 * 
 */
package com.ruley.utilities;

import java.util.List;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedMultigraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.ruley.model.beans.actions.ModifyRingerModeAction;
import com.ruley.model.beans.actions.PhoneCallAction;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.OutgoingCallTrigger;
import com.ruley.model.beans.triggers.PhoneStateTrigger;
import com.ruley.model.beans.triggers.RingerModeStateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RulesDependencyGraphUtils {
	
	private static final String CLASS_NAME = RulesDependencyGraphUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RulesDependencyGraphUtils.class.getName());
	
	private RulesDependencyGraphUtils() {
	}

	public static void resolveStronglyConnectedSets(Context context, Rule ruleToAdd) {
		LOGGER.debug(CLASS_NAME + "#resolveStronglyConnectedSets(...)");
		
//		DirectedGraph<Rule, DefaultEdge> rulesDependencyGraph = (DirectedGraph<Rule, DefaultEdge>) createRulesGraph(
//				context, ruleToAdd);
		
		// TODO: create an activity for the user to resolve the 
		// circular dependency between rules.
		// rulesDependencyGraph;
	}
	
	public static List<Set<Rule>> getStronglyConnectedSets(Context context, Rule ruleToAdd) {
		LOGGER.debug(CLASS_NAME + "#getStronglyConnectedSets(...)");
		
		DirectedGraph<Rule, DefaultEdge> rulesDependencyGraph = (DirectedGraph<Rule, DefaultEdge>) createRulesGraph(
				context, ruleToAdd);
		
		StrongConnectivityInspector<Rule, DefaultEdge> strongConnectivityInspector =
				new StrongConnectivityInspector<Rule, DefaultEdge>(rulesDependencyGraph);
		
		return strongConnectivityInspector.stronglyConnectedSets();
	}
	
	public static boolean hasStronglyConnectedSets(Context context, Rule ruleToAdd) {
		LOGGER.debug(CLASS_NAME + "#hasStronglyConnectedSets(...)");
		
		List<Set<Rule>> stronglyConnectedSets = getStronglyConnectedSets(context, ruleToAdd);
		
		// If there are strongly connected components other then the
		// active rules themselves and the newly activate rule (+1) returns true,
		// otherwise returns false.
		for (Set<Rule> ruleSet : stronglyConnectedSets) {
			if (ruleSet.size() > 1) {
				LOGGER.warn("Circular dependency in rules dependency graph");
				return true;
			}
		}
		
		return false;
	}
	
	private static DirectedGraph<Rule, DefaultEdge> createRulesGraph(Context context, Rule ruleToAdd) {
		LOGGER.debug(CLASS_NAME + "#createRulesDependecyGraph(...)");
		
		long startMillis = System.currentTimeMillis();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> activeRules = ruleDAO.list(true);
		
		activeRules.add(ruleToAdd);
		
		// Adds vertexes the rules dependency graph
		DirectedGraph<Rule, DefaultEdge> rulesDependencyGraph = new DirectedMultigraph<Rule, DefaultEdge>(DefaultEdge.class);
		for (Rule rule : activeRules) {
			rulesDependencyGraph.addVertex(rule);
		}
		
		/**
		 * TODO: Refactor and generalize this code, cos its shitty...
		 */
		// Adds edges to the rules dependency graph
		for (Rule lhsRule : activeRules) {
			for (Action action : lhsRule.getActions()) {
				// Checks ModifyRingerModeAction dependencies
				if (action instanceof ModifyRingerModeAction) {
					for (Rule rhsRule : activeRules) {
						if (lhsRule == rhsRule) continue;
						for (Trigger trigger : rhsRule.getTriggers()) {
							// Checks ModifyRingerModeAction >>> RingerModeStateTrigger
							if (trigger instanceof RingerModeStateTrigger) {
								RingerModeStateTrigger ringerModeStateTrigger = (RingerModeStateTrigger) trigger;
								int triggerWantedRingerMode = ringerModeStateTrigger.getWantedRingerMode();
								ModifyRingerModeAction ringerModeAction = (ModifyRingerModeAction) action;
								int actionWantedRingerMode = ringerModeAction.getWantedRingerMode();
								if (actionWantedRingerMode == triggerWantedRingerMode) {
									rulesDependencyGraph.addEdge(lhsRule, rhsRule);
								}
							}
						}
					}
				// Checks PhoneCallAction dependencies
				} else if (action instanceof PhoneCallAction) {
					for (Rule rhsRule : activeRules) {
						if (lhsRule == rhsRule) continue;
						for (Trigger trigger : rhsRule.getTriggers()) {
							// Checks PhoneCallAction >>> PhoneStateTrigger
							if (trigger instanceof PhoneStateTrigger) {
								PhoneStateTrigger phoneStateTrigger = (PhoneStateTrigger) trigger;
								int triggerWantedPhoneState = phoneStateTrigger.getWantedPhoneState();
								if (triggerWantedPhoneState == TelephonyManager.CALL_STATE_OFFHOOK) {
									rulesDependencyGraph.addEdge(lhsRule, rhsRule);
								}
							// Checks PhoneCallAction >>> OutgoingCallTrigger
							} else if (trigger instanceof OutgoingCallTrigger) {
								OutgoingCallTrigger phoneStateTrigger = (OutgoingCallTrigger) trigger;
								Set<String> contactsIds = phoneStateTrigger.getContacts().getContactsIds();
								PhoneCallAction phoneCallAction = (PhoneCallAction) action;
								String contactId = phoneCallAction.getContact().getContactId();
								if (contactsIds.contains(contactId)) {
									rulesDependencyGraph.addEdge(lhsRule, rhsRule);
								}
							}
						}
					}
				}
			}
		}
		
		long endMillis = System.currentTimeMillis();
		
		// Logs the rule dependency graph
		LOGGER.info("Rule dependency graph: " + (endMillis - startMillis) + " millis");
//		LOGGER.info(rulesDependencyGraph.toString());
		
		return rulesDependencyGraph;
	}

}

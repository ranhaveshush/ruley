/**
 * 
 */
package com.ruley.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.ruley.R;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;

/**
 * @author Ran
 *
 */
public class PrefsUtils {
	
	private static final String CLASS_NAME = PrefsUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PrefsUtils.class.getName());
	
	private PrefsUtils() {
	}
	
	public static int getBuildMode() {
		LOGGER.debug(CLASS_NAME + "#getBuildMode(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		int buildMode = prefs.getInt(appCtx.getString(R.string.pref_key_app_build_mode), AppState.BUILD_RELEASE);
		
		switch (buildMode) {
		case AppState.BUILD_RELEASE:
			LOGGER.info("App BUILD in BUILD_RELEASE mode");
			break;
		case AppState.BUILD_DEBUG:
			LOGGER.info("App BUILD in BUILD_DEBUG mode");
			break;
		default:
			throw new IllegalArgumentException(CLASS_NAME + " invalid app BUILD mode not BUILD_RELEASE/BUILD_DEBUG");
		}
		
		return buildMode;
	}
	
	public static void setBuildMode(int buildMode) {
		LOGGER.debug(CLASS_NAME + "#setBuildMode(...)");
		
		// Validates build mode
		switch (buildMode) {
		case AppState.BUILD_DEBUG:
		case AppState.BUILD_RELEASE:
			Context appCtx = MyApplication.getContext();
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
			Editor editor = prefs.edit();
			editor.putInt(appCtx.getString(R.string.pref_key_app_build_mode), buildMode);
			editor.commit();
			break;
		}
	}
	
	/**
	 * Gets the app root storage directory path.
	 * 
	 * @return String represents the app root directory path
	 */
	public static String getAppStorageDirPath() {
		LOGGER.debug(CLASS_NAME + "#getAppStorageDirPath(...)");
		
		Context appCtx = MyApplication.getContext();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		String appStorageDir = prefs.getString(appCtx.getString(R.string.pref_key_app_storage_directory_path), "");
		
		LOGGER.info(CLASS_NAME + "#getAppStorageDir(...) dir path: " + appStorageDir);
		
		return appStorageDir;
	}
	
	/**
	 * Sets the app root storage directory path.
	 * 
	 */
	public static void setAppStorageDirPath(String dirPath) {
		LOGGER.debug(CLASS_NAME + "#setAppStorageDirPath(...)");
		
		Context appCtx = MyApplication.getContext();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		Editor editor = prefs.edit();
		editor.putString(appCtx.getString(R.string.pref_key_app_storage_directory_path), dirPath);
		editor.commit();
	}
	
	/**
	 * Checks if the app launch for the first time.
	 * 
	 * @return true if app first run, otherwise false
	 */
	public static boolean isFirstRun() {
		LOGGER.debug(CLASS_NAME + "#isFirstRun(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		String prefKeyIsAppFirstRun = appCtx.getString(R.string.pref_key_is_app_first_run);
		boolean isFirstRun = prefs.getBoolean(prefKeyIsAppFirstRun, true);
		if (isFirstRun) {
			Editor editor = prefs.edit();
			editor.putBoolean(prefKeyIsAppFirstRun, false);
			editor.commit();
		}
		
		return isFirstRun;
	}
	
	/**
	 * Checks if the user saw the on boarding slides.
	 * 
	 * @return true if user saw the on boarding slides, otherwise false
	 */
	public static boolean isUserOnboard() {
		LOGGER.debug(CLASS_NAME + "#isUserOnboard(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		String prefKeyIsUserOnboarding = appCtx.getString(R.string.pref_key_is_user_onboard);
		boolean isUserOnboard = prefs.getBoolean(prefKeyIsUserOnboarding, false);
		if (!isUserOnboard) {
			Editor editor = prefs.edit();
			editor.putBoolean(prefKeyIsUserOnboarding, true);
			editor.commit();
		}
		
		return isUserOnboard;
	}
	
	/**
	 * Returns the user account type.
	 * 
	 * @return the user account type
	 */
	public static String getUserAccountType() {
		LOGGER.debug(CLASS_NAME + "#getUserAccountType(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		return prefs.getString(appCtx.getString(R.string.pref_key_is_user_account_type), null);
	}
	
	/**
	 * Sets the user account type.
	 * 
	 * @param accountType the user account type
	 */
	public static void setUserAccountType(String accountType) {
		LOGGER.debug(CLASS_NAME + "#setUserAccountType(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		Editor editor = prefs.edit();
		editor.putString(appCtx.getString(R.string.pref_key_is_user_account_type), accountType);
		editor.commit();
	}
	
	/**
	 * Checks if the app mode (enable / disable),
	 * If enable starts the background sysEventHandleService.
	 * 
	 * @return true if app is enabled, otherwise false
	 */
	public static boolean isAppEnabled() {
		LOGGER.debug(CLASS_NAME + "#isAppEnabled(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		boolean isAppEnabled = prefs.getBoolean(appCtx.getString(R.string.pref_key_app_mode), true);
		
		if (isAppEnabled) {
			LOGGER.info("App is ENABLED");
		} else {
			LOGGER.info("App is DISABLED");
		}
		
		return isAppEnabled;
	}
	
	/**
	 * Sets the app mode preference,
	 * according to the sys event handle background service running state.
	 */
	public static void setAppModePref(boolean isEnabled) {
		LOGGER.debug(CLASS_NAME + "#setAppModePref(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		Editor editor = prefs.edit();
		editor.putBoolean(appCtx.getString(R.string.pref_key_app_mode), isEnabled);
		editor.commit();
	}
	
	/**
	 * Checks if the app sticky notification (enable / disable),
	 * If sticky notification enabled, notification will be displayed always,
	 * otherwise sticky notification won't be displayed.  
	 * 
	 * @return true if app sticky notification is enabled, otherwise false
	 */
	public static boolean isStickyNotificationEnabled() {
		LOGGER.debug(CLASS_NAME + "#isStickyNotificationAllowed(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		boolean isStickyNotificationEnabled = prefs
				.getBoolean(appCtx.getString(R.string.pref_key_app_sticky_notification), true);
		
		if (isStickyNotificationEnabled) {
			LOGGER.info("App sticky notification is ENABLED");
		} else {
			LOGGER.info("App sticky notification is DISABLED");
		}
		
		return isStickyNotificationEnabled;
	}
	
	/**
	 * Checks if the app vocally announce (enable / disable),
	 * If vocally announce enabled, occurring events will be announced out loud,
	 * otherwise occurring events won't be announced out loud.
	 * 
	 * @return true if vocally announce is enabled, otherwise false
	 */
	public static boolean isVocallyAnnounce() {
		LOGGER.debug(CLASS_NAME + "#isVocallyAnnounce(...)");
		
		Context appCtx = MyApplication.getContext();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		boolean isVocallyAnnouce = prefs.getBoolean(appCtx.getString(R.string.pref_key_vocally_announce), false);
		
		if (isVocallyAnnouce) {
			LOGGER.info("App vocally announce is ENABLED");
		} else {
			LOGGER.info("App vocally announce is DISABLED");
		}
		
		return isVocallyAnnouce;
	}
	
	public static void checkAppVersionUpgrade() {
		LOGGER.debug(CLASS_NAME + "#checkAppVersionUpgrade(...)");
		
		Context appCtx = MyApplication.getContext();
		
		int currAppVersionCode = PackageInfoUtils.getAppVersionCode(appCtx);
		if (currAppVersionCode == PackageInfoUtils.INVALID_VERSION_CODE) {
			return;
		}
		
		String prefKeyAppVersionCode = appCtx.getString(R.string.pref_key_app_version_code);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		int prevAppVersionCode = prefs.getInt(prefKeyAppVersionCode, PackageInfoUtils.INVALID_VERSION_CODE);
		if (currAppVersionCode > prevAppVersionCode) {
			boolean isHandled = MyApplication.handleAppVersionUpgrade(prevAppVersionCode, currAppVersionCode);
			if (isHandled) {
				Editor editor = prefs.edit();
				editor.putInt(prefKeyAppVersionCode, currAppVersionCode);
				editor.commit();
			}
		}
	}
	
	public static String getKeyMaster() {
		Context appCtx = MyApplication.getContext();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		return prefs.getString(appCtx.getString(R.string.pref_key_master), null);
	}
	
	public static void setKeyMaster(String sksStr) {
		Context appCtx = MyApplication.getContext();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appCtx);
		Editor editor = prefs.edit();
		editor.putString(appCtx.getString(R.string.pref_key_master), sksStr);
		editor.commit();
	}
	
}

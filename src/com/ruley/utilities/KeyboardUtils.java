/**
 * 
 */
package com.ruley.utilities;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class KeyboardUtils {
	
	private KeyboardUtils() {
	}
	
	/**
	 * @param context
	 * @param view View represents the view requesting for input
	 * @param showFlags Integer represents the InputMethodManager flags
	 * 
	 * @see android.view.inputmethod.InputMethodManager#showSoftInput(View, int)
	 */
	public static void show(Context context, View view, int showFlags) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null) {
			imm.showSoftInput(view, showFlags);
		}
	}
	
	/**
	 * @param context
	 * @param view View represents the view requesting for input
	 * @param hideFlags Integer represents the InputMethodManager flags
	 * 
	 * @see android.view.inputmethod.InputMethodManager#hideSoftInputFromWindow(android.os.IBinder, int)
	 */
	public static void hide(Context context, View view, int hideFlags) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null) {
			imm.hideSoftInputFromWindow(view.getWindowToken(), hideFlags);
		}
	}

}

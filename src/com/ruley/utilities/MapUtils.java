/**
 * 
 */
package com.ruley.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ruley.R;
import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class MapUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MapUtils.class
			.getName());
	
	public static final int DEAULT_ANIMATION_MILLIS = 3000;
	public static final int DEFAULT_ZOOM = 16;
	public static final int DEFAULT_RADIUS_METERS = 200;
	public static final float DEFAULT_STROKE_WIDTH = 5.0F;
	
	private MapUtils() {
	}
	
	public static void setMapSettings(GoogleMap map) {
		UiSettings uiSettings = map.getUiSettings();
		uiSettings.setCompassEnabled(false);
		uiSettings.setMyLocationButtonEnabled(false);
		uiSettings.setZoomControlsEnabled(false);
	}
	
	public static GoogleMap getMap(Activity activity, GoogleMap map) {
		if (map == null) {
			MapFragment mapFragment = (MapFragment) activity.getFragmentManager().findFragmentById(R.id.map);
			map = mapFragment.getMap();
			setMapSettings(map);
			if (map == null) {
				LOGGER.error("Map isn't available");
			}
		}
		
		return map;
	}
	
	public static void animateToLocation(final GoogleMap map, LatLng latLng) {
		map.clear();
		CameraUpdate myLocationUpdate = CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM);
		map.animateCamera(myLocationUpdate, DEAULT_ANIMATION_MILLIS, null);
	}
	
	public static Marker addMarker(GoogleMap map, LatLng myLatLng) {
		return map.addMarker(new MarkerOptions()
			.position(myLatLng));
	}
	
	public static Circle addCircle(GoogleMap map, LatLng myLatLng, int radiusInMetes) {
		Context appCtx = MyApplication.getContext();
		return map.addCircle(new CircleOptions()
			.center(myLatLng)
			.radius(radiusInMetes)
			.fillColor(appCtx.getResources().getColor(R.color.radius_circle_fill_color))
			.strokeColor(appCtx.getResources().getColor(R.color.radius_circle_stroke_color))
			.strokeWidth(DEFAULT_STROKE_WIDTH));
	}

}

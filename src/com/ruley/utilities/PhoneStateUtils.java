/**
 * 
 */
package com.ruley.utilities;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PhoneStateUtils {
	
	private static final String CLASS_NAME = PhoneStateUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PhoneStateUtils.class.getName());
	
	private PhoneStateUtils() {
	}
	
	private static PhoneStateListener phoneStateListener = new PhoneStateListener() {
		
		@Override
		public void onCallForwardingIndicatorChanged(boolean cfi) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onCallForwardingIndicatorChanged(...)", Toast.LENGTH_LONG).show();			
			
			if (cfi) {
				Toast.makeText(appCtx, "CALL_FORWARDING_INDICATOR_TRUE", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(appCtx, "CALL_FORWARDING_INDICATOR_FALSE", Toast.LENGTH_LONG).show();
			}
		}
		
		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onCallStateChanged(...)", Toast.LENGTH_LONG).show();
			
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				Toast.makeText(appCtx, "CALL_STATE_IDLE", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.CALL_STATE_RINGING:
				Toast.makeText(appCtx, "CALL_STATE_RINGING", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				Toast.makeText(appCtx, "CALL_STATE_OFFHOOK", Toast.LENGTH_LONG).show();
				break;
			}
		}
		
		@Override
		public void onCellInfoChanged(List<CellInfo> cellInfos) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onCellInfoChanged(...)", Toast.LENGTH_LONG).show();
		}
		
		@Override
		public void onCellLocationChanged(CellLocation location) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onCellLocationChanged(...)", Toast.LENGTH_LONG).show();
			
			Toast.makeText(appCtx, "location.toString() = " + location.toString(), Toast.LENGTH_LONG).show();
		}
		
		@Override
		public void onDataActivity(int direction) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onDataActivity(...)", Toast.LENGTH_LONG).show();
			
			switch (direction) {
			case TelephonyManager.DATA_ACTIVITY_NONE:
				Toast.makeText(appCtx, "DATA_ACTIVITY_NONE", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_ACTIVITY_IN:
				Toast.makeText(appCtx, "DATA_ACTIVITY_IN", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_ACTIVITY_OUT:
				Toast.makeText(appCtx, "DATA_ACTIVITY_OUT", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_ACTIVITY_INOUT:
				Toast.makeText(appCtx, "DATA_ACTIVITY_INOUT", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_ACTIVITY_DORMANT:
				Toast.makeText(appCtx, "DATA_ACTIVITY_DORMANT", Toast.LENGTH_LONG).show();
				break;
			}
		}
		
		@Override
		public void onDataConnectionStateChanged(int state) {
			Context appCtx = MyApplication.getContext();			
			Toast.makeText(appCtx, CLASS_NAME + "#onDataConnectionStateChanged(...)", Toast.LENGTH_LONG).show();
			
			switch (state) {
			case TelephonyManager.DATA_DISCONNECTED:
				Toast.makeText(appCtx, "DATA_DISCONNECTED", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_CONNECTING:
				Toast.makeText(appCtx, "DATA_CONNECTING", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_CONNECTED:
				Toast.makeText(appCtx, "DATA_CONNECTED", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_SUSPENDED:
				Toast.makeText(appCtx, "DATA_SUSPENDED", Toast.LENGTH_LONG).show();
				break;
			}
		}
		
		@Override
		public void onDataConnectionStateChanged(int state, int networkType) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onDataConnectionStateChanged(...)", Toast.LENGTH_LONG).show();
			
			switch (state) {
			case TelephonyManager.DATA_DISCONNECTED:
				Toast.makeText(appCtx, "DATA_DISCONNECTED", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_CONNECTING:
				Toast.makeText(appCtx, "DATA_CONNECTING", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_CONNECTED:
				Toast.makeText(appCtx, "DATA_CONNECTED", Toast.LENGTH_LONG).show();
				break;
			case TelephonyManager.DATA_SUSPENDED:
				Toast.makeText(appCtx, "DATA_SUSPENDED", Toast.LENGTH_LONG).show();
				break;
			}
			
			Toast.makeText(appCtx, "Network type = " + networkType, Toast.LENGTH_LONG).show();
		}
		
		@Override
		public void onMessageWaitingIndicatorChanged(boolean mwi) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onMessageWaitingIndicatorChanged(...)", Toast.LENGTH_LONG).show();
			
			if (mwi) {
				Toast.makeText(appCtx, CLASS_NAME + "#onMessageWaitingIndicatorChanged(...) mwi = true danielle", Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(appCtx, CLASS_NAME + "#onMessageWaitingIndicatorChanged(...) mwi = fasle", Toast.LENGTH_LONG).show();
			}
		}
		
		@Override
		public void onServiceStateChanged(ServiceState serviceState) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onServiceStateChanged(...)", Toast.LENGTH_LONG).show();
			
			switch (serviceState.getState()) {
			case ServiceState.STATE_EMERGENCY_ONLY:
				Toast.makeText(appCtx, "STATE_EMERGENCY_ONLY", Toast.LENGTH_LONG).show();
				break;
			case ServiceState.STATE_IN_SERVICE:
				Toast.makeText(appCtx, "STATE_IN_SERVICE", Toast.LENGTH_LONG).show();
				break;
			case ServiceState.STATE_OUT_OF_SERVICE:
				Toast.makeText(appCtx, "STATE_OUT_OF_SERVICE", Toast.LENGTH_LONG).show();
				break;
			case ServiceState.STATE_POWER_OFF:
				Toast.makeText(appCtx, "STATE_POWER_OFF", Toast.LENGTH_LONG).show();
				break;
			}
		}
		
		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			Context appCtx = MyApplication.getContext();
			Toast.makeText(appCtx, CLASS_NAME + "#onSignalStrengthsChanged(...)", Toast.LENGTH_LONG).show();
			
			switch (signalStrength.getGsmSignalStrength()) {
			case ServiceState.STATE_EMERGENCY_ONLY:
				Toast.makeText(appCtx, "STATE_EMERGENCY_ONLY", Toast.LENGTH_LONG).show();
				break;
			case ServiceState.STATE_IN_SERVICE:
				Toast.makeText(appCtx, "STATE_IN_SERVICE", Toast.LENGTH_LONG).show();
				break;
			case ServiceState.STATE_OUT_OF_SERVICE:
				Toast.makeText(appCtx, "STATE_OUT_OF_SERVICE", Toast.LENGTH_LONG).show();
				break;
			case ServiceState.STATE_POWER_OFF:
				Toast.makeText(appCtx, "STATE_POWER_OFF", Toast.LENGTH_LONG).show();
				break;
			}
		}
	};
	
	/**
	 * <P>
	 * Registers the telephony manager to listen to given events.
	 * </P>
	 * <P>
	 * listenToFlags:<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_CALL_FORWARDING_INDICATOR - <BR/>
	 * Listen for changes to the call-forwarding indicator.<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_CALL_STATE - <BR/>
	 * Listen for changes to the device call state.<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_CELL_INFO - <BR/>
	 * Listen for changes to observed cell info.<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_CELL_LOCATION - <BR/>
	 * Listen for changes to the device's cell location.<BR/>
	 * Note that this will result in frequent callbacks to the listener.<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_DATA_ACTIVITY - <BR/>
	 * Listen for changes to the direction of data traffic on the data connection (cellular).<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_DATA_CONNECTION_STATE - <BR/>
	 * Listen for changes to the data connection state (cellular).<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_MESSAGE_WAITING_INDICATOR - <BR/>
	 * Listen for changes to the message-waiting indicator.<BR/>
	 * <P>
	 * <B>PhoneStateListener.LISTEN_NONE - <BR/>
	 * Stop listening for updates.<BR/></B>
	 * <P>
	 * PhoneStateListener.LISTEN_SERVICE_STATE - <BR/>
	 * Listen for changes to the network sysEventHandleService state (cellular).<BR/>
	 * <P>
	 * PhoneStateListener.LISTEN_SIGNAL_STRENGTHS - <BR/>
	 * Listen for changes to the network signal strengths (cellular).<BR/>
	 * </P>
	 * 
	 * @param listenToFlag	Integer represents the events to listen to
	 * 
	 * @see android.telephony.PhoneStateListener
	 */
	public static void listen(int listenToFlag) {
		LOGGER.debug(CLASS_NAME + "#listen(...)");
		
		Context appCtx = MyApplication.getContext();
		
		TelephonyManager telephonyManager = (TelephonyManager) appCtx.getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(phoneStateListener, listenToFlag);
	}

}

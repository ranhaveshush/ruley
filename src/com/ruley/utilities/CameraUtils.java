/**
 * 
 */
package com.ruley.utilities;

import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class CameraUtils {
	
	private CameraUtils() {
	}

	/**
	 * Gets the front or back facing camera. 
	 * 
	 * @param cameraFacing
	 * @return
	 */
	public static int getCameraId(int cameraFacing) {
		CameraInfo cameraInfo = new CameraInfo();

		for (int i = 0; i < Camera.getNumberOfCameras(); ++i) {
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == cameraFacing) {
				return i;
			}
		}

		return -1;
	}

}

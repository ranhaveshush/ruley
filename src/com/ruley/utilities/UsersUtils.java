/**
 * 
 */
package com.ruley.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;

import com.ruley.model.beans.users.AdminUser;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IUserDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class UsersUtils {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(UsersUtils.class.getName());
	
	private UsersUtils() {
	}
	
	public static void initAdminUser(Context context) {
		LOGGER.debug("UsersUtils#initAdminUser(...)");
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		User adminUser = userDAO.get(AdminUser.class);
		// If there is no admin user, create one and store it in the database
		if (adminUser == null) {
			userDAO.store(new AdminUser());
		}
	}
	
	public static void initDefaultUser(Context context) {
		LOGGER.debug("UsersUtils#initDefaultUser(...)");
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		User defaultUser = userDAO.get(DefaultUser.class);
		// If there is no default user, create one and store it in the database
		if (defaultUser == null) {
			userDAO.store(new DefaultUser());
		}
	}

}

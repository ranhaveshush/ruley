/**
 * 
 */
package com.ruley.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.res.AssetManager;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class XMLUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(XMLUtils.class
			.getName());
	
	private XMLUtils() {
	}
	
	public static Document getDocument(String filePath) {
		Context appCtx = MyApplication.getContext();
		InputStream inputStream = null;
		
		try {
			AssetManager assetManager = appCtx.getAssets();
			inputStream = assetManager.open(filePath);
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			return documentBuilder.parse(inputStream);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} catch (ParserConfigurationException e) {
			LOGGER.error("Error: ", e);
		} catch (SAXException e) {
			LOGGER.error("Error: ", e);
		} finally {
			if (inputStream != null) { try {inputStream.close();} catch(Exception e) {}}
		}
		
		return null;
	}
	
	public static NodeList getNodes(Document document, String xPathExpressionStr, QName returnType) {
		try {
			XPathFactory xPathFactory = XPathFactory.newInstance();
			XPath xPath = xPathFactory.newXPath();
			XPathExpression xPathExpression = xPath.compile(xPathExpressionStr);
			return (NodeList) xPathExpression.evaluate(document, returnType);
		} catch (XPathExpressionException e) {
			LOGGER.error("Error: ", e);
		}
		
		return null;
	}
	
	public static NodeList getNodes(String filePath, String xPathExpressionStr, QName returnType) {
		Document document = getDocument(filePath);
		
		try {
			XPathFactory xPathFactory = XPathFactory.newInstance();
			XPath xPath = xPathFactory.newXPath();
			XPathExpression xPathExpression = xPath.compile(xPathExpressionStr);
			return (NodeList) xPathExpression.evaluate(document, returnType);
		} catch (XPathExpressionException e) {
			LOGGER.error("Error: ", e);
		}
		
		return null;
	}
	
	public static List<Node> getNodes(String tagName, Node node) {
		List<Node> nodes = new ArrayList<Node>();
		NodeList childNodes = node.getChildNodes();
		for (int i=0; i < childNodes.getLength(); ++i) {
			Node data = childNodes.item(i);
			if (data.getNodeName().equalsIgnoreCase(tagName)) {
				nodes.add(data);
			}
		}
		
		return nodes;
	}
	
	public static Node getNode(String tagName, NodeList nodes) {
	    for (int x = 0; x < nodes.getLength(); x++) {
	        Node node = nodes.item(x);
	        if (node.getNodeName().equalsIgnoreCase(tagName)) {
	            return node;
	        }
	    }
	 
	    return null;
	}
	 
	public static String getNodeValue(Node node) {
		NodeList childNodes = node.getChildNodes();
		for (int x = 0; x < childNodes.getLength(); x++) {
			Node data = childNodes.item(x);
			if (data.getNodeType() == Node.TEXT_NODE)
				return data.getNodeValue();
		}
		return "";
	}

	public static String getNodeValue(String tagName, NodeList nodes) {
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName)) {
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					Node data = childNodes.item(y);
					if (data.getNodeType() == Node.TEXT_NODE)
						return data.getNodeValue();
				}
			}
		}
		return "";
	}

	public static String getNodeAttr(String attrName, Node node) {
		NamedNodeMap attrs = node.getAttributes();
		for (int y = 0; y < attrs.getLength(); y++) {
			Node attr = attrs.item(y);
			if (attr.getNodeName().equalsIgnoreCase(attrName)) {
				return attr.getNodeValue();
			}
		}
		return "";
	}

	public static String getNodeAttr(String tagName, String attrName, NodeList nodes) {
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node.getNodeName().equalsIgnoreCase(tagName)) {
				NamedNodeMap attributes = node.getAttributes();
				if (attributes != null) {					
					Node attrNode = attributes.getNamedItem(attrName);
					if (attrNode != null) {
						return attrNode.getNodeValue();
					}
				}
			}
		}

		return "";
	}

}

/**
 * 
 */
package com.ruley.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.net.Uri;
import ch.qos.logback.core.util.FileUtil;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class FilesUtils {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(FilesUtils.class.getName());

	private FilesUtils() {
	}
	
	public static boolean isFilePathValid(String filePath) {
		try {
			File file = new File(filePath);
			file.getCanonicalPath();
			return true;
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
			throw new RuntimeException("Invalid file path: " + filePath);
		}
	}
	
	public static File getFile(String filePath) {
		
		assert(filePath != null);
		
		File file = new File(filePath);

		if (!file.exists()) {
			LOGGER.warn("File doesn't exists: " + filePath);
			return null;
		} else {
			return file;
		}
		
	}

	public static File getOrCreateFile(String filePath) throws IOException {

		assert(filePath != null);

		File file = new File(filePath);
		
		if (FileUtil.isParentDirectoryCreationRequired(file)) {
			FileUtil.createMissingParentDirectories(file);
		}
		
		file.createNewFile();

		return file;
	}
	
	public static String readFile(Uri uri) {
		
		assert(uri != null);
		
		Context appCtx = MyApplication.getContext();
		
		String data = null;
		InputStream in = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		
		try {
			in = appCtx.getContentResolver().openInputStream(uri);
			isr = new InputStreamReader(in);
			br = new BufferedReader(isr);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
			data = sb.toString();
		} catch (FileNotFoundException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
				}
			}
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}
		
		return data;
	}
	
	public static String readFile(String filePath) {
		
		File file = getFile(filePath);
		
		if (file != null && file.exists()) {
			return readFile(file);
		} else {
			return null;
		}
	}

	public static String readFile(File file) {
		
		assert(file != null);
		
		StringBuilder sb = null;
		FileReader fileReader =  null;
		BufferedReader bufferedReader = null;
		
		try {
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			sb = new StringBuilder();
			String line = null;
			
			while ((line = bufferedReader.readLine()) != null) {
				sb.append(line);
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} finally {
			if (fileReader != null) {
				try {
					fileReader.close();
				} catch (Exception e) {
				}
			}
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (Exception e) {
				}
			}
		}
		
		return (sb == null) ? null : sb.toString();
	}
	
	public static void writeFile(String filePath, String data) {
		try {
			File file = getOrCreateFile(filePath);
			if (file.exists()) {
				writeFile(file, data);	
			}
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}
	}

	public static void writeFile(File file, String data) {
		
		assert(file != null && data != null);
		
		PrintWriter printWriter = null;
		
		try {
			printWriter = new PrintWriter(file.getAbsoluteFile());
			printWriter.write(data);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		} finally {
			if (printWriter != null) {
				try {
					printWriter.close();
				} catch (Exception e) {
				}
			}
		}
	}

}

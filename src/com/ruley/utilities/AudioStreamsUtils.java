/**
 * 
 */
package com.ruley.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;

import com.ruley.R;
import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AudioStreamsUtils {
	
	private AudioStreamsUtils() {
	}
	
	public static final Map<Integer, Integer> audioStreamsPosToTypeMap = Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
		{
			put(0, AudioManager.STREAM_ALARM);
			put(1, AudioManager.STREAM_DTMF);
			put(2, AudioManager.STREAM_MUSIC);
			put(3, AudioManager.STREAM_NOTIFICATION);
			put(4, AudioManager.STREAM_RING);
			put(5, AudioManager.STREAM_SYSTEM);
			put(6, AudioManager.STREAM_VOICE_CALL);
		}
	});
	
	public static final Map<Integer, Integer> audioStreamsTypeToPosMap = Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
		{
			put(AudioManager.STREAM_ALARM, 0);
			put(AudioManager.STREAM_DTMF, 1);
			put(AudioManager.STREAM_MUSIC, 2);
			put(AudioManager.STREAM_NOTIFICATION, 3);
			put(AudioManager.STREAM_RING, 4);
			put(AudioManager.STREAM_SYSTEM, 5);
			put(AudioManager.STREAM_VOICE_CALL, 6);
		}
	});
	
	public static final Map<Integer, String> audioStreamsTypeToNameMap = Collections.unmodifiableMap(new HashMap<Integer, String>() {
		{
			Context appCtx = MyApplication.getContext();
			String[] audioStreams = appCtx.getResources().getStringArray(R.array.audio_streams);
			if (audioStreams != null && audioStreams.length > 0) {
				put(AudioManager.STREAM_ALARM, audioStreams[0]);
				put(AudioManager.STREAM_DTMF, audioStreams[1]);
				put(AudioManager.STREAM_MUSIC, audioStreams[2]);
				put(AudioManager.STREAM_NOTIFICATION, audioStreams[3]);
				put(AudioManager.STREAM_RING, audioStreams[4]);
				put(AudioManager.STREAM_SYSTEM, audioStreams[5]);
				put(AudioManager.STREAM_VOICE_CALL, audioStreams[6]);
			}
		}
	});
	
	public static final Bitmap getStreamIcon(int audioStream, int sizeInPX) {
		switch (audioStream) {
		case AudioManager.STREAM_ALARM:
			return ImageUtils.getBitmap(R.drawable.stream_alarm);
		case AudioManager.STREAM_DTMF:
			return ImageUtils.getBitmap(R.drawable.stream_dtmf);
		case AudioManager.STREAM_MUSIC:
			return ImageUtils.getBitmap(R.drawable.music1);
		case AudioManager.STREAM_NOTIFICATION:
			return ImageUtils.getBitmap(R.drawable.stream_notification);
		case AudioManager.STREAM_RING:
			return ImageUtils.getBitmap(R.drawable.stream_ring);
		case AudioManager.STREAM_SYSTEM:
			return ImageUtils.getBitmap(R.drawable.stream_system);
		case AudioManager.STREAM_VOICE_CALL:
			return ImageUtils.getBitmap(R.drawable.stream_voice_call);
		default:
			return null;
		}
	}

}

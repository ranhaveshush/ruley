/**
 * 
 */
package com.ruley.utilities;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class IntentUtils {
	
	private static final String CLASS_NAME = IntentUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IntentUtils.class.getName());
	
	private IntentUtils() {
	}
	
	/**
	 * Checks if a given intent is safe,
	 * Meaning there is any activity or activities that can handle if,
	 * Otherwise the app will crash.
	 * 
	 * @param intent Intent represents the intent to check
	 * @return true if the intent is safe, otherwise false
	 */
	public static boolean isIntentSafe(Intent intent) {
		LOGGER.debug(CLASS_NAME + "#isIntentSafe(...)");
		
		Context appCtx = MyApplication.getContext();
		PackageManager packageManager = appCtx.getPackageManager();
		List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
		return activities.size() > 0;
	}

}

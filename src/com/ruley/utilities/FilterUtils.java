/**
 * 
 */
package com.ruley.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.os.PatternMatcher;
import android.text.TextUtils;

import com.ruley.config.Config;
import com.ruley.model.beans.Category;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.triggers.base.Trigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class FilterUtils {
	
	private static final String CLASS_NAME = FilterUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(FilterUtils.class.getName());
	
	private FilterUtils() {
	}
	
	public static void filterTriggersByAPILevel(Context context, int apiLevel, String triggersApiFilterFilePath) {
		LOGGER.debug(CLASS_NAME + "#filterTriggersListByAPILevel(...)");
		
		assert(context != null && triggersApiFilterFilePath != null);
		
		long startMillis = System.currentTimeMillis();
		
		LOGGER.info("API LEVEL: " + apiLevel);
		
		if (!Config.Triggers.LIST.isEmpty()) {
			long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#filterTriggersByAPILevel(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			return;
		}

		String supportedBroadcastActions = getSupportedBroadcastActions(context, apiLevel);

		Document document = XMLUtils.getDocument(triggersApiFilterFilePath);
		
		// Gets the document's root XML node
		NodeList root = document.getChildNodes();
		
		Node triggersNode = XMLUtils.getNode("triggers", root);
		List<Node> categoryNodes = XMLUtils.getNodes("category", triggersNode);
		
		for (Node categoryNode : categoryNodes) {
			// Gets the category's name
			String categoryName = XMLUtils.getNodeAttr("name", categoryNode);
			Category category = new Category(categoryName);
			List<Node> triggerNodes = XMLUtils.getNodes("trigger", categoryNode);
			for (Node triggerNode : triggerNodes) {
				NodeList triggerChildNodes = triggerNode.getChildNodes();
				// Checks the uses sdk versions
				int minSdkVersion = Integer.valueOf( XMLUtils.getNodeAttr("uses-sdk", "minSdkVersion", triggerChildNodes) );
				int targetSdkVersion =  Integer.valueOf( XMLUtils.getNodeAttr("uses-sdk", "targetSdkVersion", triggerChildNodes) );
				if (apiLevel < minSdkVersion || targetSdkVersion < apiLevel) {
					continue;
				}
				// Gets the trigger's name
				String triggerName = XMLUtils.getNodeAttr("name", triggerNode);
				// Constructs a trigger based of given trigger's name
				String triggerClassName = Config.Triggers.PACKAGE + "." + triggerName;
				Trigger trigger = null;
				try {
					trigger = (Trigger) Class.forName(triggerClassName).newInstance();
				// If failed to construct the trigger, skip it
				} catch (Exception e) {
					LOGGER.error("Error: ", e);
					continue;
				}
				// Gets the trigger's intent filters
				List<Node> intentFilterNodes = XMLUtils.getNodes("intent-filter", triggerNode);
				Set<IntentFilter> intentFilters = new HashSet<IntentFilter>();
				for (Node intentFilterNode : intentFilterNodes) {
					IntentFilter intentFilter = new IntentFilter();
					// Gets the intent filter actions
					List<Node> actionNodes = XMLUtils.getNodes("action", intentFilterNode);
					for (Node actionNode : actionNodes) {
						String action = XMLUtils.getNodeAttr("name", actionNode);
						// If this action is supported by the device API level
						if (action.startsWith(Config.APP_PACKAGE)
								|| supportedBroadcastActions.contains(action)) {
							intentFilter.addAction(action);
						}
					}
					// Gets the intent filter categories
					List<Node> intentFilterCategoryNodes = XMLUtils.getNodes("category", intentFilterNode);
					for (Node intentFilterCategorNode : intentFilterCategoryNodes) { 
						intentFilter.addCategory( XMLUtils.getNodeAttr("name", intentFilterCategorNode) );
					}
					// Gets the intent filter data
					List<Node> dataNodes = XMLUtils.getNodes("data", intentFilterNode);
					for (Node dataNode : dataNodes) {
						String scheme = XMLUtils.getNodeAttr("scheme", dataNode);
						if (!TextUtils.isEmpty(scheme)) {
							intentFilter.addDataScheme(scheme);
						}
						String host = XMLUtils.getNodeAttr("host", dataNode);
						String port = XMLUtils.getNodeAttr("port", dataNode);
						if (!TextUtils.isEmpty(host) && !TextUtils.isEmpty(port)) {
							intentFilter.addDataAuthority(host, port);
						}
						String path = XMLUtils.getNodeAttr("path", dataNode);
						String pathPrefix = XMLUtils.getNodeAttr("pathPrefix", dataNode);
						String pathPattern = XMLUtils.getNodeAttr("pathPattern", dataNode);
						if (!TextUtils.isEmpty(path)) {
							intentFilter.addDataPath(path, PatternMatcher.PATTERN_LITERAL);
						} else if (!TextUtils.isEmpty(pathPrefix)) {
							intentFilter.addDataPath(pathPrefix, PatternMatcher.PATTERN_PREFIX);
						} else if (!TextUtils.isEmpty(pathPattern)) {
							intentFilter.addDataPath(pathPattern, PatternMatcher.PATTERN_SIMPLE_GLOB);
						}
					}
					
					intentFilters.add(intentFilter);
					Config.Triggers.INTENT_FILTERS_TO_TRIGGERS_HANDLERS.put(intentFilter, trigger.getHandler());
				}
				
				if (!intentFilters.isEmpty()) {
					Config.Triggers.TRIGGERS_NAMES_TO_INTENT_FILTERS.put(triggerName, intentFilters);
					Config.Triggers.TRIGGERS_NAMES_TO_CATEGORIES.put(triggerName, category);
					Config.Triggers.LIST.add(trigger);
				}
			}
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#filterTriggersByAPILevel(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}

	public static void filterTriggersMapByDeviceHardware(Context context) {
		LOGGER.debug(CLASS_NAME + "#filterTriggersMapByDeviceHardware(...)");
		
		assert(context != null);
		
		// TODO: implement
	}
	
	public static void filterActionsByAPILevel(Context context, int apiLevel, String actionsApiFilterFilePath) {
		LOGGER.debug(CLASS_NAME + "#filterActionsByAPILevel(...)");
		
		assert(context != null && actionsApiFilterFilePath != null);
		
		long startMillis = System.currentTimeMillis();
		
		LOGGER.info("API LEVEL: " + apiLevel);
		
		if (!Config.Actions.LIST.isEmpty()) {
			long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#filterActionsByAPILevel(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			return;
		}
		
		Document document = XMLUtils.getDocument(actionsApiFilterFilePath);
		
		// Gets the document's root XML node
		NodeList root = document.getChildNodes();
		
		Node actionsNode = XMLUtils.getNode("actions", root);
		List<Node> categoryNodes = XMLUtils.getNodes("category", actionsNode);
		
		for (Node categoryNode : categoryNodes) {
			// Gets the category's name
			String categoryName = XMLUtils.getNodeAttr("name", categoryNode);
			Category category = new Category(categoryName);
			List<Node> actionNodes = XMLUtils.getNodes("action", categoryNode);
			for (Node actionNode : actionNodes) {
				NodeList actionChildNodes = actionNode.getChildNodes();
				// Checks the uses sdk versions
				int minSdkVersion = Integer.valueOf( XMLUtils.getNodeAttr("uses-sdk", "minSdkVersion", actionChildNodes) );
				int targetSdkVersion =  Integer.valueOf( XMLUtils.getNodeAttr("uses-sdk", "targetSdkVersion", actionChildNodes) );
				if (apiLevel < minSdkVersion || targetSdkVersion < apiLevel) {
					continue;
				}
				// Gets the action's name
				String actionName = XMLUtils.getNodeAttr("name", actionNode);
				// Constructs an action based of given action's name
				String actionClassName = Config.Actions.PACKAGE + "." + actionName;
				try {
					Action action = (Action) Class.forName(actionClassName).newInstance();
					Config.Actions.ACTIONS_NAMES_TO_CATEGORIES.put(actionName, category);
					Config.Actions.LIST.add(action);
				// If failed to construct the action, skip it
				} catch (Exception e) {
					LOGGER.error("Error: ", e);
					continue;
				}
			}
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#filterActionsByAPILevel(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}
	
	/**
	 * Returns the supported broadcast actions by given Android API level.
	 * 
	 * @param apiLevel	Integer represents the Android API level
	 * @return	String represents the supported broadcast actions
	 */
	private static String getSupportedBroadcastActions(Context context, int apiLevel) {
		LOGGER.debug(CLASS_NAME + "#getSupportedBroadcastActions(...)");
		
		assert(context != null);
		
		StringBuilder sb = null;
		AssetManager assetManager = null;
		InputStream inputStream = null;
		BufferedReader bufferReader = null;
		
		try {
			// Gets all supported broadcast actions by given API level
			String broadcastActionsFilePath = String.format(Config.Paths.FILE_BROADCAST_ACTIONS_FILTER, apiLevel);
			assetManager = context.getAssets();
			inputStream = assetManager.open(broadcastActionsFilePath);
			bufferReader = new BufferedReader(new InputStreamReader(inputStream));
			sb = new StringBuilder();
			String line = null;
			while ((line = bufferReader.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		} finally {
			if (inputStream != null) { try {inputStream.close();} catch (Exception e) {}}
			if (bufferReader != null) { try {bufferReader.close();} catch (IOException e) {}};
		}
		
		return sb.toString();
	}

}

/**
 * 
 */
package com.ruley.utilities;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ImageUtils {
	
	private ImageUtils() {
	}
	
	public static Bitmap toBitmap(Drawable drawable) {
		BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
		return bitmapDrawable.getBitmap();
	}
	
	public static Drawable toDrawable(Bitmap bitmap) {
		return new BitmapDrawable(MyApplication.getContext().getResources(), bitmap);
	}
	
	public static Bitmap scaleBitmap(Bitmap bitmap, int sizeInPX) {
		return Bitmap.createScaledBitmap(bitmap, sizeInPX, sizeInPX, true);
	}
	
	public static Drawable scaleDrawable(Drawable drawable, int sizeInPX) {
		Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
		Bitmap scaleBitmap = scaleBitmap(bitmap, sizeInPX);
		return toDrawable(scaleBitmap);
	}
	
	public static Bitmap getBitmap(int resourceId) {
		Resources resources = MyApplication.getContext().getResources();
		return BitmapFactory.decodeResource(resources, resourceId, null);
	}
	
	public static Drawable getDrawable(int resourceId) {
		Bitmap bitmap = ImageUtils.getBitmap(resourceId);
		return toDrawable(bitmap);
	}
	
	public static Bitmap grayscale(Bitmap bitmap) {
		final int height = bitmap.getHeight();
        final int width = bitmap.getWidth();

        final Bitmap bitmapGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmapGrayscale);
        final Paint paint = new Paint();
        final ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);
        final ColorMatrixColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
        paint.setColorFilter(colorFilter);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        
        return bitmapGrayscale;
	}
	
	public static String getBitmapFileName(Uri bitmapUri) {
		String bitmapFileName = null;

		String scheme = bitmapUri.getScheme();
		// If file
		if ("file".equals(scheme)) {
			bitmapFileName = bitmapUri.getLastPathSegment();
		// If content
		} else if ("content".equals(scheme)) {
			Context appCtx = MyApplication.getContext();
			String[] projection = { MediaStore.Images.Media.TITLE };
			Cursor cursor = appCtx .getContentResolver().query(
					bitmapUri, projection, null, null, null);
			if (cursor != null && cursor.moveToFirst()) {
				bitmapFileName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.TITLE));
			}
		}

		return bitmapFileName;
	}

}

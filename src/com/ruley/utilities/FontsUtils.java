/**
 * 
 */
package com.ruley.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.util.LruCache;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.widget.TextView;

import com.ruley.app.MyApplication;
import com.ruley.config.Config;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class FontsUtils {

	private static final String CLASS_NAME = FontsUtils.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FontsUtils.class.getName());

	private static LruCache<String, Typeface> typefaceCache = new LruCache<String, Typeface>(12);

	private FontsUtils() {
	}
	
	public static Typeface getTypeFace(String typefaceName) {
		LOGGER.debug(CLASS_NAME + "#getTypeFace(...)");
		
		Typeface typeface = Typeface.DEFAULT;
		
		try {
			typeface = typefaceCache.get(typefaceName);
			if (typeface == null) {
				Context appCtx = MyApplication.getContext();
				String typefaceAssetPath = String.format(Config.Paths.DIR_ASSETS_FONTS + "%s", typefaceName);
				typeface = Typeface.createFromAsset(appCtx.getAssets(), typefaceAssetPath);
				typefaceCache.put(typefaceName, typeface);
			}
		} catch (Exception e) {
			LOGGER.warn("Warn: ", e);
		}
		
		return typeface;
	}

	public static void setTypeFace(TextView textView, String typefaceName) {
		LOGGER.debug(CLASS_NAME + "#setTypeFace(...)");

		Typeface typeface = getTypeFace(typefaceName);
		textView.setTypeface(typeface);
	}

	public static SpannableString getSpannableString(CharSequence text, String typefaceName) {
		LOGGER.debug(CLASS_NAME + "#getSpannableString(...)");

		SpannableString spannableString = new SpannableString(text);

		try {
			TypefaceSpan typefaceSpan = new TypefaceSpan(typefaceName);
			spannableString.setSpan(typefaceSpan, 0, text.length(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		} catch (Exception e) {
			LOGGER.warn("Warn: ", e);
		}

		return spannableString;
	}
	
	public static SpannableString getSpannableString(CharSequence text, int size, boolean dip) {
		LOGGER.debug(CLASS_NAME + "#getSpannableString(...)");
		
		SpannableString spannableString = new SpannableString(text);
		
		try {
			AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(size, dip);
			spannableString.setSpan(absoluteSizeSpan, 0, text.length(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
		
		return spannableString;
	}

}

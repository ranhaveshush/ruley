/**
 * 
 */
package com.ruley.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;

import com.ruley.R;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.triggers.ActivityRecognitionStateTrigger;
import com.ruley.model.beans.triggers.LocationProximityTrigger;
import com.ruley.model.beans.triggers.LocationStateTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.IUserDAO;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.ParseUtils.FindCallback;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RulesUtils {
	
	private static final String CLASS_NAME = RulesUtils.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RulesUtils.class.getName());
	
	/**
	 * Restore type enum, Inner class representing all the available
	 * restore operations.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public enum RestoreType {
		MERGE,
		DELETE_BEFORE_RESTORE
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface BackupCallback {
		
		public void done();
		
	}
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 * @param <T>
	 */
	public interface RestoreCallback<T> {
		
		public void done(List<T> items);
		
	}
	
	private RulesUtils() {
	}
	
	public static boolean userHaveRules() {
		LOGGER.debug(CLASS_NAME + "#userHaveRules(...)");
		
		String rulesFilePath = Config.Paths.getAppStorageDirPath() + Config.Paths.FILE_JSON_RULES;
		File rulesFile = FilesUtils.getFile(rulesFilePath);
		
		return rulesFile != null && rulesFile.exists();
	}
	
	public static void activateActiveRules() {
		LOGGER.debug(CLASS_NAME + "#activateActiveRules(...)");
		
		Context appCtx = MyApplication.getContext();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> activeRules = ruleDAO.list(true);

		if (activeRules != null && !activeRules.isEmpty()) {
			for (Rule rule : activeRules) {
				rule.setActive(appCtx, true, null);
			}
		}
	}
	
	public static void inactivateActiveRules() {
		LOGGER.debug(CLASS_NAME + "#inactivateActiveRules(...)");
		
		Context appCtx = MyApplication.getContext();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> activeRules = ruleDAO.list(true);

		if (activeRules != null && !activeRules.isEmpty()) {
			for (Rule rule : activeRules) {
				rule.setActive(appCtx, false, null);
			}
		}
	}
	
	public static void inactivateLocationServiceDependentRules(Context context) {
		// Inactivates all the rules that have "Location Services" depending triggers,
		// which are "location proximity triggers" and "activity recognition triggers"
		final IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> rulesUsingLocationProximityTriggers = ruleDAO.list(LocationProximityTrigger.class, true);
		List<Rule> rulesUsingLocationStateTriggers = ruleDAO.list(LocationStateTrigger.class, true);
		List<Rule> rulesUsingActivityRecognitionTriggers = ruleDAO.list(ActivityRecognitionStateTrigger.class, true);
		List<Rule> rules = new ArrayList<Rule>();
		rules.addAll(rulesUsingLocationProximityTriggers);
		rules.addAll(rulesUsingLocationStateTriggers);
		rules.addAll(rulesUsingActivityRecognitionTriggers);
		
		// Validates rules is not empty
		if (rules == null || rules.isEmpty()) {
			return;
		}
		
		for (Rule rule : rules) {
			final Rule ruleToStore = rule;
			rule.setActive(context, false, new Rule.Listener() {
				@Override
				public void onActivated() {
				}
				@Override
				public void onInactivated() {
					LOGGER.debug(CLASS_NAME + "#onInactivated(...)");
					ruleDAO.store(DefaultUser.class, ruleToStore);
					MyApplication.updateBackgroundServices();
				}
				@Override
				public void onError(int errorMessageResourceId) {
					LOGGER.debug(CLASS_NAME + "#onError(...)");
				}
			});
		}
	}
	
	public static void performActiveRules() {
		LOGGER.debug(CLASS_NAME + "#performActiveRules(...)");
		
		Context appCtx = MyApplication.getContext();
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> rules = ruleDAO.list(true, true);
		
		// Validates rules is not empty
		if (rules == null || rules.isEmpty()) {
			return;
		}

		for (Rule rule : rules) {
			rule.perform(appCtx);
		}
	}
	
	public static void backupRules(Context context, List<Rule> rulesToBackup, final BackupCallback backupCallback) {
		LOGGER.debug(CLASS_NAME + "#backupRules(...)");
		
		ParseUtils.addRules(context, rulesToBackup, UserParse.RELATION_SAVED_RULES, new DoneCallback() {
			@Override
			public void done() {
				if (backupCallback != null) {
					backupCallback.done();
				}
			}
		});
	}
	
	public static void backupAllRules(Context context, final BackupCallback backupCallback) {
		LOGGER.debug(CLASS_NAME + "#backupAllRules(...)");
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> rulesToBackup = ruleDAO.list();
		
		backupRules(context, rulesToBackup, backupCallback);
	}
	
	public static void restoreAllRules(final Context context,
			final RestoreType restoreType,
			final RestoreCallback<Rule> restoreCallback) {
		LOGGER.debug(CLASS_NAME + "#restoreAllRules(...)");
		
		ParseUtils.findRules(context, UserParse.RELATION_SAVED_RULES, new FindCallback<Rule>() {
			@Override
			public void done(List<Rule> backupRules) {
				List<Rule> restoreRules = restoreRules(context, restoreType, backupRules);
				if (restoreCallback != null) {
					restoreCallback.done(restoreRules);
				}
			}
		});
	}
	
	public static List<Rule> restoreRules(Context context, RestoreType restoreType, List<Rule> rulesToRestore) {
		LOGGER.debug(CLASS_NAME + "#restoreRules(...)");
		
		// Validates rules to restore
		if (rulesToRestore == null || rulesToRestore.isEmpty()) {
			LOGGER.warn("WARN: invalid rules to restore");
			return null;
		}
		
		long startMillis = System.currentTimeMillis();
		
		// Gets the default user rules
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		User defaultUser = userDAO.get(DefaultUser.class);
		List<Rule> defaultUserRules = defaultUser.getRules();
		
		switch (restoreType) {
		// Merges user's current rules with rules to restore
		case MERGE:
			// Activates and restores user's rules from backup
			for (Rule ruleToRestore : rulesToRestore) {
				defaultUserRules.remove(ruleToRestore);
				defaultUserRules.add(ruleToRestore);
				ruleToRestore.setActive(context, true, null);
			}
			break;
		// Deletes the previous default user's rules,
		// Then restores the rules from backup
		case DELETE_BEFORE_RESTORE:
			// Deletes the previous default user's rules
			while (!defaultUserRules.isEmpty()) {
				Rule ruleToDelete = defaultUserRules.remove(0);
				if (ruleToDelete.isActive()) {
					ruleToDelete.setActive(context, false, null);
				}
			}
			// Activates and restores rules from backup
			for (Rule ruleToRestore : rulesToRestore) {
				defaultUserRules.add(ruleToRestore);
				if (ruleToRestore.isActive()) {
					ruleToRestore.setActive(context, true, null);
				}
			}
			break;
		default:
			throw new IllegalArgumentException(CLASS_NAME + " invalid restoreType argument");
		}
		
		userDAO.store(defaultUser);
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "##restoreRules(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
		
		return defaultUserRules;
	}
	
	public static Intent getShareIntent(TemplateRule templateRule) {
		LOGGER.debug(CLASS_NAME + "#getShareIntent(...)");
		
		assert(templateRule != null);
		
		List<TemplateRule> rules = new ArrayList<TemplateRule>();
		rules.add(templateRule);
		
		return getShareIntent(rules);
	}
	
	public static Intent getShareIntent(List<TemplateRule> templateRules) {
		LOGGER.debug(CLASS_NAME + "#getShareIntent(...)");
		
		assert(templateRules != null);
		
		Context appCtx = MyApplication.getContext();
		
		String sharedRulesText = getSharedTemplateRulesText(templateRules, appCtx);
		
		SecretKeySpec sks = CryptoUtils.getKey();
		String encryptedDataJson = CryptoUtils.encryptWithAttachedKey(sks , templateRules.toString());
		String sharedRuleFilePath = Config.Paths.getAppStorageDirPath()
				+ Config.Paths.FILE_JSON_SHARED_RULES;
		FilesUtils.writeFile(sharedRuleFilePath, encryptedDataJson);
		Uri sharedRuleUri = Uri.parse("file:///" + sharedRuleFilePath);
		String ruleyUrl = appCtx.getString(R.string.url_ruley_landing_page);
		String subject = appCtx.getString(R.string.shared_rules_email_subject);
		String text = appCtx.getString(R.string.shared_rules_email_text);
		text = String.format(text, ruleyUrl) + sharedRulesText;
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("application/ruley");
		shareIntent.putExtra(Intent.EXTRA_EMAIL, "");
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		shareIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(text));
		shareIntent.putExtra(Intent.EXTRA_STREAM, sharedRuleUri);
		shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		return shareIntent;
	}
	
	private static String getSharedTemplateRulesText(List<TemplateRule> templateRules, Context appCtx) {
		String rulesListTitle = appCtx.getString(R.string.shared_rules_email_rules_list_title);
		String rulesListWhen = appCtx.getString(R.string.shared_rules_email_rules_list_when);
		String rulesListDo = appCtx.getString(R.string.shared_rules_email_rules_list_do);
		String rulesListLineIndicator = appCtx.getString(R.string.shared_rules_email_rules_list_line_indicator);
		
		StringBuilder sb = new StringBuilder(rulesListTitle);
		for (TemplateRule tempalteRule : templateRules) {
			sb.append(rulesListWhen);
			for (Trigger trigger : tempalteRule.getTriggers()) {
				sb.append(rulesListLineIndicator).append(trigger.getDescription(appCtx)).append("<br/>");
			}
			sb.append(rulesListDo);
			for (Action action : tempalteRule.getActions()) {
				sb.append(rulesListLineIndicator).append(action.getDescription(appCtx)).append("<br/>");
			}
		}
		String sharedRulesText = sb.toString();
		return sharedRulesText;
	}

}

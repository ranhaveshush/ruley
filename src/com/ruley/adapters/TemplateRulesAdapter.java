/**
 * 
 */
package com.ruley.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.triggers.base.Trigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TemplateRulesAdapter extends BaseArrayAdapter<TemplateRule> {
	
	private Context mContext = null;
	
	private LayoutInflater mInflater = null;
	
	private int mLayoutResourceId = 0;
	
	public TemplateRulesAdapter(Context context, int layoutResourceId,
			List<TemplateRule> templateRules) {
		super(context, layoutResourceId, templateRules);

		mContext = context;
		mInflater = LayoutInflater.from(context);
		mLayoutResourceId = layoutResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(mLayoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.tvRuleTitle = (TextView) convertView.findViewById(R.id.tvRuleTitle);
			holder.llTriggers = (LinearLayout) convertView.findViewById(R.id.llTriggers);
			holder.llActions = (LinearLayout) convertView.findViewById(R.id.llActions);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		TemplateRule templateRule = getItem(position);
		holder.position = position;
		holder.tvRuleTitle.setText(templateRule.getName());
		inflateRuleTriggersViews(mContext, holder, templateRule);
		inflateRuleActionsViews(mContext, holder, templateRule);

		return convertView;
	}
	
	private void inflateRuleTriggersViews(Context context, ViewHolder holder,
			TemplateRule templateRule) {
		holder.llTriggers.removeAllViews();
		for (Trigger trigger : templateRule.getTriggers()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(trigger.getIcon());
			tvDescription.setText(trigger.getDescription(context));
			holder.llTriggers.addView(ruleComponentView);
		}
	}

	private void inflateRuleActionsViews(Context context, ViewHolder holder,
			TemplateRule templateRule) {
		holder.llActions.removeAllViews();
		for (Action action : templateRule.getActions()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(action.getIcon());
			tvDescription.setText(action.getDescription(context));
			holder.llActions.addView(ruleComponentView);
		}
	}
	
	class ViewHolder {
		int position;
		TextView tvRuleTitle;
		RelativeLayout rlRuleActions;
		LinearLayout llTriggers;
		LinearLayout llActions;
	}

}

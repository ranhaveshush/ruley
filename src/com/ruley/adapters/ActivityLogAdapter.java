/**
 * 
 */
package com.ruley.adapters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.ActivityLog;
import com.ruley.services.SysEventHandlerIntentService;
import com.ruley.utilities.FontsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ActivityLogAdapter extends BaseAdapter implements StickyListHeadersAdapter, SectionIndexer {
	
	private static final String CLASS_NAME = ActivityLogAdapter.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SysEventHandlerIntentService.class.getName());
	
	private Context mContext = null;
	
	private LayoutInflater mInflater = null;
	
	private ActivityLog[] mActivityLog = null;
	
	private Integer[] mSectionIndices = null;
	
	private Calendar[] mSectionDates = null;

	private int mTodayDayOfYear;
	
	public ActivityLogAdapter(Context context, List<ActivityLog> activityLog) {
		LOGGER.debug(CLASS_NAME + "#ActivityLogAdapter(...)");
		
		mContext  = context;
		mInflater = LayoutInflater.from(context);
		ActivityLog[] activityLogArray = new ActivityLog[activityLog.size()];
		mActivityLog = activityLog.toArray(activityLogArray);
		mSectionIndices = getSectionIndices();
		mSectionDates = getSectionDates();
		Calendar today = Calendar.getInstance(ActivityLog.DEFAULT_LOCALE);
		mTodayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
	}
	
	private Calendar getDate(ActivityLog activityLog) {
		return activityLog.getTimestamp();
	}
	
	private Integer[] getSectionIndices() {
		List<Integer> sectionIndices = new ArrayList<Integer>();
		int sectionIndicesSize = sectionIndices.size();
		if (mActivityLog != null && mActivityLog.length > 0) {
			int firstLastDate = getDate(mActivityLog[0]).get(Calendar.DAY_OF_YEAR);
			sectionIndices.add(0);
			for (int i = 0; i < sectionIndicesSize; ++i) {
				int currDate = getDate(mActivityLog[i]).get(Calendar.DAY_OF_YEAR);
				if (firstLastDate != currDate) {
					firstLastDate = currDate;
					sectionIndices.add(i);
				}
			}
		}
		Integer[] intArray = new Integer[sectionIndicesSize];
		return sectionIndices.toArray(intArray);
	}

	private Calendar[] getSectionDates() {
		List<Calendar> sectionDates = new ArrayList<Calendar>();
		for (Integer sectionIndex : mSectionIndices) {
			ActivityLog activityLog = mActivityLog[sectionIndex];
			Calendar date = getDate(activityLog);
			sectionDates.add(date);
		}
		Calendar[] datesArray = new Calendar[sectionDates.size()];
		return sectionDates.toArray(datesArray);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		ActivityLog activityLog = mActivityLog[position];
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item_rule_activity_log, parent, false);
			holder = new ViewHolder();
			holder.tvTime = (TextView) convertView.findViewById(R.id.tvTime);
			holder.tvRuleName = (TextView) convertView.findViewById(R.id.tvRuleName);
			holder.tvLogMessage = (TextView) convertView.findViewById(R.id.tvLogMessage);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.tvTime.setText(activityLog.getTimestamp(ActivityLog.SDF_TIME));
		holder.tvRuleName.setText(activityLog.getRuleName());
		holder.tvLogMessage.setText(activityLog.toMessage());
		
		return convertView;
	}
	
	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder holder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_header_rule_activity_log, parent, false);
			holder = new HeaderViewHolder();
			holder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			holder.tvDay = (TextView) convertView.findViewById(R.id.tvDay);
			FontsUtils.setTypeFace(holder.tvDay, Config.Fonts.FONT_TITLE);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}
		
		
		CharSequence formatedDateText = mActivityLog[position].getTimestamp(ActivityLog.SDF_DATE);
		holder.tvDate.setText(formatedDateText );
		String formatedDayText = getFormatedDayText(position);
		holder.tvDay.setText(formatedDayText);
		
		return convertView;
	}

	@Override
	public int getCount() {
		return mActivityLog.length;
	}

	@Override
	public ActivityLog getItem(int position) {
		return mActivityLog[position];
	}

	@Override
	public long getItemId(int position) {
		return mActivityLog[position].hashCode();
	}
	
	@Override
	public long getHeaderId(int position) {
		int date = getDate(mActivityLog[position]).get(Calendar.DAY_OF_MONTH);
		return date;
	}

	@Override
	public Calendar[] getSections() {
		return mSectionDates;
	}

	@Override
	public int getPositionForSection(int sectionIndex) {
		int sectionIndicesSize = mSectionIndices.length;
        if (sectionIndex >= sectionIndicesSize) {
            sectionIndex = sectionIndicesSize - 1;
        } else if (sectionIndex < 0) {
            sectionIndex = 0;
        }
        return mSectionIndices[sectionIndex];
	}

	@Override
	public int getSectionForPosition(int position) {
		int sectionIndicesSize = mSectionIndices.length;
        for (int i = 0; i < sectionIndicesSize; ++i) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return sectionIndicesSize - 1;
	}
	
	private String getFormatedDayText(int position) {
		String formatedDayText = null;
		ActivityLog activityLog = mActivityLog[position];
		int timestampDayOfYear = activityLog.getTimestamp().get(Calendar.DAY_OF_YEAR);
		if (mTodayDayOfYear == timestampDayOfYear) {
			formatedDayText = mContext
					.getString(R.string.list_header_activity_log_today);
		} else {
			formatedDayText = activityLog.getTimestamp(ActivityLog.SDF_DAY);
		}
		return formatedDayText;
	}
	
	static class ViewHolder {
		TextView tvTime;
		TextView tvRuleName;
		TextView tvLogMessage;
	}
	
	static class HeaderViewHolder {
		TextView tvDate;
		TextView tvDay;
	}

}

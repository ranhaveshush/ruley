package com.ruley.adapters;

import java.util.Arrays;

import android.content.Context;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.parse.ParseQueryAdapter;
import com.ruley.R;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.parse.RuleParse;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.RulesUtils;
import com.ruley.utilities.RulesUtils.RestoreType;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class RulesParseQueryAdapter extends ParseQueryAdapter<RuleParse> {
	
	private Context mContext = null;

	public RulesParseQueryAdapter(Context context,
			ParseQueryAdapter.QueryFactory<RuleParse> queryFactory) {
		super(context, queryFactory);
		
		mContext = context;
	}

	@Override
	public View getItemView(RuleParse ruleParse, View convertView, ViewGroup parent) {
		Context context = getContext();

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.list_item_rule_parse_card, null);
			holder = new ViewHolder();
			holder.tvRuleTitle = (TextView) convertView.findViewById(R.id.tvRuleTitle);
			holder.rlRuleActions = (ViewGroup) convertView.findViewById(R.id.rlRuleActions);
			holder.rlRuleActions.setOnClickListener(mRuleActionsClickListener);
			holder.llTriggers = (LinearLayout) convertView.findViewById(R.id.llTriggers);
			holder.llActions = (LinearLayout) convertView.findViewById(R.id.llActions);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Rule rule = ruleParse.getRule();
		
		// Rule's meta data
		holder.tvRuleTitle.setText(rule.getName());
		holder.rlRuleActions.setTag(R.id.tag_rule_parse, ruleParse);
		// Rule's data
		inflateRuleTriggersViews(context, holder, rule);
		inflateRuleActionsViews(context, holder, rule);
		
		return convertView;
	}
	
	private void inflateRuleActionsViews(Context context, ViewHolder holder, Rule rule) {
		holder.llActions.removeAllViews();
		for (Action action : rule.getActions()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(action.getIcon());
			tvDescription.setText(action.getDescription(context));
			holder.llActions.addView(ruleComponentView);
		}
	}

	private void inflateRuleTriggersViews(Context context, ViewHolder holder, Rule rule) {
		holder.llTriggers.removeAllViews();
		for (Trigger trigger : rule.getTriggers()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(trigger.getIcon());
			tvDescription.setText(trigger.getDescription(context));
			holder.llTriggers.addView(ruleComponentView);
		}
	}

	static class ViewHolder {
		TextView tvRuleTitle;
		ViewGroup rlRuleActions;
		LinearLayout llTriggers;
		LinearLayout llActions;
	}
	
	private OnClickListener mRuleActionsClickListener = new OnClickListener() {
		@Override
		public void onClick(final View view) {
			// Creates the popup menu
			PopupMenu popupMenu = new PopupMenu(mContext, view);
			popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// Gets the context rule id
					RuleParse ruleParse = (RuleParse) view.getTag(R.id.tag_rule_parse);
					
					switch (item.getItemId()) {
					case R.id.action_restore_rule:
						RulesUtils.restoreRules(mContext, RestoreType.MERGE, Arrays.asList(ruleParse.getRule()));
						return true;
					case R.id.action_delete_rule:
						ParseUtils.removeRules(mContext,
								Arrays.asList(ruleParse),
								UserParse.RELATION_SAVED_RULES,
								new DoneCallback() {
									@Override
									public void done() {
										loadObjects();
									}
								});
						return true;
					default:
						return false;
					}
				}
			});
			MenuInflater menuInflater = popupMenu.getMenuInflater();
			menuInflater.inflate(R.menu.list_item_backup_rule_actions, popupMenu.getMenu());
			popupMenu.show();
		}
	};

}

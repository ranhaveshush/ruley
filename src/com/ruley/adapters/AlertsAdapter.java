/**
 * 
 */
package com.ruley.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.model.beans.alerts.Alert;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AlertsAdapter extends ArrayAdapter<Alert> {
	
	private LayoutInflater mInflater = null;
	
	private int mLayoutResourceId;
	
	private List<Alert> mAlerts = null;

	public AlertsAdapter(Context context, int layoutResourceId, List<Alert> alerts) {
		super(context, layoutResourceId, alerts);
		
		mInflater = LayoutInflater.from(context);
		mLayoutResourceId = layoutResourceId;
		mAlerts = alerts;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		Alert alert = mAlerts.get(position);
		
		if (convertView == null) {
			convertView = mInflater.inflate(mLayoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.tvAlertText = (TextView) convertView.findViewById(R.id.tvAlertText);
			holder.bAlertAction = (Button) convertView.findViewById(R.id.bAlertAction);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.tvAlertText.setText(alert.getText());
		holder.bAlertAction.setText(alert.getAction());
		holder.bAlertAction.setOnClickListener(alert.getClickListener());
		
		return convertView;
	}

	static class ViewHolder {
		TextView tvAlertText;
		Button bAlertAction;
	}

}

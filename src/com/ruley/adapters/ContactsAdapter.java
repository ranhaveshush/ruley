/**
 * 
 */
package com.ruley.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ContactsAdapter extends ArrayAdapter<String> {
	
	private Context mContext = null;
	
	private LayoutInflater mInflater = null;
	
	private int mLayoutResourceId;
	
	public ContactsAdapter(Context context, int layoutResourceId, List<String> contactsIds) {
		super(context, layoutResourceId, contactsIds);
		
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mLayoutResourceId = layoutResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		String contactId = getItem(position);
		
		if (convertView == null) {
			convertView = mInflater.inflate(mLayoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.tvContactName = (TextView) convertView.findViewById(R.id.tvContactName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		// Gets contact info
		String contactName = ContactsUtils.getContactName(mContext, contactId);
		Bitmap contactPhoto = ContactsUtils.getContactPhoto(mContext, contactId);
		Drawable contactDrawable = ImageUtils.toDrawable(contactPhoto);
		
		holder.tvContactName.setCompoundDrawables(contactDrawable, null, null, null);
		holder.tvContactName.setText(contactName);
		
		return convertView;
	}
	
	static class ViewHolder {
		TextView tvContactName;
	}

	@Override
	public void add(String contactId) {
		// If the contacts list not contains this contact id, add it
		if (getPosition(contactId) < 0) {
			super.add(contactId);
		}
	}
	
}

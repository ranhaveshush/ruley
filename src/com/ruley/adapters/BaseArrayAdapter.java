/**
 * 
 */
package com.ruley.adapters;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BaseArrayAdapter<T> extends ArrayAdapter<T> {
	
	private List<T> mObjects = null;

	public BaseArrayAdapter(Context context, int layoutResourceId, List<T> objects) {
		super(context, layoutResourceId, objects);
		
		mObjects = objects;
	}
	
	public List<T> getItems() {
		return mObjects;
	}
	
	public void add(int position, T object) {
		mObjects.add(position, object);
		notifyDataSetChanged();
	}
	
	public void removeAll(List<T> objects) {
		mObjects.removeAll(objects);
		notifyDataSetChanged();
	}
	
}

package com.ruley.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.ruley.R;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.parse.TemplateRuleParse;
import com.ruley.model.parse.UserParse;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class TemplateRulesParseQueryAdapter extends BaseParseQueryAdapter<TemplateRuleParse> {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TemplateRulesParseQueryAdapter.class.getName());
	
	private static final String DATE_TEMPLATE = "MMM dd";

	private static final Locale DEFAULT_LOCALE = Locale.US;

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			DATE_TEMPLATE, DEFAULT_LOCALE);
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		public void onTemplateRuleToggleLike(int position);
		
		public void onTemplateRuleDownload(int position);
		
		public void onTemplateRuleDelete(int position);
		
		public void onTemplateRuleShare(int position);
		
	}
	
	private Listener mListener = null;

	private String mCreatedByPrefix = null;

	public TemplateRulesParseQueryAdapter(Context context, String parseRelationName,
			ParseQueryAdapter.QueryFactory<TemplateRuleParse> queryFactory, Listener listener) {
		super(context, parseRelationName, queryFactory);
		
		mListener = listener;
		LOGGER.info("#TemplateRulesParseQueryAdapter(...) context = " + context);
		mCreatedByPrefix = context.getString(R.string.created_by_prefix);
	}

	@Override
	public View getItemView(TemplateRuleParse templateRuleParse, View convertView, ViewGroup parent) {
		Context context = getContext();
		int position = getItemPosition(templateRuleParse);

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.list_item_template_rule_parse_card, null);
			holder = new ViewHolder();
			holder.tvRuleTitle = (TextView) convertView.findViewById(R.id.tvRuleTitle);
			holder.tvCreatedBy = (TextView) convertView.findViewById(R.id.tvCreatedBy);
			holder.tvCreatedAt = (TextView) convertView.findViewById(R.id.tvCreatedAt);
			holder.llTriggers = (LinearLayout) convertView.findViewById(R.id.llTriggers);
			holder.llActions = (LinearLayout) convertView.findViewById(R.id.llActions);
			holder.position = position;
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// Rule's created by - author
		holder.position = position;
		ParseUser user = templateRuleParse.getCreatedBy();
		String createdBy = null;
		if (user != null) {
			createdBy = user.getString(UserParse.FIELD_NAME);
		} else {
			createdBy = getContext().getString(R.string.created_by_not_available);
		}
		holder.tvCreatedBy.setText(mCreatedByPrefix + createdBy);
		// Rule's created at - date
		Date createdAt = templateRuleParse.getCreatedAt();
		String createdAtText = DATE_FORMAT.format(createdAt);
		holder.tvCreatedAt.setText(createdAtText);
		// Rule's template data
		TemplateRule templateRule = templateRuleParse.getTemplateRule();
		holder.tvRuleTitle.setText(templateRule.getName());
		inflateRuleTriggersViews(context, holder, templateRule);
		inflateRuleActionsViews(context, holder, templateRule);
		inflateCardFooter(convertView, holder, templateRuleParse);
		
		return convertView;
	}
	
	private void inflateRuleTriggersViews(Context context, ViewHolder holder,
			TemplateRule templateRule) {
		holder.llTriggers.removeAllViews();
		for (Trigger trigger : templateRule.getTriggers()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(trigger.getIcon());
			tvDescription.setText(trigger.getDescription(context));
			holder.llTriggers.addView(ruleComponentView);
		}
	}

	private void inflateRuleActionsViews(Context context, ViewHolder holder,
			TemplateRule templateRule) {
		holder.llActions.removeAllViews();
		for (Action action : templateRule.getActions()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(action.getIcon());
			tvDescription.setText(action.getDescription(context));
			holder.llActions.addView(ruleComponentView);
		}
	}

	private void inflateCardFooter(View convertView, ViewHolder holder, TemplateRuleParse templateRuleParse) {
		// Shows the card footer
		holder.vgCardFooter = (ViewGroup) convertView.findViewById(R.id.vgCardFooter);
		holder.vgCardFooter.setTag(R.id.tag_list_item_position, holder.position);
		holder.vgCardFooter.setVisibility(View.VISIBLE);
		// Initialize like button
		holder.bContextualAction1 = (Button) convertView.findViewById(R.id.bContextualAction1);
		if (templateRuleParse.isLiked()) {
			holder.bContextualAction1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_liked, 0, 0, 0);
		} else {
			holder.bContextualAction1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_like, 0, 0, 0);
		}
		holder.bContextualAction1.setText(String.valueOf(templateRuleParse.getNumOfLikes()));
		holder.bContextualAction1.setOnClickListener(mOnToggleLikeClickListener);
		holder.bContextualAction1.setVisibility(View.VISIBLE);
		// Initialize download button
		holder.bContextualAction2 = (Button) convertView.findViewById(R.id.bContextualAction2);
		holder.bContextualAction2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_download, 0, 0, 0);
		holder.bContextualAction2.setText(String.valueOf(templateRuleParse.getNumOfDownloads()));
		holder.bContextualAction2.setOnClickListener(mOnDownloadClickListener);
		holder.bContextualAction2.setVisibility(View.VISIBLE);
		// Initialize share button
		holder.bContextualAction4 = (Button) convertView.findViewById(R.id.bContextualAction4);
		holder.bContextualAction4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_share, 0, 0, 0);
		holder.bContextualAction4.setText(R.string.contextual_action_button_share);
		holder.bContextualAction4.setOnClickListener(mOnShareClickListener);
		holder.bContextualAction4.setVisibility(View.VISIBLE);
		
		if (UserParse.RELATION_UPLOADED_TEMPLATE_RULES.equals(getParseRelationName())) {
			// Initialize delete button
			holder.bContextualAction2 = (Button) convertView.findViewById(R.id.bContextualAction2);
			holder.bContextualAction2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_delete, 0, 0, 0);
			holder.bContextualAction2.setText(R.string.contextual_action_button_delete);
			holder.bContextualAction2.setOnClickListener(mOnDeleteClickListener);
			holder.bContextualAction2.setVisibility(View.VISIBLE);
		}
	}

	static class ViewHolder {
		int position;
		TextView tvRuleTitle;
		TextView tvCreatedBy;
		TextView tvCreatedAt;
		LinearLayout llTriggers;
		LinearLayout llActions;
		ViewGroup vgCardFooter;
		Button bContextualAction1;
		Button bContextualAction2;
		Button bContextualAction3;
		Button bContextualAction4;
	}
	
	private OnClickListener mOnToggleLikeClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Button button = (Button) v;
			// Gets the number of likes from the button's text
			Integer numOfLikes = null;
			try {
				numOfLikes = Integer.valueOf(button.getText().toString());
			} catch (NumberFormatException e) {
				LOGGER.error("Error: ", e);
			}
			// Gets the template rule's position
			View parent = (View) v.getParent();
			int position = (Integer) parent.getTag(R.id.tag_list_item_position);
			// Updates the number of likes
			if (numOfLikes != null) {
				TemplateRuleParse templateRuleParse = getItem(position);
				if (templateRuleParse.isLiked()) {
					button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_like, 0, 0, 0);
					button.setText(String.valueOf(--numOfLikes));
				} else {
					button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_liked, 0, 0, 0);
					button.setText(String.valueOf(++numOfLikes));
				}
			}
			mListener.onTemplateRuleToggleLike(position);
		}
	};
	
	private OnClickListener mOnDownloadClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			View parent = (View) v.getParent();
			int position = (Integer) parent.getTag(R.id.tag_list_item_position);
			mListener.onTemplateRuleDownload(position);
		}
	};
	
	private OnClickListener mOnDeleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			View parent = (View) v.getParent();
			int position = (Integer) parent.getTag(R.id.tag_list_item_position);
			mListener.onTemplateRuleDelete(position);
		}
	};
	
	private OnClickListener mOnShareClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			View parent = (View) v.getParent();
			int position = (Integer) parent.getTag(R.id.tag_list_item_position);
			mListener.onTemplateRuleShare(position);
		}
	};

}

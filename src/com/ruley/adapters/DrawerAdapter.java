/**
 * 
 */
package com.ruley.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DrawerAdapter extends ArrayAdapter<String> {
	
	private Context mContext = null;
	
	private LayoutInflater mInflater = null;
	
	private int mLayoutResourceId;
	
	private String[] mNavigationEntries = null;

	public DrawerAdapter(Context context, int layoutResourceId, String[] navigationEntries) {
		super(context, layoutResourceId, navigationEntries);
		
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mLayoutResourceId = layoutResourceId;
		mNavigationEntries = navigationEntries;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		String navigationEntry = mNavigationEntries[position];
		
		if (convertView == null) {
			convertView = mInflater.inflate(mLayoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.ivEntryIcon = (ImageView) convertView.findViewById(R.id.ivEntryIcon);
			holder.tvEntryTitle = (TextView) convertView.findViewById(R.id.tvEntryTitle);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		int drawableId = getDrawableIdByTitle(navigationEntry);
		holder.ivEntryIcon.setImageResource(drawableId);
		holder.tvEntryTitle.setText(navigationEntry);
		
		return convertView;
	}
	
	private int getDrawableIdByTitle(String fragmentTitle) {
		int drawableResourceId = 0;
		
		String yourRulesFragmentTitle = mContext.getString(R.string.title_fragment_your_rules);
		String backupRulesFragmentTitle = mContext.getString(R.string.title_fragment_backup_rules);
		String activityLogFragmentTitle = mContext.getString(R.string.title_fragment_activity_log);
		
		if (fragmentTitle.equals(yourRulesFragmentTitle)) {
			drawableResourceId = R.drawable.ic_drawer_your_rules;
		} else if (fragmentTitle.equals(backupRulesFragmentTitle)) {
			drawableResourceId = R.drawable.ic_drawer_backup_rules;
		} else if (fragmentTitle.equals(activityLogFragmentTitle)) {
			drawableResourceId = R.drawable.ic_drawer_activity_log;
		}
		
		return drawableResourceId;
	}
	
	static class ViewHolder {
		ImageView ivEntryIcon;
		TextView tvEntryTitle;
	}

}

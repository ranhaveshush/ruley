/**
 * 
 */
package com.ruley.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.IRule;
import com.ruley.model.beans.rules.IRuleComponent;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.utilities.FontsUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RuleComponentsAdapter extends BaseAdapter {
	
	private static final String CLASS_NAME = RuleComponentsAdapter.class.getSimpleName();
	
	/**
	 * Listener to add trigger or action events.
	 * 
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		public void onAddTrigger();
		
		public void onAddAction();
		
	}
	
	private static final int NOT_FOUND = -1;
	
	private Listener listener = null;
	
	private Context context = null;
	
	private LayoutInflater inflater = null;
	
	private List<Object> items = new ArrayList<Object>();
	
	private List<Object> selectedItems = new ArrayList<Object>();

	private String triggersHeader = null;

	private String actionsHeader = null;
	
	private OnClickListener addTriggerClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			RuleComponentsAdapter.this.listener.onAddTrigger();
		}
	};
	
	private OnClickListener addActionClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			RuleComponentsAdapter.this.listener.onAddAction();
		}
	};
	
	public RuleComponentsAdapter(Listener listener, IRule rule) {
		this.listener = listener;
		try {
			this.context = (Context) listener;
		} catch (ClassCastException cce) {
			throw new IllegalArgumentException(RuleComponentsAdapter.class.getSimpleName()
					+ "Constructor's argument listener isn't valid, cos isn't instance of Context");
		}
		
		inflater = LayoutInflater.from(context);
		
		triggersHeader = new String(context.getString(R.string.triggers_title));
		actionsHeader = new String(context.getString(R.string.actions_title));
		
		List<Trigger> triggers = rule.getTriggers();
		items.add(triggersHeader);
		if (triggers != null && !triggers.isEmpty()) {
			for (Trigger trigger : triggers) {
				items.add(trigger);
			}
		}
		
		List<Action> actions = rule.getActions();
		items.add(actionsHeader);
		if (actions != null && !actions.isEmpty()) {
			for (Action action : actions) {
				items.add(action);
			}
		}
	}
	
	@Override
	public int getCount() {
		return this.items.size();
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public Object getItem(int position) {
		return this.items.get(position);
	}

	@Override
	public boolean isEnabled(int position) {
		Object item = items.get(position);
		if (item instanceof String) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Object item = items.get(position);
		boolean isItemSelected = (selectedItems != null) ? selectedItems.contains(item) : false;

		// If item
		if (item instanceof Trigger || item instanceof Action) {
			IRuleComponent ruleComponent = (IRuleComponent) item;
			convertView = inflater.inflate(R.layout.list_item_rule_component_card_dynamic, parent, false);
			View rlCard = (View) convertView.findViewById(R.id.vgCard);
			ImageView ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
			ivIcon.setImageBitmap(ruleComponent.getIcon());
			TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
			tvDescription.setText(ruleComponent.getDescription(context));
			
			if (isItemSelected) {
				rlCard.setBackgroundResource(R.drawable.rounded_card_selected);
			} else if (!ruleComponent.isValid(context)) {
				rlCard.setBackgroundResource(R.drawable.rounded_card_invalid);
			} else {
				rlCard.setBackgroundResource(R.drawable.rounded_card_selector);
			}
		// If header
		} else if (item instanceof String) {
			convertView = inflater.inflate(R.layout.list_header_rule_component_1, parent, false);
			TextView tvHeaderTitle = (TextView) convertView.findViewById(R.id.tvHeaderTitle);
			FontsUtils.setTypeFace(tvHeaderTitle, Config.Fonts.FONT_TITLE);
			TextView tvHeaderSubtitle = (TextView) convertView.findViewById(R.id.tvHeaderSubtitle);
			tvHeaderTitle.setText((String)item);
			Resources resources = context.getResources();
			if (triggersHeader.equals(item)) {
				tvHeaderTitle.setTextColor(resources.getColor(R.color.triggers_color));
				tvHeaderSubtitle.setText(resources.getString(R.string.triggers_subtitle));
				convertView.setOnClickListener(addTriggerClickListener);
			} else if (actionsHeader.equals(item)) {
				tvHeaderTitle.setTextColor(resources.getColor(R.color.actions_color));
				tvHeaderSubtitle.setText(resources.getString(R.string.actions_subtitle));
				convertView.setOnClickListener(addActionClickListener);
			} else {
				throw new IllegalArgumentException(CLASS_NAME + "#getView(...) invalid list item header.");
			}
		}
		
		return convertView;
	}
	
	public List<Object> getItems() {
		return this.items;
	}
	
	public void setItems(List<Object> items) {
		clear();
		addAll(items);
	}
	
	public void add(int position, Object item) {
		int triggersHeaderPos = items.indexOf(triggersHeader);
		int actionsHeaderPos = items.indexOf(actionsHeader);
		if (item instanceof Trigger) {
			if (triggersHeaderPos < position && position <= actionsHeaderPos) {
				items.add(position, item);
			}
		} else if (item instanceof Action) {
			if (actionsHeaderPos < position && position <= items.size()) {
				items.add(position, item);
			}
		} else {
			throw new IllegalArgumentException();
		}
		
		super.notifyDataSetChanged();
	}
	
	public void add(Object item) {
		if (item instanceof Trigger) {
			int triggersHeaderPos = items.indexOf(triggersHeader);
			if (triggersHeaderPos == NOT_FOUND) {
				items.add(0, triggersHeader);
				items.add(1, item);
			} else {
				items.add(triggersHeaderPos+1, item);
			}
		} else if (item instanceof Action) {
			int actionsHeaderPos = items.indexOf(actionsHeader);
			if (actionsHeaderPos == NOT_FOUND) {
				items.add(actionsHeader);
				items.add(item);
			} else {
				items.add(actionsHeaderPos+1, item);
			}
		} else {
			throw new IllegalArgumentException();
		}
		
		super.notifyDataSetChanged();
	}

	public void addAll(Collection<? extends Object> collection) {
		Iterator<? extends Object> iterator = collection.iterator();
		while (iterator.hasNext()) {
			Object item = iterator.next();
			add(item);
		}
	}
	
	public void remove(Object item) {
		if (this.items != null) {
			this.items.remove(item);
		}
		if (this.selectedItems != null) {
			this.selectedItems.remove(item);
		}
		notifyDataSetChanged();
	}
	
	public void removeAll(List<Object> items) {
		for (Object item : items) {
			if (this.items != null) {
				this.items.remove(item);
			}
			if (this.selectedItems != null) {
				this.selectedItems.remove(item);
			}
		}
		notifyDataSetChanged();
	}
	
	public void clear() {
		this.items = null;
		this.items = new ArrayList<Object>();
		this.selectedItems = null;
		this.selectedItems = new ArrayList<Object>();
		notifyDataSetChanged();
	}

	public boolean isSelectedItem(int position) {
		if (this.selectedItems == null || this.selectedItems.isEmpty()) {
			return false;
		}
		return this.selectedItems.contains(this.items.get(position));
	}

	public void setSelectedItem(int position, boolean toSelect) {
		if (this.selectedItems == null) {
			this.selectedItems = new ArrayList<Object>();
		}
		if (toSelect && !isSelectedItem(position)) {
			this.selectedItems.add(this.items.get(position));
		} else if (!toSelect && isSelectedItem(position)) {
			this.selectedItems.remove(this.items.get(position));
		}
	}

	public void toggleSelectedItem(int position) {
		if (isSelectedItem(position)) {
			setSelectedItem(position, false);
		} else {
			setSelectedItem(position, true);
		}
		notifyDataSetChanged();
	}

	public List<Object> getSelectedItems() {
		return this.selectedItems;
	}

	public void clearSelectedItems() {
		this.selectedItems = null;
		this.selectedItems = new ArrayList<Object>();
		notifyDataSetChanged();
	}

}

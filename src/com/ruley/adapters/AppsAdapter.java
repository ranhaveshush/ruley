/**
 * 
 */
package com.ruley.adapters;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class AppsAdapter extends BaseAdapter implements StickyListHeadersAdapter, SectionIndexer {
	
	private static final char INVALID_APP_LABEL_FIRST_LETTER = '#';

	private LayoutInflater inflater = null;
	
	private ApplicationInfo[] items = null;
	
	private Integer[] sectionIndices = null;
	
	private Character[] sectionLetters = null;
	
	private PackageManager packageManager = null;
	
	public AppsAdapter(Context context, List<ApplicationInfo> items) {
		this.packageManager = context.getPackageManager();
		this.inflater = LayoutInflater.from(context);
		ApplicationInfo[] appInfoArray = new ApplicationInfo[items.size()];
		this.items = items.toArray(appInfoArray);
		this.sectionIndices = getSectionIndices();
		this.sectionLetters = getSectionLetters();
	}
	
	private char getFirstLetter(ApplicationInfo appInfo) {
		CharSequence appLabel = appInfo.loadLabel(packageManager);

		if (appLabel != null && appLabel.length() > 1) {
			return appLabel.subSequence(0, 1).charAt(0);
		} else {
			return INVALID_APP_LABEL_FIRST_LETTER;
		}
	}
	
    private Integer[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = getFirstLetter(items[0]);
        sectionIndices.add(0);
        for (int i=1; i < items.length; ++i) {
        	char currentChar = getFirstLetter(items[i]);
            if (currentChar != lastFirstChar) {
                lastFirstChar = currentChar;
                sectionIndices.add(i);
            }
        }
        Integer[] intArray = new Integer[sectionIndices.size()];
        
        return sectionIndices.toArray(intArray);
    }
    
    private Character[] getSectionLetters() {
        List<Character> letters = new ArrayList<Character>();
        for (Integer sectionIndex : sectionIndices) {
        	char firstLetter = getFirstLetter( items[sectionIndex] );
        	letters.add(firstLetter);
        }
        Character[] charArray = new Character[letters.size()];
        
        return letters.toArray(charArray);
    }
	
	@Override
	public int getCount() {
		return items.length;
	}

	@Override
	public Object getItem(int position) {
		return items[position];
	}

	@Override
	public long getItemId(int position) {
		return items[position].uid;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		ApplicationInfo item = items[position];
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item_app, parent, false);
			holder = new ViewHolder();
			holder.ivAppIcon = (ImageView) convertView.findViewById(R.id.ivAppIcon);
			holder.tvAppLabel = (TextView) convertView.findViewById(R.id.tvAppLabel);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		String appLabel = item.loadLabel(packageManager).toString();
		Drawable appIcon = item.loadIcon(packageManager);
		holder.tvAppLabel.setText(appLabel);
		holder.ivAppIcon.setImageDrawable(appIcon);
		
		return convertView;
	}
	
	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_header_app, parent, false);
			holder = new HeaderViewHolder();
			holder.tvHeaderTitle = (TextView) convertView.findViewById(R.id.tvHeaderTitle);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}
		
		char firstLetter = getFirstLetter(items[position]);
		holder.tvHeaderTitle.setText(String.valueOf(firstLetter));
		
		return convertView;
	}
	
	@Override
	public long getHeaderId(int position) {
		return getFirstLetter( items[position] );
	}

    @Override
    public int getPositionForSection(int sectionIndex) {
    	int sectionIndicesSize = sectionIndices.length;
        if (sectionIndex >= sectionIndicesSize) {
            sectionIndex = sectionIndicesSize - 1;
        } else if (sectionIndex < 0) {
            sectionIndex = 0;
        }
        return sectionIndices[sectionIndex];
    }

    @Override
    public int getSectionForPosition(int position) {
    	int sectionIndicesSize = sectionIndices.length;
        for (int i = 0; i < sectionIndicesSize; ++i) {
            if (position < sectionIndices[i]) {
                return i - 1;
            }
        }
        return sectionIndicesSize - 1;
    }

    @Override
    public Object[] getSections() {
        return sectionLetters;
    }
	
	static class ViewHolder {
		ImageView ivAppIcon;
		TextView tvAppLabel;
	}
	
	static class HeaderViewHolder {
		TextView tvHeaderTitle;
	}

}

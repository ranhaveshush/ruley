/**
 * 
 */
package com.ruley.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class AutocompleteAdapter extends ArrayAdapter<String> {

	private static final NoFilter NO_FILTER = new NoFilter();

	public AutocompleteAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}
	
	@Override
	public Filter getFilter() {
		return NO_FILTER;
	}

	private static class NoFilter extends Filter {
		
		@Override
		protected FilterResults performFiltering(CharSequence prefix) {
			return new FilterResults();
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// Do nothing
		}
	}

}

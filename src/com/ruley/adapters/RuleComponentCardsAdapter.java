package com.ruley.adapters;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.Category;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.IRuleComponent;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.utilities.FontsUtils;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class RuleComponentCardsAdapter extends BaseAdapter implements StickyListHeadersAdapter, SectionIndexer {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RuleComponentCardsAdapter.class.getName());

	private LayoutInflater mInflater = null;
	
	private IRuleComponent[] mRuleComponents = null;

	private Integer[] mSectionIndices = null;
	
	private Category[] mSectionCategories = null;

	public RuleComponentCardsAdapter(Context context, List<? extends IRuleComponent> ruleComponents) {
		LOGGER.debug("RuleComponentCardsAdapter(...)");

		mInflater  = LayoutInflater.from(context);
		IRuleComponent[] ruleComponentsArray = new IRuleComponent[ruleComponents.size()];
		mRuleComponents = ruleComponents.toArray(ruleComponentsArray);
		mSectionIndices = getSectionIndices();
		mSectionCategories = getSectionCategories();
	}
	
	private Category getCategory(IRuleComponent ruleComponent) {
		if (ruleComponent instanceof Action) {
			return Config.Actions.ACTIONS_NAMES_TO_CATEGORIES.get(ruleComponent.getClass().getSimpleName());
		} else if (ruleComponent instanceof Trigger) {
			return Config.Triggers.TRIGGERS_NAMES_TO_CATEGORIES.get(ruleComponent.getClass().getSimpleName());
		} else {
			return null;
		}
	}
	
	private Integer[] getSectionIndices() {
		List<Integer> sectionIndices = new ArrayList<Integer>();
		int sectionIndicesSize = sectionIndices.size();
		if (mRuleComponents != null && mRuleComponents.length > 0) {
			Category firstLastCategory = getCategory(mRuleComponents[0]);
			sectionIndices.add(0);
			for (int i=0; i < sectionIndicesSize; ++i) {
				Category currCategory = getCategory(mRuleComponents[i]);
				if (!firstLastCategory.equals(currCategory)) {
					firstLastCategory = currCategory;
					sectionIndices.add(i);
				}
			}
		}
		Integer[] intArray = new Integer[sectionIndicesSize];
		return sectionIndices.toArray(intArray);
	}

	private Category[] getSectionCategories() {
		List<Category> sectionCategories = new ArrayList<Category>();
		for (Integer sectionIndex : mSectionIndices) {
			IRuleComponent ruleComponent = mRuleComponents[sectionIndex];
			Category category = getCategory(ruleComponent);
			sectionCategories.add(category);
		}
		Category[] categoryArray = new Category[sectionCategories.size()];
		return sectionCategories.toArray(categoryArray);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final IRuleComponent ruleComponent = mRuleComponents[position];

		if (convertView == null) {
			convertView  = mInflater.inflate(R.layout.list_item_rule_component_card_static, parent, false);
			holder = new ViewHolder();
			holder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
			holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
			holder.tvDesc = (TextView) convertView.findViewById(R.id.tvDescription);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.ivIcon.setImageBitmap(ruleComponent.getIcon());
		holder.tvTitle.setText(ruleComponent.getNameId());
		holder.tvDesc.setText(ruleComponent.getDescriptionId());

		return convertView;
	}
	
	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder holder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_header_rule_component_2, parent, false);
			holder = new HeaderViewHolder();
			holder.tvHeaderTitle = (TextView) convertView.findViewById(R.id.tvHeaderTitle);
			FontsUtils.setTypeFace(holder.tvHeaderTitle, Config.Fonts.FONT_TITLE);
			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}
		
		Category category = getCategory(mRuleComponents[position]);
		holder.tvHeaderTitle.setText(category.getName());
		
		return convertView;
	}
	
	@Override
	public int getCount() {
		return mRuleComponents.length;
	}

	@Override
	public Object getItem(int position) {
		return mRuleComponents[position];
	}

	@Override
	public long getItemId(int position) {
		return (int) mRuleComponents[position].getId().getLeastSignificantBits();
	}
	
	@Override
	public long getHeaderId(int position) {
		Category category = getCategory(mRuleComponents[position]);
		return category.hashCode();
	}
	
	@Override
	public Object[] getSections() {
		return mSectionCategories;
	}

	@Override
	public int getPositionForSection(int sectionIndex) {
		int sectionIndicesSize = mSectionIndices.length;
        if (sectionIndex >= sectionIndicesSize) {
            sectionIndex = sectionIndicesSize - 1;
        } else if (sectionIndex < 0) {
            sectionIndex = 0;
        }
        return mSectionIndices[sectionIndex];
	}

	@Override
	public int getSectionForPosition(int position) {
		int sectionIndicesSize = mSectionIndices.length;
        for (int i = 0; i < sectionIndicesSize; ++i) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return sectionIndicesSize - 1;
	}
	
	static class ViewHolder {
		ImageView ivIcon;
		TextView tvTitle;
		TextView tvDesc;
	}
	
	static class HeaderViewHolder {
		TextView tvHeaderTitle;
	}

}

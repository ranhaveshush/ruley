/**
 * 
 */
package com.ruley.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * Base multiple selections adapter.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class MultiSelectionBaseAdapter<T> extends BaseAdapter {

	private List<T> items = null;
	
	private List<T> selectedItems = new ArrayList<T>();

	public MultiSelectionBaseAdapter(List<T> items) {
		this.items = items;
	}
	
	@Override
	public int getCount() {
		return this.items.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public T getItem(int position) {
		return this.items.get(position);
	}
	
	/* (non-Javadoc)
	 * @see android.widget.BaseAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	public abstract View getView(final int position, View convertView, ViewGroup parent);
	
	public List<T> getItems() {
		return this.items;
	}
	
	public void setItems(List<T> items) {
		clear();
		addAll(items);
	}
	
	public void add(int position, T item) {
		this.items.add(position, item);
		notifyDataSetChanged();
	}
	
	public void add(T item) {
		add(0, item);
	}

	public void addAll(Collection<? extends T> collection) {
		int i = 0;
		Iterator<? extends T> iterator = collection.iterator();
		while (iterator.hasNext()) {
			this.items.add(i++, iterator.next());
		}
		notifyDataSetChanged();
	}
	
	public void remove(T item) {
		if (this.items != null) {
			this.items.remove(item);
		}
		if (this.selectedItems != null) {
			this.selectedItems.remove(item);
		}
		notifyDataSetChanged();
	}
	
	public void removeAll(List<T> items) {
		for (T item : items) {
			if (this.items != null) {
				this.items.remove(item);
			}
			if (this.selectedItems != null) {
				this.selectedItems.remove(item);
			}
		}
		notifyDataSetChanged();
	}
	
	public void clear() {
		this.items = null;
		this.items = new ArrayList<T>();
		this.selectedItems = null;
		this.selectedItems = new ArrayList<T>();
		notifyDataSetChanged();
	}

	public boolean isSelectedItem(int position) {
		if (this.selectedItems == null || this.selectedItems.isEmpty()) {
			return false;
		}
		return this.selectedItems.contains(this.items.get(position));
	}

	public void setSelectedItem(int position, boolean toSelect) {
		if (this.selectedItems == null) {
			this.selectedItems = new ArrayList<T>();
		}
		if (toSelect && !isSelectedItem(position)) {
			this.selectedItems.add(this.items.get(position));
		} else if (!toSelect && isSelectedItem(position)) {
			this.selectedItems.remove(this.items.get(position));
		}
	}

	public void toggleSelectedItem(int position) {
		if (isSelectedItem(position)) {
			setSelectedItem(position, false);
		} else {
			setSelectedItem(position, true);
		}
		notifyDataSetChanged();
	}

	public List<T> getSelectedItems() {
		return this.selectedItems;
	}

	public void clearSelectedItems() {
		this.selectedItems = null;
		this.selectedItems = new ArrayList<T>();
		notifyDataSetChanged();
	}

}

/**
 * 
 */
package com.ruley.adapters;

import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class LocalesAdapter extends ArrayAdapter<Locale> {
	
	private LayoutInflater inflater = null;
	
	private List<Locale> locales = null;
	
	private int layoutResourceId;

	public LocalesAdapter(Context context, int layoutResourceId, List<Locale> locales) {
		super(context, layoutResourceId, locales);
		
		this.inflater = LayoutInflater.from(context);
		this.layoutResourceId = layoutResourceId;
		this.locales = locales;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.tvLanguage = (TextView) convertView.findViewById(android.R.id.text1);
			holder.tvLanguage.setTextSize(18);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Locale locale = locales.get(position);
		holder.tvLanguage.setText(locale.getDisplayName());
		return convertView;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.tvLanguage = (TextView) convertView.findViewById(android.R.id.text1);
			holder.tvLanguage.setTextSize(18);
			holder.tvLanguage.setPadding(16, 16, 16, 16);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		Locale locale = locales.get(position);
		holder.tvLanguage.setText(locale.getDisplayName());
		
		return convertView;
	}

	private class ViewHolder {
		TextView tvLanguage;
	}
	
}

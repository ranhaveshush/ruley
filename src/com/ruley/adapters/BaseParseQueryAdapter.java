/**
 * 
 */
package com.ruley.adapters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.ruley.R;
import com.ruley.app.AppState;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * @param <T>
 *
 */
public abstract class BaseParseQueryAdapter<T extends ParseObject> extends ParseQueryAdapter<T> {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseParseQueryAdapter.class.getName());
	
	private Context mContext = null;
	
	private String mParseRelationName = null;
	
	private List<T> mObjects = new ArrayList<T>();
	
	public BaseParseQueryAdapter(Context context, String parseRelationName,
			ParseQueryAdapter.QueryFactory<T> queryFactory) {
		super(context, queryFactory);
		
		mContext = context;
		mParseRelationName = parseRelationName;
		
		addOnQueryLoadListener(new OnQueryLoadListener<T>() {
			@Override
			public void onLoaded(List<T> objects, Exception e) {
				if (e != null) {
					LOGGER.error("Error: ", e);
					ToastUtils.show(AppState.BUILD_RELEASE,
							mContext,
							R.string.toast_connection_failed, Toast.LENGTH_SHORT);
					return;
				// If previously rules found
				} else if (objects != null && !objects.isEmpty()) {
					mObjects.clear();
					mObjects.addAll(objects);
					notifyDataSetChanged();
				}
			}
			@Override
			public void onLoading() {
			}
		});
	}
	
	@Override
	public int getCount() {
		return mObjects.size();
	}

	@Override
	public T getItem(int position) {
		return mObjects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public Context getContext() {
		return mContext;
	}
	
	public String getParseRelationName() {
		return mParseRelationName;
	}
	
	public int getItemPosition(T item) {
		return mObjects.indexOf(item);
	}
	
	public void add(T object) {
		mObjects.add(object);
		notifyDataSetChanged();
	}
	
	public void add(int location, T object) {
		mObjects.add(location, object);
		notifyDataSetChanged();
	}
	
	public void addAll(Collection<T> objects) {
		mObjects.addAll(objects);
		notifyDataSetChanged();
	}
	
	public void remove(int location) {
		mObjects.remove(location);
		notifyDataSetChanged();
	}
	
	public void remove(T object) {
		mObjects.remove(object);
		notifyDataSetChanged();
	}
	
	public void removeAll(Collection<T> objects) {
		mObjects.removeAll(objects);
		notifyDataSetChanged();
	}
	
	public void removeInBackground(T object, DoneCallback doneCallback) {
		// Validates parse relation name
		if (TextUtils.isEmpty(mParseRelationName)) {
			return;
		}
		
		mObjects.remove(object);
		notifyDataSetChanged();
		ParseUtils.removeObjects(mContext, Arrays.asList(object), mParseRelationName, doneCallback);
	}
	
	public void removeAllInBackground(Collection<T> objects, DoneCallback doneCallback) {
		// Validates parse relation name
		if (TextUtils.isEmpty(mParseRelationName)) {
			return;
		}
		
		mObjects.removeAll(objects);
		notifyDataSetChanged();
		ParseUtils.removeObjects(mContext, objects, mParseRelationName, doneCallback);
	}
}

package com.ruley.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseQueryAdapter;
import com.ruley.R;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.parse.RuleParse;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class RulesParseQueryAdapter extends BaseParseQueryAdapter<RuleParse> {
	
	private static final String DATE_TEMPLATE = "MMM dd";

	private static final Locale DEFAULT_LOCALE = Locale.US;

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			DATE_TEMPLATE, DEFAULT_LOCALE);
	
	/**
	 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
	 *
	 */
	public interface Listener {
		
		public void onRuleRestore(RuleParse ruleParse);
		
		public void onRuleDelete(RuleParse ruleParse);
		
		public void onRuleShare(RuleParse ruleParse);
		
	}
	
	private Listener mListener = null;
	
	private String mCreatedByPrefix = null;
	
	public RulesParseQueryAdapter(Context context, String parseRelationName,
			ParseQueryAdapter.QueryFactory<RuleParse> queryFactory,
			RulesParseQueryAdapter.Listener listener) {
		super(context, parseRelationName, queryFactory);
		
		mListener = listener;
		mCreatedByPrefix = context.getString(R.string.created_by_prefix);
	}

	@Override
	public View getItemView(RuleParse ruleParse, View convertView, ViewGroup parent) {
		Context context = getContext();

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.list_item_rule_parse_card, null);
			holder = new ViewHolder();
			holder.tvRuleTitle = (TextView) convertView.findViewById(R.id.tvRuleTitle);
			holder.tvCreatedBy = (TextView) convertView.findViewById(R.id.tvCreatedBy);
			holder.tvCreatedAt = (TextView) convertView.findViewById(R.id.tvCreatedAt);
			holder.llTriggers = (LinearLayout) convertView.findViewById(R.id.llTriggers);
			holder.llActions = (LinearLayout) convertView.findViewById(R.id.llActions);
			holder.vgCardFooter = (ViewGroup) convertView.findViewById(R.id.vgCardFooter);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Rule rule = ruleParse.getRule();
		
		// Rule's meta data
		holder.tvRuleTitle.setText(rule.getName());
		// Rule's created by - author
		holder.tvCreatedBy.setText(mCreatedByPrefix + rule.getCreatedByName());
		// Rule's created at - date
		Date createdAt = ruleParse.getCreatedAt();
		String createdAtText = DATE_FORMAT.format(createdAt);
		holder.tvCreatedAt.setText(createdAtText);
		// Rule's data
		inflateRuleTriggersViews(context, holder, rule);
		inflateRuleActionsViews(context, holder, rule);
		inflateCardFooter(convertView, holder, ruleParse);
		
		return convertView;
	}

	private void inflateRuleActionsViews(Context context, ViewHolder holder, Rule rule) {
		holder.llActions.removeAllViews();
		for (Action action : rule.getActions()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(action.getIcon());
			tvDescription.setText(action.getDescription(context));
			holder.llActions.addView(ruleComponentView);
		}
	}

	private void inflateRuleTriggersViews(Context context, ViewHolder holder, Rule rule) {
		holder.llTriggers.removeAllViews();
		for (Trigger trigger : rule.getTriggers()) {
			View ruleComponentView = View.inflate(context, R.layout.list_item_rule_component_in_rule_card, null);
			ImageView ivIcon = (ImageView) ruleComponentView.findViewById(R.id.ivIcon);
			TextView tvDescription = (TextView) ruleComponentView.findViewById(R.id.tvDescription);
			ivIcon.setImageBitmap(trigger.getIcon());
			tvDescription.setText(trigger.getDescription(context));
			holder.llTriggers.addView(ruleComponentView);
		}
	}
	
	private void inflateCardFooter(View convertView, ViewHolder holder, RuleParse ruleParse) {
		// Shows the card footer
		holder.vgCardFooter = (ViewGroup) convertView.findViewById(R.id.vgCardFooter);
		holder.vgCardFooter.setTag(R.id.tag_rule_parse, ruleParse);
		holder.vgCardFooter.setVisibility(View.VISIBLE);
		// Initialize restore button
		holder.bContextualAction1 = (Button) convertView.findViewById(R.id.bContextualAction1);
		holder.bContextualAction1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_restore, 0, 0, 0);
		holder.bContextualAction1.setText(R.string.contextual_action_button_restore);
		holder.bContextualAction1.setOnClickListener(mOnRestoreClickListener);
		holder.bContextualAction1.setVisibility(View.VISIBLE);
		// Initialize delete button
		holder.bContextualAction2 = (Button) convertView.findViewById(R.id.bContextualAction2);
		holder.bContextualAction2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_delete, 0, 0, 0);
		holder.bContextualAction2.setText(R.string.contextual_action_button_delete);
		holder.bContextualAction2.setOnClickListener(mOnDeleteClickListener);
		holder.bContextualAction2.setVisibility(View.VISIBLE);
		// Initialize share button
		holder.bContextualAction4 = (Button) convertView.findViewById(R.id.bContextualAction4);
		holder.bContextualAction4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.contextual_action_share, 0, 0, 0);
		holder.bContextualAction4.setText(R.string.contextual_action_button_share);
		holder.bContextualAction4.setOnClickListener(mOnShareClickListener);
		holder.bContextualAction4.setVisibility(View.VISIBLE);
	}

	static class ViewHolder {
		TextView tvRuleTitle;
		TextView tvCreatedBy;
		TextView tvCreatedAt;
		LinearLayout llTriggers;
		LinearLayout llActions;
		ViewGroup vgCardFooter;
		Button bContextualAction1;
		Button bContextualAction2;
		Button bContextualAction3;
		Button bContextualAction4;
	}
	
	private OnClickListener mOnRestoreClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			View parent = (View) v.getParent();
			RuleParse ruleParse = (RuleParse) parent.getTag(R.id.tag_rule_parse);
			mListener.onRuleRestore(ruleParse);
		}
	};
	
	private OnClickListener mOnDeleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			View parent = (View) v.getParent();
			RuleParse ruleParse = (RuleParse) parent.getTag(R.id.tag_rule_parse);
			mListener.onRuleDelete(ruleParse);
		}
	};
	
	private OnClickListener mOnShareClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			View parent = (View) v.getParent();
			RuleParse ruleParse = (RuleParse) parent.getTag(R.id.tag_rule_parse);
			mListener.onRuleShare(ruleParse);
		}
	};

}

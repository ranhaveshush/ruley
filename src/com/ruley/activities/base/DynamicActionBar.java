/**
 * 
 */
package com.ruley.activities.base;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.model.beans.IValidatable;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DynamicActionBar {
	
	private static final String CLASS_NAME = DynamicActionBar.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DynamicActionBar.class.getName());
	
	private Context mContext = null;

	/**
	 * The rule or rule component to validate.
	 */
	IValidatable mValidatable = null;

	/**
	 * List of action bar drawer_menu items to show/hide in respect to the
	 * mValidatable validation.
	 */
	List<MenuItem> mMenuItems = null;
	
	private Animation mActionButtonClickAnim = null;
	
	private String mActionDisabledText = null;
	
	public DynamicActionBar() {
		super();
		
		mContext = MyApplication.getContext();

		// Loads ActionBar actions icons animations
		mActionButtonClickAnim  = AnimationUtils.loadAnimation(
				mContext, R.anim.action_button_click);
		
		// Gets the action disabled text format from res
		mActionDisabledText = mContext.getString(R.string.toast_action_disabled);
	}

	public IValidatable getValidatable() {
		return mValidatable;
	}

	public void setValidatable(IValidatable validatable) {
		mValidatable = validatable;
	}

	public List<MenuItem> getMenuItems() {
		return mMenuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		mMenuItems = menuItems;
	}
	
	public Animation getmActionButtonClickAnim() {
		return mActionButtonClickAnim;
	}

	/**
	 * Updates the dynamic action bar according to the mValidatable
	 * validation. If the mValidatable is valid show drawer_menu items, Otherwise
	 * hide drawer_menu items.
	 * 
	 * @param context
	 */
	public void update(final Context context) {
		LOGGER.debug(CLASS_NAME + "#update(...)");

		if (!isInitialize()) {
			return;
		}

		boolean isValid = mValidatable.isValid(context);
		LOGGER.debug(mValidatable.getClass().getSimpleName() + " isValid = " + isValid);
		
		for (MenuItem menuItem : mMenuItems) {
			updateMenuItem(menuItem, isValid);
		}
	}

	private void updateMenuItem(MenuItem menuItem, boolean isValid) {
		if (isValid) {
			switch (menuItem.getItemId()) {
			case R.id.action_save_rule:
			case R.id.action_save_trigger:
			case R.id.action_save_action:
				menuItem.setIcon(R.drawable.ic_action_save_enabled);
				break;
			case R.id.action_upload_rule:
				menuItem.setIcon(R.drawable.ic_action_upload_enabled);
				break;
			case R.id.action_send_feedback:
				menuItem.setIcon(R.drawable.ic_action_send_enabled);
				break;
			}
			menuItem.setOnMenuItemClickListener(null);
		} else {
			switch (menuItem.getItemId()) {
			case R.id.action_save_rule:
			case R.id.action_save_trigger:
			case R.id.action_save_action:
				menuItem.setIcon(R.drawable.ic_action_save_disabled);
				break;
			case R.id.action_upload_rule:
				menuItem.setIcon(R.drawable.ic_action_upload_disabled);
				break;
			case R.id.action_send_feedback:
				menuItem.setIcon(R.drawable.ic_action_send_disabled);
				break;
			}
			menuItem.setOnMenuItemClickListener(new OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem menuItem) {
					String menuItemTitle = menuItem.getTitle().toString().toLowerCase(Locale.getDefault());
					String toastText = String.format(mActionDisabledText, menuItemTitle);
					ToastUtils.show(AppState.BUILD_RELEASE, mContext, toastText, Toast.LENGTH_SHORT);
					return true;
				}
			});
		}
	}

	private boolean isInitialize() {
		return mValidatable != null
				&& mMenuItems != null
				&& !mMenuItems.isEmpty();
	}
	
}

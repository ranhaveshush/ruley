/**
 * 
 */
package com.ruley.activities.base;

import android.os.Bundle;

import com.ruley.utilities.AnalyticsUtils;

/**
 * Google Analytics Base activity.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class GABaseActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Gets a Tracker (should auto-report)
		AnalyticsUtils.getInstance().getTracker(AnalyticsUtils.getDefaultTrackerName());
	}

	@Override
	public void onStart() {
		super.onStart();
		
		AnalyticsUtils.sendScreenViewEvent(this);
	}

}

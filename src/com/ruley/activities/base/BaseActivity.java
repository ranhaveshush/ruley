/**
 * 
 */
package com.ruley.activities.base;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class BaseActivity extends Activity {
	
	public DynamicActionBar mDynamicActionBar = new DynamicActionBar();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Removes the app icon from the action bar
		getActionBar().setIcon(
				new ColorDrawable(getResources().getColor(
						android.R.color.transparent)));
	}

}

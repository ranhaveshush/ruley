package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeMobileDataStateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeMobileDataStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeMobileDataStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgMobileDataState = null;
	
	private ChangeMobileDataStateAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_mobile_data_state_action_editor);
		
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgMobileDataState = (RadioGroup) findViewById(R.id.rgMobileDataState);
		rgMobileDataState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbMobileDataOn:
					action.setWantedMobileDataState(ChangeMobileDataStateAction.MOBILE_DATA_ENABLE);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbMobileDataOff:
					action.setWantedMobileDataState(ChangeMobileDataStateAction.MOBILE_DATA_DISABLE);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeMobileDataStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeMobileDataStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeMobileDataStateAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedMobileDataState()) {
		case ChangeMobileDataStateAction.MOBILE_DATA_ENABLE:
			rgMobileDataState.check(R.id.rbMobileDataOn);
			break;
		case ChangeMobileDataStateAction.MOBILE_DATA_DISABLE:
			rgMobileDataState.check(R.id.rbMobileDataOff);
			break;
		}
	}

}

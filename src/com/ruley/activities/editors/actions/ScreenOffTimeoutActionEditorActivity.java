package com.ruley.activities.editors.actions;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ScreenOffTimeoutAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ScreenOffTimeoutActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ScreenOffTimeoutActionEditorActivity.class.getName());
	
	private TextView tvDescription = null;
	
	private ArrayAdapter<String> dataAdapter = null;

	private Spinner sTimeout = null;

	private ScreenOffTimeoutAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_screen_off_timeout_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		List<String> timeouts = new ArrayList<String>();
		timeouts.addAll(ScreenOffTimeoutAction.TIMEOUTS.keySet());
		dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, timeouts);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		sTimeout = (Spinner) findViewById(R.id.sTimeout);
		sTimeout.setAdapter(dataAdapter);
		sTimeout.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				String timoutKey = dataAdapter.getItem(position);
				action.setWantedScreenOffTimeout(timoutKey);
				updateDescription(action, tvDescription);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ScreenOffTimeoutAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ScreenOffTimeoutAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ScreenOffTimeoutAction();
		}
		
		setBaseAction(action);
		
		String timeoutKey = action.getWantedScreenOffTimeout();
		for (int i=0; i < timeouts.size(); ++i) {
			String timeout = timeouts.get(i);
			if (timeoutKey.equals(timeout)) {
				sTimeout.setSelection(i);
				break;
			}
		}
	}
}

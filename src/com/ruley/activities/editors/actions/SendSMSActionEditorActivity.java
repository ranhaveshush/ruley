package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.SendSMSAction;
import com.ruley.model.beans.variables.ContactVariable;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SendSMSActionEditorActivity extends BaseActionDynamicEditorActivity {
	
	private static final String CLASS_NAME = SendSMSActionEditorActivity.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SendSMSActionEditorActivity.class.getName());

	private static final int ADD_CONTACT_REQUEST_CODE = 6201;

	private TextView tvDescription = null;
	
	private TextView tvContactName = null;
	
	private TextView etMessage = null;
	
	private SendSMSAction mAction = null;
	
	private ContactVariable mContactVariable = null;
	
	private Drawable mAppContactIcon = null;
	
	private int mIconPadding = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_send_sms_action_editor);
		
		mIconPadding = (int) getResources().getDimension(R.dimen.icon_padding);
		mAppContactIcon = ImageUtils.getDrawable(R.drawable.ic_action_add_contact);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvContactName = (TextView) findViewById(R.id.tvContactName);
		tvContactName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PickersUtils.launchContactPicker(SendSMSActionEditorActivity.this, ADD_CONTACT_REQUEST_CODE);
			}
		});
		etMessage = (EditText) findViewById(R.id.etMessage);
		etMessage.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mAction.setMessage(s.toString());
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (SendSMSAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing an existing instance
		} else if (extras != null) {
			mAction = (SendSMSAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new SendSMSAction();
		}
		
		mContactVariable = mAction.getContact();
		setBaseAction(mAction);

		String message = mAction.getMessage();
		if (!TextUtils.isEmpty(message)) {
			etMessage.setText(message);
		}
		String contactId = mContactVariable.getContactId();
		if (!TextUtils.isEmpty(contactId)) {
			updateContactInfo(contactId);
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug("onActivityResult(...)");

		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case ADD_CONTACT_REQUEST_CODE:
				Uri contactUri = data.getData();
				String contactId = contactUri.getLastPathSegment();
				mContactVariable.setContactId(contactId);
				updateContactInfo(contactId);
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	private void updateContactInfo(String contactId) {
		String contactName = ContactsUtils.getContactName(SendSMSActionEditorActivity.this, contactId);
		Bitmap contactPhoto = ContactsUtils.getContactPhoto(SendSMSActionEditorActivity.this, contactId);
		Drawable drawable = ImageUtils.toDrawable(contactPhoto);
		tvContactName.setCompoundDrawablePadding(mIconPadding);
		tvContactName.setCompoundDrawablesWithIntrinsicBounds(drawable, null, mAppContactIcon, null);
		tvContactName.setText(contactName);
	}

}
package com.ruley.activities.editors.actions;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.SendEmailAction;
import com.ruley.model.beans.variables.EmailVariable;
import com.ruley.utilities.UserAccountUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SendEmailActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SendEmailActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private ArrayAdapter<String> mUserAccountsAdapter = null; 
	
	private Spinner sEmailFromAddress = null;
	
	private EditText etEmailToAddresses = null;
	
	private EditText etEmailSubject = null;
	
	private EditText etEmailText = null;
	
	private SendEmailAction mAction = null;
	
	private EmailVariable mEmail = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_send_email_action_editor);
		
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		
		// Gets the user google accounts
		mUserAccountsAdapter = new ArrayAdapter<String>(
				SendEmailActionEditorActivity.this, android.R.layout.simple_spinner_dropdown_item);
		
		List<Account> accounts = UserAccountUtils.getAccounts(UserAccountUtils.ACCOUNT_TYPE_GOOGLE);
		for (Account account : accounts) {
			mUserAccountsAdapter.add(account.name);
		}
		
		sEmailFromAddress = (Spinner) findViewById(R.id.sEmailFromAddress);
		sEmailFromAddress.setAdapter(mUserAccountsAdapter);
		sEmailFromAddress.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				mEmail.setFromEmailAddress(null);
				updateDynamicActionBar();
				
				String accountName = mUserAccountsAdapter.getItem(position);
				AccountManager accountManager = AccountManager.get(SendEmailActionEditorActivity.this);
				Account account = UserAccountUtils.getAccount(accountManager, UserAccountUtils.ACCOUNT_TYPE_GOOGLE, accountName);
				accountManager.getAuthToken(account,
						SendEmailAction.AUTH_TOKEN_TYPE, null,
						SendEmailActionEditorActivity.this,
						mAccountManagerCallback, null);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		
		etEmailToAddresses = (EditText) findViewById(R.id.etEmailToAddresses);
		etEmailToAddresses.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				String emailAddressesStr = s.toString();
				if (TextUtils.isEmpty(emailAddressesStr)) {
					mEmail.setToEmailAddresses(null);
				} else {
					String[] emailAddressesArr = emailAddressesStr.split(SendEmailAction.EMAILS_DELIMITER);
					List<String> emailAddresses = Arrays.asList(emailAddressesArr);
					mEmail.setToEmailAddresses(emailAddresses);
				}
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});
		etEmailSubject = (EditText) findViewById(R.id.etEmailSubject);
		etEmailSubject.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mEmail.setSubject(s.toString());
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});
		etEmailText = (EditText) findViewById(R.id.etEmailText);
		etEmailText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mEmail.setText(s.toString());
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (SendEmailAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			mAction = (SendEmailAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new SendEmailAction();
		}
		
		mEmail = mAction.getEmail();
		setBaseAction(mAction);
		
		List<String> emailAddresses = mEmail.getToEmailAddresses();
		if (emailAddresses != null) {
			String emailAddressesStr = emailAddresses.toString();
			emailAddressesStr = emailAddressesStr.substring(1, emailAddressesStr.length()-1);
			etEmailToAddresses.setText(emailAddressesStr);
		}
		etEmailSubject.setText(mEmail.getSubject());
		etEmailText.setText(mEmail.getText());
		
		updateDescription(mAction, tvDescription);
	}
	
	private AccountManagerCallback<Bundle> mAccountManagerCallback = new AccountManagerCallback<Bundle>() {
		@Override
		public void run(AccountManagerFuture<Bundle> future) {
			String accountName = null;
			String authToken = null;
			
			try {
				Bundle result = future.getResult();
				accountName = result.getString(AccountManager.KEY_ACCOUNT_NAME);
				authToken = result.getString(AccountManager.KEY_AUTHTOKEN);
			} catch (Exception e) {
				LOGGER.error("Error: ", e);
			}
			
			LOGGER.info("accountName: " + accountName);
			LOGGER.info("authtoken: " + authToken);
			
			mEmail.setFromEmailAddress(accountName);
			mEmail.setAuthToken(authToken);
			updateDynamicActionBar();
		}
	};

}

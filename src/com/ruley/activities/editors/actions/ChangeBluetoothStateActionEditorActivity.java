package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeBluetoothStateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeBluetoothStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeBluetoothStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	private RadioGroup rgBluetoothState = null;
	
	private ChangeBluetoothStateAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_bluetooth_state_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgBluetoothState = (RadioGroup) findViewById(R.id.rgBluetoothState);
		rgBluetoothState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbBluetoothOn:
					action.setWantedBluetoothState(BluetoothAdapter.STATE_ON);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbBluetoothOff:
					action.setWantedBluetoothState(BluetoothAdapter.STATE_OFF);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeBluetoothStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeBluetoothStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeBluetoothStateAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedBluetoothState()) {
		case BluetoothAdapter.STATE_ON:
			rgBluetoothState.check(R.id.rbBluetoothOn);
			break;
		case BluetoothAdapter.STATE_OFF:
			rgBluetoothState.check(R.id.rbBluetoothOff);
			break;
		}
	}

}

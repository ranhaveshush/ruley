package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.RespondMissedCallsAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RespondMissedCallsActionEditorActivity extends BaseActionDynamicEditorActivity {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RespondMissedCallsActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private EditText etMessage = null;
	
	private RespondMissedCallsAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_respond_missed_calls_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		etMessage = (EditText) findViewById(R.id.etMessage);
		etMessage.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				action.setResponseMessage(s.toString());
				updateDescription(action, tvDescription);
				updateDynamicActionBar();
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (RespondMissedCallsAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing an existing instance
		} else if (extras != null) {
			action = (RespondMissedCallsAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new RespondMissedCallsAction();
		}
		
		setBaseAction(action);

		etMessage.setText(action.getResponseMessage());
		
		updateDynamicActionBar();
	}

}

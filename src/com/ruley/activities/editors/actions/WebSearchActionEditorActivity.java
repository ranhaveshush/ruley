package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.WebSearchWakeAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class WebSearchActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WebSearchActionEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private EditText etSearchQuery = null;
	
	private WebSearchWakeAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_web_search_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		etSearchQuery = (EditText) findViewById(R.id.etSearchQuery);
		etSearchQuery.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				action.setSearchQuery(s.toString());
				updateDescription(action, tvDescription);
				updateDynamicActionBar();
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (WebSearchWakeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (WebSearchWakeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new WebSearchWakeAction();
		}
		
		setBaseAction(action);
		
		etSearchQuery.setText(action.getSearchQuery());
		
		updateDescription(action, tvDescription);
		updateDynamicActionBar();
	}

}

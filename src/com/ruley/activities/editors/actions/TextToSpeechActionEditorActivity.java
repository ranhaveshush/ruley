package com.ruley.activities.editors.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.adapters.LocalesAdapter;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.TextToSpeechAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TextToSpeechActionEditorActivity extends BaseActionDynamicEditorActivity implements TextToSpeech.OnInitListener {
	
	private static final String CLASS_NAME = TextToSpeechActionEditorActivity.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TextToSpeechActionEditorActivity.class.getName());

	private static final int CHECK_TTS_DATA_REQUEST_CODE = 6501;
	
	private TextView tvDescription = null;

	private ArrayAdapter<Locale> supportedLanguagesAdapter = null;
	
	private Spinner sLanguage = null;
	
	private EditText etText = null;
	
	private TextToSpeechAction action = null;
	
	private TextToSpeech tts = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_text_to_speech_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		supportedLanguagesAdapter = new LocalesAdapter(this,
				android.R.layout.simple_spinner_item, new ArrayList<Locale>());
		supportedLanguagesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sLanguage = (Spinner) findViewById(R.id.sLanguage);
		sLanguage.setAdapter(supportedLanguagesAdapter);
		sLanguage.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Locale language = supportedLanguagesAdapter.getItem(position);
				action.setLanguage(language);
				updateDescription(action, tvDescription);
				updateDynamicActionBar();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
		etText = (EditText) findViewById(R.id.etText);
		etText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				action.setText(s.toString());
				updateDescription(action, tvDescription);
				updateDynamicActionBar();
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (TextToSpeechAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (TextToSpeechAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new TextToSpeechAction();
		}
		
		setBaseAction(action);
		
		etText.setText(action.getText());
		
		updateDescription(action, tvDescription);
		updateDynamicActionBar();
	}

	@Override
	public void onStart() {
		LOGGER.debug(CLASS_NAME + "#onStart(...)");
		super.onStart();
		
		// Checks if TTS is available
		Intent checkIntent = new Intent();
		checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
		startActivityForResult(checkIntent, CHECK_TTS_DATA_REQUEST_CODE);
	}

	@Override
	public void onStop() {
		LOGGER.debug(CLASS_NAME + "#onStop(...)");
		super.onStop();
		
		destroyTTS();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		// If TTS is not available, install it
		if (requestCode == CHECK_TTS_DATA_REQUEST_CODE) {
			// Success, create the TTS instance
			if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
				tts = new TextToSpeech(this, this);
			// Missing data, install it
			} else {
				Intent installIntent = new Intent();
		        installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
		        startActivity(installIntent);
			}
		}
	}

	@Override
	public void onInit(int status) {
		LOGGER.debug(CLASS_NAME + "#onInit(...)");
		
		// Status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
        if (status == TextToSpeech.SUCCESS) {
        	updateSupportedLanguagesAdapter();
        	destroyTTS();
        } else {
        	LOGGER.debug(CLASS_NAME + "#onInit(...) Could not initialize TextToSpeech.");
        }
	}
	
	private void destroyTTS() {
		LOGGER.debug(CLASS_NAME + "#destroyTTS(...)");
		
		// Don't forget to shutdown!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
	}

	private void updateSupportedLanguagesAdapter() {
		LOGGER.debug(CLASS_NAME + "#updateSupportedLanguagesAdapter(...)");
		
		List<Locale> supportedLanguages = getSupportedLanguages();
		// If there are supported languages, update language spinner
		if (supportedLanguages != null && !supportedLanguages.isEmpty()) {
			supportedLanguagesAdapter.clear();
			for (Locale language : supportedLanguages) {
				supportedLanguagesAdapter.add(language);
			}
			supportedLanguagesAdapter.notifyDataSetChanged();
			// Selects the action's language and updates the description and action bar
			int languagePosition = supportedLanguagesAdapter.getPosition(action.getLanguage());
			sLanguage.setSelection(languagePosition);
			updateDescription(action, tvDescription);
			updateDynamicActionBar();
		}
	}
	
	private List<Locale> getSupportedLanguages() {
		LOGGER.debug(CLASS_NAME + "#getSupportedLanguages(...)");
		
		List<Locale> availableLocales = new ArrayList<Locale>();

		try {
			Locale[] locales = Locale.getAvailableLocales();
			for (Locale locale : locales) {
				int result = tts.isLanguageAvailable(locale);
				if (result == TextToSpeech.LANG_COUNTRY_AVAILABLE) {
					availableLocales.add(locale);
				}
			}
		} catch (Exception e) {
			availableLocales.clear();
			availableLocales.add(Locale.US);
		}
		
		return availableLocales;
	}

}

/**
 * 
 */
package com.ruley.activities.editors.actions.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

import com.ruley.fragments.dialogs.DialogLauncher;
import com.ruley.fragments.dialogs.TwoButtonDialogFragment;
import com.ruley.model.beans.device.DeviceState;
import com.ruley.utilities.GooglePlayServicesUtils;
import com.ruley.utilities.LocationClientUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BaseActionGPSEditorActivity extends
		BaseActionDynamicEditorActivity implements
		TwoButtonDialogFragment.Listener {
	
	private static final String CLASS_NAME = BaseActionGPSEditorActivity.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseActionGPSEditorActivity.class.getName());
	
	public static final String FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR = "LocationServiceDisabledError";
	
	protected static final String FRAGMENT_TAG_AIRPLANE_MODE_ON_ERROR = "AirplaneModeOnError";
	
	protected static final String FRAGMENT_TAG_NETWORK_CONNECTION_OFFLINE_ERROR = "NetworkConnectionOfflineError";
	
	protected boolean mIsGooglePlayServicesAvailable = false;

	protected boolean mIsLocationServiceEnabled = false;

	protected boolean mIsAirplaneModeOn = false;

	protected boolean mIsNetworkConnectionOnline = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		mIsGooglePlayServicesAvailable  = GooglePlayServicesUtils.isGooglePlayServicesAvailable(this);
	}

	@Override
	protected void onResume() {
		LOGGER.debug(CLASS_NAME + "#onResume(...)");
		super.onResume();
		
		mIsLocationServiceEnabled = isLocationServiceEnabled();
		if (mIsLocationServiceEnabled) {
			mIsAirplaneModeOn = isAirplaneModeOn();
			if (!mIsAirplaneModeOn) {
				mIsNetworkConnectionOnline  = isNetworkConnectionOnline();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		GooglePlayServicesUtils.handleActivityResult(requestCode, resultCode, this);
	}
	
	@Override
	public void onConfirm(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onConfirm(...)");
		
		if (FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR.equals(fragmentTag)) {
			Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(settingsIntent);
		} else if (FRAGMENT_TAG_AIRPLANE_MODE_ON_ERROR.equals(fragmentTag)) {
			Intent settingsIntent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
			startActivity(settingsIntent);
		} else  if (FRAGMENT_TAG_NETWORK_CONNECTION_OFFLINE_ERROR.equals(fragmentTag)) {
			Intent settingsIntent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
			startActivity(settingsIntent);
		}
	}

	@Override
	public void onCancel(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onCancel(...)");
		
		if (FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR.equals(fragmentTag)) {
			setResult(RESULT_CANCELED);
			finish();
		}
	}
	
	private boolean isLocationServiceEnabled() {
		boolean isLocationServiceEnabled = LocationClientUtils.isLocationServiceEnabled(this);
		
		if (!isLocationServiceEnabled) {
			DialogLauncher.launchActionSecuritySettings(this, FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR);
		}
		
		return isLocationServiceEnabled;
	}
	
	private boolean isAirplaneModeOn() {
		boolean isAirplaneModeOn = DeviceState.isAirplaneModeOn();
		
		if (isAirplaneModeOn) {
			DialogLauncher.launchAirplaneModeSettings(this, FRAGMENT_TAG_AIRPLANE_MODE_ON_ERROR);
		}
		
		return isAirplaneModeOn;
	}
	
	private boolean isNetworkConnectionOnline() {
		boolean isNetworkConnectionOnline = DeviceState.isNetworkConnectionOnline();
		
		if (!isNetworkConnectionOnline) {
			DialogLauncher.launchNetworkConnectionSettings(this, FRAGMENT_TAG_NETWORK_CONNECTION_OFFLINE_ERROR);
		}
		
		return isNetworkConnectionOnline;
	}
	
}

/**
 * 
 */
package com.ruley.activities.editors.actions.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ruley.R;
import com.ruley.activities.editors.base.BaseEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.base.Action;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BaseActionDynamicEditorActivity extends BaseEditorActivity {
	
	private static final String CLASS_NAME = BaseActionDynamicEditorActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseActionDynamicEditorActivity.class.getName());
	
	private Action mBaseAction = null;
	
	public Action getBaseAction() {
		return mBaseAction;
	}

	public void setBaseAction(Action action) {
		this.mBaseAction = action;
	}
	
	public void updateDynamicActionBar() {
		mDynamicActionBar.update(BaseActionDynamicEditorActivity.this);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		
		outState.putSerializable(Config.Extra.EXTRA_ACTION, mBaseAction);
		
		super.onSaveInstanceState(outState);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");
		
		getMenuInflater().inflate(R.menu.activity_action_editor_menu, menu);
		MenuItem actionSaveAction = menu.findItem(R.id.action_save_action);
		List<MenuItem> menuItems = new ArrayList<MenuItem>(Arrays.asList(actionSaveAction));
		mDynamicActionBar.setValidatable(mBaseAction);
		mDynamicActionBar.setMenuItems(menuItems);
		mDynamicActionBar.update(BaseActionDynamicEditorActivity.this);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");

		switch (item.getItemId()) {
		case R.id.action_save_action:
			Intent resIntent = new Intent();
			resIntent.putExtra(Config.Extra.EXTRA_ACTION, mBaseAction);
			setResult(RESULT_OK, resIntent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}

package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ToggleSoundEffectsAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToggleSoundEffectsActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToggleSoundEffectsActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgSoundEffectsState = null;
	
	private ToggleSoundEffectsAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_toggle_sound_effects_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgSoundEffectsState = (RadioGroup) findViewById(R.id.rgSoundEffectsState);
		rgSoundEffectsState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbSoundEffectsOn:
					action.setWantedSoundEffectsState(ToggleSoundEffectsAction.SOUND_EFFECTS_ENABLED);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbSoundEffectsOff:
					action.setWantedSoundEffectsState(ToggleSoundEffectsAction.SOUND_EFFECTS_DISABLED);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ToggleSoundEffectsAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ToggleSoundEffectsAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ToggleSoundEffectsAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedSoundEffectsState()) {
		case ToggleSoundEffectsAction.SOUND_EFFECTS_ENABLED:
			rgSoundEffectsState.check(R.id.rbSoundEffectsOn);
			break;
		case ToggleSoundEffectsAction.SOUND_EFFECTS_DISABLED:
			rgSoundEffectsState.check(R.id.rbSoundEffectsOff);
			break;
		}
	}

}

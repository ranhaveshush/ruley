package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.StartAppWakeAction;
import com.ruley.model.beans.variables.ApplicationVariable;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class StartAppActionEditorAcivity extends BaseActionDynamicEditorActivity {
	
	private static final String CLASS_NAME = StartAppActionEditorAcivity.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(StartAppActionEditorAcivity.class.getName());
	
	private static final int APP_PICKER_REQUEST_CODE = 6401;
	
	private PackageManager mPackageManager = null;
	
	private TextView tvDescription = null;

	private TextView tvAppLabel = null;
	
	private Drawable mPickAppIcon = null;
	
	private int mIconPadding = 0;
	
	private StartAppWakeAction mAction = null;

	private ApplicationVariable mApplicationVariable = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_start_app_action_editor);
		
		mPackageManager = getPackageManager();
		mIconPadding = (int) getResources().getDimension(R.dimen.icon_padding);
		mPickAppIcon = ImageUtils.getDrawable(R.drawable.ic_action_pick_app);
		
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvAppLabel = (TextView) findViewById(R.id.tvAppLabel);
		tvAppLabel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {	
				PickersUtils.launchAppPicker(StartAppActionEditorAcivity.this, APP_PICKER_REQUEST_CODE);
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (StartAppWakeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing an existing instance
		} else if (extras != null) {
			mAction = (StartAppWakeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new StartAppWakeAction();
		}
		
		mApplicationVariable  = mAction.getApplication();
		setBaseAction(mAction);
		
		try {
			String appPackageName = mApplicationVariable.getAppPackage();
			if (!TextUtils.isEmpty(appPackageName)) {
				ApplicationInfo appInfo = mPackageManager.getApplicationInfo(appPackageName, 0);
				updateAppInfo(appInfo);
			}
		} catch (NameNotFoundException e) {
			LOGGER.error("Error: ", e);
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case APP_PICKER_REQUEST_CODE:
				ApplicationInfo appInfo = (ApplicationInfo) data.getParcelableExtra(Config.Extra.EXTRA_APP_INFO);
				String appLabel = appInfo.loadLabel(mPackageManager).toString();
				mApplicationVariable.setAppName(appLabel);
				mApplicationVariable.setAppPackage(appInfo.packageName);
				updateAppInfo(appInfo);
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	private void updateAppInfo(ApplicationInfo appInfo) {
		String appLabel = appInfo.loadLabel(mPackageManager).toString();
		Drawable appIcon = appInfo.loadIcon(mPackageManager);
		tvAppLabel.setCompoundDrawablePadding(mIconPadding);
		tvAppLabel.setCompoundDrawablesWithIntrinsicBounds(appIcon, null, mPickAppIcon, null);
		tvAppLabel.setText(appLabel);
	}

}

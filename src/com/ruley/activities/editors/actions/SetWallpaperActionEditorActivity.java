package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.SetWallpaperAction;
import com.ruley.model.beans.variables.FileVariable;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class SetWallpaperActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SetWallpaperActionEditorActivity.class.getName());

	private static final int PHOTO_PICKER_REQUEST_CODE = 6301;
	
	private TextView tvDescription = null;
	
	private ImageView ivPickedImage = null;

	private TextView tvPickImage = null;
	
	private SetWallpaperAction mAction = null;
	
	private FileVariable mFileVariable = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_set_wallpaper_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		ivPickedImage = (ImageView) findViewById(R.id.ivPickedImage);
		tvPickImage = (TextView) findViewById(R.id.tvPickImage);
		tvPickImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PickersUtils.launchImagePicker(SetWallpaperActionEditorActivity.this, PHOTO_PICKER_REQUEST_CODE);
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (SetWallpaperAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
			mFileVariable = mAction.getFile();
			updatePickedImage(mFileVariable.getFileUri());
		// If creating a new instance
		} else if (extras != null) {
			mAction = (SetWallpaperAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
			mFileVariable = mAction.getFile();
			updatePickedImage(mFileVariable.getFileUri());
		} else {
			mAction = new SetWallpaperAction();
			mFileVariable = mAction.getFile();
		}
		
		setBaseAction(mAction);
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug("onActivityResult(...)");

		if (resultCode == RESULT_OK) {
			if (requestCode == PHOTO_PICKER_REQUEST_CODE) {
				Uri imageUri = data.getData();
				String imageUriStr = imageUri.toString();
				mFileVariable.setFileUri(imageUriStr);
				updatePickedImage(imageUriStr);
			}
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	private void updatePickedImage(String bitmapUriStr) {
		try {
			Uri bitmapUri = Uri.parse(bitmapUriStr);
			String bitmapFileName = ImageUtils.getBitmapFileName(bitmapUri);
			if (!TextUtils.isEmpty(bitmapFileName)) {
				tvPickImage.setText(bitmapFileName);
			}
			ivPickedImage.setImageURI(bitmapUri);
			ivPickedImage.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			LOGGER.error("Error: ", e);
		}
	}

}
package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeWIFISleepPolicyAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeWIFISleepPolicyActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeWIFISleepPolicyActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgWIFISleepPolicy = null;
	
	private ChangeWIFISleepPolicyAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_wifi_sleep_policy_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgWIFISleepPolicy = (RadioGroup) findViewById(R.id.rgWIFISleepPolicy);
		rgWIFISleepPolicy.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbWIFISleepPolicyDefault:
					action.setWantedWIFISleepPolicy(Settings.System.WIFI_SLEEP_POLICY_DEFAULT);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbWIFISleepPolicyNever:
					action.setWantedWIFISleepPolicy(Settings.System.WIFI_SLEEP_POLICY_NEVER);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbWIFISleepPolicyNeverWhilePlugged:
					action.setWantedWIFISleepPolicy(Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeWIFISleepPolicyAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeWIFISleepPolicyAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeWIFISleepPolicyAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedWIFISleepPolicy()) {
		case Settings.System.WIFI_SLEEP_POLICY_DEFAULT:
			rgWIFISleepPolicy.check(R.id.rbWIFISleepPolicyDefault);
			break;
		case Settings.System.WIFI_SLEEP_POLICY_NEVER:
			rgWIFISleepPolicy.check(R.id.rbWIFISleepPolicyNever);
			break;
		case Settings.System.WIFI_SLEEP_POLICY_NEVER_WHILE_PLUGGED:
			rgWIFISleepPolicy.check(R.id.rbWIFISleepPolicyNeverWhilePlugged);
			break;
		}
	}

}

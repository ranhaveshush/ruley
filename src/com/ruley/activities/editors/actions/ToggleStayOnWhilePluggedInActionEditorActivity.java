package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ToggleStayOnWhilePluggedInAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToggleStayOnWhilePluggedInActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToggleStayOnWhilePluggedInActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgStayOnWhilePluggedInState = null;
	
	private ToggleStayOnWhilePluggedInAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_toggle_stay_on_while_plugged_in_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgStayOnWhilePluggedInState = (RadioGroup) findViewById(R.id.rgStayOnWhilePluggedInState);
		rgStayOnWhilePluggedInState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbStayOnWhilePluggedInOn:
					action.setWantedStayOnWhilePluggedInState(ToggleStayOnWhilePluggedInAction.STAY_ON_WHILE_PLUGGED_IN_ENABLED);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbStayOnWhilePluggedInOff:
					action.setWantedStayOnWhilePluggedInState(ToggleStayOnWhilePluggedInAction.STAY_ON_WHILE_PLUGGED_IN_DISABLED);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ToggleStayOnWhilePluggedInAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ToggleStayOnWhilePluggedInAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ToggleStayOnWhilePluggedInAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedStayOnWhilePluggedInState()) {
		case ToggleStayOnWhilePluggedInAction.STAY_ON_WHILE_PLUGGED_IN_ENABLED:
			rgStayOnWhilePluggedInState.check(R.id.rbStayOnWhilePluggedInOn);
			break;
		case ToggleStayOnWhilePluggedInAction.STAY_ON_WHILE_PLUGGED_IN_DISABLED:
			rgStayOnWhilePluggedInState.check(R.id.rbStayOnWhilePluggedInOff);
			break;
		}
	}

}

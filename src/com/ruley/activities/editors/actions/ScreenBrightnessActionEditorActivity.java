package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ScreenBrightnessAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ScreenBrightnessActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ScreenBrightnessActionEditorActivity.class.getName());
	
	private static final String BRIGHTNESS_UNITS = "%";

	private TextView tvDescription = null;
	
	private RadioGroup rgBrightnessMode = null;
	
	private TextView tvBrightnessLabel = null;
	
	private TextView tvBrightness = null;
	
	private SeekBar sbBrightness = null;
	
	private ScreenBrightnessAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_screen_brightness_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgBrightnessMode = (RadioGroup) findViewById(R.id.rgBrightnessMode);
		rgBrightnessMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbAutomatic:
					tvBrightnessLabel.setEnabled(false);
					tvBrightness.setEnabled(false);
					sbBrightness.setEnabled(false);
					action.setBrightnessMode(Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbManual:
					tvBrightnessLabel.setEnabled(true);
					tvBrightness.setEnabled(true);
					sbBrightness.setEnabled(true);
					action.setBrightnessMode(Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);				
					updateDescription(action, tvDescription);
					break;
				}
			}
		});
		tvBrightnessLabel = (TextView) findViewById(R.id.tvBrightnessLabel);
		tvBrightness = (TextView) findViewById(R.id.tvBrightness);
		sbBrightness = (SeekBar) findViewById(R.id.sbBrightness);
		sbBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvBrightness.setText(String.valueOf(progress) + BRIGHTNESS_UNITS);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setBrightnessPercentage(sbBrightness.getProgress());
				updateDescription(action, tvDescription);
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ScreenBrightnessAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ScreenBrightnessAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ScreenBrightnessAction();
		}
		
		setBaseAction(action);
		
		switch (action.getBrightnessMode()) {
		case Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC:
			rgBrightnessMode.check(R.id.rbAutomatic);
			break;
		case Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL:
			rgBrightnessMode.check(R.id.rbManual);
			break;
		default:
			break;
		}
		int brightnessPercentage = action.getBrightnessPercentage();
		sbBrightness.setProgress(brightnessPercentage);
		tvBrightness.setText(brightnessPercentage + BRIGHTNESS_UNITS);
		
		updateDescription(action, tvDescription);
	}

}

package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ModifySpeakerPhoneStateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ModifySpeakerPhoneStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifySpeakerPhoneStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgSpeakerPhoneState = null;
	
	private ModifySpeakerPhoneStateAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_modify_speaker_phone_state_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgSpeakerPhoneState = (RadioGroup) findViewById(R.id.rgSpeakerPhoneState);
		rgSpeakerPhoneState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbSpeakerPhoneOn:
					action.setWantedSpeakerPhoneState(ModifySpeakerPhoneStateAction.SPEAKER_PHONE_ON);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbSpeakerPhoneOff:
					action.setWantedSpeakerPhoneState(ModifySpeakerPhoneStateAction.SPEAKER_PHONE_OFF);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ModifySpeakerPhoneStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ModifySpeakerPhoneStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ModifySpeakerPhoneStateAction();
		}
		
		setBaseAction(action);
		
		if (action.isWantedSpeakerPhoneState()) {
			rgSpeakerPhoneState.check(R.id.rbSpeakerPhoneOn);
		} else {
			rgSpeakerPhoneState.check(R.id.rbSpeakerPhoneOff);
		}
	}

}

package com.ruley.activities.editors.actions;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionGPSEditorActivity;
import com.ruley.adapters.AutocompleteAdapter;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.fragments.dialogs.DialogLauncher;
import com.ruley.fragments.dialogs.TwoButtonDialogFragment;
import com.ruley.model.beans.actions.WazeNavigateWakeAction;
import com.ruley.model.beans.variables.LocationVariable;
import com.ruley.utilities.Geocoder;
import com.ruley.utilities.KeyboardUtils;
import com.ruley.utilities.LocationClientUtils;
import com.ruley.utilities.MapUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class WazeNavigateActionEditorActivity extends
		BaseActionGPSEditorActivity implements Geocoder.Listener,
		TwoButtonDialogFragment.Listener {
	
	private static final String CLASS_NAME = WazeNavigateActionEditorActivity.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WazeNavigateActionEditorActivity.class.getName());
	
	private static final int MESSAGE_TEXT_CHANGED = 0;
	
	private static final int AUTOCOMPLETE_DELAY = 500;

	private static final int AUTOCOMPLETE_THRESHOLD = 3;
	
	private TextView tvDescription = null;
	
	private Geocoder mGeocoder = null;
	
	private GoogleMap mMap = null;
	
	private AutocompleteAdapter autoCompleteAdapter = null;
	
	private AutoCompleteTextView actvLocation = null;
	
	private boolean mIsLocationSelected = false;
	
	private boolean mIsLocationKnown = false;
	
	private List<Address> lastSuggestedAddresses = new ArrayList<Address>();
	
	private MyHandler mHandler = new MyHandler();
	
	private WazeNavigateWakeAction mAction = null;
	
	private LocationVariable mLocation = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);
		
		if (!mIsGooglePlayServicesAvailable) {
			return;
		}

		setContentView(R.layout.activity_waze_navigate_action_editor);
		
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		autoCompleteAdapter = new AutocompleteAdapter(this,
				android.R.layout.simple_dropdown_item_1line);
		
		actvLocation = (AutoCompleteTextView) findViewById(R.id.actvLocation);
		actvLocation.setThreshold(AUTOCOMPLETE_THRESHOLD);
		actvLocation.setAdapter(autoCompleteAdapter);
		actvLocation.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				mHandler.removeMessages(MESSAGE_TEXT_CHANGED);
			}
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// If airplane mode is on, launch a dialog
				if (mIsAirplaneModeOn) {
					DialogLauncher.launchAirplaneModeSettings(
							WazeNavigateActionEditorActivity.this,
							FRAGMENT_TAG_AIRPLANE_MODE_ON_ERROR);
					return;
				// If network connection offline, launch dialog
				} else if (!mIsNetworkConnectionOnline) {
					DialogLauncher.launchNetworkConnectionSettings(
							WazeNavigateActionEditorActivity.this,
							FRAGMENT_TAG_NETWORK_CONNECTION_OFFLINE_ERROR);
					return;
				// If location text changed cos of a suggested location selected, do nothing
				} else if (mIsLocationSelected) {
					LOGGER.debug(CLASS_NAME + "#onTextChanged(...) location just selected, do nothing");
					mIsLocationSelected = false;
					return;
				// If location text changed cos of know location, do nothing
				} else if (mIsLocationKnown) {
					LOGGER.debug(CLASS_NAME + "#onTextChanged(...) location is known, do nothing");
					mIsLocationKnown = false;
					return;
				}
				
				String address = TextUtils.htmlEncode(s.toString());
				if (!TextUtils.isEmpty(address) && address.length() >= AUTOCOMPLETE_THRESHOLD) {
					Message msg = Message.obtain(mHandler, MESSAGE_TEXT_CHANGED, address);
					mHandler.sendMessageDelayed(msg, AUTOCOMPLETE_DELAY);
				} else {
					autoCompleteAdapter.clear();
					lastSuggestedAddresses.clear();
				}
			}

			@Override
			public void afterTextChanged(Editable s) {
				updateDescription(mAction, tvDescription);
			}
		});
		actvLocation.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				KeyboardUtils.hide(WazeNavigateActionEditorActivity.this, actvLocation, 0);
				Address selectedAddress = lastSuggestedAddresses.get(position);
				String selectedAddressText = getAddressText(selectedAddress);
				mIsLocationSelected = true;
				actvLocation.setText(selectedAddressText);
				double latitude = selectedAddress.getLatitude();
				double longitude = selectedAddress.getLongitude();
				LOGGER.debug(CLASS_NAME + "#onItemClick(...) Address: " + selectedAddressText);
				LOGGER.debug(CLASS_NAME + "#onItemClick(...) Geocoding: lat: " + latitude + " lng: " + longitude);
				
				// Updates UI
				markLocation(latitude, longitude);
				
				// Updates the mAction
				mLocation.setName(selectedAddressText);
				mLocation.setAddress(selectedAddressText);
				mLocation.setLatitude(latitude);
				mLocation.setLongitude(longitude);
				
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mIsLocationKnown = true;
			mAction = (WazeNavigateWakeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			mAction = (WazeNavigateWakeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new WazeNavigateWakeAction();
		}
		
		mLocation = mAction.getLocation();
		setBaseAction(mAction);
		
		mGeocoder = new Geocoder(WazeNavigateActionEditorActivity.this);
		mMap = MapUtils.getMap(WazeNavigateActionEditorActivity.this, mMap);
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	@Override
	protected void onResume() {
		LOGGER.debug(CLASS_NAME + "#onResume(...)");
		super.onResume();
		
		if (mIsGooglePlayServicesAvailable && mIsLocationServiceEnabled) {
			if (mLocation == null || !mLocation.isValid(this)) {
				animateToMyLocation();
			} else {
				mIsLocationKnown = true;
				actvLocation.setText(mLocation.getAddress());
				markLocation(mLocation.getLatitude(), mLocation.getLongitude());
			}
		}
	}
	
	private void animateToMyLocation() {
		if (mIsLocationServiceEnabled) {
			Context appCtx = MyApplication.getContext();
			new LocationClientUtils().getLastLocation(appCtx, new LocationClientUtils.Listener() {
				@Override
				public void onRemovedGeofencesByPendingIntent() {
				}
				@Override
				public void onKnownLastLocation(Location location) {
					if (location == null) {
						return;
					}
					double latitude = location.getLatitude();
					double longitude = location.getLongitude();
					markLocation(latitude, longitude);
					mGeocoder.reverseGeocoding(latitude, longitude);
				}
				@Override
				public void onAddedGeofences() {
				}
				@Override
				public void onError(int errorMessageResourceId) {
					LOGGER.warn(CLASS_NAME + "#onError(...) Reason: " + errorMessageResourceId);
				}
			});
		}
	}
	
	private void markLocation(double latitude, double longitude) {
		LatLng addressLatLng = new LatLng(latitude, longitude);
		MapUtils.animateToLocation(mMap, addressLatLng);
		MapUtils.addMarker(mMap, addressLatLng);
	}
	
	private class MyHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			if (MESSAGE_TEXT_CHANGED == msg.what) {
				String enteredAddress = (String) msg.obj;
				try {
					mGeocoder.autoCompleteAddresses(enteredAddress);
				} catch (Exception e) {
					LOGGER.error(CLASS_NAME + " Failed to get autocomplete suggestions ", e);
				}
			}
		}
		
	}
	
	private String getAddressText(Address address) {
		return address.getAddressLine(0);
	}

	@Override
	public void onFinishAutoCompleteAddress(List<Address> addresses) {
		LOGGER.debug(CLASS_NAME + "#onFinishAutoCompleteAddress(...)");
		
		autoCompleteAdapter.clear();
		lastSuggestedAddresses.clear();
		if (addresses == null) {
			LOGGER.error("Error: Addresses auto complete reutrned null.");
			return;
		}
		
		for (Address address : addresses) {
			String addressText = getAddressText(address);
			LOGGER.info(CLASS_NAME + "#onFinishAutoCompleteAddress(...) Address: " + addressText);
			autoCompleteAdapter.add(addressText);
			lastSuggestedAddresses.add(address);
		}
		
		autoCompleteAdapter.notifyDataSetChanged();
	}

	@Override
	public void onFinishGeocoding(List<Address> addresses) {
		// Do nothing
	}

	@Override
	public void onFinishReverseGeocoding(List<Address> addresses) {
		LOGGER.debug(CLASS_NAME + "#onFinishReverseGeocoding(...)");
		
		if (addresses == null || addresses.isEmpty()) {
			return;
		}
		
		// Gets the address info
		Address address = addresses.get(0);
		String addressText = getAddressText(address);
		double latitude = address.getLatitude();
		double longitude = address.getLongitude();
		LOGGER.info(CLASS_NAME + "#onFinishReverseGeocoding(...) " + addressText);
		
		// Updates UI
		mIsLocationKnown = true;
		actvLocation.setText(addressText);
		
		// Updates the mAction
		mLocation.setName(addressText);
		mLocation.setAddress(addressText);
		mLocation.setLatitude(latitude);
		mLocation.setLongitude(longitude);
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}

}

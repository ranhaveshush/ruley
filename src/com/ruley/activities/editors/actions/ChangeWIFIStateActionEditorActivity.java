package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeWIFIStateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeWIFIStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeWIFIStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgWifiState = null;
	
	private ChangeWIFIStateAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_wifi_state_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgWifiState = (RadioGroup) findViewById(R.id.rgWifiState);
		rgWifiState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (rgWifiState.getCheckedRadioButtonId()) {
				case R.id.rbWifiOn:
					action.setWantedWifiState(WifiManager.WIFI_STATE_ENABLED);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbWifiOff:
					action.setWantedWifiState(WifiManager.WIFI_STATE_DISABLED);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeWIFIStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeWIFIStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeWIFIStateAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedWifiState()) {
		case WifiManager.WIFI_STATE_ENABLED:
			rgWifiState.check(R.id.rbWifiOn);
			break;
		case WifiManager.WIFI_STATE_DISABLED:
			rgWifiState.check(R.id.rbWifiOff);
			break;
		}
		
		updateDescription(action, tvDescription);
	}

}

package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.NotificationAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class NotificationActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NotificationActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private EditText etNotificationTitle = null;

	private EditText etNotificationText = null;
	
	private CheckBox cbNotificationLights = null;
	
	private CheckBox cbNotificationSound = null;
	
	private CheckBox cbNotificationVibrate = null;
	
	private NotificationAction action = null;
	
	private OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			CheckBox checkbox = (CheckBox) view;
			boolean isChecked = checkbox.isChecked();
			switch (checkbox.getId()) {
			case R.id.cbNotificationLights:
				if (isChecked) {
					action.addFlag(Notification.FLAG_SHOW_LIGHTS);
				} else {
					action.removeFlag(Notification.FLAG_SHOW_LIGHTS);
				}
				break;
			case R.id.cbNotificationSound:
				if (isChecked) {
					action.addDefaults(Notification.DEFAULT_SOUND);
				} else {
					action.removeDefaults(Notification.DEFAULT_SOUND);
				}
				break;
			case R.id.cbNotificationVibrate:
				if (isChecked) {
					action.addDefaults(Notification.DEFAULT_VIBRATE);
				} else {
					action.removeDefaults(Notification.DEFAULT_VIBRATE);
				}
				break;
			}
			updateDescription(action, tvDescription);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_notification_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		etNotificationTitle = (EditText) findViewById(R.id.etNotificationTitle);
		etNotificationText = (EditText) findViewById(R.id.etNotificationText);
		cbNotificationLights = (CheckBox) findViewById(R.id.cbNotificationLights);
		cbNotificationSound = (CheckBox) findViewById(R.id.cbNotificationSound);
		cbNotificationVibrate = (CheckBox) findViewById(R.id.cbNotificationVibrate);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (NotificationAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (NotificationAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new NotificationAction();
		}
		
		setBaseAction(action);
		
		etNotificationTitle.setText(action.getTitle());
		etNotificationTitle.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				action.setTitle(s.toString());
				updateDescription(action, tvDescription);
				updateDynamicActionBar();
			}
		});
		etNotificationText.setText(action.getText());
		etNotificationText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				action.setText(s.toString());
				updateDescription(action, tvDescription);
				updateDynamicActionBar();
			}
		});
		
		boolean hasLights = action.hasFlags(Notification.FLAG_SHOW_LIGHTS);
		cbNotificationLights.setOnClickListener(onClickListener);
		cbNotificationLights.setChecked(hasLights);
		
		boolean hasSound = action.hasDefaults(Notification.DEFAULT_SOUND);
		cbNotificationSound.setOnClickListener(onClickListener);
		cbNotificationSound.setChecked(hasSound);
		
		boolean hasVibrate = action.hasDefaults(Notification.DEFAULT_VIBRATE);
		cbNotificationVibrate.setOnClickListener(onClickListener);
		cbNotificationVibrate.setChecked(hasVibrate);
		
		updateDescription(action, tvDescription);
		updateDynamicActionBar();
	}

}

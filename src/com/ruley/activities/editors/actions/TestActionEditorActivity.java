/**
 * 
 */
package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;

import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.TestAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TestActionEditorActivity extends BaseActionDynamicEditorActivity {
	
	private static final String CLASS_NAME = TestActionEditorActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TestActionEditorActivity.class.getName());
	
	private TestAction action = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (TestAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (TestAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new TestAction();
		}
		
		setBaseAction(action);
		
		Intent resInent = new Intent();
		resInent.putExtra(Config.Extra.EXTRA_ACTION, action);
		setResult(RESULT_OK, resInent);
		finish();
	}

}

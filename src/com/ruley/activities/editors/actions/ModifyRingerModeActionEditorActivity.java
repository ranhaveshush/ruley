package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ModifyRingerModeAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ModifyRingerModeActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyRingerModeActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgRingerMode = null;
	
	private ModifyRingerModeAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_modify_ringer_mode_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgRingerMode = (RadioGroup) findViewById(R.id.rgRingerMode);
		rgRingerMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbSilent:
					action.setWantedRingerMode(AudioManager.RINGER_MODE_SILENT);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbVibrate:
					action.setWantedRingerMode(AudioManager.RINGER_MODE_VIBRATE);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbNormal:
					action.setWantedRingerMode(AudioManager.RINGER_MODE_NORMAL);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ModifyRingerModeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ModifyRingerModeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ModifyRingerModeAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedRingerMode()) {
		case AudioManager.RINGER_MODE_SILENT:
			rgRingerMode.check(R.id.rbSilent);
			break;
		case AudioManager.RINGER_MODE_VIBRATE:
			rgRingerMode.check(R.id.rbVibrate);
			break;
		case AudioManager.RINGER_MODE_NORMAL:
			rgRingerMode.check(R.id.rbNormal);
			break;
		}
		
		updateDescription(action, tvDescription);
	}

}

package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ModifyMicrophoneMuteAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ModifyMicrophoneMuteActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyMicrophoneMuteActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgMicrophoneMute = null;
	
	private ModifyMicrophoneMuteAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_modify_microphone_mute_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgMicrophoneMute = (RadioGroup) findViewById(R.id.rgMicrophoneMute);
		rgMicrophoneMute.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbMicrophoneMuteOn:
					action.setWantedMicrophoneMute(true);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbMicrophoneMuteOff:
					action.setWantedMicrophoneMute(false);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ModifyMicrophoneMuteAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ModifyMicrophoneMuteAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ModifyMicrophoneMuteAction();
		}
		
		setBaseAction(action);
		
		if (action.isWantedMicrophoneMute()) {
			rgMicrophoneMute.check(R.id.rbMicrophoneMuteOn);
		} else {
			rgMicrophoneMute.check(R.id.rbMicrophoneMuteOff);
		}
	}

}

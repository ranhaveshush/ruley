package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeWIFIAPStateAction;
import com.ruley.model.beans.variables.WIFIAPVariable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeWIFIAPStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeWIFIAPStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private EditText etNetworkSSID = null;
	
	private EditText etNetworkPassword = null;
	
	private CheckBox cbNetworkSSIDVisibility = null;
	
	private RadioGroup rgWifiAPState = null;
	
	private ChangeWIFIAPStateAction mAction = null;

	private WIFIAPVariable mWifiAP = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_wifi_ap_state_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		etNetworkSSID = (EditText) findViewById(R.id.etNetworkSSID);
		etNetworkSSID.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mWifiAP.setNetworkSSID(s.toString());
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});
		etNetworkPassword = (EditText) findViewById(R.id.etNetworkPassword);
		etNetworkPassword.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mWifiAP.setNetworkPassword(s.toString());
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});
		cbNetworkSSIDVisibility = (CheckBox) findViewById(R.id.cbNetworkSSIDVisibility);
		cbNetworkSSIDVisibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				boolean isNetworkVisible = true;
				if (isChecked) {
					isNetworkVisible = false;
				}
				mWifiAP.setNetworkSSIDVisible(isNetworkVisible);
			}
		});
		rgWifiAPState = (RadioGroup) findViewById(R.id.rgWifiAPState);
		rgWifiAPState.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbWifiAPEnabled:
					mAction.setWantedWifiAPState(ChangeWIFIAPStateAction.WIFI_AP_ENABLED);
					updateDescription(mAction, tvDescription);
					break;
				case R.id.rbWifiAPDisabled:
					mAction.setWantedWifiAPState(ChangeWIFIAPStateAction.WIFI_AP_DISABLED);
					updateDescription(mAction, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (ChangeWIFIAPStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			mAction = (ChangeWIFIAPStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new ChangeWIFIAPStateAction();
		}
		
		mWifiAP  = mAction.getWifiAP();
		setBaseAction(mAction);
		
		etNetworkSSID.setText(mWifiAP.getNetworkSSID());
		etNetworkPassword.setText(mWifiAP.getNetworkPassword());
		cbNetworkSSIDVisibility.setChecked(!mWifiAP.isNetworkSSIDVisible());
		switch (mAction.getWantedWifiAPState()) {
		case ChangeWIFIAPStateAction.WIFI_AP_ENABLED:
			rgWifiAPState.check(R.id.rbWifiAPEnabled);
			break;
		case ChangeWIFIAPStateAction.WIFI_AP_DISABLED:
			rgWifiAPState.check(R.id.rbWifiAPDisabled);
			break;
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}

}

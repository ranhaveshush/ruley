package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ModifyAudioStreamSoloAction;
import com.ruley.utilities.AudioStreamsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ModifyAudioStreamSoloActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyAudioStreamSoloActionEditorActivity.class.getName());
	
	private TextView tvDescription = null;
	
	private Spinner sAudioStreamType = null;
	
	private RadioGroup rgAudioStreamSolo = null;
	
	private ModifyAudioStreamSoloAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_modify_audio_stream_solo_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		sAudioStreamType = (Spinner) findViewById(R.id.sAudioStreamType);
		sAudioStreamType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				int audioStreamType = AudioStreamsUtils.audioStreamsPosToTypeMap.get(position);
				action.setAudioStreamType(audioStreamType);
				updateDescription(action, tvDescription);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		rgAudioStreamSolo = (RadioGroup) findViewById(R.id.rgAudioStreamSolo);
		rgAudioStreamSolo.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbAudioStreamSolo:
					action.setAudioStreamSolo(ModifyAudioStreamSoloAction.AUDIO_STREAM_SOLO);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbAudioStreamUnsolo:
					action.setAudioStreamSolo(ModifyAudioStreamSoloAction.AUDIO_STREAM_UNSOLO);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ModifyAudioStreamSoloAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ModifyAudioStreamSoloAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ModifyAudioStreamSoloAction();
		}
		
		setBaseAction(action);
		
		int audioStreamType = action.getAudioStreamType();
		int audioStreamTypePos = AudioStreamsUtils.audioStreamsTypeToPosMap.get(audioStreamType);
		sAudioStreamType.setSelection(audioStreamTypePos);
		if (action.isAudioStreamSolo()) {
			rgAudioStreamSolo.check(R.id.rbAudioStreamSolo);
		} else {
			rgAudioStreamSolo.check(R.id.rbAudioStreamUnsolo);
		}
	}

}

package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeFlashlightStateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeFlashlightStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeFlashlightStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgFlashlightState = null;
	
	private ChangeFlashlightStateAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_flashlight_state_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgFlashlightState = (RadioGroup) findViewById(R.id.rgFlashlightState);
		rgFlashlightState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbFlashlightOn:
					action.setWantedFlashlightState(ChangeFlashlightStateAction.FLASHLIGHT_ON);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbFlashlightOff:
					action.setWantedFlashlightState(ChangeFlashlightStateAction.FLASHLIGHT_OFF);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeFlashlightStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeFlashlightStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeFlashlightStateAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedFlashlightState()) {
		case ChangeFlashlightStateAction.FLASHLIGHT_ON:
			rgFlashlightState.check(R.id.rbFlashlightOn);
			break;
		case ChangeFlashlightStateAction.FLASHLIGHT_OFF:
			rgFlashlightState.check(R.id.rbFlashlightOff);
			break;
		}
	}

}

package com.ruley.activities.editors.actions;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.VibrateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class VibrateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(VibrateActionEditorActivity.class.getName());
	
	private static final long MINUTE_IN_MILLIS = 1000 * 60;
	
	private Button bVibratePattern = null;
	
	private RadioGroup rgRepeat = null;
	
	private VibrateAction action = null;
	
	private boolean isRecording = false;
	
	private List<Long> vibratePattern = new ArrayList<Long>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_vibrate_action_editor);

		bVibratePattern = (Button) findViewById(R.id.bVibratePattern);
		bVibratePattern.setOnTouchListener(new OnTouchListener() {
			private long downStartTime;
			private long downEndTime;
			private long upEndTime;
			private long upStartTime;
			private long upTime;
			private long downTime;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (isRecording && event.getAction() == MotionEvent.ACTION_DOWN) {
					getVibrator().vibrate(MINUTE_IN_MILLIS);
					downStartTime = upEndTime =  System.currentTimeMillis();
					upTime = upEndTime - upStartTime;
					// First press
					if (upTime == upEndTime) {
						vibratePattern.add(0L);
					} else {
						vibratePattern.add(upTime);
					}
					return true;
				} else if (isRecording && event.getAction() == MotionEvent.ACTION_UP) {
					getVibrator().cancel();
					downEndTime = upStartTime = System.currentTimeMillis();
					downTime = downEndTime - downStartTime;
					vibratePattern.add(downTime);
					return true;
				}
				
				return false;
			}
		});
		
		rgRepeat = (RadioGroup) findViewById(R.id.rgRepeat);
		rgRepeat.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbRepeatOn:
					action.setRepeat(VibrateAction.REPEAT_ON);
					break;
				case R.id.rbRepeatOff:
					action.setRepeat(VibrateAction.REPEAT_OFF);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (VibrateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (VibrateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new VibrateAction();
		}
		
		setBaseAction(action);
		
		switch (action.getRepeat()) {
		case VibrateAction.REPEAT_ON:
			rgRepeat.check(R.id.rbRepeatOn);
			break;
		case VibrateAction.REPEAT_OFF:
			rgRepeat.check(R.id.rbRepeatOff);
			break;
		}
	}
	
	private Vibrator getVibrator() {
		Context appCtx = MyApplication.getContext();
		return (Vibrator) appCtx.getSystemService(Context.VIBRATOR_SERVICE);
	}
	
	private boolean toggleRecording() {
		if (isRecording) {
			return isRecording = false;
		} else {
			return isRecording = true;
		}
	}
	
	public boolean onMenuItemClick(MenuItem item) {
		if (toggleRecording()) {
			vibratePattern = new ArrayList<Long>();
		} else {
			Long[] objArray = vibratePattern.toArray(new Long[vibratePattern.size()]);
			long[] primitivesArray = new long[objArray.length];
			for (int i=0; i < objArray.length; ++i) {
				primitivesArray[i] = objArray[i];
			}
			action.setVibratePattern(primitivesArray);
		}
		return true;
	}

}

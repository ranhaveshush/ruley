package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeAirplaneModeAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeAirplaneModeActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeAirplaneModeActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgAirplaneMode = null;
	
	private ChangeAirplaneModeAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_airplane_mode_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgAirplaneMode = (RadioGroup) findViewById(R.id.rgAirplaneMode);
		rgAirplaneMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbOn:
					action.setWantedAirplaneMode(ChangeAirplaneModeAction.AIRPLANE_MODE_ON);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbOff:
					action.setWantedAirplaneMode(ChangeAirplaneModeAction.AIRPLANE_MODE_OFF);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeAirplaneModeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeAirplaneModeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeAirplaneModeAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedAirplaneMode()) {
		case ChangeAirplaneModeAction.AIRPLANE_MODE_ON:
			rgAirplaneMode.check(R.id.rbOn);
			break;
		case ChangeAirplaneModeAction.AIRPLANE_MODE_OFF:
			rgAirplaneMode.check(R.id.rbOff);
			break;
		}
	}

}

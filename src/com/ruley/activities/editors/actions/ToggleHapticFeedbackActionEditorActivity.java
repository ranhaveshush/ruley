package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ToggleHapticFeedbackAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ToggleHapticFeedbackActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ToggleHapticFeedbackActionEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private RadioGroup rgHapticFeedbackState = null;
	
	private ToggleHapticFeedbackAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_toggle_haptic_feedback_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgHapticFeedbackState = (RadioGroup) findViewById(R.id.rgHapticFeedbackState);
		rgHapticFeedbackState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbHapticFeedbackOn:
					action.setWantedHapticFeedbackState(ToggleHapticFeedbackAction.HAPTIC_FEEDBACK_ENABLED);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbHapticFeedbackOff:
					action.setWantedHapticFeedbackState(ToggleHapticFeedbackAction.HAPTIC_FEEDBACK_DISABLED);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ToggleHapticFeedbackAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ToggleHapticFeedbackAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ToggleHapticFeedbackAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedHapticFeedbackState()) {
		case ToggleHapticFeedbackAction.HAPTIC_FEEDBACK_ENABLED:
			rgHapticFeedbackState.check(R.id.rbHapticFeedbackOn);
			break;
		case ToggleHapticFeedbackAction.HAPTIC_FEEDBACK_DISABLED:
			rgHapticFeedbackState.check(R.id.rbHapticFeedbackOff);
			break;
		}
	}

}

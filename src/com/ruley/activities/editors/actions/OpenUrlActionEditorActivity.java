package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.OpenUrlWakeAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class OpenUrlActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OpenUrlActionEditorActivity.class.getName());

	private TextView tvDescription = null;

	private EditText etUrl = null;

	private OpenUrlWakeAction mAction = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_open_url_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		etUrl = (EditText) findViewById(R.id.etUrl);
		etUrl.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				String url = s.toString();
				if (!hasHttpScheme(url)) {
					url = OpenUrlWakeAction.HTTP_PROTOCOL + url;
				}
				mAction.setUrl(url);
				updateDescription(mAction, tvDescription);
				updateDynamicActionBar();
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (OpenUrlWakeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			mAction = (OpenUrlWakeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new OpenUrlWakeAction();
		}
		
		setBaseAction(mAction);

		etUrl.setText(mAction.getUrl());

		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	private boolean hasHttpScheme(String url) {
		return url.startsWith(OpenUrlWakeAction.HTTP_PROTOCOL)
		|| url.startsWith(OpenUrlWakeAction.HTTPS_PROTOCOL);
	}

}

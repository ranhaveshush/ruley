package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.PhoneCallAction;
import com.ruley.model.beans.variables.ContactVariable;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PhoneCallActionEditorActivity extends BaseActionDynamicEditorActivity {
	
	private static final String CLASS_NAME = PhoneCallActionEditorActivity.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PhoneCallActionEditorActivity.class.getName());

	private static final int ADD_CONTACT_REQUEST_CODE = 6101;

	private TextView tvDescription = null;
	
	private TextView tvContactName = null;
	
	private PhoneCallAction mAction = null;
	
	private ContactVariable mContactVariable = null;
	
	private Drawable mAppContactIcon = null;
	
	private int mIconPadding = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_phone_call_action_editor);
		
		mIconPadding = (int) getResources().getDimension(R.dimen.icon_padding);
		mAppContactIcon = ImageUtils.getDrawable(R.drawable.ic_action_add_contact);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvContactName = (TextView) findViewById(R.id.tvContactName);
		tvContactName.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PickersUtils.launchContactPicker(PhoneCallActionEditorActivity.this, ADD_CONTACT_REQUEST_CODE);
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mAction = (PhoneCallAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing an existing instance
		} else if (extras != null) {
			mAction = (PhoneCallAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			mAction = new PhoneCallAction();
		}
		
		mContactVariable = mAction.getContact();
		setBaseAction(mAction);

		String contactId = mContactVariable.getContactId();
		if (!TextUtils.isEmpty(contactId)) {
			updateContactInfo(contactId);
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug("onActivityResult(...)");

		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case ADD_CONTACT_REQUEST_CODE:
				Uri contactUri = data.getData();
				String contactId = contactUri.getLastPathSegment();
				mContactVariable.setContactId(contactId);
				updateContactInfo(contactId);
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
		
		updateDescription(mAction, tvDescription);
		updateDynamicActionBar();
	}
	
	private void updateContactInfo(String contactId) {
		String contactName = ContactsUtils.getContactName(PhoneCallActionEditorActivity.this, contactId);
		Bitmap contactPhoto = ContactsUtils.getContactPhoto(PhoneCallActionEditorActivity.this, contactId);
		Drawable drawable = ImageUtils.toDrawable(contactPhoto);
		tvContactName.setCompoundDrawablePadding(mIconPadding);
		tvContactName.setCompoundDrawablesWithIntrinsicBounds(drawable, null, mAppContactIcon, null);
		tvContactName.setText(contactName);
	}

}
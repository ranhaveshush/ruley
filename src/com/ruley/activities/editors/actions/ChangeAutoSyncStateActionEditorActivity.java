package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ChangeAutoSyncStateAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ChangeAutoSyncStateActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeAutoSyncStateActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	private RadioGroup rgAutoSyncState = null;
	
	private ChangeAutoSyncStateAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_change_auto_sync_state_action_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgAutoSyncState = (RadioGroup) findViewById(R.id.rgAutoSyncState);
		rgAutoSyncState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbAutoSyncOn:
					action.setWantedAutoSyncState(ChangeAutoSyncStateAction.AUTO_SYNC_ON);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbAutoSyncOff:
					action.setWantedAutoSyncState(ChangeAutoSyncStateAction.AUTO_SYNC_OFF);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ChangeAutoSyncStateAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ChangeAutoSyncStateAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ChangeAutoSyncStateAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedAutoSyncState()) {
		case ChangeAutoSyncStateAction.AUTO_SYNC_ON:
			rgAutoSyncState.check(R.id.rbAutoSyncOn);
			break;
		case ChangeAutoSyncStateAction.AUTO_SYNC_OFF:
			rgAutoSyncState.check(R.id.rbAutoSyncOff);
			break;
		}
	}

}

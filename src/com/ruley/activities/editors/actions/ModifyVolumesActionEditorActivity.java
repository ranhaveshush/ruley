package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.ModifyVolumesAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class ModifyVolumesActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ModifyVolumesActionEditorActivity.class.getName());

	private TextView tvDescription = null;
	private TextView tvAlarmVolume = null;
	private TextView tvDTMFVolume = null;
	private TextView tvMusicVolume = null;
	private TextView tvNotificationVolume = null;
	private TextView tvRingVolume = null;
	private TextView tvSystemVolume = null;
	private TextView tvVoiceVolume = null;

	private SeekBar sbAlarmVolume = null;
	private SeekBar sbDTMFVolume = null;
	private SeekBar sbMusicVolume = null;
	private SeekBar sbNotificationVolume = null;
	private SeekBar sbRingVolume = null;
	private SeekBar sbSystemVolume = null;
	private SeekBar sbVoiceVolume = null;
	
	private ModifyVolumesAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_modify_volume_action_editor);

		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

		int maxVolume;

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvAlarmVolume = (TextView) findViewById(R.id.tvAlarmVolume);
		sbAlarmVolume = (SeekBar) findViewById(R.id.sbAlarmVolume);
		sbAlarmVolume.setMax(maxVolume);
		sbAlarmVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_ALARM, sbAlarmVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvAlarmVolume.setText(String.valueOf(progress));
			}
		});

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_DTMF);
		tvDTMFVolume = (TextView) findViewById(R.id.tvDTMFVolume);
		sbDTMFVolume = (SeekBar) findViewById(R.id.sbDTMFVolume);
		sbDTMFVolume.setMax(maxVolume);
		sbDTMFVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_DTMF, sbDTMFVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvDTMFVolume.setText(String.valueOf(progress));
			}
		});

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		tvMusicVolume = (TextView) findViewById(R.id.tvMusicVolume);
		sbMusicVolume = (SeekBar) findViewById(R.id.sbMusicVolume);
		sbMusicVolume.setMax(maxVolume);
		sbMusicVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_MUSIC, sbMusicVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvMusicVolume.setText(String.valueOf(progress));
			}
		});

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION);
		tvNotificationVolume = (TextView) findViewById(R.id.tvNotificationVolume);
		sbNotificationVolume = (SeekBar) findViewById(R.id.sbNotificationVolume);
		sbNotificationVolume.setMax(maxVolume);
		sbNotificationVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_NOTIFICATION, sbNotificationVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvNotificationVolume.setText(String.valueOf(progress));
			}
		});

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
		tvRingVolume = (TextView) findViewById(R.id.tvRingVolume);
		sbRingVolume = (SeekBar) findViewById(R.id.sbRingVolume);
		sbRingVolume.setMax(maxVolume);
		sbRingVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_RING, sbRingVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvRingVolume.setText(String.valueOf(progress));
			}
		});

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
		tvSystemVolume = (TextView) findViewById(R.id.tvSystemVolume);
		sbSystemVolume = (SeekBar) findViewById(R.id.sbSystemVolume);
		sbSystemVolume.setMax(maxVolume);
		sbSystemVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_SYSTEM, sbSystemVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvSystemVolume.setText(String.valueOf(progress));
			}
		});

		maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL);
		tvVoiceVolume = (TextView) findViewById(R.id.tvVoiceCallVolume);
		sbVoiceVolume = (SeekBar) findViewById(R.id.sbVoiceCallVolume);
		sbVoiceVolume.setMax(maxVolume);
		sbVoiceVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				action.setVolume(AudioManager.STREAM_VOICE_CALL, sbVoiceVolume.getProgress());
				updateDescription(action, tvDescription);
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				tvVoiceVolume.setText(String.valueOf(progress));
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (ModifyVolumesAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (ModifyVolumesAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new ModifyVolumesAction();
		}
		
		setBaseAction(action);
		
		int alarmVolume = action.getVolume(AudioManager.STREAM_ALARM);
		tvAlarmVolume.setText(String.valueOf(alarmVolume));
		sbAlarmVolume.setProgress(alarmVolume);
		int dtmfVolume = action.getVolume(AudioManager.STREAM_DTMF);
		tvDTMFVolume.setText(String.valueOf(dtmfVolume));
		sbDTMFVolume.setProgress(dtmfVolume);
		int musicVolume = action.getVolume(AudioManager.STREAM_MUSIC);
		tvMusicVolume.setText(String.valueOf(musicVolume));
		sbMusicVolume.setProgress(musicVolume);
		int notificationVolume = action.getVolume(AudioManager.STREAM_NOTIFICATION);
		tvNotificationVolume.setText(String.valueOf(notificationVolume));
		sbNotificationVolume.setProgress(notificationVolume);
		int ringVolume = action.getVolume(AudioManager.STREAM_RING);
		tvRingVolume.setText(String.valueOf(ringVolume));
		sbRingVolume.setProgress(ringVolume);
		int systemVolume = action.getVolume(AudioManager.STREAM_SYSTEM);
		tvSystemVolume.setText(String.valueOf(systemVolume));
		sbSystemVolume.setProgress(systemVolume);
		int voiceCallVolume = action.getVolume(AudioManager.STREAM_VOICE_CALL);
		tvVoiceVolume.setText(String.valueOf(voiceCallVolume));
		sbVoiceVolume.setProgress(voiceCallVolume);
		// TODO: get flags input from the user
		//int flags = action.getFlags();
		
		updateDescription(action, tvDescription);
	}

}

package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.WakeLockAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
// TODO: this action is very dangerous power consumption.
public class WakeLockActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WakeLockActionEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private RadioGroup rgWakeLockFlags = null;
	
	private WakeLockAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_wake_lock_action_editor);

		rgWakeLockFlags = (RadioGroup) findViewById(R.id.rgWakeLockFlags);
		rgWakeLockFlags.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbWakeLockPartial:
					action.setWantedLevelAndFlags(PowerManager.PARTIAL_WAKE_LOCK);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbWakeLockScreenDim:
					action.setWantedLevelAndFlags(PowerManager.SCREEN_DIM_WAKE_LOCK);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbWakeLockScreenBright:
					action.setWantedLevelAndFlags(PowerManager.SCREEN_BRIGHT_WAKE_LOCK);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbWakeLockFull:
					action.setWantedLevelAndFlags(PowerManager.FULL_WAKE_LOCK);
					updateDescription(action, tvDescription);
					break;
				default:
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (WakeLockAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (WakeLockAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new WakeLockAction();
		}
		
		setBaseAction(action);
		
		switch (action.getWantedLevelAndFlags()) {
		case PowerManager.PARTIAL_WAKE_LOCK:
			rgWakeLockFlags.check(R.id.rbWakeLockPartial);
			break;
		case PowerManager.SCREEN_DIM_WAKE_LOCK:
			rgWakeLockFlags.check(R.id.rbWakeLockScreenDim);
			break;
		case PowerManager.SCREEN_BRIGHT_WAKE_LOCK:
			rgWakeLockFlags.check(R.id.rbWakeLockScreenBright);
			break;
		case PowerManager.FULL_WAKE_LOCK:
			rgWakeLockFlags.check(R.id.rbWakeLockFull);
			break;
		default:
			break;
		}
	}

}

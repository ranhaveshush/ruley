package com.ruley.activities.editors.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.actions.base.BaseActionDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.actions.TakeVideoWakeAction;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TakeVideoActionEditorActivity extends BaseActionDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TakeVideoActionEditorActivity.class.getName());
	
	private TextView tvDescription = null;
	
	private RadioGroup rgCameraPreview = null;
	
	private RadioGroup rgCameraFacing = null;
	
	private RadioButton rbCameraFacingBack = null;

	private RadioButton rbCameraFacingFront = null;

	private TakeVideoWakeAction action = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_take_video_action_editor);
		
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgCameraPreview = (RadioGroup) findViewById(R.id.rgCameraPreview);
		rgCameraPreview.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbCameraPreviewOn:
					action.setPreview(true);
					action.setCameraFacing(CameraInfo.CAMERA_FACING_BACK);
					rgCameraFacing.check(R.id.rbCameraFacingBack);
					rbCameraFacingBack.setEnabled(false);
					rbCameraFacingFront.setEnabled(false);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbCameraPreviewOff:
					action.setPreview(false);
					rbCameraFacingBack.setEnabled(true);
					rbCameraFacingFront.setEnabled(true);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});
		rgCameraFacing = (RadioGroup) findViewById(R.id.rgCameraFacing);
		rgCameraFacing.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbCameraFacingFront:
					action.setCameraFacing(CameraInfo.CAMERA_FACING_FRONT);
					updateDescription(action, tvDescription);
					break;
				case R.id.rbCameraFacingBack:
					action.setCameraFacing(CameraInfo.CAMERA_FACING_BACK);
					updateDescription(action, tvDescription);
					break;
				}
			}
		});
		rbCameraFacingBack = (RadioButton) findViewById(R.id.rbCameraFacingBack);
		rbCameraFacingFront = (RadioButton) findViewById(R.id.rbCameraFacingFront);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			action = (TakeVideoWakeAction) savedInstanceState.getSerializable(Config.Extra.EXTRA_ACTION);
		// If editing existing instance
		} else if (extras != null) {
			action = (TakeVideoWakeAction) extras.getSerializable(Config.Extra.EXTRA_ACTION);
		// If creating a new instance
		} else {
			action = new TakeVideoWakeAction();
		}
		
		setBaseAction(action);
		
		if (action.isPreview()) {
			rgCameraPreview.check(R.id.rbCameraPreviewOn);
			rbCameraFacingBack.setEnabled(false);
			rbCameraFacingFront.setEnabled(false);
		} else {
			rgCameraPreview.check(R.id.rbCameraPreviewOff);
			rbCameraFacingBack.setEnabled(true);
			rbCameraFacingFront.setEnabled(true);
		}
		switch (action.getCameraFacing()) {
		case CameraInfo.CAMERA_FACING_FRONT:
			rgCameraFacing.check(R.id.rbCameraFacingFront);
			break;
		case CameraInfo.CAMERA_FACING_BACK:
			rgCameraFacing.check(R.id.rbCameraFacingBack);
			break;
		}
		
		updateDescription(action, tvDescription);
	}

}

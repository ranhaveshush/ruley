/**
 * 
 */
package com.ruley.activities.editors.base;

import android.widget.TextView;

import com.ruley.activities.base.GABaseActivity;
import com.ruley.model.beans.rules.IRuleComponent;


/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class BaseEditorActivity extends GABaseActivity {
	
	protected void updateDescription(IRuleComponent ruleComponent, TextView tvDescription) {
		String description = ruleComponent.getDescription(BaseEditorActivity.this);
		tvDescription.setText(description);
	}
	
}

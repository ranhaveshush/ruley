/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.BatteryStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BatteryStateTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BatteryStateTriggerEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private RadioGroup rgBatteryState = null;
	
	private BatteryStateTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_battery_state_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgBatteryState = (RadioGroup) findViewById(R.id.rgBatteryState);
		rgBatteryState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbBatteryLow:
					trigger.setWantedBatteryState(BatteryStateTrigger.BATTERY_LOW);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbBatteryOk:
					trigger.setWantedBatteryState(BatteryStateTrigger.BATTERY_OK);
					updateDescription(trigger, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (BatteryStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (BatteryStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new BatteryStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		switch (trigger.getWantedBatteryState()) {
		case BatteryStateTrigger.BATTERY_LOW:
			rgBatteryState.check(R.id.rbBatteryLow);
			break;
		case BatteryStateTrigger.BATTERY_OK:
			rgBatteryState.check(R.id.rbBatteryOk);
			break;
		}
	}

}

package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;

import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.BootCompletedTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BootCompletedTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BootCompletedTriggerEditorActivity.class.getName());
	
	private BootCompletedTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LOGGER.debug("onCreate(...)");

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (BootCompletedTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (BootCompletedTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new BootCompletedTrigger();
		}
		
		setBaseTrigger(trigger);
		
		Intent resInent = new Intent();
		resInent.putExtra(Config.Extra.EXTRA_TRIGGER, trigger);
		setResult(RESULT_OK, resInent);
		finish();
	}

}

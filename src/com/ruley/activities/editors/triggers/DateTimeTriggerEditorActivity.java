/**
 * 
 */
package com.ruley.activities.editors.triggers;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.activities.pickers.DatePickerFragment;
import com.ruley.activities.pickers.TimePickerFragment;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.DateTimeTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DateTimeTriggerEditorActivity extends BaseTriggerDynamicEditorActivity implements TimePickerFragment.Listener, DatePickerFragment.Listener {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateTimeTriggerEditorActivity.class.getName());

	private static final String FRAGMENT_TAG_TIME_PICKER = "ReminderTimePicker";
	private static final String FRAGMENT_TAG_DATE_PICKER = "ReminderDatePicker";
	
	private TextView tvDescription = null;
	
	private Button bTime = null;
	private Button bDate = null;
	
	private CheckBox cbRepeat = null;
	
	private CheckBox cbSunday = null;
	private CheckBox cbMonday = null;
	private CheckBox cbTuesday = null;
	private CheckBox cbWednesday = null;
	private CheckBox cbThursday = null;
	private CheckBox cbFriday = null;
	private CheckBox cbSaturday = null;

	private DateTimeTrigger trigger = null;
	
	private OnCheckedChangeListener dayOfTheWeekCheckedChanged = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			int dayOfTheWeekIndex = (Integer) buttonView.getTag();
			trigger.setRepeatDayOfTheWeek(dayOfTheWeekIndex, isChecked);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_date_time_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		bTime = (Button) findViewById(R.id.bTime);
		bDate = (Button) findViewById(R.id.bDate);
		
		cbSunday = (CheckBox) findViewById(R.id.cbSunday);
		cbSunday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbSunday.setTag(0);
		
		cbMonday = (CheckBox) findViewById(R.id.cbMonday);
		cbMonday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbMonday.setTag(1);
		
		cbTuesday = (CheckBox) findViewById(R.id.cbTuesday);
		cbTuesday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbTuesday.setTag(2);
		
		cbWednesday = (CheckBox) findViewById(R.id.cbWednesday);
		cbWednesday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbWednesday.setTag(3);
		
		cbThursday = (CheckBox) findViewById(R.id.cbThursday);
		cbThursday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbThursday.setTag(4);
		
		cbFriday = (CheckBox) findViewById(R.id.cbFriday);
		cbFriday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbFriday.setTag(5);
		
		cbSaturday = (CheckBox) findViewById(R.id.cbSaturday);
		cbSaturday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbSaturday.setTag(6);
		
		cbRepeat = (CheckBox) findViewById(R.id.cbRepeat);
		cbRepeat.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					trigger.setRepeat(true);
					cbSunday.setEnabled(true);
					cbMonday.setEnabled(true);
					cbTuesday.setEnabled(true);
					cbWednesday.setEnabled(true);
					cbThursday.setEnabled(true);
					cbFriday.setEnabled(true);
					cbSaturday.setEnabled(true);
					bDate.setVisibility(View.GONE);
				} else {
					trigger.setRepeat(false);
					cbSunday.setEnabled(false);
					cbMonday.setEnabled(false);
					cbTuesday.setEnabled(false);
					cbWednesday.setEnabled(false);
					cbThursday.setEnabled(false);
					cbFriday.setEnabled(false);
					cbSaturday.setEnabled(false);
					bDate.setVisibility(View.VISIBLE);
				}
				updateDescription(trigger, tvDescription);
				updateDynamicActionBar();
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (DateTimeTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (DateTimeTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new DateTimeTrigger();
		}
		
		setBaseTrigger(trigger);
		
		GregorianCalendar dateTime = trigger.getDateTime();
		
		String timeStr = DateTimeTrigger.SIMPLE_TIME_FORMAT.format(dateTime.getTime());
		bTime.setText(timeStr);
		bTime.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Calls datetime picker fragment dialog
				Calendar dateTime = trigger.getDateTime();
				if (dateTime == null) {
					dateTime = GregorianCalendar.getInstance(DateTimeTrigger.LOCALE);
				}
				int hourOfDay = dateTime.get(Calendar.HOUR_OF_DAY);
				int minute = dateTime.get(Calendar.MINUTE);
				DialogFragment timePickerFragment = TimePickerFragment.newInstance(hourOfDay, minute);
				timePickerFragment.show(getFragmentManager(), FRAGMENT_TAG_TIME_PICKER);
			}
		});
		String dateStr = DateTimeTrigger.SIMPLE_DATE_FORMAT.format(dateTime.getTime());
		bDate.setText(dateStr);
		bDate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Calls datetime picker fragment dialog
				Calendar dateTime = trigger.getDateTime();
				if (dateTime == null) {
					dateTime = GregorianCalendar.getInstance(DateTimeTrigger.LOCALE);
				}
				int year = dateTime.get(Calendar.YEAR);
				int month = dateTime.get(Calendar.MONTH);
				int dayOfMoth = dateTime.get(Calendar.DAY_OF_MONTH);
				DialogFragment datePickerFragment = DatePickerFragment.newInstance(year, month, dayOfMoth);
				datePickerFragment.show(getFragmentManager(), FRAGMENT_TAG_DATE_PICKER);
			}
		});
		
		if (trigger.isRepeat()) {
			cbRepeat.setChecked(true);
			setSelectedDaysOfTheWeek();
		}
		
		updateDescription(trigger, tvDescription);
	}
	
	@Override
	public void onTimeOn(String fragmentTag, int hourOfDay, int minute) {
		
		if (FRAGMENT_TAG_TIME_PICKER.equals(fragmentTag)) {
			GregorianCalendar dateTime = trigger.getDateTime();
			dateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTime.set(Calendar.MINUTE, minute);
			String timeStr = DateTimeTrigger.SIMPLE_TIME_FORMAT.format(dateTime.getTime());
			bTime.setText(timeStr);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		}
	}

	@Override
	public void onTimeOff(String fragmentTag) {
	}

	@Override
	public void onDateOn(String fragmentTag, int year, int monthOfYear, int dayOfMonth) {
		
		if (FRAGMENT_TAG_DATE_PICKER.equals(fragmentTag)) {
			GregorianCalendar dateTime = trigger.getDateTime();
			dateTime.set(Calendar.YEAR, year);
			dateTime.set(Calendar.MONTH, monthOfYear);
			dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			String dateStr = DateTimeTrigger.SIMPLE_DATE_FORMAT.format(dateTime.getTime());
			bDate.setText(dateStr);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		}
	}

	@Override
	public void onDateOff(String fragmentTag) {
	}
	
	/**
	 * Checks the match checkbox of the selected repeat days.
	 */
	private void setSelectedDaysOfTheWeek() {
		for (int i=0; i < 7; ++i) {
			if (trigger.isRepeatDayOfTheWeek(i)) {
				switch (i) {
				case 0:
					cbSunday.setChecked(true);
					break;
				case 1:
					cbMonday.setChecked(true);
					break;
				case 2:
					cbTuesday.setChecked(true);
					break;
				case 3:
					cbWednesday.setChecked(true);
					break;
				case 4:
					cbThursday.setChecked(true);
					break;
				case 5:
					cbFriday.setChecked(true);
					break;
				case 6:
					cbSaturday.setChecked(true);
					break;
				}
			}
		}
	}

}

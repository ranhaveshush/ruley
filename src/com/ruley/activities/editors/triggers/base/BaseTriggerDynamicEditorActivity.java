/**
 * 
 */
package com.ruley.activities.editors.triggers.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ruley.R;
import com.ruley.activities.editors.base.BaseEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.base.Trigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BaseTriggerDynamicEditorActivity extends BaseEditorActivity {
	
	private static final String CLASS_NAME = BaseTriggerDynamicEditorActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseTriggerDynamicEditorActivity.class.getName());
	
	private Trigger mBaseTrigger = null;
	
	public Trigger getBaseTrigger() {
		return mBaseTrigger;
	}

	public void setBaseTrigger(Trigger trigger) {
		mBaseTrigger = trigger;
	}
	
	public void updateDynamicActionBar() {
		mDynamicActionBar.update(BaseTriggerDynamicEditorActivity.this);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		
		outState.putSerializable(Config.Extra.EXTRA_TRIGGER, mBaseTrigger);
		
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");
		
		getMenuInflater().inflate(R.menu.activity_trigger_editor_menu, menu);
		MenuItem actionSaveTrigger = menu.findItem(R.id.action_save_trigger);
		List<MenuItem> menuItems = new ArrayList<MenuItem>(Arrays.asList(actionSaveTrigger));
		mDynamicActionBar.setValidatable(mBaseTrigger);
		mDynamicActionBar.setMenuItems(menuItems);
		mDynamicActionBar.update(BaseTriggerDynamicEditorActivity.this);
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");

		switch (item.getItemId()) {
		case R.id.action_save_trigger:
			Intent resInent = new Intent();
			resInent.putExtra(Config.Extra.EXTRA_TRIGGER, mBaseTrigger);
			setResult(RESULT_OK, resInent);
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}

/**
 * 
 */
package com.ruley.activities.editors.triggers;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.activities.pickers.DatePickerFragment;
import com.ruley.activities.pickers.TimePickerFragment;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.DateTimeRangeStateTrigger;
import com.ruley.model.beans.triggers.DateTimeTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DateTimeRangeStateTriggerEditorActivity extends BaseTriggerDynamicEditorActivity implements TimePickerFragment.Listener, DatePickerFragment.Listener {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateTimeRangeStateTriggerEditorActivity.class.getName());

	private static final String FRAGMENT_TAG_TIME_START_PICKER = "TimeStartPicker";
	private static final String FRAGMENT_TAG_DATE_START_PICKER = "DateStartPicker";
	private static final String FRAGMENT_TAG_TIME_END_PICKER = "TimeEndPicker";
	private static final String FRAGMENT_TAG_DATE_END_PICKER = "DateEndPicker";
	
	private TextView tvDescription = null;
	
	private Button bTimeStart = null;
	private Button bDateStart = null;
	
	private Button bTimeEnd = null;
	private Button bDateEnd = null;
	
	private CheckBox cbRepeat = null;
	
	private CheckBox cbSunday = null;
	private CheckBox cbMonday = null;
	private CheckBox cbTuesday = null;
	private CheckBox cbWednesday = null;
	private CheckBox cbThursday = null;
	private CheckBox cbFriday = null;
	private CheckBox cbSaturday = null;

	private DateTimeRangeStateTrigger trigger = null;
	
	private OnCheckedChangeListener dayOfTheWeekCheckedChanged = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			int dayOfTheWeekIndex = (Integer) buttonView.getTag();
			trigger.setRepeatDayOfTheWeek(dayOfTheWeekIndex, isChecked);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_date_time_range_state_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		
		bTimeStart = (Button) findViewById(R.id.bTimeStart);
		bDateStart = (Button) findViewById(R.id.bDateStart);
		
		bTimeEnd = (Button) findViewById(R.id.bTimeEnd);
		bDateEnd = (Button) findViewById(R.id.bDateEnd);
		
		cbSunday = (CheckBox) findViewById(R.id.cbSunday);
		cbSunday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbSunday.setTag(0);
		
		cbMonday = (CheckBox) findViewById(R.id.cbMonday);
		cbMonday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbMonday.setTag(1);
		
		cbTuesday = (CheckBox) findViewById(R.id.cbTuesday);
		cbTuesday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbTuesday.setTag(2);
		
		cbWednesday = (CheckBox) findViewById(R.id.cbWednesday);
		cbWednesday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbWednesday.setTag(3);
		
		cbThursday = (CheckBox) findViewById(R.id.cbThursday);
		cbThursday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbThursday.setTag(4);
		
		cbFriday = (CheckBox) findViewById(R.id.cbFriday);
		cbFriday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbFriday.setTag(5);
		
		cbSaturday = (CheckBox) findViewById(R.id.cbSaturday);
		cbSaturday.setOnCheckedChangeListener(dayOfTheWeekCheckedChanged);
		cbSaturday.setTag(6);
		
		cbRepeat = (CheckBox) findViewById(R.id.cbRepeat);
		cbRepeat.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					trigger.setRepeat(true);
					cbSunday.setEnabled(true);
					cbMonday.setEnabled(true);
					cbTuesday.setEnabled(true);
					cbWednesday.setEnabled(true);
					cbThursday.setEnabled(true);
					cbFriday.setEnabled(true);
					cbSaturday.setEnabled(true);
					bDateStart.setVisibility(View.GONE);
					bDateEnd.setVisibility(View.GONE);
				} else {
					trigger.setRepeat(false);
					cbSunday.setEnabled(false);
					cbMonday.setEnabled(false);
					cbTuesday.setEnabled(false);
					cbWednesday.setEnabled(false);
					cbThursday.setEnabled(false);
					cbFriday.setEnabled(false);
					cbSaturday.setEnabled(false);
					bDateStart.setVisibility(View.VISIBLE);
					bDateEnd.setVisibility(View.VISIBLE);
				}
				updateDescription(trigger, tvDescription);
				updateDynamicActionBar();
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (DateTimeRangeStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (DateTimeRangeStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new DateTimeRangeStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		GregorianCalendar dateTimeStart = trigger.getDateTimeStart();
		GregorianCalendar dateTimeEnd = trigger.getDateTimeEnd();
		
		String timeStartStr = DateTimeRangeStateTrigger.SIMPLE_TIME_FORMAT.format(dateTimeStart.getTime());
		String dateStartStr = DateTimeRangeStateTrigger.SIMPLE_DATE_FORMAT.format(dateTimeStart.getTime());
		String timeEndStr = DateTimeRangeStateTrigger.SIMPLE_TIME_FORMAT.format(dateTimeEnd.getTime());
		String dateEndStr = DateTimeRangeStateTrigger.SIMPLE_DATE_FORMAT.format(dateTimeEnd.getTime());
		
		bTimeStart.setText(timeStartStr);
		bTimeStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Calls datetime picker fragment dialog
				Calendar dateTimeStart = trigger.getDateTimeStart();
				if (dateTimeStart == null) {
					dateTimeStart = GregorianCalendar.getInstance(DateTimeRangeStateTrigger.LOCALE);
				}
				int hour = dateTimeStart.get(Calendar.HOUR_OF_DAY);
				int minute = dateTimeStart.get(Calendar.MINUTE);
				DialogFragment timePickerFragment = TimePickerFragment.newInstance(hour, minute);
				timePickerFragment.show(getFragmentManager(), FRAGMENT_TAG_TIME_START_PICKER);
			}
		});
		bDateStart.setText(dateStartStr);
		bDateStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Calls datetime picker fragment dialog
				Calendar dateTimeStart = trigger.getDateTimeStart();
				if (dateTimeStart == null) {
					dateTimeStart = GregorianCalendar.getInstance(DateTimeRangeStateTrigger.LOCALE);
				}
				int year = dateTimeStart.get(Calendar.YEAR);
				int month = dateTimeStart.get(Calendar.MONTH);
				int dayOfMonth = dateTimeStart.get(Calendar.DAY_OF_MONTH);
				DialogFragment datePickerFragment = DatePickerFragment.newInstance(year, month, dayOfMonth);
				datePickerFragment.show(getFragmentManager(), FRAGMENT_TAG_DATE_START_PICKER);
			}
		});
		bTimeEnd.setText(timeEndStr);
		bTimeEnd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Calls datetime picker fragment dialog
				Calendar dateTimeEnd = trigger.getDateTimeEnd();
				if (dateTimeEnd == null) {
					dateTimeEnd = GregorianCalendar.getInstance(DateTimeRangeStateTrigger.LOCALE);
				}
				int hour = dateTimeEnd.get(Calendar.HOUR_OF_DAY);
				int minute = dateTimeEnd.get(Calendar.MINUTE);
				DialogFragment timePickerFragment = TimePickerFragment.newInstance(hour, minute);
				timePickerFragment.show(getFragmentManager(), FRAGMENT_TAG_TIME_END_PICKER);
			}
		});
		bDateEnd.setText(dateEndStr);
		bDateEnd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Calls datetime picker fragment dialog
				Calendar dateTimeEnd = trigger.getDateTimeEnd();
				if (dateTimeEnd == null) {
					dateTimeEnd = GregorianCalendar.getInstance(DateTimeRangeStateTrigger.LOCALE);
				}
				int year = dateTimeEnd.get(Calendar.YEAR);
				int month = dateTimeEnd.get(Calendar.MONTH);
				int dayOfMonth = dateTimeEnd.get(Calendar.DAY_OF_MONTH);
				DialogFragment datePickerFragment = DatePickerFragment.newInstance(year, month, dayOfMonth);
				datePickerFragment.show(getFragmentManager(), FRAGMENT_TAG_DATE_END_PICKER);
			}
		});
		
		if (trigger.isRepeat()) {
			cbRepeat.setChecked(true);
			setSelectedDaysOfTheWeek();
		}
		
		updateDescription(trigger, tvDescription);
	}

	@Override
	public void onTimeOn(String fragmentTag, int hourOfDay, int minute) {
		
		if (FRAGMENT_TAG_TIME_START_PICKER.equals(fragmentTag)) {
			GregorianCalendar dateTimeStart = trigger.getDateTimeStart();
			dateTimeStart.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTimeStart.set(Calendar.MINUTE, minute);
			dateTimeStart.set(Calendar.SECOND, 0);
			dateTimeStart.set(Calendar.MILLISECOND, 0);
			String timeStr = DateTimeTrigger.SIMPLE_TIME_FORMAT.format(dateTimeStart.getTime());
			bTimeStart.setText(timeStr);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		} else if (FRAGMENT_TAG_TIME_END_PICKER.equals(fragmentTag)) {
			GregorianCalendar dateTimeEnd = trigger.getDateTimeEnd();
			dateTimeEnd.set(Calendar.HOUR_OF_DAY, hourOfDay);
			dateTimeEnd.set(Calendar.MINUTE, minute);
			dateTimeEnd.set(Calendar.SECOND, 0);
			dateTimeEnd.set(Calendar.MILLISECOND, 0);
			String timeStr = DateTimeTrigger.SIMPLE_TIME_FORMAT.format(dateTimeEnd.getTime());
			bTimeEnd.setText(timeStr);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();			
		}
	}

	@Override
	public void onTimeOff(String fragmentTag) {
	}

	@Override
	public void onDateOn(String fragmentTag, int year, int monthOfYear, int dayOfMonth) {
		
		if (FRAGMENT_TAG_DATE_START_PICKER.equals(fragmentTag)) {
			GregorianCalendar dateTimeStart = trigger.getDateTimeStart();
			dateTimeStart.set(Calendar.YEAR, year);
			dateTimeStart.set(Calendar.MONTH, monthOfYear);
			dateTimeStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			String dateStr = DateTimeTrigger.SIMPLE_DATE_FORMAT.format(dateTimeStart.getTime());
			bDateStart.setText(dateStr);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		} else if (FRAGMENT_TAG_DATE_END_PICKER.equals(fragmentTag)) {
			GregorianCalendar dateTimeEnd = trigger.getDateTimeEnd();
			dateTimeEnd.set(Calendar.YEAR, year);
			dateTimeEnd.set(Calendar.MONTH, monthOfYear);
			dateTimeEnd.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			String dateStr = DateTimeTrigger.SIMPLE_DATE_FORMAT.format(dateTimeEnd.getTime());
			bDateEnd.setText(dateStr);
			updateDescription(trigger, tvDescription);
			updateDynamicActionBar();
		}
	}

	@Override
	public void onDateOff(String fragmentTag) {
	}
	
	/**
	 * Checks the match checkbox of the selected repeat days.
	 */
	private void setSelectedDaysOfTheWeek() {
		for (int i=0; i < 7; ++i) {
			if (trigger.isRepeatDayOfTheWeek(i)) {
				switch (i) {
				case 0:
					cbSunday.setChecked(true);
					break;
				case 1:
					cbMonday.setChecked(true);
					break;
				case 2:
					cbTuesday.setChecked(true);
					break;
				case 3:
					cbWednesday.setChecked(true);
					break;
				case 4:
					cbThursday.setChecked(true);
					break;
				case 5:
					cbFriday.setChecked(true);
					break;
				case 6:
					cbSaturday.setChecked(true);
					break;
				}
			}
		}
	}

}

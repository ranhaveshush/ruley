/**
 * 
 */
package com.ruley.activities.editors.triggers;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.OutgoingCallTrigger;
import com.ruley.model.beans.variables.ContactsVariable;
import com.ruley.utilities.ContactsUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.PickersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class OutgoingCallTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final String CLASS_NAME = OutgoingCallTriggerEditorActivity.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(OutgoingCallTriggerEditorActivity.class.getName());

	private static final int ADD_CONTACT_REQUEST_CODE = 7101;
	
	private LayoutInflater mInflater = null;
	
	private TextView tvDescription = null;
	
	private TextView tvAddContact = null;
	
	private LinearLayout llContacts = null;

	private OutgoingCallTrigger mTrigger = null;
	
	private ContactsVariable mContacts = null;
	
	private OnClickListener mOnContactClickListener =  new OnClickListener() {
		@Override
		public void onClick(View view) {
			String contactId = (String) view.getTag();
			mContacts.getContactsIds().remove(contactId);
			llContacts.removeView(view);
			
			updateDescription(mTrigger, tvDescription);
			updateDynamicActionBar();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_outgoing_call_trigger_editor);
		
		mInflater = LayoutInflater.from(OutgoingCallTriggerEditorActivity.this);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvAddContact = (TextView) findViewById(R.id.tvAddContact);
		tvAddContact.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PickersUtils.launchContactPicker(OutgoingCallTriggerEditorActivity.this, ADD_CONTACT_REQUEST_CODE);
			}
		});
		llContacts = (LinearLayout) findViewById(R.id.llContacts);
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mTrigger = (OutgoingCallTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
			mContacts = mTrigger.getContacts();
			updateContactsList(mContacts.getContactsIds());
		// If editing an existing instance
		} else if (extras != null) {
			mTrigger = (OutgoingCallTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
			mContacts = mTrigger.getContacts();
			updateContactsList(mContacts.getContactsIds());
		// If creating a new instance
		} else {
			mTrigger = new OutgoingCallTrigger();
			mContacts = mTrigger.getContacts();
		}
		
		setBaseTrigger(mTrigger);
		
		updateDescription(mTrigger, tvDescription);
		updateDynamicActionBar();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug("onActivityResult(...)");

		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case ADD_CONTACT_REQUEST_CODE:
				Uri contactUri = data.getData();
				String contactId = contactUri.getLastPathSegment();
				// Only if contacts list not contains this contact, add him
				Collection<String> contactsIds = mContacts.getContactsIds();
				if (!contactsIds.contains(contactId)) {
					contactsIds.add(contactId);
					addContact(contactId);
					// Update the description
					updateDescription(mTrigger, tvDescription);
					updateDynamicActionBar();
				}
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
		
		updateDescription(mTrigger, tvDescription);
		updateDynamicActionBar();
	}
	
	private void updateContactsList(Collection<String> contactsIds) {
		llContacts.removeAllViews();
		for (String contactId : contactsIds) {
			addContact(contactId);
		}
	}

	private void addContact(String contactId) {
		// Gets the contact info
		String contactName = ContactsUtils.getContactName(this, contactId);
		Bitmap contactPhoto = ContactsUtils.getContactPhoto(this, contactId);
		Drawable drawable = ImageUtils.toDrawable(contactPhoto);
		// Constructs the contact list item
		TextView tvContact = (TextView) mInflater.inflate(R.layout.list_item_contact, null);
		tvContact.setTag(contactId);
		tvContact.setCompoundDrawables(drawable, null, null, null);
		tvContact.setText(contactName);
		tvContact.setOnClickListener(mOnContactClickListener);
		llContacts.addView(tvContact);
	}
	
}

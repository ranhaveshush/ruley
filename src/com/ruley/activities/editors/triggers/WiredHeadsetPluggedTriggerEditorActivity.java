/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.WiredHeadsetPluggedStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class WiredHeadsetPluggedTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(WiredHeadsetPluggedTriggerEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgHeadsetState = null;
	
	private WiredHeadsetPluggedStateTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_wired_headset_plugged_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgHeadsetState = (RadioGroup) findViewById(R.id.rgHeadsetState);
		rgHeadsetState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbHeadsetPlugged:
					trigger.setWantedPluggedState(WiredHeadsetPluggedStateTrigger.HEADSET_PLUGGED);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbHeadsetUnplugged:
					trigger.setWantedPluggedState(WiredHeadsetPluggedStateTrigger.HEADSET_UNPLUGGED);
					updateDescription(trigger, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (WiredHeadsetPluggedStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (WiredHeadsetPluggedStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new WiredHeadsetPluggedStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		switch (trigger.getWantedPluggedState()) {
		case WiredHeadsetPluggedStateTrigger.HEADSET_PLUGGED:
			rgHeadsetState.check(R.id.rbHeadsetPlugged);
			break;
		case WiredHeadsetPluggedStateTrigger.HEADSET_UNPLUGGED:
			rgHeadsetState.check(R.id.rbHeadsetUnplugged);
			break;
		}
	}
	
}

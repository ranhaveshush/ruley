/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.PhoneStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class PhoneStateTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PhoneStateTriggerEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgPhoneState = null;
	
	private PhoneStateTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_phone_state_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgPhoneState = (RadioGroup) findViewById(R.id.rgPhoneState);
		rgPhoneState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbIdle:
					trigger.setWantedPhoneState(TelephonyManager.CALL_STATE_IDLE);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbOffHook:
					trigger.setWantedPhoneState(TelephonyManager.CALL_STATE_OFFHOOK);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbRinging:
					trigger.setWantedPhoneState(TelephonyManager.CALL_STATE_RINGING);
					updateDescription(trigger, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (PhoneStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (PhoneStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new PhoneStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		switch (trigger.getWantedPhoneState()) {
		case TelephonyManager.CALL_STATE_IDLE:
			rgPhoneState.check(R.id.rbIdle);
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			rgPhoneState.check(R.id.rbOffHook);
			break;
		case TelephonyManager.CALL_STATE_RINGING:
			rgPhoneState.check(R.id.rbRinging);
			break;
		}
	}

}

/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.google.android.gms.location.DetectedActivity;
import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerGPSEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.ActivityRecognitionStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ActivityRecognitionStateTriggerEditorActivity extends BaseTriggerGPSEditorActivity {
	
	private static final String CLASS_NAME = ActivityRecognitionStateTriggerEditorActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ActivityRecognitionStateTriggerEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private RadioGroup rgUserActivity = null;
	
	private ActivityRecognitionStateTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);
		
		if (!mIsGooglePlayServicesAvailable) {
			return;
		}
		
		setContentView(R.layout.activity_activity_recognition_state_trigger_editor);
		
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgUserActivity  = (RadioGroup) findViewById(R.id.rgUserActivity);
		rgUserActivity.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbStill:
					trigger.setWantedUserActivityState(DetectedActivity.STILL);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbWalking:
					trigger.setWantedUserActivityState(DetectedActivity.WALKING);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbRunning:
					trigger.setWantedUserActivityState(DetectedActivity.RUNNING);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbOnBicycle:
					trigger.setWantedUserActivityState(DetectedActivity.ON_BICYCLE);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbInVehicle:
					trigger.setWantedUserActivityState(DetectedActivity.IN_VEHICLE);
					updateDescription(trigger, tvDescription);
					break;
				default:
					throw new IllegalArgumentException(CLASS_NAME + " invalid user activity.");
				}
			}
		});
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (ActivityRecognitionStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (ActivityRecognitionStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new ActivityRecognitionStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		switch (trigger.getWantedUserActivityState()) {
		case DetectedActivity.STILL:
			rgUserActivity.check(R.id.rbStill);
			break;
		case DetectedActivity.WALKING:
			rgUserActivity.check(R.id.rbWalking);
			break;
		case DetectedActivity.RUNNING:
			rgUserActivity.check(R.id.rbRunning);
			break;
		case DetectedActivity.ON_BICYCLE:
			rgUserActivity.check(R.id.rbOnBicycle);
			break;
		case DetectedActivity.IN_VEHICLE:
			rgUserActivity.check(R.id.rbInVehicle);
			break;
		}
	}
	

}

/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.DeviceStorageStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DeviceStorageStateTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DeviceStorageStateTriggerEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private RadioGroup rgDeviceStorageState = null;
	
	private DeviceStorageStateTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_device_storage_state_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgDeviceStorageState = (RadioGroup) findViewById(R.id.rgDeviceStorageState);
		rgDeviceStorageState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbDeviceStorageOk:
					trigger.setWantedDeviceStorageState(DeviceStorageStateTrigger.DEVICE_STORAGE_OK);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbDeviceStorageLow:
					trigger.setWantedDeviceStorageState(DeviceStorageStateTrigger.DEVICE_STORAGE_LOW);
					updateDescription(trigger, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (DeviceStorageStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (DeviceStorageStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new DeviceStorageStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		switch (trigger.getWantedDeviceStorageState()) {
		case DeviceStorageStateTrigger.DEVICE_STORAGE_OK:
			rgDeviceStorageState.check(R.id.rbDeviceStorageOk);
			break;
		case DeviceStorageStateTrigger.DEVICE_STORAGE_LOW:
			rgDeviceStorageState.check(R.id.rbDeviceStorageLow);
			break;
		}
	}

}

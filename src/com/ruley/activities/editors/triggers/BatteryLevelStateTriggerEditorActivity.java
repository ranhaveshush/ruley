/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.edmodo.rangebar.RangeBar;
import com.edmodo.rangebar.RangeBar.OnRangeBarChangeListener;
import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.BatteryLevelStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class BatteryLevelStateTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BatteryLevelStateTriggerEditorActivity.class.getName());
	
	private TextView tvDescription = null;

	private TextView tvBatteryLevelStart = null;
	
	private TextView tvBatteryLevelEnd = null;
	
	private RangeBar rbBatteryLevel = null;
	
	private BatteryLevelStateTrigger trigger = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_battery_level_state_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		tvBatteryLevelStart = (TextView) findViewById(R.id.tvBatteryLevelStart);
		tvBatteryLevelEnd = (TextView) findViewById(R.id.tvBatteryLevelEnd);
		rbBatteryLevel = (RangeBar) findViewById(R.id.rbBatteryLevel);
		rbBatteryLevel.setOnRangeBarChangeListener(new OnRangeBarChangeListener() {
			@Override
			public void onIndexChangeListener(RangeBar rangeBar, int leftThumbIndex, int rightThumbIndex) {
				tvBatteryLevelStart.setText(String.valueOf(leftThumbIndex));
				tvBatteryLevelEnd.setText(String.valueOf(rightThumbIndex));
				trigger.setWantedBatteryLevelStart(leftThumbIndex);
				trigger.setWantedBatteryLevelEnd(rightThumbIndex);
				updateDescription(trigger, tvDescription);
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (BatteryLevelStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (BatteryLevelStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new BatteryLevelStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		int batteryLevelStart = trigger.getWantedBatteryLevelStart();
		int batteryLevelEnd = trigger.getWantedBatteryLevelEnd();
		tvBatteryLevelStart.setText(String.valueOf(batteryLevelStart));
		tvBatteryLevelEnd.setText(String.valueOf(batteryLevelEnd));
		rbBatteryLevel.setThumbIndices(batteryLevelStart, batteryLevelEnd);
		
		updateDescription(trigger, tvDescription);
	}

}

/**
 * 
 */
package com.ruley.activities.editors.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.activities.editors.triggers.base.BaseTriggerDynamicEditorActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.ScreenStateTrigger;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ScreenStateTriggerEditorActivity extends BaseTriggerDynamicEditorActivity {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ScreenStateTriggerEditorActivity.class.getName());

	private TextView tvDescription = null;
	
	private RadioGroup rgScreenState = null;
	
	private ScreenStateTrigger trigger = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_screen_state_trigger_editor);

		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rgScreenState = (RadioGroup) findViewById(R.id.rgScreenState);
		rgScreenState.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbScreenOn:
					trigger.setWantedScreenState(ScreenStateTrigger.SCREEN_ON);
					updateDescription(trigger, tvDescription);
					break;
				case R.id.rbScreenOff:
					trigger.setWantedScreenState(ScreenStateTrigger.SCREEN_OFF);
					updateDescription(trigger, tvDescription);
					break;
				}
			}
		});

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();

		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			trigger = (ScreenStateTrigger) savedInstanceState.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If editing existing instance
		} else if (extras != null) {
			trigger = (ScreenStateTrigger) extras.getSerializable(Config.Extra.EXTRA_TRIGGER);
		// If creating a new instance
		} else {
			trigger = new ScreenStateTrigger();
		}
		
		setBaseTrigger(trigger);
		
		switch (trigger.getWantedScreenState()) {
		case ScreenStateTrigger.SCREEN_ON:
			rgScreenState.check(R.id.rbScreenOn);
			break;
		case ScreenStateTrigger.SCREEN_OFF:
			rgScreenState.check(R.id.rbScreenOff);
			break;
		}
	}

}

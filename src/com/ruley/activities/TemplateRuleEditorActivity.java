package com.ruley.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.adapters.RuleComponentsAdapter;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.fragments.dialogs.DialogLauncher;
import com.ruley.fragments.dialogs.OneButtonDialogFragment;
import com.ruley.fragments.dialogs.TwoButtonDialogFragment;
import com.ruley.listeners.OnResultListener;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.IRuleComponent;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.Rule.Listener;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.utilities.AnalyticsUtils;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.RulesDependencyGraphUtils;
import com.ruley.utilities.ToastUtils;

import de.timroes.android.listview.EnhancedListView;
import de.timroes.android.listview.EnhancedListView.Undoable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TemplateRuleEditorActivity extends GABaseActivity implements
RuleComponentsAdapter.Listener, OneButtonDialogFragment.Listener, TwoButtonDialogFragment.Listener, OnResultListener {

	private static final String CLASS_NAME = TemplateRuleEditorActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CLASS_NAME);
	
	private static final IActivityLogDAO ACTIVITY_LOG_DAO = DAOFactory
			.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
	
	private static final IRuleDAO RULE_DAO = DAOFactory.getFactory(
			DAOFactory.getFactoryType()).getRuleDAO();
	
	public static final int ADD_TRIGGER_REQUEST_CODE = 2001;
	
	public static final int ADD_ACTION_REQUEST_CODE = 2002;
	
	private static final int EDIT_TRIGGER_REQUEST_CODE = 2003;
	
	private static final int EDIT_ACTION_REQUEST_CODE = 2004;

	private static final String FRAGMENT_TAG_INVALID_RULE = "invalidRule";

	private static final String FRAGMENT_TAG_RULE_NAME_MISSING = "ruleNameMissing";

	private Context mAppCtx = null;

	private TemplateRule mTemplateRule = null;
	
	private ViewGroup mRlRuleNameContainer = null;
	
	private EditText mEtRuleName = null;
	
	private RuleComponentsAdapter mRuleComponentsAdapter = null;
	
	private EnhancedListView mElvRuleComponents = null;
	
	private ActionMode mActionMode = null;
	
	private OnItemClickListener mRuleComponentItemEditorReferral = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			IRuleComponent item = (IRuleComponent) mRuleComponentsAdapter.getItem(position);
			Class<? extends Activity> editor = item.getEditor();
			if (item instanceof Trigger) {
				Intent intent = new Intent(TemplateRuleEditorActivity.this, editor);
				intent.putExtra(Config.Extra.EXTRA_TRIGGER, item);
				startActivityForResult(intent, EDIT_TRIGGER_REQUEST_CODE);
			} else if (item instanceof Action) {
				Intent intent = new Intent(TemplateRuleEditorActivity.this, editor);
				intent.putExtra(Config.Extra.EXTRA_ACTION, item);
				startActivityForResult(intent, EDIT_ACTION_REQUEST_CODE);
			}
		}
	};
	
	private OnItemLongClickListener mRuleComponentItemStartActionMode = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			mRuleComponentsAdapter.toggleSelectedItem(position);
			startActionMode();
			return true;
		}
	};
	
	private OnItemClickListener mRuleComponentItemToggleSelection = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			mRuleComponentsAdapter.toggleSelectedItem(position);
			finishActionMode();
		}
	};
	
	/**
	 * Template Rule Editor states enum. Inner class representing all the possiable mState
	 * the editor may be in.
	 * 
	 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
	 * 
	 */
	public enum State {
		DOWNLOAD_RULE, // Downloads new rule
		UPLOAD_RULE // Uploads new rule
	}

	private State mState = null;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		
		outState.putSerializable(Config.Extra.EXTRA_STATE, mState);
		outState.putSerializable(Config.Extra.EXTRA_TEMPLATE_RULE, mTemplateRule);
		
		// Calls the superclass so it can save the view hierarchy mState
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_rule_editor);
		
		setResult(RESULT_CANCELED);

		mAppCtx = MyApplication.getContext();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mState = (State) savedInstanceState.getSerializable(Config.Extra.EXTRA_STATE);
			mTemplateRule = (TemplateRule) savedInstanceState.getSerializable(Config.Extra.EXTRA_TEMPLATE_RULE);
		// If creating a new instance
		} else {
			// Gets the action from the intent
			Intent intent = getIntent();
			String action = intent.getAction();
			LOGGER.info("action: " + action);
	
			// Sets the mState of the activity by the invoked action
			if (Config.Action.ACTION_DOWNLOAD_RULE.equals(action)) {
				mState = State.DOWNLOAD_RULE;
			} else if (Config.Action.ACTION_UPLOAD_RULE.equals(action)) {
				mState = State.UPLOAD_RULE;
			}
			
			switch (mState) {
			case DOWNLOAD_RULE:
				mTemplateRule = (TemplateRule) intent.getSerializableExtra(Config.Extra.EXTRA_TEMPLATE_RULE);
				break;
			case UPLOAD_RULE:
				mTemplateRule = (TemplateRule) intent.getSerializableExtra(Config.Extra.EXTRA_TEMPLATE_RULE);
				break;
			default:
				throw new IllegalStateException(CLASS_NAME + " caused by invalid action");
			}
		}
		
		// Sets the title of the activity
		switch (mState) {
		case DOWNLOAD_RULE:
			setTitle(R.string.title_download_rule);
			break;
		case UPLOAD_RULE:
			setTitle(R.string.title_upload_rule);
			break;
		}
		
		mRlRuleNameContainer  = (ViewGroup) findViewById(R.id.rlRuleNameContainer);
		mRlRuleNameContainer.setVisibility(View.VISIBLE);
		mEtRuleName = (EditText) findViewById(R.id.etRuleName);
		mEtRuleName.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mTemplateRule.setName(s.toString());
				mDynamicActionBar.update(TemplateRuleEditorActivity.this);
			}
		});
		String ruleName = mTemplateRule.getName();
		if (TextUtils.isEmpty(ruleName)) {
			mEtRuleName.setText(getString(R.string.rule_name_text));
		} else {
			mEtRuleName.setText(ruleName);
		}
		
		mRuleComponentsAdapter = new RuleComponentsAdapter(TemplateRuleEditorActivity.this, mTemplateRule);
		mElvRuleComponents = (EnhancedListView) findViewById(R.id.elvRuleComponents);
		mElvRuleComponents.setAdapter(mRuleComponentsAdapter);
		mElvRuleComponents.setOnItemClickListener(mRuleComponentItemEditorReferral);
		mElvRuleComponents.setOnItemLongClickListener(mRuleComponentItemStartActionMode);
		mElvRuleComponents.setDismissCallback(new EnhancedListView.OnDismissCallback() {
			@Override
			public Undoable onDismiss(EnhancedListView listView, final int position) {
				LOGGER.debug(CLASS_NAME + "#onDismiss(...)");
				final IRuleComponent ruleComponentToDelete = (IRuleComponent) mRuleComponentsAdapter.getItem(position);
				removeRuleComponent(ruleComponentToDelete);
				return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                    	restoreRuleComponent(position, ruleComponentToDelete);
                    }
                };
			}
		});
		mElvRuleComponents.enableSwipeToDismiss()
			.setSwipeDirection(EnhancedListView.SwipeDirection.BOTH)
			.setUndoStyle(EnhancedListView.UndoStyle.MULTILEVEL_POPUP)
			.setRequireTouchBeforeDismiss(false)
			.setUndoHideDelay(3000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug("onCreateOptionsMenu(...)");

		getMenuInflater().inflate(R.menu.activity_template_rule_editor_menu, menu);
		MenuItem actionSaveRule = menu.findItem(R.id.action_save_rule);
		MenuItem actionUploadRule = menu.findItem(R.id.action_upload_rule);
		MenuItem actionTestActions = menu.findItem(R.id.action_test_actions);
		List<MenuItem> menuItems = new ArrayList<MenuItem>(Arrays.asList(
				actionSaveRule, actionTestActions));
		mDynamicActionBar.setValidatable(mTemplateRule);
		mDynamicActionBar.setMenuItems(menuItems);
		mDynamicActionBar.update(TemplateRuleEditorActivity.this);
		
		switch (mState) {
		case DOWNLOAD_RULE:
			actionSaveRule.setVisible(true);
			actionUploadRule.setVisible(false);
			actionTestActions.setVisible(false);
			break;
		case UPLOAD_RULE:
			actionSaveRule.setVisible(false);
			actionUploadRule.setVisible(true);
			actionTestActions.setVisible(false);
			break;
		}
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");
		LOGGER.info("item selected: " + item.getTitle());
		
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
	    case android.R.id.home:
	    	onBackPressed();
	    	return true;
		case R.id.action_save_rule:
			storeRule(this);
			return true;
		case R.id.action_upload_rule:
			ParseUtils.shareTemplateRule(TemplateRuleEditorActivity.this, mTemplateRule, new DoneCallback() {
				@Override
				public void done() {
					setResult(RESULT_OK);
					finish();
				}
			});
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		if (resultCode == RESULT_OK) {
			Trigger trigger = (Trigger) data.getSerializableExtra(Config.Extra.EXTRA_TRIGGER);
			Action action = (Action) data.getSerializableExtra(Config.Extra.EXTRA_ACTION);
			switch (requestCode) {
			case ADD_TRIGGER_REQUEST_CODE:
				LOGGER.debug("Add Trigger: " + trigger);
				mTemplateRule.getTriggers().add(trigger);
				mRuleComponentsAdapter.add(trigger);
				break;
			case ADD_ACTION_REQUEST_CODE:
				LOGGER.debug("Add Action: " + action);
				mTemplateRule.getActions().add(action);
				mRuleComponentsAdapter.add(action);
				break;
			case EDIT_TRIGGER_REQUEST_CODE:
				LOGGER.debug("Edit Trigger: " + trigger);
				// Updates the rule
				List<Trigger> triggers = mTemplateRule.getTriggers();
				triggers.remove(trigger);
				triggers.add(0, trigger);
				// Updates the rule components list view
				mRuleComponentsAdapter.remove(trigger);
				mRuleComponentsAdapter.add(trigger);
				mRuleComponentsAdapter.notifyDataSetChanged();
				break;
			case EDIT_ACTION_REQUEST_CODE:
				LOGGER.debug("Edit Action: " + action);
				// Updates the rule
				List<Action> actions = mTemplateRule.getActions();
				actions.remove(action);
				actions.add(0, action);
				// Updates the rule components list view
				mRuleComponentsAdapter.remove(action);
				mRuleComponentsAdapter.add(action);
				mRuleComponentsAdapter.notifyDataSetChanged();
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
			mDynamicActionBar.update(TemplateRuleEditorActivity.this);
		}
	}
	
	@Override
	public void onBackPressed() {
		LOGGER.debug(CLASS_NAME + "#onBackPressed(...)");
		
		switch (mState) {
		case DOWNLOAD_RULE:
		case UPLOAD_RULE:
			onResultCanceled();
			break;
		}
	}
	
	private void startActionMode() {
		List<Object> selectedItems = mRuleComponentsAdapter.getSelectedItems();
		boolean hasSelectedItems = !selectedItems.isEmpty();
		if (mActionMode == null && hasSelectedItems) {
			mActionMode = TemplateRuleEditorActivity.this.startActionMode(mActionModeCallback);
			String acitonModeTitle = getString(R.string.title_action_mode);
			mActionMode.setTitle(String.format(acitonModeTitle, selectedItems.size()));
		}
	}
	
	private void finishActionMode() {
		List<Object> selectedItems = mRuleComponentsAdapter.getSelectedItems();
		String acitonModeTitle = getString(R.string.title_action_mode);
		mActionMode.setTitle(String.format(acitonModeTitle, selectedItems.size()));
		boolean hasSelectedItems = !selectedItems.isEmpty();
		if (mActionMode != null && !hasSelectedItems) {
			mActionMode.finish();
			mActionMode = null;
		}
	}
	
	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.list_your_rules_action_mode, menu);
			
			mElvRuleComponents.setOnItemClickListener(mRuleComponentItemToggleSelection);
			mElvRuleComponents.setOnItemLongClickListener(null);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.action_delete_selected:
				final List<IRuleComponent> selectedItems = new ArrayList<IRuleComponent>(
						(ArrayList<IRuleComponent>)(List<?>) mRuleComponentsAdapter.getSelectedItems());
				// Removes selected items from rule
				for (IRuleComponent selectedItem : selectedItems) {
					removeRuleComponent(selectedItem);
				}
				mode.finish();
				return true;
			default:
				return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mElvRuleComponents.setOnItemClickListener(mRuleComponentItemEditorReferral);
			mElvRuleComponents.setOnItemLongClickListener(mRuleComponentItemStartActionMode);
			mActionMode = null;
			mRuleComponentsAdapter.clearSelectedItems();
			mDynamicActionBar.update(TemplateRuleEditorActivity.this);
		}
	};
	
	private void removeRuleComponent(IRuleComponent ruleComponentToDelete) {
		if (ruleComponentToDelete instanceof Trigger) {
			mRuleComponentsAdapter.remove(ruleComponentToDelete);
			mTemplateRule.getTriggers().remove(ruleComponentToDelete);
		} else if (ruleComponentToDelete instanceof Action) {
			mRuleComponentsAdapter.remove(ruleComponentToDelete);
			mTemplateRule.getActions().remove(ruleComponentToDelete);
		}
		mDynamicActionBar.update(TemplateRuleEditorActivity.this);
	}
	
	private void restoreRuleComponent(int position, IRuleComponent ruleComponentToRestore) {
		LOGGER.debug(CLASS_NAME + "#restoreRuleComponent(...)");
		
		if (ruleComponentToRestore instanceof Trigger) {
			mRuleComponentsAdapter.add(position, ruleComponentToRestore);
			mTemplateRule.getTriggers().add((Trigger)ruleComponentToRestore);
		} else if (ruleComponentToRestore instanceof Action) {
			mRuleComponentsAdapter.add(position, ruleComponentToRestore);
			mTemplateRule.getActions().add((Action)ruleComponentToRestore);
		}
		mDynamicActionBar.update(TemplateRuleEditorActivity.this);
	}
	
	private void storeRule(final OnResultListener onResultListener) {
		LOGGER.debug(CLASS_NAME + "#storeRule(...)");
		
		// Validates that the user doesn't have this rule
		boolean doesUserHaveThisRule = RULE_DAO.get(mTemplateRule.getId()) != null;
		if (doesUserHaveThisRule) {
			ToastUtils.show(AppState.BUILD_RELEASE,
					TemplateRuleEditorActivity.this,
					R.string.toast_rule_already_dowloaded,
					Toast.LENGTH_SHORT);
			onResultListener.onResultCanceled();
			return;
		}
		
		boolean doesTemplateRuleMine = ParseUtils.doesTemplateRuleMine(
				TemplateRuleEditorActivity.this, mTemplateRule);
		
		final Rule ruleToStore = mTemplateRule.toRule();
		ruleToStore.setMine(doesTemplateRuleMine);
		
		ruleToStore.setActive(mAppCtx, true, new Listener() {
			@Override
			public void onInactivated() {
			}
			@Override
			public void onActivated() {
				RULE_DAO.store(DefaultUser.class, ruleToStore);
				if (AppState.isAppEnabled()) {
					MyApplication.updateBackgroundServices();
					ruleToStore.perform(TemplateRuleEditorActivity.this);
				}
				logToActivityLog(ruleToStore);
				AnalyticsUtils.sendRuleDownloaded();
				onResultListener.onResultOk();
			}
			@Override
			public void onError(int errorMessageResourceId) {
				switch (errorMessageResourceId) {
				case R.string.rule_activation_error_circular_dependency:							
					RulesDependencyGraphUtils.resolveStronglyConnectedSets(mAppCtx, ruleToStore);
					break;
				case R.string.trigger_activation_error_location_service_disabled:
				case R.string.location_status_codes_geofence_not_available:
					DialogLauncher.launchActionSecuritySettings(
							TemplateRuleEditorActivity.this,
							MainActivity.FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR);
					break;
				default:
					DialogLauncher.launchGeneralErrorDialog(
							TemplateRuleEditorActivity.this,
							MainActivity.FRAGMENT_TAG_GENERAL_ERROR,
							errorMessageResourceId);
					break;
				}
			}
		});
	}
	
	private void logToActivityLog(final Rule ruleToStore) {
		String action = null;
		if (mState == State.DOWNLOAD_RULE) {
			action = getString(R.string.rule_downloaded);
		}
		ACTIVITY_LOG_DAO.log(ruleToStore.getId(), ruleToStore.getName(), action);
	}

	@Override
	public void onAddTrigger() {
		Intent intent = new Intent(TemplateRuleEditorActivity.this, RuleComponentSelectActivity.class);
		intent.setAction(Config.Action.ACTION_ADD_TRIGGER);
		startActivityForResult(intent, ADD_TRIGGER_REQUEST_CODE);
	}

	@Override
	public void onAddAction() {
		Intent intent = new Intent(TemplateRuleEditorActivity.this, RuleComponentSelectActivity.class);
		intent.setAction(Config.Action.ACTION_ADD_ACTION);
		startActivityForResult(intent, ADD_ACTION_REQUEST_CODE);
	}

	@Override
	public void onConfirm(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onConfirm(...)");
		
		if (FRAGMENT_TAG_INVALID_RULE.equals(fragmentTag)) {
			onResultCanceled();
		} else if (FRAGMENT_TAG_RULE_NAME_MISSING.equals(fragmentTag)) {
			onResultCanceled();
		}
	}

	@Override
	public void onCancel(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onCancel(...)");
		
		if (FRAGMENT_TAG_INVALID_RULE.equals(fragmentTag)) {
			// Do nothing
		} else if (FRAGMENT_TAG_RULE_NAME_MISSING.equals(fragmentTag)) {
			mEtRuleName.requestFocus();
		}
	}

	@Override
	public void onResultOk() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra(Config.Extra.EXTRA_TEMPLATE_RULE, mTemplateRule);
		setResult(RESULT_OK, resultIntent);
		finish();
	}

	@Override
	public void onResultCanceled() {
		setResult(RESULT_CANCELED);
		finish();
	}
	
}

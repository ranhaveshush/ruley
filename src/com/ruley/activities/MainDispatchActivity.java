/**
 * 
 */
package com.ruley.activities;

import android.os.Bundle;

import com.parse.ui.ParseLoginDispatchActivity;
import com.ruley.utilities.AnalyticsUtils;
import com.ruley.utilities.PrefsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class MainDispatchActivity extends ParseLoginDispatchActivity {
	
	@Override
	protected Class<?> getTargetClass() {
		return MainActivity.class;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (!PrefsUtils.isUserOnboard()) {
			// Send virtual screen view for Google Analytics goal funnels - Signups
			AnalyticsUtils.sendScreenViewEvent("Onboarding - user see onboarding slides");
		}
		super.onCreate(savedInstanceState);
	}
	
}

/**
 * 
 */
package com.ruley.activities;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.adapters.DrawerAdapter;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.fragments.ActivityLogFragment;
import com.ruley.fragments.BackupRulesFragment;
import com.ruley.fragments.YourRulesFragment;
import com.ruley.fragments.dialogs.DialogLauncher;
import com.ruley.fragments.dialogs.OneButtonDialogFragment;
import com.ruley.fragments.dialogs.RuleActivationErrorDialogFragment;
import com.ruley.fragments.dialogs.TwoButtonDialogFragment;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.FontsUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 * 
 */
public class MainActivity extends GABaseActivity implements
		OneButtonDialogFragment.Listener,
		TwoButtonDialogFragment.Listener,
		RuleActivationErrorDialogFragment.Listener {

	private static final String CLASS_NAME = MainActivity.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MainActivity.class.getName());
	
	public static final String FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR = "locationServiceDisabledError";

	public static final String FRAGMENT_TAG_TRIGGER_ACTIVATION_ERROR = "triggerActivationError";

	public static final String FRAGMENT_TAG_GENERAL_ERROR = "generalError";

	private static final String FRAGMENT_TAG_EXIT_APP = "exitApp";

	private FragmentManager mFragmentManager = null;

	private String[] mNavigationEntries = null;

	private DrawerLayout mdlDrawer = null;

	private LinearLayout mllDrawer = null;
	
	private ProfilePictureView mppvUserPicture = null;

	private TextView mtvUsername = null;

	private DrawerAdapter mDrawerAdapter = null;

	private ListView mlvDrawerMenu = null;

	private ActionBarDrawerToggle mDrawerToggle = null;

	private CharSequence mFragmentTitle = null;

	private CharSequence mDrawerTitle = null;

	private String mYourRulesFragmentTitle = null;
	
	private String mBackupRulesFragmentTitle = null;
	
	private String mActivityLogFragmentTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		mFragmentManager = getFragmentManager();
		
		// Gets drawer titles from the resources
		mDrawerTitle = getString(R.string.app_name);
		mYourRulesFragmentTitle = getString(R.string.title_fragment_your_rules);
		mBackupRulesFragmentTitle = getString(R.string.title_fragment_backup_rules);
		mActivityLogFragmentTitle = getString(R.string.title_fragment_activity_log);

		mdlDrawer = (DrawerLayout) findViewById(R.id.dlDrawer);
		mdlDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		mllDrawer = (LinearLayout) findViewById(R.id.llDrawer);
		mppvUserPicture = (ProfilePictureView) findViewById(R.id.ppvUserPicture);
		mtvUsername = (TextView) findViewById(R.id.tvUsername);
		mNavigationEntries = getResources().getStringArray(R.array.navigation_entries);
		mDrawerAdapter = new DrawerAdapter(MainActivity.this,
				R.layout.list_item_drawer, mNavigationEntries);
		mlvDrawerMenu = (ListView) findViewById(R.id.lvDrawerMenu);
		mlvDrawerMenu.setAdapter(mDrawerAdapter);
		mlvDrawerMenu.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				selectItem(position);
			}
		});
		
		// Enables ActionBar app icon to behave as action to button_click nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, mdlDrawer,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				getActionBar().setTitle(mFragmentTitle);
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				SpannableString spannableTitle = new SpannableString(mDrawerTitle);
				spannableTitle = FontsUtils.getSpannableString(spannableTitle, Config.Fonts.FONT_TITLE);
				spannableTitle = FontsUtils.getSpannableString(spannableTitle,
						getResources().getInteger(R.integer.font_size_drawer_title),
						true);
				getActionBar().setTitle(spannableTitle);
				invalidateOptionsMenu();
			}
		};
		mdlDrawer.setDrawerListener(mDrawerToggle);
		
		// Loads the user profile
		ParseUser currentUser = ParseUser.getCurrentUser();
		// If the user logged in with Facebook login
		if (ParseFacebookUtils.isLinked(currentUser)) {
			Session session = ParseFacebookUtils.getSession();
			makeMeRequest(session);
		}
		String username = currentUser.getString(UserParse.FIELD_NAME);
		mtvUsername.setText(username);

		// If creating new instance
		if (savedInstanceState == null) {
			selectItem(0);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");

		getMenuInflater().inflate(R.menu.activity_main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");

		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
		case R.id.action_feedback:
			Intent reportIntent = new Intent(MainActivity.this, FeedbackActivity.class);
			startActivity(reportIntent);
			return true;
		case R.id.action_settings:
			Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
			startActivity(settingsIntent);
			return true;
		case R.id.action_log_out:
			MyApplication.logoutApp();
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onPostCreate(...)");
		super.onPostCreate(savedInstanceState);
		// Syncs the button_click state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		LOGGER.debug(CLASS_NAME + "#onConfigurationChanged(...)");
		super.onConfigurationChanged(newConfig);
		// Passes any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	/**
	 * Loads user Facebook profile data to navigation drawer.
	 * 
	 * @param session
	 */
	private void makeMeRequest(final Session session) {
		Request request = Request.newMeRequest(session, new GraphUserCallback() {
			@Override
			public void onCompleted(GraphUser user, Response response) {
				// If the response is successful
				if (session == Session.getActiveSession()) {
					if (user != null) {
						mppvUserPicture.setVisibility(View.VISIBLE);
						mppvUserPicture.setProfileId(user.getId());
					}
				}
				// If the response is failed
				if (response.getError() != null) {
					// TODO: Handle errors, will do so later
				}
			}
		});
		request.executeAsync();
	}

	/**
	 * Swaps fragments in the main content view.
	 */
	private void selectItem(int position) {
		LOGGER.debug(CLASS_NAME + "#selectItem(...)");

		String fragmentTitle = mNavigationEntries[position];

		Fragment fragment = getSelectedFragmentByTitle(fragmentTitle);

		// Inserts the fragment by replacing any existing fragment
		mFragmentManager.beginTransaction()
				.replace(R.id.flContent, fragment, fragmentTitle).commit();

		// Highlights the selected item, update the title, and close the drawer
		mlvDrawerMenu.setItemChecked(position, true);
		setTitle(fragmentTitle);
		mdlDrawer.closeDrawer(mllDrawer);
	}

	private Fragment getSelectedFragmentByTitle(String fragmentTitle) {
		LOGGER.debug(CLASS_NAME + "#getSelectedFragmentByTitle(...)");

		Fragment fragment = null;

		if (fragmentTitle.equals(mYourRulesFragmentTitle)) {
			fragment = mFragmentManager.findFragmentByTag(mYourRulesFragmentTitle);
			if (fragment == null) {
				fragment = new YourRulesFragment();
			}
		} else if (fragmentTitle.equals(mBackupRulesFragmentTitle)) {
			fragment = mFragmentManager.findFragmentByTag(mBackupRulesFragmentTitle);
			if (fragment == null) {
				fragment = new BackupRulesFragment();
			}
		} else if (fragmentTitle.equals(mActivityLogFragmentTitle)) {
			fragment = mFragmentManager.findFragmentByTag(mActivityLogFragmentTitle);
			if (fragment == null) {
				fragment = new ActivityLogFragment();
			}
		}

		return fragment;
	}

	@Override
	public void setTitle(CharSequence title) {
		LOGGER.debug(CLASS_NAME + "#setTitle(...)");

		mFragmentTitle = title;
		getActionBar().setTitle(mFragmentTitle);
	}
	
	@Override
	public void onBackPressed() {
		LOGGER.debug(CLASS_NAME + "#onBackPressed(...)");
		
		DialogLauncher.launchExitAppDialog(MainActivity.this, FRAGMENT_TAG_EXIT_APP);
	}

	@Override
	public void onConfirm(String fragmentTag, UUID ruleId) {
		LOGGER.debug(CLASS_NAME + "#onConfirm(...)");
		
		if (FRAGMENT_TAG_TRIGGER_ACTIVATION_ERROR.equals(fragmentTag)) {
			Intent intent = new Intent(MainActivity.this, RuleEditorActivity.class);
			intent.setAction(Config.Action.ACTION_EDIT_RULE);
			intent.putExtra(Config.Extra.EXTRA_RULE_ID, ruleId);
			startActivity(intent);
		}
	}
	
	@Override
	public void onConfirm(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onConfirm(...)");

		if (FRAGMENT_TAG_EXIT_APP.equals(fragmentTag)) {
			super.onBackPressed();
		} else if (FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR.equals(fragmentTag)) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
		} else if (FRAGMENT_TAG_GENERAL_ERROR.equals(fragmentTag)) {
			// Do nothing
		}
	}

	@Override
	public void onCancel(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onCancel(...)");

		if (FRAGMENT_TAG_EXIT_APP.equals(fragmentTag)) {
			// Do nothing
		} else if (FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR.equals(fragmentTag)) {
			// Do nothing
		} else if (FRAGMENT_TAG_TRIGGER_ACTIVATION_ERROR.equals(fragmentTag)) {
			// Do nothing
		} else if (FRAGMENT_TAG_GENERAL_ERROR.equals(fragmentTag)) {
			// Do nothing
		}
	}

}

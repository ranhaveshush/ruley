package com.ruley.activities;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.IValidatable;
import com.ruley.utilities.PickersUtils;

public class FeedbackActivity extends GABaseActivity implements IValidatable {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(FeedbackActivity.class.getName());
	
	private Spinner sEmailFrom = null;

	private Spinner sFeedbackType = null;
	
	private EditText etEmailSubject = null;
	
	private EditText etEmailText = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug("onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_feedback);

		sEmailFrom = (Spinner) findViewById(R.id.sEmailFrom);
		sFeedbackType = (Spinner) findViewById(R.id.sFeedbackType);
		etEmailSubject = (EditText) findViewById(R.id.etEmailSubject);
		etEmailSubject.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mDynamicActionBar.update(FeedbackActivity.this);
			}
		});
		etEmailText = (EditText) findViewById(R.id.etEmailText);
		etEmailText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mDynamicActionBar.update(FeedbackActivity.this);
			}
		});
		
		// Gets the user google accounts
		ArrayAdapter<String> userAccountsAdapter = new ArrayAdapter<String>(
				FeedbackActivity.this, android.R.layout.simple_spinner_dropdown_item);
		AccountManager accountManager = AccountManager.get(FeedbackActivity.this);
		Account[] userAccounts = accountManager.getAccountsByType("com.google");
		for (Account userAccount : userAccounts) {
			userAccountsAdapter.add(userAccount.name);
		}
		sEmailFrom.setAdapter(userAccountsAdapter);
		
		// Gets the reports types
		ArrayAdapter<CharSequence> feedbackTypes = ArrayAdapter.createFromResource(FeedbackActivity.this,
				R.array.feedback_types, android.R.layout.simple_spinner_dropdown_item);
		sFeedbackType.setAdapter(feedbackTypes);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug("onCreateOptionsMenu(...)");
		
		getMenuInflater().inflate(R.menu.activity_feedback_menu, menu);
		MenuItem actionSendRule = menu.findItem(R.id.action_send_feedback);
		List<MenuItem> menuItems = Arrays.asList(actionSendRule);
		mDynamicActionBar.setValidatable(FeedbackActivity.this);
		mDynamicActionBar.setMenuItems(menuItems);
		mDynamicActionBar.update(FeedbackActivity.this);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_send_feedback:
			String feedbackType = sFeedbackType.getSelectedItem().toString();
			String subject = "[" + feedbackType + "] - " + etEmailSubject.getText().toString();
			String text = etEmailText.getText().toString();
			PickersUtils.launchEmailPicker(FeedbackActivity.this, Config.MY_EMAIL, subject, text);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean isValid(Context context) {
		return !TextUtils.isEmpty(etEmailSubject.getText())
				&& !TextUtils.isEmpty(etEmailText.getText());
	}

}

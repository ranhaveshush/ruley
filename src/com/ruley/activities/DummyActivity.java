/**
 * 
 */
package com.ruley.activities;

import android.app.Activity;
import android.os.Bundle;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
// TODO: delete this workaround after Google will fix the bug
public class DummyActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		finish();
	}
}

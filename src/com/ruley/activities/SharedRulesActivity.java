/**
 * 
 */
package com.ruley.activities;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.adapters.TemplateRulesAdapter;
import com.ruley.app.AppState;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.connection.file.json.JsonHelper;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.utilities.CryptoUtils;
import com.ruley.utilities.FilesUtils;
import com.ruley.utilities.RulesUtils;
import com.ruley.utilities.ToastUtils;

import de.timroes.android.listview.EnhancedListView;
import de.timroes.android.listview.EnhancedListView.Undoable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SharedRulesActivity extends GABaseActivity {
	
	private static final String CLASS_NAME = SharedRulesActivity.class
			.getSimpleName();

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SharedRulesActivity.class.getName());

	protected static final int DOWNLOAD_RULE_REQUEST_CODE = 5001;
	
	private TemplateRulesAdapter mSharedRulesAdapter = null;
	
	private EnhancedListView mElvTemplateRules = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_shared_rules);
		
		Intent intent = getIntent();
		// Validates intent
		if (intent == null) {
			LOGGER.error(CLASS_NAME + "#onCreate(...) intent is NULL");
			finish();
			return;
		}
		
		LOGGER.info(CLASS_NAME + "#onCreate(...) intent action: " + intent.getAction());
		LOGGER.info(CLASS_NAME + "#onCreate(...) intent data: " + intent.getDataString());
		LOGGER.info(CLASS_NAME + "#onCreate(...) intent type: " + intent.getType());
		
		Uri dataUri = intent.getData();
		
		// Validates dataUri
		if (dataUri == null) {
			LOGGER.error(CLASS_NAME + "#onCreate(...) dataUri is NULL");
			finish();
			return;
		}
		
		String encryptedRulesJson = FilesUtils.readFile(dataUri);
		
		// Validates dataUri content
		if (TextUtils.isEmpty(encryptedRulesJson)) {
			LOGGER.error(CLASS_NAME + "#onCreate(...) dataUri content is NULL");
			finish();
			return;
		}
		
		String rulesJson = CryptoUtils.decryptWithAttachedKey(encryptedRulesJson);
		LOGGER.debug(CLASS_NAME + "#onCreate(...) rulesJson: " + rulesJson);
		
		try {
			List<TemplateRule> templateRules = JsonHelper.getInstance().getMapper()
					.readValue(rulesJson, new TypeReference<List<TemplateRule>>() {});
			
			mSharedRulesAdapter = new TemplateRulesAdapter(
					SharedRulesActivity.this, R.layout.list_item_template_rule_card,
					templateRules);
			mElvTemplateRules = (EnhancedListView) findViewById(R.id.elvTemplateRules);
			mElvTemplateRules.setAdapter(mSharedRulesAdapter);
			mElvTemplateRules.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					TemplateRule templateRuleToDownload = mSharedRulesAdapter.getItem(position);
					launchTemplateRuleEditorActivity(templateRuleToDownload);
				}
			});
			mElvTemplateRules.setDismissCallback(new EnhancedListView.OnDismissCallback() {
				@Override
				public Undoable onDismiss(EnhancedListView listView, final int position) {
					final TemplateRule ruleToDelete = mSharedRulesAdapter.getItem(position);
					mSharedRulesAdapter.removeAll(Arrays.asList(ruleToDelete));
					return new EnhancedListView.Undoable() {
	                    @Override
	                    public void undo() {
	                    	mSharedRulesAdapter.add(position, ruleToDelete);
	                    }
	                };
				}
			});
			mElvTemplateRules.enableSwipeToDismiss()
				.setSwipeDirection(EnhancedListView.SwipeDirection.BOTH)
				.setUndoStyle(EnhancedListView.UndoStyle.MULTILEVEL_POPUP)
				.setRequireTouchBeforeDismiss(false)
				.setUndoHideDelay(3000);
		} catch (JsonParseException e) {
			LOGGER.error("Error: ", e);
		} catch (JsonMappingException e) {
			LOGGER.error("Error: ", e);
		} catch (IOException e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug(CLASS_NAME + "#onCreateOptionsMenu(...)");
		
		getMenuInflater().inflate(R.menu.activity_shared_rules_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");
		
		switch (item.getItemId()) {
		case R.id.action_save_shared_rules:
			boolean doesSharedRulesStored = storeSharedRules(mSharedRulesAdapter.getItems());
			if (doesSharedRulesStored) {
				finish();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case DOWNLOAD_RULE_REQUEST_CODE:
				TemplateRule downlodedTemplateRule = (TemplateRule) data.getSerializableExtra(Config.Extra.EXTRA_TEMPLATE_RULE);
				mSharedRulesAdapter.remove(downlodedTemplateRule);
				// If there aren't any rules to download, finish
				if (mSharedRulesAdapter.isEmpty()) {
					finish();
				}
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
	}
	
	private void launchTemplateRuleEditorActivity(
			TemplateRule templateRuleToDownload) {
		Intent intent = new Intent(SharedRulesActivity.this, TemplateRuleEditorActivity.class);
		intent.setAction(Config.Action.ACTION_DOWNLOAD_RULE);
		intent.putExtra(Config.Extra.EXTRA_TEMPLATE_RULE, templateRuleToDownload);
		startActivityForResult(intent, DOWNLOAD_RULE_REQUEST_CODE);
	}
	
	private boolean validateTemplateRules(List<TemplateRule> templateRules) {
		for (TemplateRule templateRule : templateRules) {
			if (!templateRule.isValid(SharedRulesActivity.this)) {
				launchTemplateRuleEditorActivity(templateRule);
				ToastUtils.show(AppState.BUILD_RELEASE, SharedRulesActivity.this,
						R.string.toast_shared_template_rule_invalid,
						Toast.LENGTH_SHORT);
				return false;
			}
		}
		return true;
	}
	
	private boolean storeSharedRules(List<TemplateRule> templateRules) {
		// Validates template rules
		if (templateRules == null || templateRules.isEmpty()) {
			ToastUtils.show(AppState.BUILD_RELEASE, SharedRulesActivity.this,
					R.string.toast_shared_template_rules_empty,
					Toast.LENGTH_SHORT);
			return true;
		}
		// Validates all template rules are valid
		boolean validateTemplateRules = validateTemplateRules(templateRules);
		if (!validateTemplateRules) {
			return false;
		}
		// Store valid rules
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		for (TemplateRule templateRule : templateRules) {
			Rule rule = new Rule(templateRule);
			ruleDAO.store(DefaultUser.class, rule);
		}
		RulesUtils.activateActiveRules();
		return true;
	}
	
}

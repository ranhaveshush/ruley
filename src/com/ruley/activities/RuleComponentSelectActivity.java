package com.ruley.activities;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.adapters.RuleComponentCardsAdapter;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.IRuleComponent;

public class RuleComponentSelectActivity extends GABaseActivity {
	
	private static final String CLASS_NAME = RuleComponentSelectActivity.class.getSimpleName();

	private static final Logger LOGGER = LoggerFactory.getLogger(RuleComponentSelectActivity.class.getName());

	private static final int ADD_RULE_COMPONENT_REQUEST_CODE = 3001;

	private StickyListHeadersListView slv = null;
	
	/**
	 * Rule Component Select Activity states enum. Inner class representing all the possiable state
	 * the rule component select activity may be in.
	 * 
	 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
	 * 
	 */
	public enum State {
		ADD_TRIGGER, ADD_ACTION
	}

	private State state = null;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		
		outState.putSerializable("state", state);
		
		// Calls the superclass so it can save the view hierarchy state
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_rule_component_select);
		
		String action = getIntent().getAction();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			state = (State) savedInstanceState.getSerializable("state");
		} else if (Config.Action.ACTION_ADD_TRIGGER.equals(action)) {
			state = State.ADD_TRIGGER;
		} else if (Config.Action.ACTION_ADD_ACTION.equals(action)) {
			state = State.ADD_ACTION;
		}

		List<? extends IRuleComponent> tempList = null;
		switch (state) {
		case ADD_TRIGGER:
			setTitle(R.string.title_activity_trigger_select);
			tempList = Config.Triggers.LIST;
			break;
		case ADD_ACTION:
			setTitle(R.string.title_activity_action_select);
			tempList = Config.Actions.LIST;
			break;
		default:
			throw new IllegalStateException(CLASS_NAME + " caused by invalid action");
		}

		final List<? extends IRuleComponent> ruleComponents = tempList;
		RuleComponentCardsAdapter ruleComponentAdapter = new RuleComponentCardsAdapter(this, ruleComponents);
		slv = (StickyListHeadersListView) findViewById(android.R.id.list);
        slv.setAreHeadersSticky(true);
		slv.setAdapter(ruleComponentAdapter);
		slv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				LOGGER.debug("onListItemClick(...)");
				
				IRuleComponent ruleComponent = ruleComponents.get(position);
				Class<? extends Activity> ruleComponentEditorClass = ruleComponent.getEditor();
				Intent intent = new Intent(RuleComponentSelectActivity.this, ruleComponentEditorClass);
				startActivityForResult(intent, ADD_RULE_COMPONENT_REQUEST_CODE);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");
		LOGGER.info("item selected: " + item.getTitle());

		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");

		if (resultCode == RESULT_OK) {
			if (requestCode == ADD_RULE_COMPONENT_REQUEST_CODE) {
				String extraRuleComponent = null;
				switch (state) {
				case ADD_TRIGGER:
					extraRuleComponent = Config.Extra.EXTRA_TRIGGER;
					break;
				case ADD_ACTION:
					extraRuleComponent = Config.Extra.EXTRA_ACTION;
					break;
				}
				Intent resIntent = new Intent();
				resIntent.putExtra(extraRuleComponent,
						data.getSerializableExtra(extraRuleComponent));
				setResult(RESULT_OK, resIntent);
				finish();
			}
		}
	}

}

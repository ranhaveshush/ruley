package com.ruley.activities.pickers;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.adapters.AppsAdapter;
import com.ruley.config.Config;
import com.ruley.utilities.AppsUtils;

public class AppPickerActivity extends GABaseActivity {
	
	private List<ApplicationInfo> mLaunchableInstalledApps = null;
	
	private View mVgLoading = null;
	
	private AppsAdapter mAppsAdapter = null;
	
	private StickyListHeadersListView mSlvApps = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_app_picker);
		
		mVgLoading = findViewById(R.id.vgLoading);
		mSlvApps = (StickyListHeadersListView) findViewById(R.id.slvApps);
		mSlvApps.setDrawingListUnderStickyHeader(true);
        mSlvApps.setAreHeadersSticky(true);
		mSlvApps.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ApplicationInfo appInfo = mLaunchableInstalledApps.get(position);
				
				Intent resIntent = new Intent();
				resIntent.putExtra(Config.Extra.EXTRA_APP_INFO, appInfo);
				setResult(RESULT_OK, resIntent);
				finish();
			}
		});
		
		// Asynchronously listing launchable installed apps
		new GetLaunchalbeInstalledAppsAsyncTask().execute();
	}
	
	private class GetLaunchalbeInstalledAppsAsyncTask extends AsyncTask<Void, Integer, List<ApplicationInfo>> {

		@Override
		protected void onPreExecute() {
			showLoadingView();
		}

		@Override
		protected List<ApplicationInfo> doInBackground(Void... params) {
			return AppsUtils.getLaunchableInstalledApps();
		}
		
		@Override
		protected void onPostExecute(List<ApplicationInfo> apps) {
			
			hideLoadingView();
			
			if (apps != null && !apps.isEmpty()) {
				mLaunchableInstalledApps = apps;
				mAppsAdapter = new AppsAdapter(AppPickerActivity.this, mLaunchableInstalledApps);
				mSlvApps.setAdapter(mAppsAdapter);
			}
		}
		
	}
	
	private void showLoadingView() {
		mVgLoading.setAlpha(1f);
		mVgLoading.setVisibility(View.VISIBLE);
	}
	
	private void hideLoadingView() {
		mVgLoading.animate().alpha(0f).setDuration(1000).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mVgLoading.setVisibility(View.GONE);
			}
		});
	}
	
}

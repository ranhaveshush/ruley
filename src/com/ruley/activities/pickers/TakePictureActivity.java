/**
 * 
 */
package com.ruley.activities.pickers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ruley.R;
import com.ruley.utilities.CameraUtils;
import com.ruley.utilities.FilesUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TakePictureActivity extends Activity {
	
	private static final String CLASS_NAME = TakePictureActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TakePictureActivity.class.getName());
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmss");
	
	public static final String EXTRA_CAMERA_FACING = "cameraFacing";
	
	public static final String EXTRA_DIRECTORY_PATH = "directoryPath";
	
	private Camera mCamera = null;
	
	private int mCameraFacing = CameraInfo.CAMERA_FACING_FRONT;
	
	private String mDirectoryPath = null;
	
	private String mPicFilePath = null;

	private SurfaceView mDummySurfaceView = null;

	private SurfaceHolder mSurfaceHolder;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		super.onSaveInstanceState(outState);
		
		outState.putInt(EXTRA_CAMERA_FACING, mCameraFacing);
		outState.putString(EXTRA_DIRECTORY_PATH, mDirectoryPath);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_picture);
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mCameraFacing = savedInstanceState.getInt(EXTRA_CAMERA_FACING);
			mDirectoryPath = savedInstanceState.getString(EXTRA_DIRECTORY_PATH);
		// If creating a new instance
		} else {
			Intent intent = getIntent();
			mCameraFacing = intent.getIntExtra(EXTRA_CAMERA_FACING, CameraInfo.CAMERA_FACING_FRONT);
			mDirectoryPath = intent.getStringExtra(EXTRA_DIRECTORY_PATH);
		}
		
		// Constructs the picture file path
		StringBuilder sb = new StringBuilder();
		sb.append(mDirectoryPath);
		sb.append("pic_");
		sb.append(DATE_FORMAT.format(new Date()));
		sb.append(".jpg");
		mPicFilePath = sb.toString();
	}
	
	@Override
	protected void onResume() {
		LOGGER.debug(CLASS_NAME + "#onResume(...)");
		super.onResume();
		
		mDummySurfaceView = (SurfaceView) findViewById(R.id.svDummy);
		mSurfaceHolder = mDummySurfaceView.getHolder();
		mSurfaceHolder.addCallback(mSurfaceCallback);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	protected void onPause() {
		LOGGER.debug(CLASS_NAME + "#onPause(...)");
		super.onPause();
		
		releaseCamera();
	}

	SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			LOGGER.debug(CLASS_NAME + "#surfaceCreated(...)");
			
			int cameraId = CameraUtils.getCameraId(mCameraFacing);
			try {
				mCamera = Camera.open(cameraId);
				if (mCamera != null) {
					Parameters parameters = mCamera.getParameters();
					parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
					mCamera.setParameters(parameters);
					mCamera.setDisplayOrientation(90);
					mCamera.setPreviewDisplay(holder);
				}
			} catch (Exception e) {
				LOGGER.error("Error: ", e);
				finish();
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			LOGGER.debug(CLASS_NAME + "#surfaceChanged(...)");

			mCamera.startPreview();

			mCamera.takePicture(null, null, new Camera.PictureCallback() {
				@Override
				public void onPictureTaken(byte[] data, Camera camera) {
					FileOutputStream fos = null;
					try {
						File picFile = FilesUtils.getOrCreateFile(mPicFilePath);
						fos = new FileOutputStream(picFile);
						fos.write(data);
						fos.flush();
						LOGGER.info("Picture captured");
					} catch (FileNotFoundException e) {
						LOGGER.error("Error: ", e);
					} catch (IOException e) {
						LOGGER.error("Error: ", e);
					} finally {
						if (fos != null) {
							try {
								fos.close();
							} catch (IOException e) {
								LOGGER.error("Error: ", e);
							}
						}
						finish();
					}
				}
			});
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			LOGGER.debug(CLASS_NAME + "#surfaceDestroyed(...)");
		}
	};
	
	private void releaseCamera() {
		LOGGER.debug(CLASS_NAME + "#releaseCamera(...)");
		
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			LOGGER.info("Camera Released");
		}
	}

}

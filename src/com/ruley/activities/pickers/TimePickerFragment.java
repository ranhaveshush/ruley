/**
 * 
 */
package com.ruley.activities.pickers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TimePickerFragment extends DialogFragment {

	private static final String CLASS_NAME = TimePickerFragment.class
			.getSimpleName();
	
	private static final String HOUR = "hour";
	
	private static final String MINUTE = "minute";
	
	/**
	 * Callback interface.
	 * Container activity must implement this interface.
	 * 
	 * @author ran
	 *
	 */
	public interface Listener {
		/**
		 * When time on this is the event callback function
		 * to notify the parent activity.
		 * 
		 * @param fragmentTag	String represents the fragment tag
		 * @param hourOfDay		Integer represents the hour
		 * @param minute		Integer represents the minute
		 */
		public void onTimeOn(String fragmentTag, int hourOfDay, int minute);
		
		/**
		 * When time off this is the event callback function
		 * to notify the parent activity.
		 * 
		 * @param fragmentTag	String represents the fragment tag
		 */
		public void onTimeOff(String fragmentTag);
	}
	
	private Listener listener = null;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		try {
			listener = (Listener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.getClass().getSimpleName() + " must implement " + CLASS_NAME + ".Listener");
		}
	}
	
	public static TimePickerFragment newInstance(int hour, int minute) {
		TimePickerFragment fragment = new TimePickerFragment();
		
		Bundle args = new Bundle();
		args.putInt(HOUR, hour);
		args.putInt(MINUTE, minute);
		fragment.setArguments(args);
		
		return fragment;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Gets the dialog arguments
		Bundle args = getArguments();
		int hour = args.getInt(HOUR);
		int minute = args.getInt(MINUTE);

		// Inflates the time picker view
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View timeDialog = inflater.inflate(R.layout.dialog_time, null);
		TextView tvDialogTitle = (TextView) timeDialog.findViewById(R.id.tvDialogTitle);
		if (tvDialogTitle != null) {
			tvDialogTitle.setText(R.string.picker_title_time);
		}
		final TimePicker tpTime = (TimePicker) timeDialog.findViewById(R.id.tpTime);
		tpTime.setIs24HourView(true);
		tpTime.setCurrentHour(hour);
		tpTime.setCurrentMinute(minute);
				
		// Creates a new instance of AlertDialog and return it
		AlertDialog timePickerDialog = new AlertDialog.Builder(getActivity())
		.setView(timeDialog)
		.setPositiveButton(R.string.dialog_button_set, new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	listener.onTimeOn(getTag(),
	        			tpTime.getCurrentHour(),
	        			tpTime.getCurrentMinute());
	        }
	    })
		.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	        	listener.onTimeOff(getTag());
	        }
	    })
	    .create();
		
		return timePickerDialog;
	}

}
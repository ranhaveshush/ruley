/**
 * 
 */
package com.ruley.activities.pickers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DatePickerFragment extends DialogFragment {

	private static final String CLASS_NAME = DatePickerFragment.class
			.getSimpleName();
	
	private static final String YEAR = "year";
	
	private static final String MONTH = "month";
	
	private static final String DAY_OF_MONTH = "dayOfMonth";
	
	/**
	 * Callback interface.
	 * Container activity must implement this interface.
	 * 
	 * @author ran
	 *
	 */
	public interface Listener {
		/**
		 * When date on this is the event callback function
		 * to notify the parent activity.
		 * 
		 * @param fragmentTag	String represents the fragment's tag
		 * @param year			Integer represents the year
		 * @param monthOfYear	Integer represents the month
		 * @param dayOfMonth	Integer represents the day
		 */
		public void onDateOn(String fragmentTag, int year, int monthOfYear, int dayOfMonth);
		
		/**
		 * When date off this is the event callback function
		 * to notify the parent activity.
		 * 
		 * @param fragmentTag	String represents the fragment's tag
		 */
		public void onDateOff(String fragmentTag);
	}
	
	/**
	 * Listener.
	 */
	private Listener mListener = null;
	
	/**
	 * Holds the hosting activity.
	 */
	private Activity hostingActivity = null;
	
	@Override
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		try {
			mListener = (Listener) activity;
			// Gets the hosting activity
			hostingActivity = activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.getClass().getSimpleName() + " must implement " + CLASS_NAME + ".Listener");
		}
	}
	
	public static DatePickerFragment newInstance(int year, int month, int dayOfMonth) {
		DatePickerFragment fragment = new DatePickerFragment();
		
		Bundle args = new Bundle();
		args.putInt(YEAR, year);
		args.putInt(MONTH, month);
		args.putInt(DAY_OF_MONTH, dayOfMonth);
		fragment.setArguments(args );
		
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Gets the dialog arguments
		Bundle args = getArguments();
		int year = args.getInt(YEAR);
		int month = args.getInt(MONTH);
		int dayOfMonth = args.getInt(DAY_OF_MONTH);
		
		// Inflates the date picker view
		LayoutInflater inflater = hostingActivity.getLayoutInflater();
		final View dateDialog = (View) inflater.inflate(R.layout.dialog_date, null);
		TextView tvDialogTitle = (TextView) dateDialog.findViewById(R.id.tvDialogTitle);
		if (tvDialogTitle != null) {
			tvDialogTitle.setText(R.string.picker_title_date);
		}
		final DatePicker dpDate = (DatePicker) dateDialog.findViewById(R.id.dpDate);
		dpDate.updateDate(year, month, dayOfMonth);
		
		// Creates a new instance of AlertDialog and return it
		AlertDialog datePickerDialog = new AlertDialog.Builder(hostingActivity)
		.setView(dateDialog)
		.setPositiveButton(R.string.dialog_button_set, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	mListener.onDateOn(getTag(),
            			dpDate.getYear(),
            			dpDate.getMonth(),
            			dpDate.getDayOfMonth());
            }
        })
		.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            	mListener.onDateOff(getTag());
            }
        })
        .create();
		
		return datePickerDialog;
	}

}

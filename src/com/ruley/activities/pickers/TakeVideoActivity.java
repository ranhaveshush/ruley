/**
 * 
 */
package com.ruley.activities.pickers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.ruley.R;
import com.ruley.utilities.CameraUtils;
import com.ruley.utilities.FilesUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class TakeVideoActivity extends Activity {
	
	private static final String CLASS_NAME = TakeVideoActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TakeVideoActivity.class.getName());
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HHmmss");
	
	/**
	 * 20 seconds video file duration.
	 */
	private static final int MAX_DURATION_IN_MILLIS = 1000 * 20;
	
	/**
	 * 5MB video file size.
	 */
	private static final long MAX_FILE_SIZE_IN_BYTES = 5 * 1048576;
	
	public static final String EXTRA_CAMERA_FACING = "cameraFacing";
	
	public static final String EXTRA_DIRECTORY_PATH = "directoryPath";
	
	private MediaRecorder mMediaRecorder = null;
	
	private Camera mCamera = null;
	
	private int mCameraFacing = CameraInfo.CAMERA_FACING_FRONT;
	
	private String mDirectoryPath = null;
	
	private String mVideoFilePath = null;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		super.onSaveInstanceState(outState);
		
		outState.putInt(EXTRA_CAMERA_FACING, mCameraFacing);
		outState.putString(EXTRA_DIRECTORY_PATH, mDirectoryPath);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_picture);
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mCameraFacing = savedInstanceState.getInt(EXTRA_CAMERA_FACING);
			mDirectoryPath = savedInstanceState.getString(EXTRA_DIRECTORY_PATH);
		// If creating a new instance
		} else {
			Intent intent = getIntent();
			mCameraFacing = intent.getIntExtra(EXTRA_CAMERA_FACING, CameraInfo.CAMERA_FACING_FRONT);
			mDirectoryPath = intent.getStringExtra(EXTRA_DIRECTORY_PATH);
		}
		
		// Constructs the picture file path
		StringBuilder sb = new StringBuilder();
		sb.append(mDirectoryPath);
		sb.append("vid_");
		sb.append(DATE_FORMAT.format(new Date()));
		sb.append(".mp4");
		mVideoFilePath = sb.toString();
		
		SurfaceView dummySurfaceView = (SurfaceView) findViewById(R.id.svDummy);
		SurfaceHolder surfaceHolder = dummySurfaceView.getHolder();
		surfaceHolder.addCallback(mSurfaceCallback);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	protected void onPause() {
		LOGGER.debug(CLASS_NAME + "#onPause(...)");
		super.onPause();
		
		releaseMediaRecorder();
		releaseCamera();
	}

	SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			LOGGER.debug(CLASS_NAME + "#surfaceCreated(...)");
			
			int cameraId = CameraUtils.getCameraId(mCameraFacing);
			try {
				mCamera = Camera.open(cameraId);
				if (mCamera != null) {
					Parameters parameters = mCamera.getParameters();
					parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
					mCamera.setDisplayOrientation(180);
					mCamera.setParameters(parameters);
				}
				mCamera.setPreviewDisplay(holder);
			} catch (Exception e) {
				LOGGER.warn("This device doesn't have a back-facing camera");
				LOGGER.error("Error: ", e);
				finish();
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
			LOGGER.debug(CLASS_NAME + "#surfaceChanged(...)");

			mCamera.startPreview();
			
			mMediaRecorder = getMediaRecorder(holder);
			if (mMediaRecorder != null) {
				mMediaRecorder.start();
				finishActivityAfter(MAX_DURATION_IN_MILLIS);
//				ToastUtils.show(AppState.BUILD_RELEASE, TakeVideoActivity.this,
//						getWindow().getDecorView(), Toast.LENGTH_LONG,
//						R.layout.toast_rule_editor,
//						R.string.toast_video_recording_text,
//						R.string.toast_video_recording_button,
//						new OnClickListener() {
//							@Override
//							public void onClick(View view) {
//								finish();
//							}
//						});
			} else {
				finish();
			}
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			LOGGER.debug(CLASS_NAME + "#surfaceDestroyed(...)");
		}
	};
	
	private MediaRecorder getMediaRecorder(SurfaceHolder holder) {
		LOGGER.debug(CLASS_NAME + "#getMediaRecorder(...)");
		
		MediaRecorder mediaRecorder = new MediaRecorder();
		
		try {
			mCamera.unlock();
			mediaRecorder.setCamera(mCamera);
			mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
			File videoFile = FilesUtils.getOrCreateFile(mVideoFilePath);
			mediaRecorder.setOutputFile(videoFile.getPath());
			mediaRecorder.setMaxDuration(MAX_DURATION_IN_MILLIS);
			mediaRecorder.setMaxFileSize(MAX_FILE_SIZE_IN_BYTES);
			mediaRecorder.setPreviewDisplay(holder.getSurface());
			mediaRecorder.prepare();
		} catch (IllegalStateException ise) {
			LOGGER.error("IllegalStateException preparing MediaRecorder: ", ise);
			return null;
		} catch (IOException ioe) {
			LOGGER.error("IOException preparing MediaRecorder: ", ioe);
			return null;
		}
		
		return mediaRecorder;
	}
	
	private void releaseMediaRecorder() {
		LOGGER.debug(CLASS_NAME + "#releaseMediaRecorder(...)");
		
		if (mMediaRecorder != null) {
			mMediaRecorder.stop();
			mMediaRecorder.release();
			mCamera.lock();
			LOGGER.info("Media Recorder Released");
		}
	}
	
	private void releaseCamera() {
		LOGGER.debug(CLASS_NAME + "#releaseCamera(...)");
		
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			LOGGER.info("Camera Released");
		}
	}
	
	private void finishActivityAfter(long delayMillis) {
		LOGGER.debug(CLASS_NAME + "#finishActivityAfter(...)");
		
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				finish();
			}
		}, delayMillis);
	}

}

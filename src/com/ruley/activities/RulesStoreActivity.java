package com.ruley.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.config.Config;
import com.ruley.fragments.StoreSectionFragment;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.parse.TemplateRuleParse;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.UpdateCallback;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RulesStoreActivity extends GABaseActivity implements ActionBar.TabListener, StoreSectionFragment.Listener {
	
	private static final String CLASS_NAME = RulesStoreActivity.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RulesStoreActivity.class.getName());

	private static final int DOWNLOAD_RULE_REQUEST_CODE = 4001;
	
	private SectionsPagerAdapter mSectionsPagerAdapter = null;

	private ViewPager mViewPager = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_rules_store);
		
		setResult(RESULT_CANCELED);
		
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		String[] categoriesTitles = getResources().getStringArray(R.array.rules_store_categories);

		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getFragmentManager(), categoriesTitles);

		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		
		mViewPager.setCurrentItem(0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
			case DOWNLOAD_RULE_REQUEST_CODE:
				final TemplateRule downlodedTemplateRule = (TemplateRule) data.getSerializableExtra(Config.Extra.EXTRA_TEMPLATE_RULE);
				ParseUtils.updateTemplateRuleDownloaded(
						RulesStoreActivity.this, downlodedTemplateRule,
						new UpdateCallback<TemplateRuleParse>() {
							@Override
							public void done(TemplateRuleParse templateRuleParse) {
								refreshStoreSections(templateRuleParse);
							}
						});
				setResult(RESULT_OK);
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		LOGGER.debug(CLASS_NAME + "#onTabSelected(...)");
		
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// Do nothing
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// Do nothing
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		
		List<String> mTitles = null;
		
		List<Fragment> mFragments = null;

		public SectionsPagerAdapter(FragmentManager fm, String[] categoriesTitles) {
			super(fm);
			
			init(categoriesTitles);
		}
		
		public void init(String[] categoriesTitles) {
			// Validates categories
			if (categoriesTitles == null || categoriesTitles.length <= 0) {
				return;
			}
			
			mTitles = new ArrayList<String>();
			mFragments = new ArrayList<Fragment>();
			
			for (int i=0; i < categoriesTitles.length; ++i) {
				mTitles.add(categoriesTitles[i]);
				mFragments.add(StoreSectionFragment.newInstance(i));
			}
		}
		
		public void addItem(String title, Fragment fragment) {
			mTitles.add(title);
			mFragments.add(fragment);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragments.get(position);
		}

		@Override
		public int getCount() {
			return mFragments.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mTitles.get(position).toUpperCase(Locale.US);
		}
	}
	
	@Override
	public void onTemplateRuleToggleLike(TemplateRuleParse templateRuleParse) {
		refreshStoreSections(templateRuleParse);
	}
	
	@Override
	public void onTemplateRuleDownloaded(TemplateRuleParse templateRuleParse) {
		TemplateRule templateRuleToDownload = templateRuleParse.getTemplateRule();
		// Launches the template rule editor activity
		Intent intent = new Intent(RulesStoreActivity.this, TemplateRuleEditorActivity.class);
		intent.setAction(Config.Action.ACTION_DOWNLOAD_RULE);
		intent.putExtra(Config.Extra.EXTRA_TEMPLATE_RULE, templateRuleToDownload);
		startActivityForResult(intent, DOWNLOAD_RULE_REQUEST_CODE);
	}
	
	@Override
	public void onTemplateRuleDeleted(TemplateRuleParse templateRuleParse) {
		refreshStoreSections(templateRuleParse);
	}
	
	private void refreshStoreSections(TemplateRuleParse templateRuleParse) {
		for (int i=0; i < mSectionsPagerAdapter.getCount(); ++i) {
			StoreSectionFragment storeSectionFragment = (StoreSectionFragment) mSectionsPagerAdapter.getItem(i);
			if (storeSectionFragment.isVisible()) {
				storeSectionFragment.refresh(templateRuleParse);
			}
		}
	}

}

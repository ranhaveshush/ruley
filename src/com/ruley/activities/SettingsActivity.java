package com.ruley.activities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.base.GABasePreferenceActivity;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.utilities.PackageInfoUtils;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SettingsActivity extends GABasePreferenceActivity implements OnSharedPreferenceChangeListener {
	
	private static final String CLASS_NAME = SettingsActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SettingsActivity.class.getName());

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);
		
		initAppVersionPref();
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    
	    getPreferenceScreen().getSharedPreferences()
	            .registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
	    super.onPause();
	    
	    getPreferenceScreen().getSharedPreferences()
	            .unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		LOGGER.debug(CLASS_NAME + "#onSharedPreferenceChanged(...)");
		
		if (getString(R.string.pref_key_app_mode).equals(key)) {
			if (AppState.isAppEnabled()) {
				MyApplication.startBackgroundServices();
				if (AppState.isStickyNotificationEnabled()) {
					AppState.notifyActiveRules();
				}
			} else {
				MyApplication.stopBackgroundServices();
				AppState.cancelNotifications();
			}
		} else if (getString(R.string.pref_key_app_sticky_notification).equals(key)) {
			if (AppState.isStickyNotificationEnabled() && AppState.isAppEnabled()) {
				AppState.notifyActiveRules();
			} else {
				AppState.cancelNotifications();
			}
		}
	}
	
	private void initAppVersionPref() {
		Preference appVersionPref = findPreference(getString(R.string.pref_key_app_version_name));
		String appVersionName = PackageInfoUtils.getAppVersionName(SettingsActivity.this);
		appVersionPref.setSummary(appVersionName);
		
		// If the user is Ruley team user, enable easter egg
		if (ParseUtils.isRuleyTeamUser()) {
			appVersionPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
				
				int clickCount = 5;
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					if (--clickCount < 0) {
						throw new RuntimeException("This is a crash");
					} else {
						ToastUtils.show(AppState.BUILD_RELEASE, SettingsActivity.this, clickCount + " to crash", Toast.LENGTH_SHORT);
						return true;
					}
				}
			});
		}
	}
	
}
package com.ruley.activities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.base.GABaseActivity;
import com.ruley.adapters.RuleComponentsAdapter;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.fragments.dialogs.DialogLauncher;
import com.ruley.fragments.dialogs.OneButtonDialogFragment;
import com.ruley.fragments.dialogs.TwoButtonDialogFragment;
import com.ruley.listeners.OnResultListener;
import com.ruley.model.beans.actions.base.Action;
import com.ruley.model.beans.rules.IRuleComponent;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.rules.Rule.Listener;
import com.ruley.model.beans.rules.TemplateRule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.parse.RuleParse;
import com.ruley.model.parse.UserParse;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;
import com.ruley.utilities.RulesDependencyGraphUtils;
import com.ruley.utilities.RulesUtils;
import com.ruley.utilities.RulesUtils.BackupCallback;
import com.ruley.utilities.RulesUtils.RestoreType;
import com.ruley.utilities.ToastUtils;

import de.timroes.android.listview.EnhancedListView;
import de.timroes.android.listview.EnhancedListView.Undoable;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class RuleEditorActivity extends GABaseActivity implements
RuleComponentsAdapter.Listener, OneButtonDialogFragment.Listener, TwoButtonDialogFragment.Listener, OnResultListener {

	private static final String CLASS_NAME = RuleEditorActivity.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CLASS_NAME);
	
	private static final IActivityLogDAO ACTIVITY_LOG_DAO = DAOFactory
			.getFactory(DAOFactory.getFactoryType()).getActivityLogDAO();
	
	private static final IRuleDAO RULE_DAO = DAOFactory.getFactory(
			DAOFactory.getFactoryType()).getRuleDAO();
	
	public static final int ADD_TRIGGER_REQUEST_CODE = 2001;
	
	public static final int ADD_ACTION_REQUEST_CODE = 2002;
	
	private static final int EDIT_TRIGGER_REQUEST_CODE = 2003;
	
	private static final int EDIT_ACTION_REQUEST_CODE = 2004;

	private static final String FRAGMENT_TAG_INVALID_RULE = "invalidRule";

	private static final String FRAGMENT_TAG_RULE_NAME_MISSING = "ruleNameMissing";

	private Context mAppCtx = null;

	private Rule mRule = null;
	
	private boolean mIsRuleActive = true;
	
	private ViewGroup mRlRuleNameContainer = null;
	
	private EditText mEtRuleName = null;
	
	private RuleComponentsAdapter mRuleComponentsAdapter = null;
	
	private EnhancedListView mElvRuleComponents = null;
	
	private ActionMode mActionMode = null;
	
	private OnItemClickListener mRuleComponentItemEditorReferral = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			IRuleComponent item = (IRuleComponent) mRuleComponentsAdapter.getItem(position);
			Class<? extends Activity> editor = item.getEditor();
			if (item instanceof Trigger) {
				Intent intent = new Intent(RuleEditorActivity.this, editor);
				intent.putExtra(Config.Extra.EXTRA_TRIGGER, item);
				startActivityForResult(intent, EDIT_TRIGGER_REQUEST_CODE);
			} else if (item instanceof Action) {
				Intent intent = new Intent(RuleEditorActivity.this, editor);
				intent.putExtra(Config.Extra.EXTRA_ACTION, item);
				startActivityForResult(intent, EDIT_ACTION_REQUEST_CODE);
			}
		}
	};
	
	private OnItemLongClickListener mRuleComponentItemStartActionMode = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			mRuleComponentsAdapter.toggleSelectedItem(position);
			startActionMode();
			return true;
		}
	};
	
	private OnItemClickListener mRuleComponentItemToggleSelection = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			mRuleComponentsAdapter.toggleSelectedItem(position);
			finishActionMode();
		}
	};
	
	private CompoundButton mToggleLike = null;
	
	private OnCheckedChangeListener mOnLikeStateChangelistener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			Animation animation = mDynamicActionBar.getmActionButtonClickAnim();
			buttonView.startAnimation(animation);
			// Updates the client
			mRule.toggleLiked();
			// Updates the server
			TemplateRule templateRule = new TemplateRule(mRule);
			ParseUtils.toggleTemplateRuleLiked(RuleEditorActivity.this, templateRule, templateRule.isLiked(), null);
		}
	};
	
	/**
	 * Rule Editor states enum. Inner class representing all the possiable mState
	 * the editor may be in.
	 * 
	 * @author Ran Haveshush Email: ran.haveshush.dev@gmail.com
	 * 
	 */
	public enum State {
		CREATE_RULE, // Creates new rule
		EDIT_RULE, // Edits existing rule
		EDIT_BACKUP_RULE // Edits backup rule
	}

	private State mState = null;

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		
		outState.putSerializable(Config.Extra.EXTRA_STATE, mState);
		outState.putSerializable(Config.Extra.EXTRA_RULE, mRule);
		
		// Calls the superclass so it can save the view hierarchy mState
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_rule_editor);
		
		setResult(RESULT_CANCELED);

		mAppCtx = MyApplication.getContext();
		
		// If recreating a previously destroyed instance
		if (savedInstanceState != null) {
			mState = (State) savedInstanceState.getSerializable(Config.Extra.EXTRA_STATE);
			mRule = (Rule) savedInstanceState.getSerializable(Config.Extra.EXTRA_RULE);
		// If creating a new instance
		} else {
			// Gets the action from the intent
			Intent intent = getIntent();
			String action = intent.getAction();
			LOGGER.info("action: " + action);
	
			// Sets the mState of the activity by the invoked action
			if (Config.Action.ACTION_CREATE_RULE.equals(action)) {
				mState = State.CREATE_RULE;
			} else if (Config.Action.ACTION_EDIT_RULE.equals(action)) {
				mState = State.EDIT_RULE;
			} else if (Config.Action.ACTION_EDIT_BACKUP_RULE.equals(action)) {
				mState = State.EDIT_BACKUP_RULE;
			}
			
			switch (mState) {
			case CREATE_RULE:
				mRule = new Rule();
				break;
			case EDIT_RULE:
				final UUID ruleId = (UUID) intent.getSerializableExtra(Config.Extra.EXTRA_RULE_ID);
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				mRule = ruleDAO.get(ruleId);
				// TODO: check if this is right
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_COUNT_START);
				break;
			case EDIT_BACKUP_RULE:
				mRule = (Rule) intent.getSerializableExtra(Config.Extra.EXTRA_RULE);
				// TODO: check if this is right
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_COUNT_START);
				break;
			default:
				throw new IllegalStateException(CLASS_NAME + " caused by invalid action");
			}
		}
		
		// Sets the title of the activity
		switch (mState) {
		case CREATE_RULE:
			setTitle(R.string.title_create_rule);
			break;
		case EDIT_RULE:
		case EDIT_BACKUP_RULE:
			setTitle(R.string.title_edit_rule);
			break;
		}
		
		// Inactivates rule before editing
		mIsRuleActive = mRule.isActive();
		if (mIsRuleActive) {
			mRule.setActive(mAppCtx, false, null);
		}
		
		mRlRuleNameContainer  = (ViewGroup) findViewById(R.id.rlRuleNameContainer);
		mEtRuleName = (EditText) findViewById(R.id.etRuleName);
		mEtRuleName.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				mRule.setName(s.toString());
				mDynamicActionBar.update(RuleEditorActivity.this);
			}
		});
		String ruleName = mRule.getName();
		if (TextUtils.isEmpty(ruleName)) {
			mEtRuleName.setText(getString(R.string.rule_name_text));
		} else {
			mEtRuleName.setText(ruleName);
			showRuleName();
		}
		
		mRuleComponentsAdapter = new RuleComponentsAdapter(RuleEditorActivity.this, mRule);
		mElvRuleComponents = (EnhancedListView) findViewById(R.id.elvRuleComponents);
		mElvRuleComponents.setAdapter(mRuleComponentsAdapter);
		mElvRuleComponents.setOnItemClickListener(mRuleComponentItemEditorReferral);
		mElvRuleComponents.setOnItemLongClickListener(mRuleComponentItemStartActionMode);
		mElvRuleComponents.setDismissCallback(new EnhancedListView.OnDismissCallback() {
			@Override
			public Undoable onDismiss(EnhancedListView listView, final int position) {
				LOGGER.debug(CLASS_NAME + "#onDismiss(...)");
				final IRuleComponent ruleComponentToDelete = (IRuleComponent) mRuleComponentsAdapter.getItem(position);
				removeRuleComponent(ruleComponentToDelete);
				return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                    	restoreRuleComponent(position, ruleComponentToDelete);
                    }
                };
			}
		});
		mElvRuleComponents.enableSwipeToDismiss()
			.setSwipeDirection(EnhancedListView.SwipeDirection.BOTH)
			.setUndoStyle(EnhancedListView.UndoStyle.MULTILEVEL_POPUP)
			.setRequireTouchBeforeDismiss(false)
			.setUndoHideDelay(3000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		LOGGER.debug("onCreateOptionsMenu(...)");

		getMenuInflater().inflate(R.menu.activity_rule_editor_menu, menu);
		MenuItem actionSaveRule = menu.findItem(R.id.action_save_rule);
		MenuItem actionUploadRule = menu.findItem(R.id.action_upload_rule);
		MenuItem actionLikeRule = menu.findItem(R.id.action_like_rule);
		MenuItem actionBackupRule = menu.findItem(R.id.action_backup_rule);
		MenuItem actionRestoreRule = menu.findItem(R.id.action_restore_rule);
		MenuItem actionDeleteRule = menu.findItem(R.id.action_delete_rule);
		MenuItem actionTestActions = menu.findItem(R.id.action_test_actions);
		List<MenuItem> menuItems = new ArrayList<MenuItem>(Arrays.asList(
				actionSaveRule, actionUploadRule, actionLikeRule,
				actionTestActions));
		mDynamicActionBar.setValidatable(mRule);
		mDynamicActionBar.setMenuItems(menuItems);
		mDynamicActionBar.update(RuleEditorActivity.this);
		
		switch (mState) {
		case CREATE_RULE:
			actionSaveRule.setVisible(true);
			actionUploadRule.setVisible(false);
			actionLikeRule.setVisible(false);
			actionBackupRule.setVisible(false);
			actionRestoreRule.setVisible(false);
			actionDeleteRule.setVisible(false);
			actionTestActions.setVisible(true);
			break;
		case EDIT_RULE:
			actionSaveRule.setVisible(true);
			if (mRule.isMine()) {
				actionUploadRule.setVisible(true);
				actionLikeRule.setVisible(false);
			} else {
				actionUploadRule.setVisible(false);
				actionLikeRule.setVisible(true);
				if (mToggleLike == null) {
					mToggleLike = (CompoundButton) MenuItemCompat.getActionView(actionLikeRule);
					mToggleLike.setChecked(mRule.isLiked());
					mToggleLike.setOnCheckedChangeListener(mOnLikeStateChangelistener);
				}
			}
			actionBackupRule.setVisible(true);
			actionRestoreRule.setVisible(false);
			actionDeleteRule.setVisible(true);
			actionTestActions.setVisible(true);
			break;
		case EDIT_BACKUP_RULE:
			actionSaveRule.setVisible(true);
			actionUploadRule.setVisible(false);
			actionLikeRule.setVisible(false);
			actionBackupRule.setVisible(false);
			actionRestoreRule.setVisible(true);
			actionDeleteRule.setVisible(true);
			actionTestActions.setVisible(false);
			break;
		}
		
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		LOGGER.debug(CLASS_NAME + "#onOptionsItemSelected(...)");
		LOGGER.info("item selected: " + item.getTitle());
		
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
	    case android.R.id.home:
	    	onBackPressed();
	    	return true;
		case R.id.action_save_rule:
			if (mState == State.EDIT_BACKUP_RULE) {
				backupRule(RuleEditorActivity.this);
			} else if (mState == State.CREATE_RULE || mState == State.EDIT_RULE) {
				storeRule(RuleEditorActivity.this);
			}
			return true;
		case R.id.action_upload_rule:
			Intent intent = new Intent(RuleEditorActivity.this, TemplateRuleEditorActivity.class);
			intent.setAction(Config.Action.ACTION_UPLOAD_RULE);
			intent.putExtra(Config.Extra.EXTRA_TEMPLATE_RULE, mRule.toTemplateRule());
			startActivity(intent);
			return true;
		case R.id.action_backup_rule:
			backupRule(RuleEditorActivity.this);
			return true;
		case R.id.action_restore_rule:
			restoreRule(RuleEditorActivity.this);
			return true;
		case R.id.action_delete_rule:
			if (mState == State.EDIT_RULE) {
				deleteRule(RuleEditorActivity.this);
			} else if (mState == State.EDIT_BACKUP_RULE) {
				deleteBackupRule(RuleEditorActivity.this);
			}
			return true;
		case R.id.action_test_actions:
			for (Action action : mRule.getActions()) {
				action.perform(mAppCtx);
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		
		if (resultCode == RESULT_OK) {
			Trigger trigger = (Trigger) data.getSerializableExtra(Config.Extra.EXTRA_TRIGGER);
			Action action = (Action) data.getSerializableExtra(Config.Extra.EXTRA_ACTION);
			switch (requestCode) {
			case ADD_TRIGGER_REQUEST_CODE:
				LOGGER.debug("Add Trigger: " + trigger);
				mRule.getTriggers().add(trigger);
				mRuleComponentsAdapter.add(trigger);
				break;
			case ADD_ACTION_REQUEST_CODE:
				LOGGER.debug("Add Action: " + action);
				mRule.getActions().add(action);
				mRuleComponentsAdapter.add(action);
				break;
			case EDIT_TRIGGER_REQUEST_CODE:
				LOGGER.debug("Edit Trigger: " + trigger);
				// Updates the rule
				List<Trigger> triggers = mRule.getTriggers();
				triggers.remove(trigger);
				triggers.add(0, trigger);
				// Updates the rule components list view
				mRuleComponentsAdapter.remove(trigger);
				mRuleComponentsAdapter.add(trigger);
				mRuleComponentsAdapter.notifyDataSetChanged();
				break;
			case EDIT_ACTION_REQUEST_CODE:
				LOGGER.debug("Edit Action: " + action);
				// Updates the rule
				List<Action> actions = mRule.getActions();
				actions.remove(action);
				actions.add(0, action);
				// Updates the rule components list view
				mRuleComponentsAdapter.remove(action);
				mRuleComponentsAdapter.add(action);
				mRuleComponentsAdapter.notifyDataSetChanged();
				break;
			default:
				throw new IllegalStateException(CLASS_NAME
						+ " caused by invalid request code");
			}
			showRuleName();
			mDynamicActionBar.update(RuleEditorActivity.this);
		}
	}
	
	@Override
	public void onBackPressed() {
		LOGGER.debug(CLASS_NAME + "#onBackPressed(...)");
		
		switch (mState) {
		case CREATE_RULE:
		case EDIT_RULE:
			handleBackNavigation(this);
			break;
		case EDIT_BACKUP_RULE:
			finish();
			break;
		}
	}
	
	private void handleBackNavigation(OnResultListener onResultListener) {
		// If rule is valid, store it
		if (mRule.isValid(mAppCtx)) {
			storeRule(onResultListener);
		// If rule missing rule components only, notify the user
		} else if ( (mRule.getTriggers() == null || mRule.getTriggers().isEmpty())
				|| (mRule.getActions() == null || mRule.getActions().isEmpty()) ) {
			DialogLauncher.launchInvalidRuleDialog(RuleEditorActivity.this, FRAGMENT_TAG_INVALID_RULE);
		// If rule missing rule name only, notify the user
		} else if (TextUtils.isEmpty(mRule.getName())) {
			DialogLauncher.launchRuleNameMissingDialog(RuleEditorActivity.this, FRAGMENT_TAG_RULE_NAME_MISSING);
		// Otherwise, do nothing
		} else {
			onResultListener.onResultCanceled();
		}
	}
	
	private void startActionMode() {
		List<Object> selectedItems = mRuleComponentsAdapter.getSelectedItems();
		boolean hasSelectedItems = !selectedItems.isEmpty();
		if (mActionMode == null && hasSelectedItems) {
			mActionMode = RuleEditorActivity.this.startActionMode(mActionModeCallback);
			String acitonModeTitle = getString(R.string.title_action_mode);
			mActionMode.setTitle(String.format(acitonModeTitle, selectedItems.size()));
		}
	}
	
	private void finishActionMode() {
		List<Object> selectedItems = mRuleComponentsAdapter.getSelectedItems();
		String acitonModeTitle = getString(R.string.title_action_mode);
		mActionMode.setTitle(String.format(acitonModeTitle, selectedItems.size()));
		boolean hasSelectedItems = !selectedItems.isEmpty();
		if (mActionMode != null && !hasSelectedItems) {
			mActionMode.finish();
			mActionMode = null;
		}
	}
	
	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.list_your_rules_action_mode, menu);
			
			mElvRuleComponents.setOnItemClickListener(mRuleComponentItemToggleSelection);
			mElvRuleComponents.setOnItemLongClickListener(null);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.action_delete_selected:
				final List<IRuleComponent> selectedItems = new ArrayList<IRuleComponent>(
						(ArrayList<IRuleComponent>)(List<?>) mRuleComponentsAdapter.getSelectedItems());
				// Removes selected items from rule
				for (IRuleComponent selectedItem : selectedItems) {
					removeRuleComponent(selectedItem);
				}
				mode.finish();
				return true;
			default:
				return false;
			}
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mElvRuleComponents.setOnItemClickListener(mRuleComponentItemEditorReferral);
			mElvRuleComponents.setOnItemLongClickListener(mRuleComponentItemStartActionMode);
			mActionMode = null;
			mRuleComponentsAdapter.clearSelectedItems();
			mDynamicActionBar.update(RuleEditorActivity.this);
		}
	};
	
	private void removeRuleComponent(IRuleComponent ruleComponentToDelete) {
		if (ruleComponentToDelete instanceof Trigger) {
			mRuleComponentsAdapter.remove(ruleComponentToDelete);
			mRule.getTriggers().remove(ruleComponentToDelete);
		} else if (ruleComponentToDelete instanceof Action) {
			mRuleComponentsAdapter.remove(ruleComponentToDelete);
			mRule.getActions().remove(ruleComponentToDelete);
		}
		mDynamicActionBar.update(RuleEditorActivity.this);
	}
	
	private void restoreRuleComponent(int position, IRuleComponent ruleComponentToRestore) {
		LOGGER.debug(CLASS_NAME + "#restoreRuleComponent(...)");
		
		if (ruleComponentToRestore instanceof Trigger) {
			mRuleComponentsAdapter.add(position, ruleComponentToRestore);
			mRule.getTriggers().add((Trigger)ruleComponentToRestore);
		} else if (ruleComponentToRestore instanceof Action) {
			mRuleComponentsAdapter.add(position, ruleComponentToRestore);
			mRule.getActions().add((Action)ruleComponentToRestore);
		}
		mDynamicActionBar.update(RuleEditorActivity.this);
	}
	
	private void storeRule(final OnResultListener onResultListener) {
		LOGGER.debug(CLASS_NAME + "#storeRule(...)");
		
		switch (mState) {
		case CREATE_RULE:
		case EDIT_RULE:
			final Rule ruleToStore = mRule;
			
			// If it is a new or downloaded rule, auto activate it
			// If the rule were active before activate the rule
			if (mState == State.CREATE_RULE || mIsRuleActive) {
				ruleToStore.setActive(mAppCtx, true, new Listener() {
					@Override
					public void onInactivated() {
					}
					@Override
					public void onActivated() {
						RULE_DAO.store(DefaultUser.class, ruleToStore);
						if (AppState.isAppEnabled()) {
							MyApplication.updateBackgroundServices();
							ruleToStore.perform(RuleEditorActivity.this);
						}
						String saveRuleToastText = getResources().getString(R.string.toast_save_rule, mRule.getName());
						ToastUtils.show(AppState.BUILD_RELEASE,
								RuleEditorActivity.this,
								saveRuleToastText,
								Toast.LENGTH_SHORT);
						logToActivityLog(ruleToStore);
						onResultListener.onResultOk();
					}
					@Override
					public void onError(int errorMessageResourceId) {
						switch (errorMessageResourceId) {
						case R.string.rule_activation_error_circular_dependency:							
							RulesDependencyGraphUtils.resolveStronglyConnectedSets(mAppCtx, mRule);
							ToastUtils.show(AppState.BUILD_RELEASE,
									RuleEditorActivity.this,
									R.string.toast_rule_circular_dependency,
									Toast.LENGTH_LONG);
							break;
						case R.string.trigger_activation_error_location_service_disabled:
						case R.string.location_status_codes_geofence_not_available:
							DialogLauncher.launchActionSecuritySettings(
									RuleEditorActivity.this,
									MainActivity.FRAGMENT_TAG_LOCATION_SERVICE_DISABLED_ERROR);
							break;
						default:
							DialogLauncher.launchGeneralErrorDialog(
									RuleEditorActivity.this,
									MainActivity.FRAGMENT_TAG_GENERAL_ERROR,
									errorMessageResourceId);
							break;
						}
					}
				});
			} else {
				RULE_DAO.store(DefaultUser.class, ruleToStore);
				String saveRuleToastText = getResources().getString(R.string.toast_save_rule, mRule.getName());
				ToastUtils.show(AppState.BUILD_RELEASE,
						RuleEditorActivity.this,
						saveRuleToastText,
						Toast.LENGTH_SHORT);
				logToActivityLog(ruleToStore);
				onResultListener.onResultOk();
			}
			return;
		default:
			throw new IllegalStateException(CLASS_NAME + " caused by invalid action");
		}
	}
	
	private void backupRule(final OnResultListener onResultListener) {
		LOGGER.debug(CLASS_NAME + "#backupRule(...)");
		
		RulesUtils.backupRules(RuleEditorActivity.this, Arrays.asList(mRule), new BackupCallback() {
			@Override
			public void done() {
				String backupRuleToastText = getResources().getString(R.string.toast_backup_rule, mRule.getName());
				ToastUtils.show(AppState.BUILD_RELEASE,
						RuleEditorActivity.this,
						backupRuleToastText,
						Toast.LENGTH_SHORT);
				if (mState == State.EDIT_BACKUP_RULE) {
					onResultListener.onResultOk();
				}
			}
		});
	}
	
	private void restoreRule(final OnResultListener onResultListener) {
		LOGGER.debug(CLASS_NAME + "#restoreRule(...)");
		
		List<Rule> rulesToRestore = Arrays.asList(mRule);
		RulesUtils.restoreRules(RuleEditorActivity.this, RestoreType.MERGE, rulesToRestore);
		
		String restoreRuleToastText = getString(R.string.toast_restore_rule, mRule.getName());
		ToastUtils.show(AppState.BUILD_RELEASE,
				RuleEditorActivity.this,
				restoreRuleToastText,
				Toast.LENGTH_SHORT);
		
		onResultListener.onResultOk();
	}
	
	private void deleteRule(final OnResultListener onResultListener) {
		LOGGER.debug(CLASS_NAME + "#deleteRule(...)");
		
		// Inactivates rule to delete
		if (mRule.isActive()) {
			mRule.setActive(RuleEditorActivity.this, false, new Listener() {
				@Override
				public void onInactivated() {
					RULE_DAO.delete(mRule);
					String deleteRuleToastText = getResources().getString(R.string.toast_delete_rule, mRule.getName());
					ToastUtils.show(AppState.BUILD_RELEASE,
							RuleEditorActivity.this,
							deleteRuleToastText,
							Toast.LENGTH_SHORT);
					String action = getString(R.string.rule_deleted);
					ACTIVITY_LOG_DAO.log(mRule.getId(), mRule.getName(), action);
					if (AppState.isAppEnabled()) {
						MyApplication.updateBackgroundServices();
					}
					onResultListener.onResultOk();
				}
				@Override
				public void onActivated() {
				}
				@Override
				public void onError(int errorMessageResourceId) {
				}
			});
		} else {
			RULE_DAO.delete(mRule);
			String deleteRuleToastText = getResources().getString(R.string.toast_delete_rule, mRule.getName());
			ToastUtils.show(AppState.BUILD_RELEASE,
					RuleEditorActivity.this,
					deleteRuleToastText,
					Toast.LENGTH_SHORT);
			String action = getString(R.string.rule_deleted);
			ACTIVITY_LOG_DAO.log(mRule.getId(), mRule.getName(), action);
			onResultListener.onResultOk();
		}
	}
	
	private void deleteBackupRule(final OnResultListener onResultListener) {
		LOGGER.debug(CLASS_NAME + "#deleteBackupRule(...)");
		
		RuleParse ruleToDelete = new RuleParse(mRule);
		List<RuleParse> rulesToDelete = Arrays.asList(ruleToDelete);
		ParseUtils.removeRules(RuleEditorActivity.this, rulesToDelete,
				UserParse.RELATION_SAVED_RULES, new DoneCallback() {
			@Override
			public void done() {
				String deleteRuleToastText = getString(R.string.toast_delete_rule, mRule.getName());
				ToastUtils.show(AppState.BUILD_RELEASE,
						RuleEditorActivity.this,
						deleteRuleToastText,
						Toast.LENGTH_SHORT);
				onResultListener.onResultOk();
			}
		});
	}
	
	private void showRuleName() {
		LOGGER.debug(CLASS_NAME + "#showRuleName(...)");
		
		boolean showRuleName = !mRule.getActions().isEmpty()
				&& !mRule.getTriggers().isEmpty()
				&& mRule.validateTriggers(RuleEditorActivity.this);
		
		if (showRuleName) {
			mRlRuleNameContainer.setVisibility(View.VISIBLE);
		} else {
			mRlRuleNameContainer.setVisibility(View.GONE);
		}
	}
	
	private void logToActivityLog(final Rule ruleToStore) {
		String action = null;
		if (mState == State.CREATE_RULE) {
			action = getString(R.string.rule_created);
		} else if (mState == State.EDIT_RULE) {
			action = getString(R.string.rule_edited);
		}
		ACTIVITY_LOG_DAO.log(ruleToStore.getId(), ruleToStore.getName(), action);
	}

	@Override
	public void onAddTrigger() {
		Intent intent = new Intent(RuleEditorActivity.this, RuleComponentSelectActivity.class);
		intent.setAction(Config.Action.ACTION_ADD_TRIGGER);
		startActivityForResult(intent, ADD_TRIGGER_REQUEST_CODE);
	}

	@Override
	public void onAddAction() {
		Intent intent = new Intent(RuleEditorActivity.this, RuleComponentSelectActivity.class);
		intent.setAction(Config.Action.ACTION_ADD_ACTION);
		startActivityForResult(intent, ADD_ACTION_REQUEST_CODE);
	}

	@Override
	public void onConfirm(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onConfirm(...)");
		
		if (FRAGMENT_TAG_INVALID_RULE.equals(fragmentTag)) {
			onResultCanceled();
		} else if (FRAGMENT_TAG_RULE_NAME_MISSING.equals(fragmentTag)) {
			onResultCanceled();
		}
	}

	@Override
	public void onCancel(String fragmentTag) {
		LOGGER.debug(CLASS_NAME + "#onCancel(...)");
		
		if (FRAGMENT_TAG_INVALID_RULE.equals(fragmentTag)) {
			// Do nothing
		} else if (FRAGMENT_TAG_RULE_NAME_MISSING.equals(fragmentTag)) {
			mEtRuleName.requestFocus();
		}
	}

	@Override
	public void onResultOk() {
		Intent resultIntent = new Intent();
		resultIntent.putExtra(Config.Extra.EXTRA_RULE, mRule);
		setResult(RESULT_OK, resultIntent);
		finish();
	}

	@Override
	public void onResultCanceled() {
		setResult(RESULT_CANCELED);
		finish();
	}
	
}

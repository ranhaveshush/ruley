/**
 * 
 */
package com.ruley.activities.dialogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class CheckRuleDialogActivity extends DialogActivity {
	
	private static final String CLASS_NAME = CheckRuleDialogActivity.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CheckRuleDialogActivity.class.getName());

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		String dialogCheckRuleMessage = getString(R.string.dialog_check_rule_message);
		dialogCheckRuleMessage = String.format(dialogCheckRuleMessage, mRule.getName());
		mtvMessage.setText(dialogCheckRuleMessage);
		mbPositive.setText(R.string.dialog_check_rule_positive_button);
		mbPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_SHARE);
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				ruleDAO.store(DefaultUser.class, mRule);
				
				// Launches share rule dialog activity
				Intent intent = new Intent(CheckRuleDialogActivity.this, ShareRuleDialogActivity.class);
				intent.putExtra(Config.Extra.EXTRA_RULE, mRule);
				startActivity(intent);
				finish();
			}
		});
		mbNatural.setVisibility(View.GONE);
		mbNegative.setText(R.string.dialog_check_rule_negative_button);
		mbNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_REPORT);
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				ruleDAO.store(DefaultUser.class, mRule);
				
				// Launches report rule dialog activity
				Intent intent = new Intent(CheckRuleDialogActivity.this, ReportRuleDialogActivity.class);
				intent.putExtra(Config.Extra.EXTRA_RULE, mRule);
				startActivity(intent);
				finish();
			}
		});
	}

	
	
}

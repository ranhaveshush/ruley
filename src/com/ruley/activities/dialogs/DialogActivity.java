/**
 * 
 */
package com.ruley.activities.dialogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class DialogActivity extends Activity {
	
	private static final String CLASS_NAME = DialogActivity.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DialogActivity.class.getName());
	
	protected Rule mRule = null;
	
	private View mvBackground = null;
	
	protected TextView mtvMessage = null;
	
	protected Button mbPositive = null;
	
	protected Button mbNatural = null;
	
	protected Button mbNegative = null;
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		
		outState.putSerializable(Config.Extra.EXTRA_RULE, mRule);
		
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_dialog);
		
		if (savedInstanceState != null) {
			mRule = (Rule) savedInstanceState.getSerializable(Config.Extra.EXTRA_RULE);
		} else {
			mRule = (Rule) getIntent().getSerializableExtra(Config.Extra.EXTRA_RULE);
		}
		
		mvBackground = (View) findViewById(R.id.vBackground);
		mvBackground.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mtvMessage = (TextView) findViewById(R.id.tvMessage);
		mbPositive = (Button) findViewById(R.id.bPositive);
		mbNatural = (Button) findViewById(R.id.bNatural);
		mbNegative = (Button) findViewById(R.id.bNegative);
	}

}

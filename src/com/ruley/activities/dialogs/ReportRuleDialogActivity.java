/**
 * 
 */
package com.ruley.activities.dialogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.utilities.PickersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ReportRuleDialogActivity extends DialogActivity {
	
	private static final String CLASS_NAME = ReportRuleDialogActivity.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ReportRuleDialogActivity.class.getName());

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		String dialogCheckRuleMessage = getString(R.string.dialog_report_rule_message);
		dialogCheckRuleMessage = String.format(dialogCheckRuleMessage, mRule.getName());
		mtvMessage.setText(dialogCheckRuleMessage);
		mbPositive.setText(R.string.dialog_report_rule_positive_button);
		mbPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_REPORTED);
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				ruleDAO.store(DefaultUser.class, mRule);
				
				// Launches report rule email
				String subject = "[Rule - Report] " + mRule.getName();
				
				String deviceInfoTitle = getString(R.string.dialog_report_rule_email_device_info_title);
				String deviceDescription = deviceInfoTitle + " " + Build.MANUFACTURER + ", " +  Build.MODEL + "\n";
				
				String androidInfoTitle = getString(R.string.dialog_report_rule_email_android_info_title);
				String androidDescription = androidInfoTitle + " API level " +  Build.VERSION.SDK_INT + "\n\n";
				
				String ruleInfoTitle = getString(R.string.dialog_report_rule_email_rule_info_title) + "\n";
				String ruleDescription = ruleInfoTitle + mRule.generateDescription(ReportRuleDialogActivity.this) + "\n";
				
				String problemTitle = getString(R.string.dialog_report_rule_email_problem_title);
				String text = deviceDescription + androidDescription + ruleDescription + problemTitle + "\n";
				
				PickersUtils.launchEmailPicker(ReportRuleDialogActivity.this, Config.MY_EMAIL, subject, text);
				
				finish();
			}
		});
		mbNatural.setText(R.string.dialog_report_rule_natural_button);
		mbNatural.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mbNegative.setText(R.string.dialog_report_rule_negative_button);
		mbNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_WONT_REPORT);
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				ruleDAO.store(DefaultUser.class, mRule);
				
				finish();
			}
		});
	}

	
	
}

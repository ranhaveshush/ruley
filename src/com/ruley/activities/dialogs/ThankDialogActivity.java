/**
 * 
 */
package com.ruley.activities.dialogs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.ruley.R;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ThankDialogActivity extends DialogActivity {
	
	private static final String CLASS_NAME = ThankDialogActivity.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ThankDialogActivity.class.getName());

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		mtvMessage.setText(R.string.dialog_thanks_message);
		mbPositive.setText(R.string.dialog_thanks_positive_button);
		mbPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mbNatural.setVisibility(View.GONE);
		mbNegative.setVisibility(View.GONE);
	}

}

/**
 * 
 */
package com.ruley.activities.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.ruley.R;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.utilities.AnalyticsUtils;
import com.ruley.utilities.ImageUtils;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.ParseUtils.DoneCallback;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ShareRuleDialogActivity extends DialogActivity {
	
	private static final String CLASS_NAME = ShareRuleDialogActivity.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ShareRuleDialogActivity.class.getName());
	
	private UiLifecycleHelper mUiHelper = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate(savedInstanceState);
		
		mUiHelper = new UiLifecycleHelper(ShareRuleDialogActivity.this, null);
		mUiHelper.onCreate(savedInstanceState);
		
		String dialogShareRuleMessage = getString(R.string.dialog_share_rule_message);
		dialogShareRuleMessage = String.format(dialogShareRuleMessage, mRule.getName());
		mtvMessage.setText(dialogShareRuleMessage);
		mbPositive.setText(R.string.dialog_share_rule_positive_button);
		mbPositive.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ParseUtils.shareTemplateRule(ShareRuleDialogActivity.this, mRule.toTemplateRule(), new DoneCallback() {
					@Override
					public void done() {
						shareRuleOnFacebook();
					}
				});
			}
		});
		mbNatural.setText(R.string.dialog_share_rule_natural_button);
		mbNatural.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		mbNegative.setText(R.string.dialog_share_rule_negative_button);
		mbNegative.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mRule.setPerformanceState(Rule.PERFORMANCE_STATE_WONT_SHARE);
				IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
				ruleDAO.store(DefaultUser.class, mRule);
				
				finish();
			}
		});
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
		LOGGER.debug(CLASS_NAME + "#onSaveInstanceState(...)");
		super.onSaveInstanceState(outState);
		
		mUiHelper.onSaveInstanceState(outState);
	}
	
	@Override
	protected void onResume() {
		LOGGER.debug(CLASS_NAME + "#onResume(...)");
		super.onResume();
		
		mUiHelper.onResume();
	}

	@Override
	protected void onPause() {
		LOGGER.debug(CLASS_NAME + "#onPause(...)");
		super.onPause();
		
		mUiHelper.onPause();
	}

	@Override
	protected void onDestroy() {
		LOGGER.debug(CLASS_NAME + "#onDestroy(...)");
		super.onDestroy();
		
		mUiHelper.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		LOGGER.debug(CLASS_NAME + "#onActivityResult(...)");
		super.onActivityResult(requestCode, resultCode, data);
		
		mUiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
			@Override
			public void onError(PendingCall pendingCall, Exception error, Bundle data) {
				LOGGER.error("Error: ", error);
				finish();
			}
			@Override
			public void onComplete(PendingCall pendingCall, Bundle data) {
				boolean didComplete = FacebookDialog.getNativeDialogDidComplete(data);
				String completionGesture = FacebookDialog.getNativeDialogCompletionGesture(data);
				
				if (!didComplete) {
					LOGGER.error(CLASS_NAME + " facebook share dialog completed with an error");
					finish();
				} else if (FacebookDialog.COMPLETION_GESTURE_CANCEL.equals(completionGesture)) {
					LOGGER.warn(CLASS_NAME + " the user completed the facebook share dialog with cancel gesture");
					finish();
				} else {
					mRule.setPerformanceState(Rule.PERFORMANCE_STATE_SHARED);
					IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
					ruleDAO.store(DefaultUser.class, mRule);
					
					AnalyticsUtils.sendSocialInteraction("Facebook", "Share", "Rule");
					
					// Launches thank activity dialog
					Intent intent = new Intent(ShareRuleDialogActivity.this, ThankDialogActivity.class);
					intent.putExtra(Config.Extra.EXTRA_RULE, mRule);
					startActivity(intent);
					
					finish();
				}
			}
		});
	}
	
	@SuppressLint("NewApi")
	private void shareRuleOnFacebook() {
		String facebookAppNamespace = getString(R.string.facebook_app_namespace);
		
		String objectTypeRule = getString(R.string.open_graph_object_type_rule);
		OpenGraphObject ruleObject = OpenGraphObject.Factory
				.createForPost(facebookAppNamespace + ":" + objectTypeRule);
		ruleObject.setProperty("title", mRule.getName());
		
		Drawable drawable = getResources().getDrawable(R.drawable.open_graph_action_icon);
		Bitmap bitmap = ImageUtils.toBitmap(drawable);
		List<Bitmap> images = new ArrayList<Bitmap>();
		images.add(bitmap);

		String actionTypeShared = getString(R.string.open_graph_action_type_shared);
		OpenGraphAction sharedAction = GraphObject.Factory
				.create(OpenGraphAction.class);
		sharedAction.setType(facebookAppNamespace + ":" + actionTypeShared);
		sharedAction.setProperty(objectTypeRule, ruleObject);

		FacebookDialog shareDialog = new FacebookDialog.OpenGraphActionDialogBuilder(
				ShareRuleDialogActivity.this, sharedAction, objectTypeRule)
			.setImageAttachmentsForAction(images)
			.build();
		mUiHelper.trackPendingDialogCall(shareDialog.present());
	}

}

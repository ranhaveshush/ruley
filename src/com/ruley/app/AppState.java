/**
 * 
 */
package com.ruley.app;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.ruley.R;
import com.ruley.activities.MainDispatchActivity;
import com.ruley.activities.SettingsActivity;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.users.DefaultUser;
import com.ruley.model.beans.users.base.User;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IUserDAO;
import com.ruley.utilities.PrefsUtils;

/**
 * Represents the state of the app.
 * Which rules are activate.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public abstract class AppState {
	
	private static final String CLASS_NAME = AppState.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AppState.class.getName());
	
	public static final int BUILD_DEBUG = 0;
	public static final int BUILD_BETA = 1;
	public static final int BUILD_RELEASE = 2;
	
	public static void notifyActiveRules() {
		LOGGER.debug(CLASS_NAME + "#notifyActiveRules(...)");
		
		Context appCtx = MyApplication.getContext();
		
		IUserDAO userDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getUserDAO();
		
		List<Rule> activeRules = null;
		
		// Gets default user active rules
		User defaultUser = userDAO.get(DefaultUser.class);
		if (defaultUser != null) {
			activeRules = defaultUser.getActiveRules();
		}
		
		// Gets the pending intents
		PendingIntent startAppPendingIntent = getStartAppPendingIntent(appCtx);
		PendingIntent appSettingsPendingIntent = getAppSettingsPendingIntent(appCtx);
		
		// Gets the number of active rules
		int numberOfActiveRules = (activeRules == null) ? 0 : activeRules.size();
		// Constructs the notification texts
		String notificationTitleActiveRulesText = appCtx.getResources().getQuantityString(R.plurals.notify_active_rules, numberOfActiveRules);
		notificationTitleActiveRulesText = String.format(notificationTitleActiveRulesText, numberOfActiveRules);
		String notificationActionSettingsText = appCtx.getString(R.string.notification_action_settings);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(appCtx)
			.setOngoing(true)
			.setPriority(NotificationCompat.PRIORITY_MAX)
		    .setSmallIcon(R.drawable.ic_stat_notification)
		    .setContentTitle(Config.APP_NAME)
			.setContentText(notificationTitleActiveRulesText)
			.setContentIntent(startAppPendingIntent)
			.addAction(R.drawable.ic_stat_settings, notificationActionSettingsText, appSettingsPendingIntent);
		
		if (activeRules != null && !activeRules.isEmpty()) {
			String title = appCtx.getString(R.string.notifiction_title_active_rules);
			NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
			inboxStyle.setBigContentTitle(title);
			for (Rule rule : activeRules) {
				inboxStyle.addLine(rule.getName());
			}
			builder.setStyle(inboxStyle);
		}
		
		Notification notification = builder.build();
		
		NotificationManager notificationManager =
			    (NotificationManager) appCtx.getSystemService(Context.NOTIFICATION_SERVICE);
		
		notificationManager.notify(R.id.appstate_notificaion_id, notification);
	}

	public static void cancelNotifications() {
		LOGGER.debug(CLASS_NAME + "#cancelNotifications(...)");
		
		Context appCtx = MyApplication.getContext();
		
		// Cancels the app state sticky notification
		NotificationManager notificationManager = (NotificationManager) appCtx.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(R.id.appstate_notificaion_id);
	}

	/**
	 * {@link PrefsUtils#getBuildMode()}
	 */
	public static int getBuildMode() {
		return PrefsUtils.getBuildMode();
	}

	/**
	 * {@link PrefsUtils#setBuildMode(int)}
	 */
	public static void setBuildMode(int buildMode) {
		PrefsUtils.setBuildMode(buildMode);
	}
	
	/**
	 * {@link PrefsUtils#isAppEnabled()}
	 */
	public static boolean isAppEnabled() {
		return PrefsUtils.isAppEnabled();
	}
	
	/**
	 * {@link PrefsUtils#setAppModePref(boolean)}
	 */
	public static void setAppModePref(boolean enabled) {
		PrefsUtils.setAppModePref(enabled);
	}

	/**
	 * {@link PrefsUtils#isFirstRun()}
	 */
	public static boolean isFirstRun() {
		return PrefsUtils.isFirstRun();
	}

	/**
	 * {@link PrefsUtils#isStickyNotificationEnabled()}
	 */
	public static boolean isStickyNotificationEnabled() {
		return PrefsUtils.isStickyNotificationEnabled();
	}
	
	private static PendingIntent getAppSettingsPendingIntent(Context context) {
		Intent appSettingsIntent = new Intent(context, SettingsActivity.class);
		PendingIntent appSettingsPendingIntent = PendingIntent.getActivity(context,
				R.id.appstate_notificaion_id, appSettingsIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		return appSettingsPendingIntent;
	}
	
	private static PendingIntent getStartAppPendingIntent(Context context) {
		Intent startAppIntent = new Intent(context, MainDispatchActivity.class);
		PendingIntent startAppPendingIntent = PendingIntent.getActivity(context,
				R.id.appstate_notificaion_id, startAppIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		return startAppPendingIntent;
	}

}
/**
 * 
 */
package com.ruley.app;

import io.fabric.sdk.android.Fabric;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.ExceptionReporter;
import com.google.android.gms.analytics.Tracker;
import com.parse.ParseACL;
import com.parse.ParseUser;
import com.ruley.R;
import com.ruley.activities.MainActivity;
import com.ruley.activities.MainDispatchActivity;
import com.ruley.broadcastreceivers.ParseLoginBroadcastReceiver;
import com.ruley.broadcastreceivers.PeriodicCheckBroadcastReceiver;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.ContentObserverTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IActivityLogDAO;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.parse.UserParse;
import com.ruley.services.ContentObserversService;
import com.ruley.services.SysEventReceiverService;
import com.ruley.utilities.AnalyticsUtils;
import com.ruley.utilities.FilterUtils;
import com.ruley.utilities.ParseUtils;
import com.ruley.utilities.PrefsUtils;
import com.ruley.utilities.UsersUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class MyApplication extends Application {
	
	private static final String CLASS_NAME = MyApplication.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MyApplication.class);
	
	private static final int RESTART_APP_IN_MILLIS = 2000;
	
	private static Context mAppContext = null;
	
	private Tracker mMyTracker = null;
	
	private UncaughtExceptionHandler mMyUEH = null;
	
	private UncaughtExceptionHandler mMyER = null;
	
	@Override
	public void onCreate() {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate();
		
		Fabric.with(this, new Crashlytics());
		
		mAppContext = getApplicationContext();
		
//		initUncaughtExceptionHandler();
		
		enableStrictMode(false);
		
		// Sets the default values of the app preferences
		PreferenceManager.setDefaultValues(mAppContext, R.xml.preferences, false);
		
		// Sets the build mode of the app
		AppState.setBuildMode(AppState.BUILD_BETA);
		
		ParseUtils.initParse(MyApplication.this);
		
		// If recurring logged in user, start app
		if (ParseUser.getCurrentUser() != null) {
			startApp();
		}
	}

	public static Context getContext() {
		LOGGER.debug(CLASS_NAME + "#getContext(...)");
		
		return mAppContext;
	}
	
	public static void onSignup() {
		LOGGER.debug(CLASS_NAME + "#onSignup(...)");

		String accountType = ParseUtils.getCurrentUserAccoutType();
		// If invalid user account
		if (TextUtils.isEmpty(accountType)) {
			LOGGER.warn(CLASS_NAME + "#onSignup(...) invalid user account type");
			return;
		}
		ParseUser currentUser = ParseUser.getCurrentUser();
		// If not a new user
		if (!currentUser.isNew()) {
			LOGGER.warn(CLASS_NAME + "#onSignup(...) current user isn't new user");
			return;
		}

		AnalyticsUtils.sendSignupEvent(accountType);
	}
	
	public static void onLogin() {
		LOGGER.debug(CLASS_NAME + "#onLogin(...)");
		
		String accountType = ParseUtils.getCurrentUserAccoutType();
		// If invalid user account
		if (TextUtils.isEmpty(accountType)) {
			LOGGER.warn(CLASS_NAME + "#onLogin(...) invalid user account type");
			return;
		}
		
		PrefsUtils.setUserAccountType(accountType);
		AnalyticsUtils.sendLoginEvent(accountType);
		
		// Send virtual screen view for Google Analytics goal funnels - Signups 
		AnalyticsUtils.sendScreenViewEvent("Onboarding - user logged with " + accountType);
		
		// Sets the current user's data as private
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseACL parseUserACL = ParseUtils.getUserParseACL(currentUser);
		currentUser.setACL(parseUserACL);
		currentUser.saveInBackground();
		
		// Enables the app
		AppState.setAppModePref(true);
		
		// If logged in, init the app root storage directory
		// for the current logged in user
		String username = currentUser.getUsername();
		initAppStorageDirectory(username);
	}
	
	public static void onLogout() {
		LOGGER.debug(CLASS_NAME + "#onLogout(...)");
		
		// Clears the user liked rules ids
		UserParse.clearLikedRulesIds();
		
		// Sets the app root storage directory path to external
		// instead of the just logged out user to keep his data safe
		Config.Paths.setDirAppStorage(Config.Paths.DIR_STORAGE_EXTERNAL);
		
		String accountType = PrefsUtils.getUserAccountType();
		if (!TextUtils.isEmpty(accountType)) {
			PrefsUtils.setUserAccountType(null);
			AnalyticsUtils.sendLogoutEvent(accountType);
		}
	}
	
	public static void startApp() {
		LOGGER.debug(CLASS_NAME + "#startApp(...)");
		
		String accountType = ParseUtils.getCurrentUserAccoutType();
		// If invalid user account, crash
		if (TextUtils.isEmpty(accountType)) {
			LOGGER.warn(CLASS_NAME + "#startApp(...) invalid user account type, logging out...");
			logoutApp();
		}
		PrefsUtils.setUserAccountType(accountType);
		
		// If logged in, init the app root storage directory
		// for the current logged in user
		ParseUser currentUser = ParseUser.getCurrentUser();
		String username = currentUser.getUsername();
		initAppStorageDirectory(username);
				
		initAppUsers();
		ruleComponentsFiltering();
		// Handles the app version upgrade
		PrefsUtils.checkAppVersionUpgrade();
		// Cache the user liked rules ids
		UserParse.cacheLikedRulesIds(getContext());
		
		if (AppState.isAppEnabled()) {
			startBackgroundServices();
		}
	}
	
	public static void stopApp() {
		LOGGER.debug(CLASS_NAME + "#stopApp(...)");
		
		if (AppState.isAppEnabled()) {
			stopBackgroundServices();
		}
	}
	
	public static void logoutApp() {
		LOGGER.debug(CLASS_NAME + "#logoutApp(...)");
		
		ParseUser.logOut();
		
		// Notify with a broadcast the logout went well,
		// so the app will stop its background services
		Intent logoutIntent = new Intent(mAppContext, ParseLoginBroadcastReceiver.class);
		logoutIntent.setAction(Config.Action.ACTION_LOGOUT);
		mAppContext.sendBroadcast(logoutIntent);
		
		// Redirects the user to the login activity
		Intent loginIntent = new Intent(mAppContext, MainDispatchActivity.class);
		loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mAppContext.startActivity(loginIntent);
	}
	
	public static boolean handleAppVersionUpgrade(int prevAppVersionCode, int currAppVersionCode) {
		LOGGER.debug(CLASS_NAME + "#handleAppVersionUpgrade(...)");
		LOGGER.info("Upgrading app version from " + prevAppVersionCode + " to " + currAppVersionCode);
		
		LOGGER.info("Clear all activity log");
		IActivityLogDAO activityLogDAO = DAOFactory.getFactory(
				DAOFactory.getFactoryType()).getActivityLogDAO();
		activityLogDAO.clear();
		
		return true;
	}
	
	@SuppressLint("NewApi")
	private void restartApp() {
		Context appCtx = getApplicationContext();
		
		Intent restartAppIntent = new Intent(appCtx, MainDispatchActivity.class);
		PendingIntent restartAppPendingIntent = PendingIntent.getActivity(appCtx, 0, restartAppIntent, PendingIntent.FLAG_ONE_SHOT);
		
		AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			am.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, RESTART_APP_IN_MILLIS, restartAppPendingIntent);
		} else {
			am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, RESTART_APP_IN_MILLIS, restartAppPendingIntent);
		}
		System.exit(2);
	}
	
	public static boolean isAppRestartNeeded() {
		LOGGER.debug(CLASS_NAME + "#isAppRestartNeeded(...)");
		
		// If the parse user isn't logged in, it's anonymous user,
		// restart isn't needed
		if (!ParseUtils.isParseUserLoggedIn()) {
			LOGGER.info("Parse user isn't logged in (anonymous user), so app restart isn't needed");
			return false;
		}
		
		// If the app was disabled, restart isn't needed
		if (!AppState.isAppEnabled()) {
			LOGGER.info("App is disabled, so app restart isn't needed");
			return false;
		}
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		
		List<Rule> activeRules = null;
		int activeRuleSize = 0;
		boolean serviceRunning = false;
		
		// Checks if system event receiver service needs to be restarted
		activeRules = ruleDAO.list(Trigger.class, true);
		activeRuleSize = (activeRules != null) ? activeRules.size() : 0;
		serviceRunning = SysEventReceiverService.isServiceRunning();
		boolean isSysEventReceiverServiceRestartNeeded = activeRuleSize > 0 && !serviceRunning;

		// Checks if content observer service needs to be restarted
		activeRules = ruleDAO.list(ContentObserverTrigger.class, true);
		activeRuleSize = (activeRules != null) ? activeRules.size() : 0;
		serviceRunning = ContentObserversService.isServiceRunning();
		boolean isContentObserverServiceRestartNeeded = activeRuleSize > 0 && !serviceRunning;
		
		boolean isAppRestartNeeded = isSysEventReceiverServiceRestartNeeded || isContentObserverServiceRestartNeeded;
		LOGGER.info("App is enabled,"
				+ ((isAppRestartNeeded) ? " but isn't running so app restart is needed"
						: " and it's running so app restart isn't needed"));
		
		return isAppRestartNeeded;
	}
	
	public static void startBackgroundServices() {
		SysEventReceiverService.startService();
		ContentObserversService.startService();
		PeriodicCheckBroadcastReceiver.start(mAppContext);
	}
	
	public static void stopBackgroundServices() {
		SysEventReceiverService.stopService();
		ContentObserversService.stopService();
		PeriodicCheckBroadcastReceiver.cancel(mAppContext);
	}
	
	public static void updateBackgroundServices() {
		SysEventReceiverService.updateBroadcastReceiver();
		ContentObserversService.updateContentObservers();
	}
	
	private void initUncaughtExceptionHandler() {
		mMyTracker = AnalyticsUtils.getInstance().getTracker(AnalyticsUtils.getDefaultTrackerName());
		mMyUEH = new UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				// Log the uncaught exception (local: logback file log)
				LOGGER.error("UNCAUGHT EXCEPTION:");
				LOGGER.error("--------------------------------------------", ex);
				LOGGER.error("--------------------------------------------");
			}
		};
		mMyER = new ExceptionReporter(mMyTracker, mMyUEH, mAppContext);
		
		Thread.setDefaultUncaughtExceptionHandler(mMyER);
	}
	
	/**
	 * Enables/Disables strict mode.
	 * 
	 * @param enable
	 */
	private void enableStrictMode(boolean enable) {
		if (enable) {
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
					.detectDiskReads()
					.detectDiskWrites()
					.detectNetwork()
					.penaltyLog()
					.build());
			StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
					.detectLeakedSqlLiteObjects()
					.detectLeakedClosableObjects()
					.penaltyLog()
					.penaltyDeath()
					.build());
		}
	}
	
	/**
	 * Changes the app's storage directory,
	 * according to the development mode state (debug/release).
	 */
	public static void initAppStorageDirectory(String username) {
		String userDataDir = username.replaceAll("@", "_");
		// If external storage available,
		// Sets the app storage path to external
		if (isExternalStorageWritable()) {
			String appStorageDirPath = Config.Paths.DIR_STORAGE_EXTERNAL
					+ userDataDir
					+ File.separator;
			Config.Paths.setDirAppStorage(appStorageDirPath);
		// If external storage isn't available,
		// Sets the app storage path to internal
		} else {
			String appStorageDirPath = Config.Paths.DIR_STORAGE_INTERNAL
					+ userDataDir
					+ File.separator;
			Config.Paths.setDirAppStorage(appStorageDirPath);
		}
	}
	
	/**
	 * Checks if external storage is available for read and write.
	 */
	public static boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        return true;
	    }
	    return false;
	}

	/**
	 * Initializes application users.
	 */
	public static void initAppUsers() {
		UsersUtils.initAdminUser(mAppContext);
		UsersUtils.initDefaultUser(mAppContext);
	}
	
	/**
	 * Filtering rule components,
	 * by current device Android API level
	 * or by current device Hardware.
	 */
	public static void ruleComponentsFiltering() {
		// TODO: filter trigger once every API level change
		int apiLevel = android.os.Build.VERSION.SDK_INT;
		FilterUtils.filterTriggersByAPILevel(mAppContext, apiLevel, Config.Paths.FILE_TRIGGERS_API_FILTER);
		FilterUtils.filterActionsByAPILevel(mAppContext, apiLevel, Config.Paths.FILE_ACTIONS_API_FILTER);
		FilterUtils.filterTriggersMapByDeviceHardware(mAppContext);
	}
	
}
/**
 * 
 */
package com.ruley.services;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.DummyActivity;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.broadcastreceivers.SysEventReceiverWakefulBroadcastReceiver;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ToastUtils;

/**
 * This Service class is the wrapper context for the SysEventReceiverWakefulBroadcastReceiver class.
 * This class handles initialization and registration of it aggregated broadcast receiver.
 * 
 * Notice:
 * I delegate the system events handling work
 * from the SysEventReceiverService Service class
 * because this IntentService class runs in a separate thread from
 * the main thread whereas the Service class runs in the main thread.
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SysEventReceiverService extends Service {
	
	private static final String CLASS_NAME = SysEventReceiverService.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SysEventReceiverService.class);
	
	private Looper mServiceLooper = null;
	
	private ServiceHandler mServiceHandler = null;
	
	private ITriggerDAO mTriggerDAO = null;
	
	private SysEventReceiverWakefulBroadcastReceiver mSysEventReceiverBroadcastReceiver = null;
	
	/**
	 * This method override is a work around for a known bug in
	 * Android Kit Kat API level 19 where Swipe app out of recent tasks
	 * permanently kills app (like force-stop) even though it's running background services!
	 * 
	 * @see https://code.google.com/p/android/issues/detail?id=53313
	 */
	// TODO: delete this workaround after Google will fix the bug
	@Override
	@TargetApi(19)
	public void onTaskRemoved(Intent rootIntent) {
		LOGGER.debug(CLASS_NAME + "#onTaskRemoved(...)");
		
		Intent intent = new Intent(this, DummyActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	@Override
	public void onCreate() {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		
		LOGGER.info(CLASS_NAME + " CREATED");
		ToastUtils.show(AppState.BUILD_DEBUG, getApplicationContext(),
				R.string.toast_event_receiver_created, Toast.LENGTH_SHORT);
		
		// Start up the thread running the service.  Note that we create a
	    // separate thread because the service normally runs in the process's
	    // main thread, which we don't want to block.  We also make it
	    // background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread(CLASS_NAME, Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		
		// Get the HandlerThread's Looper and use it for our Handler
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		
		if (!isBroadcastReceiverInitialized()) {
			initBroadcastReceiver();
			registerBroadcastReceiver();
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LOGGER.debug(CLASS_NAME + "#onStartCommand(...)");
		LOGGER.info(CLASS_NAME + " STARTED\n------------------------------");
		
		// Validates arguments
		if (intent == null) {
			stopSelf(startId);
			// If we get killed, after returning from here, restart
			return START_STICKY;
		}
		
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		msg.obj = intent;
		mServiceHandler.sendMessage(msg);
		
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onBind(...)");
		LOGGER.warn(CLASS_NAME + " IBinder interface not implemented");
		
		return null;
	}
	
	@Override
	public void onDestroy() {
		LOGGER.debug(CLASS_NAME + "#onDestroy(...)");
		
		if (isBroadcastReceiverInitialized()) {
			unregisterBroadcastReveiver();
		}
		
		LOGGER.info(CLASS_NAME + " DESTROIED");
		ToastUtils.show(AppState.BUILD_DEBUG, getApplicationContext(),
				R.string.toast_event_receiver_destroied, Toast.LENGTH_SHORT);
		
		// If the app is enabled and the service is needed, restart the service
		if (AppState.isAppEnabled() && isServiceNeeded()) {
			startService();
		}
	}
	
	private boolean isBroadcastReceiverInitialized() {
		LOGGER.debug(CLASS_NAME + "#isBroadcastReceiverInitialized(...)");
		
		boolean isBroadcastReceiverInitialized = mSysEventReceiverBroadcastReceiver != null;
		
		if (isBroadcastReceiverInitialized) {
			LOGGER.info(CLASS_NAME + " broadcast receiver is INITIALIZED");
		} else {
			LOGGER.info(CLASS_NAME + " broadcast receiver is NOT INITIALIZED");
		}
		
		return isBroadcastReceiverInitialized;
	}

	private void initBroadcastReceiver() {
		LOGGER.debug(CLASS_NAME + "#initBroadcastReceiver(...)");
		
		if (mSysEventReceiverBroadcastReceiver == null) {
			mSysEventReceiverBroadcastReceiver = new SysEventReceiverWakefulBroadcastReceiver();
		}
	}
	
	private void registerBroadcastReceiver() {
		LOGGER.debug(CLASS_NAME + "#registerBroadcastReceiver(...)");
		
		long startMillis = System.currentTimeMillis();
		
		if (mTriggerDAO == null) {
			mTriggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		}
		List<Trigger> activeTriggers = mTriggerDAO.list(true);
		
		// If there aren't any active triggers, service isn't needed, stop service
		if (activeTriggers == null || activeTriggers.isEmpty()) {
			LOGGER.info("There aren't any active triggers, service isn't needed, stop service");
			stopSelf();
		}
		
		if (activeTriggers != null && !activeTriggers.isEmpty()) {
			// Registers location service disabled intent filter 
			IntentFilter locationSerivceDisabledIntentFilter = new IntentFilter();
			locationSerivceDisabledIntentFilter.addAction(Config.Action.ACTION_LOCATION_SERVICE_DISABLED);
			registerReceiver(mSysEventReceiverBroadcastReceiver, locationSerivceDisabledIntentFilter);
			// Registers all the active triggers intent filters
			for (Trigger activeTrigger : activeTriggers) {
				String triggerClassName = activeTrigger.getClass().getSimpleName();
				Set<IntentFilter> intentFilters = Config.Triggers.TRIGGERS_NAMES_TO_INTENT_FILTERS.get(triggerClassName);
				for (IntentFilter intentFilter : intentFilters) {
					intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
					registerReceiver(mSysEventReceiverBroadcastReceiver, intentFilter);
				}
			}
		}
		
		if (AppState.isStickyNotificationEnabled()) {
			AppState.notifyActiveRules();
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#registerBroadcastReceiver(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}
	
	private void unregisterBroadcastReveiver() {
		LOGGER.debug(CLASS_NAME + "#unregisterBroadcastReveiver(...)");
		
		try {
			unregisterReceiver(mSysEventReceiverBroadcastReceiver);
		} catch (Exception e) {
			// Do nothing
			// A workaround to thrown exception when string to unregistered
			// already unregistered broadcast receiver.
		} finally {
			mSysEventReceiverBroadcastReceiver = null;
		}
	}
	
	private void reregisterBroadcastReceiver() {
		LOGGER.debug(CLASS_NAME + "#reregisterBroadcastReceiver(...)");
		
		unregisterBroadcastReveiver();
		initBroadcastReceiver();
		registerBroadcastReceiver();
	}
	
	/**
	 * Checks if this service is needed. If there is at least one active
	 * trigger, this service is needed for listening to this trigger, otherwise
	 * this service isn't needed.
	 * 
	 * @return If there is at least one active trigger returns true, otherwise
	 *         returns false
	 */
	public static boolean isServiceNeeded() {
		LOGGER.debug(CLASS_NAME + "#isServiceNeeded(...)");
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> activeRules = ruleDAO.list(Trigger.class, true);
		
		return (activeRules != null) && !activeRules.isEmpty();
	}
	
	public static boolean isServiceRunning() {
		LOGGER.debug(CLASS_NAME + "#isServiceRunning(...)");
		
		Context appCtx = MyApplication.getContext();
		
		ActivityManager manager = (ActivityManager) appCtx.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (SysEventReceiverService.class.getName().equals(service.service.getClassName())) {
				LOGGER.info(CLASS_NAME + " service is RUNNING!");
				return true;
			}
		}
		
		LOGGER.info(CLASS_NAME + " service is NOT RUNNING!");
		return false;
	}
	
	/**
	 * Async: Starts the background service SysEventReceiverService.
	 */
	public static void startService() {
		LOGGER.debug(CLASS_NAME + "#startService(...)");
		
		Context appCtx = MyApplication.getContext();
	
		Intent serviceIntent = new Intent(appCtx, SysEventReceiverService.class);
		appCtx.startService(serviceIntent);
	}
	
	/**
	 * Async: Stops the background service SysEventHandleService.
	 */
	public static void stopService() {
		LOGGER.debug(CLASS_NAME + "#stopService(...)");
		
		Context appCtx = MyApplication.getContext();
		
		Intent serviceIntent = new Intent(appCtx, SysEventReceiverService.class);
		appCtx.stopService(serviceIntent);
	}
	
	/**
	 * Async: Updates (Reregisters) the broadcast receiver SysEventReceiverWakefulBroadcastReceiver.
	 */
	public static void updateBroadcastReceiver() {
		LOGGER.debug(CLASS_NAME + "#updateBroadcastReceiver(...)");
		
		Context appCtx = MyApplication.getContext();

		// Async: Updates the background sysEventHandleService's
		// broadcast receiver SysEventReceiverWakefulBroadcastReceiver
		Intent serviceIntent = new Intent(appCtx, SysEventReceiverService.class);
		serviceIntent.setAction(Config.Action.ACTION_UPDATE_BROADCAST_RECEIVER);
		appCtx.startService(serviceIntent);
	}
	
	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {

		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			Intent intent = (Intent) msg.obj;
			if (intent != null) {
				String action = intent.getAction();
				if (!TextUtils.isEmpty(action)) {
					if (Config.Action.ACTION_UPDATE_BROADCAST_RECEIVER.equals(action)) {
						reregisterBroadcastReceiver();
					}
				}
			}
		}
		
	}
	
}
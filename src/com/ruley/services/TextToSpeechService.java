/**
 * 
 */
package com.ruley.services;

import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.LinkedBlockingDeque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnUtteranceCompletedListener;

import com.ruley.app.MyApplication;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@SuppressWarnings("deprecation")
public class TextToSpeechService extends Service {
	
	private static final String CLASS_NAME = TextToSpeechService.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TextToSpeechService.class.getName());
	
	private final int DEFAULT_AUDIO_STREAM = AudioManager.STREAM_NOTIFICATION;
	
	private static final float DEFAULT_SPEECH_RATE = 0.8F;
	
	private static final Locale DEFAULT_LANGUAGE = Locale.US;
	
	private static final String EXTRA_TEXT = "text";
	
	private static final String EXTRA_LANGUAGE = "language";
	
	private static final String EXTRA_SPEECH_RATE = "speechRate";
	
	private Looper mServiceLooper = null;
	
	private ServiceHandler mServiceHandler = null;
	
	private TextToSpeech mTts = null;
	
	private boolean mIsTtsInitialized = false;
	
	private LinkedBlockingDeque<TTSMessage> mPendingTtsMessages = new LinkedBlockingDeque<TTSMessage>();
	
	private AudioManager mAudioManager = null;
	
	private boolean mIsAudioFocusRequestGranted = false;
	
	private OnAudioFocusChangeListener onAudioFocusChangeListener = new OnAudioFocusChangeListener() {
		@Override
		public void onAudioFocusChange(int focusChange) {
			LOGGER.debug(CLASS_NAME + "#onAudioFocusChange(...)");

			if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
				LOGGER.debug(CLASS_NAME + "#onAudioFocusChange(...) audio focus loss");
				// TODO: Lower the volume
			} else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
				LOGGER.debug(CLASS_NAME + "#onAudioFocusChange(...) audio focus gain");
				// TODO: Raise it back to normal
			}
		}
	};
	
	@Override
	public IBinder onBind(Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onBind(...)");
		LOGGER.warn(CLASS_NAME + " IBinder interface not implemented");
		
		return null;
	}

	@Override
	public void onCreate() {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		super.onCreate();
		
		// Starts up the thread running the service. Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block. We also make it
		// background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread(CLASS_NAME, Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// Gets the HandlerThread's Looper and use it for our Handler
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		
		// Gets the text to speech
		mTts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
			@Override
			public void onInit(int status) {
				LOGGER.debug(CLASS_NAME + "#onInit(...)");
				// Status can be either TextToSpeech.SUCCESS or TextToSpeech.ERROR.
				if (status == TextToSpeech.SUCCESS) {
					LOGGER.info(CLASS_NAME + "#onInit(...) TextToSpeech initialized successfully");
					// Sets text to speech utterance completed listener
					mTts.setOnUtteranceCompletedListener(new OnUtteranceCompletedListener() {
						@Override
						public void onUtteranceCompleted(String utteranceId) {
							LOGGER.debug(CLASS_NAME + "#onUtteranceCompleted(...)");
							// Stops the service using the utteranceId = startId,
							// so that we don't stop the service in the middle of handling another job
							stopSelf( Integer.valueOf(utteranceId) );
						}
					});
					mIsTtsInitialized = true;
					onFinishInitTts();
				} else {
					LOGGER.error(CLASS_NAME + "#onInit(...) TextToSpeech initialization failed");
				}
			}
		});
	}

	@Override
	public int onStartCommand(final Intent intent, final int flags, final int startId) {
		LOGGER.debug(CLASS_NAME + "#onStartCommand(...)");
		
		// Validates arguments
		if (intent == null) {
			stopSelf(startId);
			// If we get killed, after returning from here, restart
			return START_STICKY;
		}
		
		TTSMessage ttsMessage = createTTSMessage(intent, startId);
		
		if (!mIsTtsInitialized) {
			LOGGER.info("TTS not initialized");
			try {
				mPendingTtsMessages.put(ttsMessage);
			} catch (InterruptedException e) {
				LOGGER.error("Error: ", e);
			}
		} else {
			LOGGER.info("TTS initialized");
			sendMessage(ttsMessage);
		}
		
		// If we get killed, after returning from here, restart
		return START_STICKY;
	}
	
	@SuppressLint("NewApi")
	@Override
	public void onDestroy() {
		LOGGER.debug(CLASS_NAME + "#onDestroy(...)");
		
		// Releases audio focus is previously granted
		if (mIsAudioFocusRequestGranted) {
			mAudioManager.abandonAudioFocus(onAudioFocusChangeListener);
		}
		
		// Releases text to speech
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
        
        // Release the thread looper
        if (mServiceLooper != null) {
        	if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
        		mServiceLooper.quitSafely();
        	} else {
        		mServiceLooper.quit();
        	}
        }
        
        mIsTtsInitialized = false;

        super.onDestroy();
	}
	
	/**
	 * Speaks out loud a given text.
	 * 
	 * @param text
	 *            String represents text to speech
	 * @param language
	 *            Locale represents the language
	 * @param speechRate
	 *            Float represents the speech rate
	 * 
	 * @see android.speech.tts.TextToSpeech
	 */
	public static void speek(String text, Locale language, float speechRate) {
		LOGGER.debug(CLASS_NAME + "#speek(...)");
		
		// Validates arguments
		if (text == null) {
			return;
		}
		language = (language != null) ? language : DEFAULT_LANGUAGE;
		speechRate = (speechRate > 0F) ? speechRate : DEFAULT_SPEECH_RATE;

		Context appCtx = MyApplication.getContext();
		Intent ttsIntent = new Intent(appCtx, TextToSpeechService.class);
		ttsIntent.putExtra(EXTRA_TEXT, text);
		ttsIntent.putExtra(EXTRA_LANGUAGE, language);
		ttsIntent.putExtra(EXTRA_SPEECH_RATE, speechRate);
		appCtx.startService(ttsIntent);
	}
	
	/**
	 * Speaks out loud a given text.
	 * with the default speech rate 0.8F.
	 * 
	 * @param text
	 *            String represents text to speech
	 * @param language
	 *            Locale represents the language
	 * 
	 * @see android.speech.tts.TextToSpeech
	 */
	public static void speek(String text, Locale language) {
		speek(text, language, DEFAULT_SPEECH_RATE);
	}
	
	/**
	 * Speaks out loud a given text,
	 * with the default Locale.US
	 * and default speech rate 0.8F.
	 * 
	 * @param text
	 *            String represents text to speech
	 * 
	 * @see android.speech.tts.TextToSpeech
	 */
	public static void speek(String text) {
		speek(text, DEFAULT_LANGUAGE);
	}
	
	private void onFinishInitTts() {
		LOGGER.debug(CLASS_NAME + "#onFinishInitTts(...)");
		
		// Requests for audio focus
		mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		int result = mAudioManager.requestAudioFocus(
				onAudioFocusChangeListener,
				DEFAULT_AUDIO_STREAM,
				AudioManager.AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK);
		mIsAudioFocusRequestGranted = result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
		
		try {
			while (!mPendingTtsMessages.isEmpty()) {
				TTSMessage ttsMessage = mPendingTtsMessages.take();
				sendMessage(ttsMessage);
			}
		} catch (InterruptedException e) {
			LOGGER.error("Error: ", e);
		}
	}
	
	private TTSMessage createTTSMessage(final Intent intent, final int startId) {
		LOGGER.debug(CLASS_NAME + "#createTTSMessage(...)");
		
		// Constructs a text to speech message
		String text = intent.getStringExtra(EXTRA_TEXT);
		Locale language = (Locale) intent.getSerializableExtra(EXTRA_LANGUAGE);
		float speechRate = (float) intent.getFloatExtra(EXTRA_SPEECH_RATE, DEFAULT_SPEECH_RATE);
		
		TTSMessage ttsMessage = new TTSMessage(startId, text, language, speechRate);
		
		return ttsMessage;
	}

	private void sendMessage(TTSMessage ttsMessage) {
		LOGGER.debug(CLASS_NAME + "#sendMessage(...)");
		
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the
		// job
		Message msg = mServiceHandler.obtainMessage();
		msg.obj = ttsMessage;
		mServiceHandler.sendMessage(msg);
	}
	
	private void speak(TTSMessage ttsMessage) {
		LOGGER.debug(CLASS_NAME + "#speak(...)");
		
		assert(mTts != null && ttsMessage != null); 
		
		HashMap<String, String> ttsParams = new HashMap<String, String>();
		ttsParams.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(DEFAULT_AUDIO_STREAM));
		ttsParams.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(ttsMessage.startId));
		
		int result = TextToSpeech.ERROR;
		
		// Note that a language may not be available,
		// and the result will indicate this.
		result = mTts.setLanguage(ttsMessage.language);
		if (result == TextToSpeech.LANG_MISSING_DATA
				|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
			LOGGER.warn(ttsMessage.language.getDisplayName() + " language is not available.");
			mTts.setLanguage(DEFAULT_LANGUAGE);
			LOGGER.info(DEFAULT_LANGUAGE.getDisplayName() + " default language choosen.");
		}
		
		// Note that a speech rate may not be available,
		// and the result will indicate this.
		result = mTts.setSpeechRate(ttsMessage.speechRate);
		if (result == TextToSpeech.ERROR) {
			LOGGER.warn(ttsMessage.speechRate + " speech rate is not supported.");
			mTts.setSpeechRate(DEFAULT_SPEECH_RATE);
			LOGGER.info(ttsMessage.speechRate + " default speech rate choosen.");
		}
		
		mTts.speak(ttsMessage.text, TextToSpeech.QUEUE_ADD, ttsParams);
	}
	
	private final class ServiceHandler extends Handler {

		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			LOGGER.debug(CLASS_NAME + "#handleMessage(...)");
			
			TTSMessage ttsMessage = (TTSMessage) msg.obj;
			speak(ttsMessage);
		}

	}
	
	private final class TTSMessage {
		
		private int startId;
		private String text;
		private Locale language;
		private float speechRate;
		
		public TTSMessage(int startId, String text, Locale language, float speechRate) {
			super();
			this.startId = startId;
			this.text = text;
			this.language = language;
			this.speechRate = speechRate;
		}
		
	}

}

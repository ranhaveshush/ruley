/**
 * 
 */
package com.ruley.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationStatusCodes;
import com.ruley.broadcastreceivers.SysEventReceiverWakefulBroadcastReceiver;
import com.ruley.config.Config;
import com.ruley.model.beans.triggers.base.Trigger.ITriggerHandler;
import com.ruley.utilities.RulesUtils;

/**
 * This IntentService class receives the system events in the form of
 * Intent's actions from the SysEventReceiverWakefulBroadcastReceiver class
 * and handles the system events according to the user defined rules triggers.
 * 
 * Notice:
 * I delegate the system events handling work
 * from the SysEventReceiverService Service class
 * because this IntentService class runs in a separate thread from
 * the main thread whereas the the Service class runs in the main thread. 
 * 
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class SysEventHandlerIntentService extends IntentService {

	private static final String CLASS_NAME = SysEventHandlerIntentService.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SysEventHandlerIntentService.class.getName());
	
	public SysEventHandlerIntentService() {
		super(CLASS_NAME);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		LOGGER.info(CLASS_NAME + " CREATED");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		LOGGER.info(CLASS_NAME + " DESTROIED");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onHandleIntent(...)");
		
		long startMillis = System.currentTimeMillis();
		
		if (null == intent) {
			LOGGER.warn("Invalid Intent: " + intent);
			
			// Release the wake lock provided by the WakefulBroadcastReceiver.
	        SysEventReceiverWakefulBroadcastReceiver.completeWakefulIntent(intent);
	        
	        long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#registerBroadcastReceiver(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			
			return;
		}
		
		String action = intent.getAction();
		LOGGER.info("Action: " + action);
		
		// If "Location Services" Disabled
		if (LocationClient.hasError(intent)) {
			int errorCode = LocationClient.getErrorCode(intent);
			if (LocationStatusCodes.GEOFENCE_NOT_AVAILABLE == errorCode
				&& Config.Action.ACTION_LOCATION_SERVICE_DISABLED.equals(action)) {
				LOGGER.warn(CLASS_NAME + "#handle(...) Location Services DISABLED, inactivate dependent rules.");
				RulesUtils.inactivateLocationServiceDependentRules(SysEventHandlerIntentService.this);
			}
			// Release the wake lock provided by the WakefulBroadcastReceiver.
	        SysEventReceiverWakefulBroadcastReceiver.completeWakefulIntent(intent);
	        
	        long endMillis = System.currentTimeMillis();
			LOGGER.info(CLASS_NAME + "#registerBroadcastReceiver(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
			
			return;
		}
		
		List<ITriggerHandler> matchedTriggersHandlers = filterTriggersHandlersByIntent(intent);
		triggerMatchedTriggersHandlers(intent, matchedTriggersHandlers);
		
		// Release the wake lock provided by the WakefulBroadcastReceiver.
        SysEventReceiverWakefulBroadcastReceiver.completeWakefulIntent(intent);
        
        long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#registerBroadcastReceiver(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}

	/**
	 * Filters the relevant triggers handlers which are matching the incoming intent.
	 * 
	 * @param intent
	 * @return
	 */
	private List<ITriggerHandler> filterTriggersHandlersByIntent(Intent intent) {
		List<ITriggerHandler> triggersHandlers = new ArrayList<ITriggerHandler>();
		Set<Entry<IntentFilter, ITriggerHandler>> entrySet = Config.Triggers.INTENT_FILTERS_TO_TRIGGERS_HANDLERS.entrySet();
		for (Entry<IntentFilter, ITriggerHandler> entry : entrySet) {
			IntentFilter intentFilter = entry.getKey();
			int match = intentFilter.match(getContentResolver(), intent, true, CLASS_NAME);
			// If intent match intent filter
			if (match >= IntentFilter.MATCH_ADJUSTMENT_NORMAL) {
				triggersHandlers.add(entry.getValue());
			}
		}
		return triggersHandlers;
	}
	
	/**
	 * Triggers the filtered matched triggers handlers.
	 * 
	 * @param intent
	 * @param triggersHandlers
	 */
	private void triggerMatchedTriggersHandlers(Intent intent, List<ITriggerHandler> triggersHandlers) {
		for (ITriggerHandler triggerHandler : triggersHandlers) {
			LOGGER.info(triggerHandler.getClass().getSimpleName() + " HANDLE " + intent.getAction());
			Bundle extras = intent.getExtras();
			if (null != extras) {
				LOGGER.info("Extras: #" + extras.size());
				for (String key : extras.keySet()) {
				    Object value = extras.get(key);
				    LOGGER.info(String.format("%s = %s (%s)", key,
					        value.toString(), value.getClass().getName()));
				}
			} else {
				LOGGER.info("Extras: null");
				extras = new Bundle();
			}
			
			triggerHandler.handle(SysEventHandlerIntentService.this, intent);
		}
	}

}

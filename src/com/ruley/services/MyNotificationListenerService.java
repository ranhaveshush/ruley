/**
 * 
 */
package com.ruley.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import com.ruley.app.MyApplication;
import com.ruley.config.Config;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
@SuppressLint("NewApi")
public class MyNotificationListenerService extends NotificationListenerService {
	
	private static final String CLASS_NAME = MyNotificationListenerService.class
			.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(MyNotificationListenerService.class.getName());
	
	private Set<String> mExcludedAppsPackageNames = new HashSet<String>();

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LOGGER.debug(CLASS_NAME + "#onStartCommand(...)");
		
		// Validates intent
		if (intent == null) {
			return super.onStartCommand(intent, flags, startId);
		}
		// Validates intent action
		String action = intent.getAction();
		if (TextUtils.isEmpty(action)) {
			return super.onStartCommand(intent, flags, startId);
		}

		if (Config.Action.ACTION_EXCLUDE_APPS.equals(action)) {
			ArrayList<String> excludedAppsPackageNames = intent
					.getStringArrayListExtra(Config.Extra.EXTRA_EXCLUDED_APPS_PACKAGE_NAMES);
			mExcludedAppsPackageNames.clear();
			mExcludedAppsPackageNames.addAll(excludedAppsPackageNames);
		}
		
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onNotificationPosted(StatusBarNotification sbn) {
		LOGGER.debug(CLASS_NAME + "#onNotificationPosted(...)");
		
		// Gets the app package name
		String packageName = sbn.getPackageName();
		LOGGER.info(CLASS_NAME + " app package " + packageName);
		
		// Validates app package name
		if (packageName == null || TextUtils.isEmpty(packageName)) {
			LOGGER.info(CLASS_NAME + " invalid app package");
			return;
		}
		// Filters notification by excluded app package
		if (mExcludedAppsPackageNames.contains(packageName)) {
			LOGGER.info(CLASS_NAME + " excluded app package");
			return;
		}
		
		Notification notification = sbn.getNotification();
		String title = notification.extras.getString(Notification.EXTRA_TITLE);
		LOGGER.info(CLASS_NAME + " notification titile = " + title);
		String text = notification.extras.getString(Notification.EXTRA_TEXT);
		LOGGER.info(CLASS_NAME + " notification text = " + text);
		
		// Constructs the speak text
		String speak = "";
		String appLabel = getAppLabel(packageName);
		if (!TextUtils.isEmpty(appLabel)) {
			speak += appLabel + ": ";
		}
		if (!TextUtils.isEmpty(title)) {
			speak += title + ": ";
		}
		if (!TextUtils.isEmpty(text)) {
			speak += text;
		}
		
		TextToSpeechService.speek(speak);
	}

	@Override
	public void onNotificationRemoved(StatusBarNotification sbn) {
		LOGGER.debug(CLASS_NAME + "#onNotificationRemoved(...)");
		
		// Do nothing
	}
	
	public static boolean isServiceRunning() {
		LOGGER.debug(CLASS_NAME + "#isServiceRunning(...)");
		
		Context appCtx = MyApplication.getContext();
		
		ActivityManager manager = (ActivityManager) appCtx.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (MyNotificationListenerService.class.getName().equals(service.service.getClassName())) {
				LOGGER.info(CLASS_NAME + " service is RUNNING!");
				return true;
			}
		}
		
		LOGGER.info(CLASS_NAME + " service is NOT RUNNING!");
		return false;
	}
	
	/**
	 * Async: Starts the background service MyNotificationListenerService.
	 */
	public static void startService() {
		LOGGER.debug(CLASS_NAME + "#startService(...)");
		
		Context appCtx = MyApplication.getContext();
	
		Intent serviceIntent = new Intent(appCtx, MyNotificationListenerService.class);
		appCtx.startService(serviceIntent);
	}
	
	/**
	 * Async: Stops the background service MyNotificationListenerService.
	 */
	public static void stopService() {
		LOGGER.debug(CLASS_NAME + "#stopService(...)");
		
		Context appCtx = MyApplication.getContext();
		
		Intent serviceIntent = new Intent(appCtx, MyNotificationListenerService.class);
		appCtx.stopService(serviceIntent);
	}
	
	/**
	 * Async: Excludes the given apps package names notifications from been read.
	 */
	public static void exclude(Set<String> excludedAppsPackageNames) {
		LOGGER.debug(CLASS_NAME + "#exclude(...)");
		
		long startMillis = System.currentTimeMillis();
		
		if (MyNotificationListenerService.isServiceRunning()) {
			Context appCtx = MyApplication.getContext();
			Intent serviceIntent = new Intent(appCtx, MyNotificationListenerService.class);
			serviceIntent.setAction(Config.Action.ACTION_EXCLUDE_APPS);
			serviceIntent.putStringArrayListExtra(
					Config.Extra.EXTRA_EXCLUDED_APPS_PACKAGE_NAMES,
					new ArrayList<String>(excludedAppsPackageNames));
			appCtx.startService(serviceIntent);
		}
		
		long endMillis = System.currentTimeMillis();
		LOGGER.info(CLASS_NAME + "#exclude(...) END ----- TIME: " + (endMillis - startMillis) + " millis");
	}
	
	private String getAppLabel(String packageName) {
		String appLabel = null;
		try {
			PackageManager packageManager = getPackageManager();
			ApplicationInfo appInfo = packageManager.getApplicationInfo(packageName, 0);
			appLabel = packageManager.getApplicationLabel(appInfo).toString();
			LOGGER.info(CLASS_NAME + " appLabel = " + appLabel);
		} catch (NameNotFoundException e) {
			LOGGER.error("Error: ", e);
		}
		return appLabel;
	}

}

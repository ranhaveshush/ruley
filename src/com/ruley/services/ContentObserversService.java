/**
 * 
 */
package com.ruley.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.Toast;

import com.ruley.R;
import com.ruley.activities.DummyActivity;
import com.ruley.app.AppState;
import com.ruley.app.MyApplication;
import com.ruley.config.Config;
import com.ruley.model.beans.rules.Rule;
import com.ruley.model.beans.triggers.base.ContentObserverTrigger;
import com.ruley.model.beans.triggers.base.Trigger;
import com.ruley.model.dao.factories.DAOFactory;
import com.ruley.model.dao.logic.IRuleDAO;
import com.ruley.model.dao.logic.ITriggerDAO;
import com.ruley.utilities.ToastUtils;

/**
 * @author Ran Haveshush, ran.haveshush.dev@gmail.com
 *
 */
public class ContentObserversService extends Service {
	
	private static final String CLASS_NAME = ContentObserversService.class.getSimpleName();
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ContentObserversService.class);
	
	private Looper mServiceLooper = null;
	
	private ServiceHandler mServiceHandler = null;
	
	private boolean mIsServiceInitialized = false;
	
	private Map<String, ContentObserver> mContentObservers = null;
	
	/**
	 * This method override is a work around for a known bug in
	 * Android Kit Kat API level 19 where swipe app out of recent tasks
	 * permanently kills app (like force-stop) even though it's running background services!
	 * 
	 * @see https://code.google.com/p/android/issues/detail?id=53313
	 */
	// TODO: delete this workaround after Google will fix the bug
	@Override
	@TargetApi(19)
	public void onTaskRemoved(Intent rootIntent) {
		LOGGER.debug(CLASS_NAME + "#onTaskRemoved(...)");
		
		Intent intent = new Intent(this, DummyActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	@Override
	public void onCreate() {
		LOGGER.debug(CLASS_NAME + "#onCreate(...)");
		
		LOGGER.info(CLASS_NAME + " CREATED");
		ToastUtils.show(AppState.BUILD_DEBUG, getApplicationContext(),
				R.string.toast_content_observer_created, Toast.LENGTH_SHORT);
		
		// Start up the thread running the service.  Note that we create a
	    // separate thread because the service normally runs in the process's
	    // main thread, which we don't want to block.  We also make it
	    // background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread(CLASS_NAME, Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		
		// Get the HandlerThread's Looper and use it for our Handler	
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);
		
		if (!mIsServiceInitialized) {
			initContentObservers();
			mIsServiceInitialized = true;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LOGGER.debug(CLASS_NAME + "#onStartCommand(...)");
		
		// Validates arguments
		if (intent == null) {
			stopSelf(startId);
			// If we get killed, after returning from here, restart
			return START_STICKY;
		}
		
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		msg.obj = intent;
		mServiceHandler.sendMessage(msg);
		
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		LOGGER.debug(CLASS_NAME + "#onBind(...)");
		LOGGER.warn(CLASS_NAME + " IBinder interface not implemented");
		
		return null;
	}

	@Override
	public void onDestroy() {
		LOGGER.debug(CLASS_NAME + "#onDestroy(...)");

		if (mIsServiceInitialized) {
			clearContentObservers(mContentObservers.values());
			mIsServiceInitialized = false;
		}
		
		LOGGER.info(CLASS_NAME + " DESTROIED");
		ToastUtils.show(AppState.BUILD_DEBUG, getApplicationContext(),
				R.string.toast_content_observer_destroied, Toast.LENGTH_SHORT);
		
		// If the app is enabled and the service is needed, restart the service
		if (AppState.isAppEnabled() && isServiceNeeded()) {
			startService();
		}
	}
	
	/**
	 * @see android.content.ContentResolver#registerContentObserver
	 */
	private void _registerContentObserver(Uri uri, boolean notifyForDescendents, ContentObserver contentObserver) {
		LOGGER.debug(CLASS_NAME + "#_registerContentObserver(...)");
		
		String contentObserverName = contentObserver.getClass().getSimpleName();
		
		// Only if content observer isn't registered, register it
		if (!mContentObservers.containsKey(contentObserverName)) {
			mContentObservers.put(contentObserverName, contentObserver);
			getContentResolver().registerContentObserver(uri, notifyForDescendents, contentObserver);
		}
	}
	
	/**
	 * @see android.content.ContentResolver#unregisterContentObserver
	 */
	private void _unregisterContentObserver(String contentObserverName) {
		LOGGER.debug(CLASS_NAME + "#_unregisterContentObserver(...)");
		
		// Unregister content observer that isn't needed
		if (!isContentObserverNeeded(contentObserverName)) {
			ContentObserver contentObserver = mContentObservers.remove(contentObserverName);
			if (contentObserver != null) {
				getContentResolver().unregisterContentObserver(contentObserver);
			}
		}
		
		// If unregistering last content observer,
		// Stop this service
		if (mContentObservers.isEmpty()) {
			stopSelf();
		}
	}
	
	private void _clearContentObserver(String contentObserverName) {
		LOGGER.debug(CLASS_NAME + "#_unregisterContentObserver(...)");
		
		// Force clean, unregister content observer
		ContentObserver contentObserver = mContentObservers.remove(contentObserverName);
		if (contentObserver != null) {
			getContentResolver().unregisterContentObserver(contentObserver);
		}
	}

	/**
	 * Content observer is needed only if, there is at least one active content
	 * observer trigger, that uses this content observer.
	 * 
	 * @param contentObserverName
	 * @return Returns true if there is at least one active content observer
	 *         trigger that uses this content observer, otherwise returns false
	 */
	private boolean isContentObserverNeeded(String contentObserverName) {
		boolean isContentObserverNeeded = false;
		
		ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		List<Trigger> activeTriggers = triggerDAO.list(ContentObserverTrigger.class, true);
		
		for (Trigger activeTrigger : activeTriggers) {
			if (activeTrigger instanceof ContentObserverTrigger) {
				ContentObserverTrigger activeContentObserverTrigger = (ContentObserverTrigger) activeTrigger;
				if (activeContentObserverTrigger.getContentObserverClass().getSimpleName().equals(contentObserverName)) {
					isContentObserverNeeded = true;
					break;
				}
			}
		}
		
		return isContentObserverNeeded;
	}
	
	private void initContentObservers() {
		mContentObservers = new HashMap<String, ContentObserver>();
		registerContentObserverTriggers();
	}
	
	private void clearContentObservers(Collection<ContentObserver> contentObservers) {
		LOGGER.debug(CLASS_NAME + "#clearContentObservers(...)");
		
		for (ContentObserver contentObserver : contentObservers) {
			_clearContentObserver(contentObserver.getClass().getSimpleName());
		}
		
		mContentObservers.clear();
	}
	
	private void registerContentObserverTriggers() {
		LOGGER.debug(CLASS_NAME + "#registerContentObserverTriggers(...)");
		
		Context appCtx = MyApplication.getContext();
		
		ITriggerDAO triggerDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getTriggerDAO();
		List<Trigger> activeTriggers = triggerDAO.list(ContentObserverTrigger.class, true);
		
		// If there aren't any active content observer triggers to register,
		// this service isn't needed, so stop it
		if (activeTriggers == null || activeTriggers.isEmpty()) {
			LOGGER.info("There aren't any active content observer triggers, service isn't needed, stop service");
			stopSelf();
		}
		
		for (Trigger activeTrigger : activeTriggers) {
			if (activeTrigger instanceof ContentObserverTrigger) {
				ContentObserverTrigger activeContentObserverTrigger = (ContentObserverTrigger) activeTrigger;
				activeContentObserverTrigger.registerContentObservers(appCtx);
			}
		}
	}

	private void unregisterContentObservers(Collection<ContentObserver> contentObservers) {
		LOGGER.debug(CLASS_NAME + "#unregisterContentObservers(...)");
		
		for (ContentObserver contentObserver : contentObservers) {
			_unregisterContentObserver(contentObserver.getClass().getSimpleName());
		}
	}
	
	/**
	 * Checks if this service is needed. If there is at least one active content
	 * observer trigger, this service is needed for listening to this content
	 * observer trigger, otherwise this service isn't needed.
	 * 
	 * @return If there is at least one active content observer trigger returns
	 *         true, otherwise returns false
	 */
	public static boolean isServiceNeeded() {
		LOGGER.debug(CLASS_NAME + "#isServiceNeeded(...)");
		
		IRuleDAO ruleDAO = DAOFactory.getFactory(DAOFactory.getFactoryType()).getRuleDAO();
		List<Rule> activeRules = ruleDAO.list(ContentObserverTrigger.class, true);
		
		return (activeRules != null) && !activeRules.isEmpty();
	}
	
	public static boolean isServiceRunning() {
		LOGGER.debug(CLASS_NAME + "#isServiceRunning(...)");
		
		Context appCtx = MyApplication.getContext();
		
		ActivityManager manager = (ActivityManager) appCtx.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (ContentObserversService.class.getName().equals(service.service.getClassName())) {
				LOGGER.info(CLASS_NAME + " service is RUNNING!");
				return true;
			}
		}
		
		LOGGER.info(CLASS_NAME + " service is NOT RUNNING!");
		return false;
	}
	
	/**
	 * <P>
	 * Async: Registers the content observer by given class simple name.
	 * </P>
	 * <P>
	 * If this is the first content observer,<BR/>
	 * the service will be started automatically.
	 * </P>
	 * 
	 * @param uri
	 * @param notifyForDescendents
	 * @param contentObserverName
	 *            String represents the content observer simple class name
	 *            
	 * @see android.content.ContentResolver#registerContentObserver
	 */
	public static void registerContentObserver(Uri uri, boolean notifyForDescendents, String contentObserverName) {
		LOGGER.debug(CLASS_NAME + "#registerContentObserver(...)");
		
		assert(uri != null && contentObserverName != null);
		
		Context appCtx = MyApplication.getContext();
	
		// Async: Starts the background service
		Intent serviceIntent = new Intent(appCtx, ContentObserversService.class);
		serviceIntent.setAction(Config.Action.ACTION_REGISTER_CONTENT_OBSERVER);
		serviceIntent.setData(uri);
		serviceIntent.putExtra(Config.Extra.EXTRA_NOTIFY_FOR_DESCENDENTS, notifyForDescendents);
		serviceIntent.putExtra(Config.Extra.EXTRA_CONTENT_OBSERVER_NAME, contentObserverName);
		appCtx.startService(serviceIntent);
	}
	
	/**
	 * <P>
	 * Async: Unregisters the content observer by given class simple name.
	 * </P>
	 * <P>
	 * If this is the first content observer,<BR/>
	 * the service will be started automatically.
	 * </P>
	 * 
	 * @param contentObserverName
	 *            String represents the content observer simple class name
	 *            
	 * @see android.content.ContentResolver#unregisterContentObserver
	 */
	public static void unregisterContentObserver(String contentObserverName) {
		LOGGER.debug(CLASS_NAME + "#unregisterContentObserver(...)");
		
		assert(contentObserverName != null);
		
		Context appCtx = MyApplication.getContext();
	
		// Async: Starts the background service
		Intent serviceIntent = new Intent(appCtx, ContentObserversService.class);
		serviceIntent.setAction(Config.Action.ACTION_UNREGISTER_CONTENT_OBSERVER);
		serviceIntent.putExtra(Config.Extra.EXTRA_CONTENT_OBSERVER_NAME, contentObserverName);
		appCtx.startService(serviceIntent);
	}

	/**
	 * Async: Starts the background service ContentObserversService.
	 */
	public static void startService() {
		LOGGER.debug(CLASS_NAME + "#startService(...)");
		
		Context appCtx = MyApplication.getContext();
	
		Intent serviceIntent = new Intent(appCtx, ContentObserversService.class);
		appCtx.startService(serviceIntent);
	}
	
	/**
	 * Async: Stops the background service ContentObserversService.
	 */
	public static void stopService() {
		LOGGER.debug(CLASS_NAME + "#stopService(...)");
		
		Context appCtx = MyApplication.getContext();
		
		Intent serviceIntent = new Intent(appCtx, ContentObserversService.class);
		appCtx.stopService(serviceIntent);
	}
	
	/**
	 * <P>
	 * Async: Updates the background service ContentObserversService.
	 * </P>
	 * Reregisters content observers according to active ContentObserversTriggers.
	 */
	public static void updateContentObservers() {
		LOGGER.debug(CLASS_NAME + "#updateService(...)");
		
		Context appCtx = MyApplication.getContext();
		
		Intent serviceIntent = new Intent(appCtx, ContentObserversService.class);
		serviceIntent.setAction(Config.Action.ACTION_UPDATE_CONTENT_OBSERVERS);
		appCtx.startService(serviceIntent);
	}
	
	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {

		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			Intent intent = (Intent) msg.obj;
			if (intent != null) {
				String action = intent.getAction();
				Bundle extras = intent.getExtras();
				if (action != null && extras != null) {
					if (Config.Action.ACTION_REGISTER_CONTENT_OBSERVER.equals(action)) {
						try {
							String contentObserverName = extras.getString(Config.Extra.EXTRA_CONTENT_OBSERVER_NAME);
							ContentObserver contentObserver = (ContentObserver) Class.forName(
											Config.CONTENT_OBSERVER_PACKAGE + "." + contentObserverName).newInstance();
							Uri uri = intent.getData();
							boolean notifyForDescendents = extras.getBoolean(Config.Extra.EXTRA_NOTIFY_FOR_DESCENDENTS);
							if (contentObserver != null && uri != null) {
								_registerContentObserver(uri, notifyForDescendents, contentObserver);
							}
						} catch (InstantiationException e) {
							LOGGER.error("Error: ", e);
						} catch (IllegalAccessException e) {
							LOGGER.error("Error: ", e);
						} catch (ClassNotFoundException e) {
							LOGGER.error("Error: ", e);
						}
					} else if (Config.Action.ACTION_UNREGISTER_CONTENT_OBSERVER.equals(action)) {
						String contentObserverName = extras.getString(Config.Extra.EXTRA_CONTENT_OBSERVER_NAME);
						if (contentObserverName != null) {
							_unregisterContentObserver(contentObserverName);
						}
					} else if (Config.Action.ACTION_UPDATE_CONTENT_OBSERVERS.endsWith(action)) {
						unregisterContentObservers(mContentObservers.values());
						registerContentObserverTriggers();
					}
				}
			}
		}
		
	}
	
}

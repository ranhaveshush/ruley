/**
 * 
 */
package com.ruley.listeners;

/**
 * @author Ran
 *
 */
public interface OnResultListener {
	
	public void onResultOk();
	
	public void onResultCanceled();

}
